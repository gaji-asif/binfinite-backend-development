
	function validate()
{
	 var x = document.forms["contestForm"]["email"].value;
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");


    if (document.forms.contestForm.name.value == "")
    {
        alert("Please enter your full name.");
		contestForm.name.focus();
        return false;
    }


    else if (document.forms.contestForm.ic.value == "")
    {
        alert("Please enter your IC No.");
		contestForm.ic.focus();
        return false;
    }

	 else if (document.forms.contestForm.ic.value.length !=12)
    {
        alert("IC No must be 12 digits.");
		contestForm.ic.focus();
        return false;
    }

	else if (document.forms.contestForm.mobile.value == "")
    {
        alert("Please enter your mobile phone no.");
		contestForm.mobile.focus();
        return false;
    }

	else if (document.forms.contestForm.mobile.value.length < 9)
    {
        alert("Mobile phone no must be at least 9 digits.");
		contestForm.mobile.focus();
        return false;
    }

	else if (document.forms.contestForm.email.value == "")
    {
        alert("Please enter your email address.");
		contestForm.email.focus();
        return false;
    }

		else if (document.forms.contestForm.bcard.value == "")
	    {
	        alert("Please enter your BCard number.");
			contestForm.bcard.focus();
	        return false;
	    }

		else if (document.forms.contestForm.voucher.value == "")
	    {
	        alert("Please enter your unique voucher number.");
			contestForm.voucher.focus();
	        return false;
	    }


	else if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
	{
        alert("Please enter a valid e-mail address");
        return false;
    }

	else if (document.forms.contestForm.tnc.checked == false)
    {
        alert ('You must agree to the T&C.');
		contestForm.tnc.focus();
        return false;
    }

    return true;
}
