
/*my version */

	function validate()
{
	 var x = document.forms["contact_form"]["email"].value;
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
	var captcha_response = grecaptcha.getResponse();

    if (document.forms.contact_form.name.value == "")
    {
        alert("Please enter your name.");
		contact_form.name.focus();
        return false;
    }

	else if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
    {

    alert('Please provide a valid email address');
    contact_form.email.focus();
    return false;

    }

	else if (document.forms.contact_form.mobile.value == "")
    {
        alert("You must provide your mobile no.");
		contact_form.mobile.focus();
        return false;
    }

		else if (document.forms.contact_form.card.value.length() < 17)
	    {
	        alert("You must provide 16 digits.");
			contact_form.card.focus();
	        return false;
	    }

	else if (document.forms.contact_form.reason.value == "selectreason") {
          alert("Please select your contact reason.");
		  contact_form.reason.focus();
          return false;
    }

	else if (document.forms.contact_form.member.value == "selectmember") {
          alert("Please specify if you are a member.");
		  contact_form.member.focus();
          return false;
    }

	 else if (document.forms.contact_form.message.value == "")
    {
        alert("Please enter your message.");
		contact_form.message.focus();
        return false;
    }

	else if(captcha_response.length == 0)
	{
		// Captcha is not Passed
		alert("Please tick the recaptcha.");
	return false;
	}


    return true;
}
