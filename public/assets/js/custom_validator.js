$(function() {
	$('#validation-message').hide();
});

function alert_error(text, id) {
	$('#validation-message > p').html(text);
	$('#validation-message').show();

	$(id).css('border-color', "#ff0000");
}

function reset_error(id) {
	$('#validation-message').hide();
	$(id).removeAttr('style');
}