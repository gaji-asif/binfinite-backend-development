<?php

return [

    'merchant_account_id' => '92ae4548-354f-4d73-b46a-59458dd1ef6c',
    'requested_amount_currency' => 'MYR',
    'payment_method' => 'creditcard',
    'secret_key' => 'a6620f11-1a15-4091-a657-67632fbc2e84',
    'transaction_type' => 'purchase',
    'redirect_url' => 'https://binfinite.web2square.com/payment/capture',
    'order_prefix' => 'BINF-{NO}',
    'sst_rate' => 0.06,
];
