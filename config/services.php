<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
        'webhook' => [
            'secret' => env('STRIPE_WEBHOOK_SECRET'),
            'tolerance' => env('STRIPE_WEBHOOK_TOLERANCE', 300),
        ],
    ],

     /* Social Media */
    'facebook' => [
        'client_id'     => env('FB_ID','812537402411711'),
        'client_secret' => env('FB_SECRET','15394bc3d02081f0eb88f4b68a1c38cd'),
        'redirect'      => env('FB_URL','https://local-binfinite.web2square.com/auth/facebook/callback'),
    ],
    'google' => [
        'client_id'     => env('GOOGLE_ID','182268577213-pldgalfss7n3mi9kspa916htqtjeopi9.apps.googleusercontent.com'),
        'client_secret' => env('GOOGLE_SECRET','lZgCjSoCYL9f-yQ97uVQPQqY'),
        'redirect'      => env('GOOGLE_URL','https://local-binfinite.web2square.com/auth/google/callback'),
    ],

    'mylms' => [
        // 'wsdl' => env('MYLMS_WSDL', 'https://dev.binfinite.com.my/bcardwsapi/service.asmx?wsdl'),
        'wsdl' => env('MYLMS_WSDL', 'https://dev.binfinite.com.my/bcardwssingle/service.asmx?wsdl'),
        'wsdl_virtualcard' => env('MYLMS_WSDL_VIRTUALCARD', 'https://dev.binfinite.com.my/bcardVirtualCards/service.asmx?wsdl'),
        'wskey' => env('MYLMS_KEY', '190536604'),
        'wscompanycode' => env('MYLMS_COMPANYCODE', 'Z9999'),
        'wsbranchcode' => env('MYLMS_BRANCHCODE', 'TEST064'),
    ],

    'shoponline' => [
        'affiliate_id' => env('SHOPONLINE_AFFILIATE_ID', '29792'),
    ]
];
