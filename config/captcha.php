<?php

return [
    'secret' => env('NOCAPTCHA_SECRET', '6LeuLp4UAAAAAH3PCoyFBpFENicNIO8zSfDd7naw'),
    'sitekey' => env('NOCAPTCHA_SITEKEY', '6LeuLp4UAAAAALZYmzS-HE4j3ipjuYYAYR05qYn7'),
    'options' => [
        'timeout' => 30,
    ],
];
