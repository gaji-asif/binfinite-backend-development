<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Rinvex\Categories\Traits\Categorizable;
use App\Traits\StorageImageResolver;

class MerchantOffer extends Model
{
	use Categorizable, StorageImageResolver;
	
	/**
	 * Valid offer types (for online promo only)
	 *
	 */
	const DISCOUNT_PURCHASE_PERCENT = 'discount_purchase_percent';
	const DISCOUNT_REDEMPTION_PERCENT = 'discount_redemption_percent';
	const DISCOUNT_PURCHASE_VALUE = 'discount_purchase_value';
	const DISCOUNT_REDEMPTION_VALUE = 'discount_redemption_value';
	const EARN_PURCHASE_MULTIPLIER = 'earn_purchase_multiplier';
	const EARN_BONUS_POINTS = 'earn_bonus_points';
	const FREE_VOUCHER = 'free_voucher';

	/**
	 * Redeem/purchase types (for online promo only)
	 *
	 */
	const REDEEM_TYPES = [
		self::DISCOUNT_PURCHASE_PERCENT,
		self::DISCOUNT_REDEMPTION_PERCENT,
		self::DISCOUNT_PURCHASE_VALUE,
		self::DISCOUNT_REDEMPTION_VALUE,
	];

	/**
	 * Earn points types (for online promo only)
	 *
	 */
	const EARN_TYPES = [
		self::EARN_PURCHASE_MULTIPLIER,
		self::EARN_BONUS_POINTS,
	];

	/**
	 * Free item types (for online promo only)
	 *
	 */
	const FREE_TYPES = [
		self::FREE_VOUCHER,
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'expire_at' => 'date'
	];

	/**
	 * Relation to merchant
	 *
	 */
	public function merchant()
	{
		return $this->belongsTo('App\Merchant', 'merchant_id');
	}

	/**
	 * Relation to merchant_outlets through merchant_offers_outlets
	 *
	 */
	public function outlets()
	{
		return $this->belongsToMany('App\MerchantOutlet', 'merchant_offers_outlets');
	}

	/**
	 * Attribute for formatted expiry date
	 *
	 */
	public function getExpireDateAttribute()
	{
		return $this->expire_at->format('jS F Y');
	}

	/**
	 * Attribute for full url for offer thumbnail image
	 *
	 */
	public function getThumbnailImageAttribute()
	{
		return $this->resolveImage($this->thumbnail_image_url);
	}

	/**
	 * Attribute for full url for offer banner image
	 *
	 */
	public function getBannerImageAttribute()
	{
		return $this->resolveImage($this->offer_banner_url);
	}

	/**
	 * Attribute for offer URL
	 *
	 */
	public function getOfferUrlAttribute()
	{
		return route('partner_offer', [$this->merchant->id, $this->merchant->slug, $this->id]);
	}

	/**
	 * Attribute for generating action button text
	 *
	 */
	public function getButtonActualTextAttribute()
	{
		if ($this->button_text) {
			return $this->button_text;
		}

		if (in_array($this->offerType, self::REDEEM_TYPES)) {
			return 'Redeem';
		}

		return 'Earn';
	}

	/**
	 * Scope for getting only offers that hasnt expired
	 *
	 */
	public function scopeActive($query, $value = true)
	{
		if ($value) {
			return $query->whereDate('expire_at', '>=', now());
		}

		return $query->whereDate('expire_at', '<', now());
	}

	/**
	 * Temporary attribute to generate array for partner profile page with offers
	 *
	 */
	public function getPartnerPromoArrayAttribute()
	{
		return [
            'name' => $this->merchant->name,
            'logoImage' => $this->merchant->logoImage,
            'ribbon' => $this->offer_subtext,
            'image' => $this->thumbnailImage,
            'title' => $this->offer_text,
            'point' => $this->conversion_text,
            'validDate' => $this->expireDate,
            'url' => $this->offerUrl,
            'actionTitle' => $this->buttonActualText,
        ];
	}

	/**
	 * Temporary attribute to generate array for earn points page
	 *
	 */
	public function getEarnPartnerPromoArrayAttribute()
	{
		return [
            'name' => $this->merchant->name,
            'logoImage' => $this->merchant->logoImage,
            'image' => $this->thumbnailImage,
            'title' => $this->offer_text,
            'point' => $this->conversion_text,
            'validDate' => $this->expireDate,
            'partner' => $this->merchant->name,
            'company' => $this->merchant->name,
            'url' => $this->offerUrl,
            'actionTitle' => $this->buttonActualText,
        ];
	}
}
