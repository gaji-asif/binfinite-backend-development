<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $fillable = [
        'id',
        'user_id',
        'invoice_no',
        'full_name',
        'address',
        'amount',
        'net_amount',
        'sst_amount',
        'currency',
        'status',
        'payment_method',
        'category',
        'refund_amount',
        'refund_ref',
        'refund_date',
        'response_code',
        'response_message',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
