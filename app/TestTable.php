<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TestTable extends Model
{
    protected $table = 'steve_test';

    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];
}
