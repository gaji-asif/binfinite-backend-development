<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-81045901-2', 'auto');
 ga('send', 'pageview');
 </script>

<?php
session_start();
//require_once 'config.php';

$merchantcode = $_GET['merchantcode'];
$merchantname = $_GET['merchantname'];

$bcard = $_GET['bcard'];

if (empty($merchantcode) || empty($merchantname) || empty($bcard)) {
    echo "Please login to proceed<br><a href=\"login.php\">Login</a>";
    die;
}

?>
<!doctype html>

<style media="screen">


@media(max-width:540px) {
	 .mobile-size {
		 font-size: 12px;
	 }
}
</style>

<html lang="en-US" class="no-js">

<body class="">

	<div id="page_wrapper">



			<?php if (!empty($the_merchant_seo_meta_title)) {?>
				<title><?php echo $the_merchant_seo_meta_title; ?></title>
			<?php } else {?>
				<title>B Infinite | Loyalty Program in Malaysia</title>
			<?php }?>

			<!-- meta -->
			<meta charset="UTF-8">
			<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
			<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1">


			<?php if (!empty($the_merchant_seo_metatag_keywords)) {?>
				<meta name="keywords" content="<?php echo $the_merchant_seo_metatag_keywords; ?>" />
			<?php } else {?>
				<meta name="keywords" content="Malaysia Loyalty Card, Loyalty Program in Malaysia, Online Redemption, BCard, B Infinite, Redemption Programme, Berjaya Corporation, Berjaya Card, BLoyalty" />
			<?php }?>

			<?php if (!empty($the_merchant_seo_meta_desc)) {?>
				<meta name="description" content="<?php echo $the_merchant_seo_meta_desc; ?>" />
			<?php } else {?>
				<meta name="description" content="B Infinite (formerly BCARD) is a Loyalty Program in Malaysia by BLoyalty Sdn Bhd, a subsidiary of Berjaya Corporation Bhd" />
			<?php }?>

			<meta property="og:image" content="/img/b-infinite-logo.png"/>

			<!-- Google Fonts CSS Stylesheet // More here http://www.google.com/fonts#UsePlace:use/Collection:Open+Sans
			<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400italic,400,600,600italic,700,800,800italic" rel="stylesheet" type="text/css">
			<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>-->

			<!-- ***** Boostrap Custom / Addons Stylesheets ***** -->
			<link rel="stylesheet" href="/shoponline-sg/css/bootstrap.css" type="text/css" media="all">
			<link rel="stylesheet" href="/shoponline-sg/css/sliders/ios/style.css" type="text/css" media="all">

			<link rel="stylesheet" href="/shoponline-sg/css/template.css" type="text/css" media="all">
			<link rel="stylesheet" href="/shoponline-sg/css/responsive.css" type="text/css" media="all">
			<link rel="stylesheet" href="/shoponline-sg/css/base-sizing.css" type="text/css" media="all">
			<!-- Updates CSS Stylesheet -->
			<link rel="stylesheet" href="/shoponline-sg/css/updates.css" type="text/css" />
			<!-- Custom CSS Stylesheet (where you should add your own css rules) -->
			<link rel="stylesheet" href="/shoponline-sg/css/custom.css" type="text/css" />
			<link href="/shoponline-sg/css/new-font.css" rel="stylesheet">
			<link href="/shoponline-sg/css/main.css" rel="stylesheet">
			<link href="/shoponline-sg/css/animate.css" rel="stylesheet">


			<!-- Modernizr Library -->
			<script type="text/javascript" src="/shoponline-sg/js/vendor/modernizr.min.js"></script>

			<!-- jQuery Library -->
			<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
			<script>window.jQuery || document.write('<script src="/shoponline-sg/js/vendor/jquery-1.10.1.min.js">\x3C/script>')</script>
			<script src="https://code.jquery.com/jquery-migrate-1.2.1.js"></script>


			<!-- Analytic Code -->

			<script>
			 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
			  ga('create', 'UA-81045901-2', 'auto');
			 ga('send', 'pageview');
			 </script>

		<script type="text/javascript">

		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-81045901-2']);
		  _gaq.push(['_trackPageview']);

		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();

		</script>




		<!-- /.END PAGE CUSTOM SCRIPTS & STYLES -->


		<script src="https://apis.google.com/js/platform.js" async defer></script>
			<script type="text/javascript" async defer src="//assets.pinterest.com/js/pinit.js"></script>

			<section class="hg_section ptop-40">
					<div class="hg_section_size container">
						<div class="row">


								<div class="latest_posts default-style kl-style-2 container">
									<div class="row gutter-sm">

										<div class="col-md-12 col-sm-12">
									<div class="text_box" style="text-align:justify;">
									<!-- Form Module-->
									<div class="module form-module2">
										<div class="toggle"><i class="fa fa-times fa-pencil"></i>
											<div class="tooltip">Click Me</div>
										</div>
									  <div class="form">
										<!-- Jeff mod starts -->
										<strong>
											<p style="color: #ec008c; text-align:center;">Few important things to take note before shopping
											<br>
											<span style="color: #033f88; text-align:center;">In order for you to EARN BPoints</span>
											<p>
										</strong>
											<div class="col-md-4 col-sm-4 col-xs-12" style="text-align: center; margin-top:-20px;">
												<img src="/shoponline-sg/img/estore/redirect-1.png" style="width:40%" alt="">
												<p class="mobile-size" style="text-align:center;">Stay within the same website link to complete your purchase</p>
											</div>

											<div class="col-md-4 col-sm-4 col-xs-12" style="text-align: center; margin-top:-20px;">
												<img src="/shoponline-sg/img/estore/redirect-2.png" style="width:40%" alt="">
												<p class="mobile-size" style="text-align:center;">Ensure that you have unblocked your browser cookies before clicking into any merchant link</p>
											</div>

											<div class="col-md-4 col-sm-4 col-xs-12" style="text-align: center; margin-top:-20px;">
												<img src="/shoponline-sg/img/estore/redirect-3.png" style="width:40%" alt="">
												<p class="mobile-size" style="text-align:center;">Should there be any error,  return back to the Shop Online! main page</p>
											</div>
											<p style="text-align:center;">&nbsp;</p>
										</p>

										<!-- Jeff mod ends -->
									  </div>

											<div class="cta container" style="text-align:center;">
												<a href="https://invol.co/aff_m?offer_id=<?php echo $merchantcode; ?>&aff_id=105554&source=deeplink_generator&aff_sub=<?php echo $bcard; ?>"  rel="nofollow">
													<span class="btn" style="background: #ec008c; color: #fff;">Click here to continue</span>
												</a>
											</div>

									</div>

									</div>

						</div>
					</div>
								<!-- end // latest posts style 2 -->

						</div>
					</div>
				</section>




	<script>
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		 alert('Only numbers are allowed.');
        return false;
    }
    return true;
}
	</script>


		<a href="#" id="totop">TOP</a>
		<!-- JS FILES // These should be loaded in every page -->
		<script type="text/javascript" src="/shoponline-sg/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="/shoponline-sg/js/kl-plugins.js"></script>
		<script type="text/javascript" src="addons/smoothscroll/smoothscroll.js"></script>
		<!-- JS FILES // Loaded on this page -->
		<script type="text/javascript" src="addons/scrollme/jquery.scrollme.js"></script>
		<!-- <script type="text/javascript" src="sliders/caroufredsel/jquery.carouFredSel-packed.js"></script> -->
		<script type="text/javascript" src="/shoponline-sg/js/kl-recent-work-carousel2.js"></script>
		<script type="text/javascript" src="/shoponline-sg/js/kl-testimonials-fader.js"></script>
		<script type="text/javascript" src="addons/flickrfeed/jquery.jflickrfeed.min.js"></script>
		<script type="text/javascript" src="addons/magnific_popup/jquery.magnific-popup.js"></script>
		<script type="text/javascript" src="/shoponline-sg/js/kl-screenshot-box.js"></script>
		<script type="text/javascript" src="/shoponline-sg/js/kl-logo-carousel.js"></script>

		<!-- JS codes -->
		<script type="text/javascript" src="/shoponline-sg/js/kl-scripts.js"></script>
		<!-- JS codes -->
		<script type="text/javascript" src="/shoponline-sg/js/kl-custom.js"></script>
		<script src="/shoponline-sg/js/plugins.js"></script>
		<script src="/shoponline-sg/js/app.js"></script>
		<script src="/shoponline-sg/js/jquery.appear.js"></script>
		<script src="/shoponline-sg/js/main.js"></script>


	<!-- BX Slider -->
		<script type="text/javascript" src="bxSlider/jquery.bxslider.js"></script>
		<script type="text/javascript" src="bxSlider/jquery.bxslider.min.js"></script>
		<link rel="stylesheet" href="bxSlider/jquery.bxslider.css" type="text/css" media="screen" />

	<script>
	$(document).ready(function(){
	  $('.bxslider').bxSlider({
		   pager: true,
		   auto: true,
		   speed: 3000,
		   pause: 7000,
		    adaptiveHeight: true
	  });
	});

	</script>

	<link rel="stylesheet" href="/shoponline-sg/NivoSlider/nivo-slider.css" type="text/css" />
	<script src="/shoponline-sg/NivoSlider/jquery.nivo.slider.pack.js" type="text/javascript"></script>
	<link rel="stylesheet" href="/shoponline-sg/NivoSlider/themes/default/default.css" type="text/css" />

	<script type="text/javascript">
	$(window).on('load', function() {
	    $('#slider').nivoSlider();
	});
	</script>

	 <!-- FancyBox -->
	<link rel="stylesheet" href="/shoponline-sg/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
	<script type="text/javascript" src="/shoponline-sg/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>

	<!-- Optionally add helpers - button, thumbnail and/or media -->
	<link rel="stylesheet" href="/shoponline-sg/fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
	<script type="text/javascript" src="/shoponline-sg/fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
	<script type="text/javascript" src="/shoponline-sg/fancybox/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

	<link rel="stylesheet" href="/shoponline-sg/fancybox/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
	<script type="text/javascript" src="/shoponline-sg/fancybox/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>




	<!-- jPages -->

	  <link rel="stylesheet" href="/shoponline-sg/css/jPages.css">
	  <!-- <script type="text/javascript" src="/shoponline-sg/js/jquery-1.8.2.min.js"></script> -->
	  <script src="/shoponline-sg/js/jPages.js"></script>



	<script>
		/* when document is ready */
		$( document ).ready(function() {
			$("div.holder").jPages({
					containerID  : "history",
					perPage      : 20,
			//    startPage    : 1,
			//    startRange   : 1,
			//    midRange     : 15,
			//    endRange     : 1
			});
		});
	</script>

	<!-- Swiper -->

	<link rel="stylesheet" href="swiper/dist/css/swiper.css">
	<script src="swiper/dist/js/swiper.min.js"></script>
	<script>

	    var swiper = new Swiper('.swiper-container', {
			autoplay: 6000,
			autoplayDisableOnInteraction: false,
	        pagination: '.swiper-pagination',
	        paginationClickable: true,
			// Navigation arrows
	    nextButton: '.swiper-button-next',
	    prevButton: '.swiper-button-prev',
	    });

		</script>

	</div><!-- end page-wrapper -->

</body>
</html>
