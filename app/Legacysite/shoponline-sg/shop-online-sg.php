<?php
//Sort function
function cmp($a, $b)
{
    return strcmp($a['estore_name'], $b['estore_name']);
}

//require_once 'shop-online-connection-sg.php';
require_once 'libs/mob-detect.php';
$detect = new Mobile_Detect;

$estore = array();

$category = $_GET['category'] ?? '';


$display = [1,4];
// Not Mobile, Not Tablet
if (!$detect->isMobile() && !$detect->isTablet()) $display = [1,4];
// iOS
if ($detect->isiOS()) $display = [2,4];
// Android
if ($detect->isAndroidOS()) $display = [3,4];
// Is Mobile
if ($detect->isMobile())  $display = [5];

$estore = DB::table('legacy_shoponline_sg')
	->where('status', '=', '1')
	->whereIn('display', $display)
	->orderBy('estore_name', 'ASC')
	->select('*')
	->get()
	->map(function($v) {
		return (array) $v;
	})->toArray();

usort($estore, "cmp");

?>


<html lang="en-US" class="no-js">
<link rel="stylesheet" type="text/css" href="/shoponline-sg/addons/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="/shoponline-sg/addons/slick/slick-theme.css"/>

<style media="screen">
a.menu_links { cursor: pointer; }


#portfolio-nav li.current a {
	background: #ed008c !important;
	color: #fff;
}

#portfolio-nav a:hover{
	background: #ed008c !important;
}

#portfolio-nav a:hover{
	background: #ed008c !important;
}

.name{
	color: #ed008c !important;
	text-transform: none;
}

@media(max-width:540px) {
	 .desktop-slider {
		 display: none;
	 }
}

@media (min-width: 540px){
	 .mobile-slider {
		 display:none;
	 }
}

@media(max-width:540px) {
	 .desktop-element {
		 position: absolute !important;
		 left :-1000px !important;
		 width: 0px !important;
		 height: 0px !important;
		 margin-bottom: 0px !important;
	 }
}

@media (min-width: 540px){
	 .mobile-element {
		 position: absolute !important;
		 left :-1000px !important;
		 width: 0px !important;
		 height: 0px !important;
		 margin-bottom: 0px !important;
	 }
}

.size-15{
  font-size:15px !important;
}

.colblack{
  color : #434343 !important;

}

.chaser {
  display : none !important;
}

</style>

<?php include '../gtm.php';?>

	<div id="page_wrapper">
  <?php
session_start();
//require_once 'config.php';
if (!empty($_GET['action']) && $_GET['action'] == 'logout') {
    $_SESSION["bcard_logged_in"] = false;
    unset($_SESSION["bcard_number"]);
    unset($_SESSION["bcard_password"]);
    unset($_SESSION["profile"]);
    unset($_SESSION["adcoin_redirect"]);
}
?>


	<?php if (!empty($the_merchant_seo_meta_title)) {?>
		<title><?php echo $the_merchant_seo_meta_title; ?></title>
	<?php } else {?>
		<title>B Infinite | Loyalty Program in Malaysia</title>
	<?php }?>

	<!-- meta -->
	<meta charset="UTF-8">
	<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1">


	<?php if (!empty($the_merchant_seo_metatag_keywords)) {?>
		<meta name="keywords" content="<?php echo $the_merchant_seo_metatag_keywords; ?>" />
	<?php } else {?>
		<meta name="keywords" content="Malaysia Loyalty Card, Loyalty Program in Malaysia, Online Redemption, BCard, B Infinite, Redemption Programme, Berjaya Corporation, Berjaya Card, BLoyalty" />
	<?php }?>

	<?php if (!empty($the_merchant_seo_meta_desc)) {?>
		<meta name="description" content="<?php echo $the_merchant_seo_meta_desc; ?>" />
	<?php } else {?>
		<meta name="description" content="B Infinite (formerly BCARD) is a Loyalty Program in Malaysia by BLoyalty Sdn Bhd, a subsidiary of Berjaya Corporation Bhd" />
	<?php }?>

	<meta property="og:image" content="/shoponline-sg/img/b-infinite-logo.png"/>

	<!-- ***** Boostrap Custom / Addons Stylesheets ***** -->
	<link rel="stylesheet" href="/shoponline-sg/css/bootstrap.css" type="text/css" media="all">
	<link rel="stylesheet" href="/shoponline-sg/css/sliders/ios/style.css" type="text/css" media="all">

	<link rel="stylesheet" href="/shoponline-sg/css/template.css" type="text/css" media="all">
	<link rel="stylesheet" href="/shoponline-sg/css/responsive.css" type="text/css" media="all">
	<link rel="stylesheet" href="/shoponline-sg/css/base-sizing.css" type="text/css" media="all">
	<!-- Updates CSS Stylesheet -->
	<link rel="stylesheet" href="/shoponline-sg/css/updates.css" type="text/css" />
	<!-- Custom CSS Stylesheet (where you should add your own css rules) -->
	<link rel="stylesheet" href="/shoponline-sg/css/custom.css" type="text/css" />
	<link href="/shoponline-sg/css/new-font.css" rel="stylesheet">
	<link href="/shoponline-sg/css/main.css" rel="stylesheet">
	<link href="/shoponline-sg/css/animate.css" rel="stylesheet">


	<!-- Modernizr Library -->
	<script type="text/javascript" src="/shoponline-sg/js/vendor/modernizr.min.js"></script>

	<!-- jQuery Library -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="/shoponline-sg/js/vendor/jquery-1.10.1.min.js">\x3C/script>')</script>
	<script src="https://code.jquery.com/jquery-migrate-1.2.1.js"></script>


	<!-- Analytic Code -->

	<script>
	 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	  ga('create', 'UA-81045901-2', 'auto');
	 ga('send', 'pageview');
	 </script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-81045901-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>





		<section class="hg_section ptop-35 pbottom-80">

      <!--
				<div class="container" style="margin-bottom: 20px;">
          <div class="desktop-slider slider">
						<img class="desktop-slider" src="/shoponline-sg/img/estore/shoponline-shop-190114.jpg" style="width:100%" alt="">
						<img class="desktop-slider" src="/shoponline-sg/img/estore/shoponline-shop-190102.jpg" style="width:100%" alt="">
          </div>
          <div class="mobile-slider slider">
  			    <img class="mobile-slider" src="/shoponline-sg/img/slider/shoponline-m-190114.jpg" style="width:100%" alt="">
    	      <img class="mobile-slider" src="/shoponline-sg/img/slider/shoponline-m-190102.jpg" style="width:100%" alt="">
    		  </div>
    	 </div>
			 -->


			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<!-- Portfolio sortable element -->

						<div class="hg-portfolio-sortable">

							<ul id="portfolio-nav" class="fixclear">
                <!--<li class="<?php if ($category == "specialdeals") {echo "current";}?>"><a href="#" class="size-15 colblack" data-filter=".specialdeals">Special Deals</a></li>-->
								<li class="<?php if (empty($category)) {echo "current";}?>"><a href="#" class="size-15 colblack" data-filter=".estore,.travels,.fashion,.beauty,.lifestyle,.grocery">All</a></li>
								<li class="<?php if ($category == "estore") {echo "current";}?>"><a href="#" class="size-15 colblack" data-filter=".estore">EShop</a></li>
								<li class="<?php if ($category == "travels") {echo "current";}?>"><a href="#" class="size-15 colblack" data-filter=".travels">Travels</a></li>
								<li class="<?php if ($category == "fashion") {echo "current";}?>"><a href="#" class="size-15 colblack" data-filter=".fashion">Fashion</a></li>
								<li class="<?php if ($category == "beauty") {echo "current";}?>"><a href="#" class="size-15 colblack" data-filter=".beauty">Beauty</a></li>
								<li class="<?php if ($category == "lifestyle") {echo "current";}?>"><a href="#" class="size-15 colblack" data-filter=".lifestyle">Lifestyle</a></li>
								<li class="<?php if ($category == "grocery") {echo "current";}?>"><a href="#" class="size-15 colblack" data-filter=".grocery">Grocery</a></li>

							</ul>

							<div class="clear">
							</div>
							<ul id="thumbs" class="fixclear">

							<?php
foreach ($estore as $k => $v) {
    ?>


							<span class="">
								<li style="width:25%;" class="item kl-has-overlay <?php echo $v['estore_category']; ?>">
									<div class="inner-item">
										<div class="img-intro" style="text-align:center;">
                      <?php
if (!$detect->isMobile() && !$detect->isTablet() && $v['message'] == 1) {
        ?>
  <a data-toggle="modal" data-target="#mobileoffer"></a>
<?php
} else {
        ?>
                        <a href="shop-online-sg-check.php?redirect=eshop&merchantcode=<?php echo $v['estore_link']; ?>&merchantname=<?php echo urlencode($v['estore_name']); ?>" target="_blank" class="various"></a>
												<?php }?>

																						<img src="/shoponline-sg/img/estore/<?php echo $v['estore_image']; ?>" alt="<?php echo $v['estore_name']; ?>" style="width:50%;" title="<?php echo $v['estore_name']; ?>" />

											<div class="overlay">
												<div class="overlay-inner">
													<span class="glyphicon glyphicon-shopping-cart"></span>
												</div>
											</div>
										</div>

										<h4>
                      <?php
if (!$detect->isMobile() && !$detect->isTablet() && $v['message'] == 1) {
        ?>
                  <a data-toggle="modal" data-target="#mobileoffer"><span class="name size-15"><?php echo $v['estore_name']; ?></span></a>
                <?php
} else {

        ?>
	<a href="shop-online-sg-check.php?redirect=eshop&merchantcode=<?php echo $v['estore_link']; ?>&merchantname=<?php echo urlencode($v['estore_name']); ?>"  target="_blank" class="various"><span class="name size-15"><?php echo $v['estore_name']; ?></span></a>

<?php }?>
										</h4>


										<span class="moduleDesc size-15 colblack"><?php echo $v['estore_desc']; ?></span>

										<div class="clear">
										</div>
									</div>
								</li>
							</span>

							<?php
}
?>






							</ul>
						</div>


					</div>
					<!--/ col-md-12 col-sm-12 -->
				</div>
				<!--/ row -->
			</div>
			<!--/ container -->
		</section>

    <script type="text/javascript" src="/shoponline-sg/addons/slick/slick.min.js"></script>

		<script type="text/javascript" src="/shoponline-sg/js/trigger/kl-portfolio-sortable.js"></script>
		<script type="text/javascript" src="/shoponline-sg/js/plugins/jquery.isotope.min.js"></script>

<script>
$('.slider').slick({
  autoplay: true,
  autoplaySpeed: 2000
});
</script>

<a href="#" id="totop">TOP</a>
	<!-- JS FILES // These should be loaded in every page -->
	<script type="text/javascript" src="/shoponline-sg/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/shoponline-sg/js/kl-plugins.js"></script>
	<script type="text/javascript" src="/shoponline-sg/addons/smoothscroll/smoothscroll.js"></script>
	<!-- JS FILES // Loaded on this page -->
	<script type="text/javascript" src="/shoponline-sg/addons/scrollme/jquery.scrollme.js"></script>
	<!-- <script type="text/javascript" src="sliders/caroufredsel/jquery.carouFredSel-packed.js"></script> -->
	<script type="text/javascript" src="/shoponline-sg/js/kl-recent-work-carousel2.js"></script>
	<script type="text/javascript" src="/shoponline-sg/js/kl-testimonials-fader.js"></script>
	<script type="text/javascript" src="/shoponline-sg/addons/flickrfeed/jquery.jflickrfeed.min.js"></script>
	<script type="text/javascript" src="/shoponline-sg/addons/magnific_popup/jquery.magnific-popup.js"></script>
	<script type="text/javascript" src="/shoponline-sg/js/kl-screenshot-box.js"></script>
	<script type="text/javascript" src="/shoponline-sg/js/kl-logo-carousel.js"></script>

	<!-- JS codes -->
	<script type="text/javascript" src="/shoponline-sg/js/kl-scripts.js"></script>
	<!-- JS codes -->
	<script type="text/javascript" src="/shoponline-sg/js/kl-custom.js"></script>
	<script src="/shoponline-sg/js/plugins.js"></script>
	<script src="/shoponline-sg/js/app.js"></script>
	<script src="/shoponline-sg/js/jquery.appear.js"></script>
	<script src="/shoponline-sg/js/main.js"></script>


<!-- BX Slider -->
	<script type="text/javascript" src="/shoponline-sg/bxSlider/jquery.bxslider.js"></script>
	<script type="text/javascript" src="/shoponline-sg/bxSlider/jquery.bxslider.min.js"></script>
	<link rel="stylesheet" href="/shoponline-sg/bxSlider/jquery.bxslider.css" type="text/css" media="screen" />

<script>
$(document).ready(function(){
  $('.bxslider').bxSlider({
	   pager: true,
	   auto: true,
	   speed: 3000,
	   pause: 7000,
	    adaptiveHeight: true
  });
});

</script>

<link rel="stylesheet" href="/shoponline-sg/NivoSlider/nivo-slider.css" type="text/css" />
<script src="/shoponline-sg/NivoSlider/jquery.nivo.slider.pack.js" type="text/javascript"></script>
<link rel="stylesheet" href="/shoponline-sg/NivoSlider/themes/default/default.css" type="text/css" />

<script type="text/javascript">
$(window).on('load', function() {
    $('#slider').nivoSlider();
});
</script>

 <!-- FancyBox -->
<link rel="stylesheet" href="/shoponline-sg/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="/shoponline-sg/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>

<!-- Optionally add helpers - button, thumbnail and/or media -->
<link rel="stylesheet" href="/shoponline-sg/fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
<script type="text/javascript" src="/shoponline-sg/fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="/shoponline-sg/fancybox/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

<link rel="stylesheet" href="/shoponline-sg/fancybox/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
<script type="text/javascript" src="/shoponline-sg/fancybox/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>


<script type="text/javascript">

$(document).ready(function() {
	$(".fancybox-button").fancybox({
		prevEffect		: 'none',
		nextEffect		: 'none',
		closeBtn		: false,
		helpers		: {
			title	: { type : 'inside' },
			buttons	: {}
		}
	});
});

</script>

<script>
	$(document).ready(function() {
	$(".various").fancybox({
		maxWidth	: 1000,
		maxHeight	: 800,
		fitToView	: true,
		width		: '100%',
		height		: '95%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none',
		'titleShow': false,
		'autoscale': false,
		'autoDimensions': false,
		padding: 5,
		afterClose: function () { // USE THIS IT IS YOUR ANSWER THE KEY WORD IS "afterClose"
        parent.location.reload();
    }
	});

});


</script>




	<!-- end page-wrapper -->

</body>

<!-- Modal -->
<div id="mobileoffer" style="z-index: 999999" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Mobile Offer</h4>
      </div>
      <div class="modal-body">
        <p>
          Offer is available on mobile platform only.
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!-- modal end -->

</html>
