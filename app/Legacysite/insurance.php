<!doctype html>
 <style>
	@media(max-width:767px) {
	  .frame {
	    height: 1200px;
	  }
	}

	@media (min-width: 767px) {
	  .frame {
	    height: 550px;
	  }
	}
 </style>
<html lang="en-US" class="no-js">

<?php include 'gtm-insurance.php';?>


  <link rel="stylesheet" href="/aig-assets/new-font.css" type="text/css" media="all">
  <link rel="stylesheet" href="/aig-assets/bootstrap.css" type="text/css" media="all">
  <link rel="stylesheet" href="/aig-assets/template.css" type="text/css" media="all">
  <script src="/aig-assets/jquery-1.11.1.min.js" async></script>

	<div id="page_wrapper">
		<?php //include 'top.php';?>
		<div id="page_header" class="page-subheader site-subheader-cst uh_zn_def_header_style maskcontainer--mask3">
			<div class="bgback">
			</div>
			<div class="kl-bg-source">
			<div class="kl-bg-source__overlay"></div>
			</div>
			<div class="th-sparkles">
			</div>
			<!-- DEFAULT HEADER STYLE -->
			<div class="ph-content-wrap ptop-0">
				<div class="ph-content-v-center ptop-25">
					<div class="container">
							<div class="row">
								<div class="col-sm-12">
									<div class="subheader-titles">

                    <h4 class="subheader-subtitle">Online Insurance Store</h4>
                    <img src="/aig-assets/insurance/aig-logo.png" style="margin-bottom:5px; height:50px;" align="right">
									</div>
								</div>

								<div class="col-sm-6">

									<div class="clearfix"></div>
								</div>

							</div>
							<!-- end row -->
						</div>
				</div>
			</div>

		</div>

	<section style="padding-top:0px !important; padding-bottom:0px !important;">



              <!-- Slideshow - CSS3 Panels element -->
              <div class="kl-slideshow kl-slideshow-css3panels">
                <!-- Fake loading -->
                <div class="fake-loading loading-1s">
                </div>

                <!-- CSS3 Panels container with custom height -->
                <div class="css3panels-container css3panels--resize cssp-capt-animated cssp-capt-fadeout frame" data-panels="4">



                  <!-- Panel #1 light style -->
                  <div class="css3panel css3panel--1 cp-theme--light frame-1">
                    <!-- Panel wrapper with custom height -->
                    <div class="css3panel-inner" style="height:700px">
                      <!-- Main image wrapper with custom height -->
                      <div class="css3panel-mainimage-wrapper" style="height:700px">
                        <!-- Background main image -->
                        <div style="background-image:url(/aig-assets/insurance/overseas-1.jpg);" class="css3panel-mainimage">

                          <!-- Gradient/Color overlay -->
                          <div class="css3p-overlay css3p-overlay--gradient" style="">
                          </div>
                        </div>
                        <!--/ Background main image -->
                      </div>
                      <!--/ Main image wrapper with custom height -->
                    </div>
                    <!-- Panel wrapper with custom height -->

                    <!-- Caption -->
                    <div class="css3panel-caption">
                      <!-- Link with title -->
											<a href="insurance-travel.php" target="_self" title="Travel">
												<h3 class="css3panel-title captiontitle--dbg ff-alternative title-size-normal">TRAVEL INSURANCE</h3>
											</a>

                      <!--/ Link with title -->

                      <!-- Description -->
                      <div class="css3panel-text">
                        Travel more, worry less with Travel Insurance from as low as RM12*per person + <br />Earn 100 BPoints<br>&nbsp;
                      </div>
                      <!--/ Description -->

                      <!-- Button -->
                      <div class="css3panel-btn-area">
                        <!-- Button skewed full color skewed style -->
                        <a href="insurance-travel.php" class="btn btn-fullcolor btn-skewed" target="_self" title="Learn More">Learn More</a>
                      </div>
                      <!--/ Button -->
                    </div>
                    <!--/ Caption -->
                  </div>
                  <!--/ Panel #1 -->



									                  <!-- Panel #2 light style -->
									                  <div class="css3panel css3panel--1 cp-theme--light frame-2">
									                    <!-- Panel wrapper with custom height -->
									                    <div class="css3panel-inner" style="height:700px">
									                      <!-- Main image wrapper with custom height -->
									                      <div class="css3panel-mainimage-wrapper" style="height:700px">
									                        <!-- Background main image -->
									                        <div style="background-image:url(/aig-assets/insurance/car-insurance-1.jpg);" class="css3panel-mainimage">

									                          <!-- Gradient/Color overlay -->
									                          <div class="css3p-overlay css3p-overlay--gradient" style="">
									                          </div>
									                        </div>
									                        <!--/ Background main image -->
									                      </div>
									                      <!--/ Main image wrapper with custom height -->
									                    </div>
									                    <!-- Panel wrapper with custom height -->

									                    <!-- Caption -->
									                    <div class="css3panel-caption">
									                      <!-- Link with title -->
																				<a href="insurance-auto.php" target="_self" title="SPEED">
																					<h3 class="css3panel-title captiontitle--dbg ff-alternative title-size-normal">CAR INSURANCE</h3>
																				</a>

									                      <!--/ Link with title -->

									                      <!-- Description -->
									                      <div class="css3panel-text">
                                          Enjoy 10% Rebate + Earn 150 BPoints with FREE Road Tax Delivery!<br>&nbsp;<br>&nbsp;
									                      </div>
									                      <!--/ Description -->

									                      <!-- Button -->
									                      <div class="css3panel-btn-area">
									                        <!-- Button skewed full color skewed style -->
									                        <a href="insurance-auto.php" class="btn btn-fullcolor btn-skewed" target="_self" title="Learn More">Learn More</a>
									                      </div>
									                      <!--/ Button -->
									                    </div>
									                    <!--/ Caption -->
									                  </div>
									                  <!--/ Panel #2 -->



																		<!-- Panel #2 light style -->
																		<div class="css3panel css3panel--1 cp-theme--light frame-3">
																			<!-- Panel wrapper with custom height -->
																			<div class="css3panel-inner" style="height:700px">
																				<!-- Main image wrapper with custom height -->
																				<div class="css3panel-mainimage-wrapper" style="height:700px">
																					<!-- Background main image -->
																					<div style="background-image:url(/aig-assets/insurance/fraud-1.jpg);" class="css3panel-mainimage">

																						<!-- Gradient/Color overlay -->
																						<div class="css3p-overlay css3p-overlay--gradient" style="">
																						</div>
																					</div>
																					<!--/ Background main image -->
																				</div>
																				<!--/ Main image wrapper with custom height -->
																			</div>
																			<!-- Panel wrapper with custom height -->

																			<!-- Caption -->
																			<div class="css3panel-caption">
																				<!-- Link with title -->
                                        <a href="#" target="_self" title="SPEED">
																				<!--<a href="insurance-fraudulent-cover.php" target="_self" title="SPEED">-->
																					<h3 class="css3panel-title captiontitle--dbg ff-alternative title-size-normal">FRAUDULENT CHARGE COVER</h3>
																				</a>

																				<!--/ Link with title -->

																				<!-- Description -->
																				<div class="css3panel-text">
                                          Security is a priority, not an option. Get Fraudulent Charge Cover now at as low as RM0.09* per day<br>&nbsp;
																				</div>
																				<!--/ Description -->

																				<!-- Button -->
																				<div class="css3panel-btn-area">
																					<!-- Button skewed full color skewed style -->
                                          <a href="insurance-fraudulent-cover.php" class="btn btn-fullcolor btn-skewed" target="_self" title="Learn More">Learn More</a>
																				</div>
																				<!--/ Button -->
																			</div>
																			<!--/ Caption -->
																		</div>
																		<!--/ Panel #2 -->

                </div>
                <!-- CSS3 Panels container -->

                <div class="clearfix">
                </div>

                <!-- Bottom mask style 2 -->
                <div class="kl-bottommask kl-bottommask--shadow_ud">
                </div>
                <!--/ Bottom mask style 2 -->
              </div>
              <!--/ Slideshow - CSS3 Panels element -->
              <link rel="stylesheet" href="/aig-assets/sliders/css3-panels-custom/css3-panels.css" type="text/css" media="all">




		</section>

		<?php // include 'bottom.php';?>

	</div><!-- end page-wrapper -->



</body>
</html>
