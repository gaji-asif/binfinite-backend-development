<?php include ('top.php');?>

<?php

 require_once "netallianz-config.php";




if (!empty($_GET['action'])&& $_GET['action']=='submit' && isset($_POST['name']) && isset($_POST['ic'])){

  if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])){
      //get verify response data
      $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$CONF['secret'].'&response='.$_POST['g-recaptcha-response']);
      $responseData = json_decode($verifyResponse);

      if($responseData->success){

          //$member_id = mysql_result(mysql_query("SELECT MAX(member_id) FROM member"),0)+1;
          $name = strtoupper($_POST['name']);
          $ic = $_POST['ic'];
          $mobile = $_POST['mobile'];
          $email = $_POST['email'];
          $bcard = $_POST['bcard'];
          $voucher = $_POST['voucher'];
          $date_created = date('Y-m-d');

          //Check Exist Registration By Date
          $q1 = DB::select("SELECT * FROM aig_fraudulent_members WHERE ic = ? ORDER BY date_created DESC LIMIT 1", [ $ic ]);

          if (sizeOf($q1) > 0) {
            
            $r1 = $q1[0];

            // Date Checking Var Start (Vincent)
              $date = $r1->date_created;
              $olddate = str_replace('/', '-', $date);
              $datetime = strtotime($olddate);
            // Date Checking Var End (Vincent)

            // Date Checking Start (Vincent)
            if($r1){
              if(time() - $datetime  <= 31536000) {
                echo "<script>";
                echo "window.location='exist.php?id=existsubmissionyear'";
                echo "</script>";
                exit();
              }
            }
            // Date Checking End (Vincent)
          }



            //Check Voucher
            $q2 = DB::select("SELECT * FROM aig_fraudulent_vouchers WHERE voucher_code = ? AND voucher_status = 0 LIMIT 1", [ $voucher ]);

            if (sizeOf($q2) > 0) {
              $r2 = $q2[0];

              if (empty($r2->voucher_code)) {
                echo "<script>";
                echo "window.location='exist.php?id=invalidvoucher'";
                echo "</script>";
                exit();
              }

              DB::transaction(function () use ($name, $ic, $mobile, $email, $bcard, $voucher, $date_created) {
                  DB::insert("INSERT INTO aig_fraudulent_members SET
                             name = ?,
                             ic = ?,
                             mobile = ?,
                             email = ?,
                             bcard = ?,
                             voucher = ?,
                             date_created = ?", [
                              $name,
                              $ic,
                              $mobile,
                              $email,
                              $bcard,
                              $voucher,
                              $date_created,
                             ]);

                  DB::update("UPDATE aig_fraudulent_vouchers SET
                               voucher_status = 1 WHERE voucher_code = ?", [$voucher]);
              });


                echo "<script>";
                echo "window.location='register-success.php'";
                echo "</script>";
            } else {
              // Invalid voucher?
              echo('<h1 style="text-align: center; margin-top: 30px;">Voucher invalid</h1>');
            }
            

          }



      }else{
            echo "<script>";
            echo "window.location='robot-verification.php'";
            echo "</script>";
      }

}




?>

<!DOCTYPE html>
<html>
  <head>

 <script>
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
     alert('Only numbers are allowed.');
        return false;
    }
    return true;
}
  </script>

<script>
  function isNumberDot(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 46 || charCode > 57)) {
     alert('Only numbers are allowed.');
        return false;
    }
    return true;
}
</script>

  <script>
  function lettersOnly(evt) {
       evt = (evt) ? evt : event;
       var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
          ((evt.which) ? evt.which : 0));
       if (charCode > 32 && (charCode < 65 || charCode > 90) &&
          (charCode < 97 || charCode > 122)) {
          alert("Only letters are allowed.");
          return false;
       }
       return true;
     }
  </script>

  <script src="form-validation.js"></script>

</head>

<style>
.box-content {
   text-align:center;
    padding: 30px 30px 10px 30px;
}

.form-control:focus {
  border: 2px solid #b10320 !important;
}

/* Landscape */
@media only screen
  and (min-device-width: 768px)
  and (max-device-width: 1024px)
  and (orientation: landscape)
  and (-webkit-min-device-pixel-ratio: 1) {

  .bx-wrapper img {

  width:100%;
  display: block;
  height: 540px !important;
  }

}
</style>

  <body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NJXXSGL"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div class="body">

      <div role="main" class="main">
        <div class="slider-container">

          <div class="row">

              <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-12"><br />

                <div class="col-md-12"><h4 class="short" style="  text-transform:uppercase;"><b>ACTIVATE YOUR FRAUDULENT CHARGE COVER POLICY HERE</b></h4><p style="color:#1e1e1e; text-transform:uppercase; font-size:10px;">Leave us your details to to complete your purchase.</p></div>

                <form id="contestForm" action="index.php?action=submit" method="POST" onsubmit="return validate();">
                  <input type="hidden" name="_token" value="<?= csrf_token() ?>">

                  <div class="form-group">
                    <div class="col-md-6">
                      <label>Name </label>
                      <input type="text" value="" maxlength="100" class="form-control" name="name" id="name" placeholder="As per NRIC" onkeypress="return lettersOnly(event)">
                    </div>
                    <div class="col-md-6">
                      <label>IC No</label>
                      <input type="text" value="" maxlength="12" class="form-control" name="ic" id="ic" onkeypress="return isNumber(event)"placeholder="Example : 921212101234">
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-md-6">
                      <label>Mobile No</label>
                    <div class="input-group">
                            <span class="input-group-addon">
                              +60
                            </span>
                      <input type="text" value="" maxlength="10" class="form-control" name="mobile" id="mobile" onkeypress="return isNumber(event)" placeholder="123456789">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <label>Email </label>
                    <input type="text" value="" maxlength="100" class="form-control" name="email" id="email" placeholder="example@gmail.com">
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-md-6">
                    <label>BCard Number</label>
                    <input type="text" value="" class="form-control" maxlength="16" name="bcard" id="bcard" placeholder="eg : 6298438888888888 ">
                  </div>

                  <div class="col-md-6">
                    <label>Unique Voucher Number</label>
                    <input type="text" value="" class="form-control" name="voucher" id="voucher" placeholder="Refer voucher redeemed at B Infinite mobile app">
                  </div>
                </div>


                  <div class="form-group">
                    <div class="col-md-12">
                        <div class="checkbox">
                          <label style="font-size:12px;">
                            <input type="checkbox" value="" id="tnc">
                            I agree to the BInfinite's <a href="/privacy.php" target="_blank">Privacy Policy</a> and to be contacted by their Partner AIG Malaysia Insurance Berhad for marketing purposes.

                          </label>
                        </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <script src='https://www.google.com/recaptcha/api.js'></script>
                    <div class="g-recaptcha" data-sitekey="<?php echo $CONF['public'] ?>" style="transform:scale(0.85);-webkit-transform:scale(0.85);transform-origin:0 0;-webkit-transform-origin:0 0;"></div>
                  </div>

                  <div class="col-md-6">
                    <input type="submit" value="Submit" id="submit" class="btn btn-primary btn-lg" style="background-color: #424242; margin-left:20px;">
                  </div>
                <br /><br />
              </form>
              </div>
            </div>
          </div>





        </div>


      </div>



      <!-- FOOTER -->
      <?php include ('bottom.php');?>
    </div>

  </body>
</html>
