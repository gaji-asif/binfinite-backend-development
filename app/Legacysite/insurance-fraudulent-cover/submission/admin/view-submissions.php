<?php

ob_start();
require_once "../netallianz-config.php";
require_once "authentication.php";

if(!doAuth()){
    header("Location: login.php");
}


$result = array();

$q = mysql_query("SELECT * FROM member");
while($r = mysql_fetch_assoc($q)){
  array_push($result, $r);
}




?>

<!DOCTYPE html>
<html lang="en-US">
 <head>

 </head>

  <style>
  table tr th {
	  background:#e42f46;
	  color:#fff;
	  text-align:center;
  }

   table tr td {
	  background: #ebebeb;
	  text-align:center;
	  color:#323232;
	  font-size: 14px;
  }
  </style>


  <body class="size-1140">

	<?php include ('top.php');?>

    <!-- MAIN -->
    <main role="main">



      <!-- Section 3 -->
      <section class="section background-white">

        <div class="full-width text-center">

          <div class="line">
            <h2 class="">Submission <span class="text-primary">List</span></h2>
			<br /><br />

	  	<table class="hotels">


			  <tr>
			  <th colspan="7" style="font-size:18px;">Submission</th>
			  </tr>

              <tr>
                <th>No</th>
                <th>Name</th>
                <th>NRIC</th>
                <th>Mobile</th>
                <th>Email</th>
                <th>BCard</th>
                <th>Voucher</th>
                <th>Date Created</th>

              </tr>
              <?php
              if($result){
                foreach($result as $k => $v){
              ?>
              <tr>
                <td><?php echo $v['member_id']; ?></td>
                <td><?php echo $v['name']; ?></td>
                <td><?php echo $v['ic']; ?></td>
                <td><?php echo $v['mobile']; ?></td>
                <td><?php echo $v['email']; ?></td>
                <td><?php echo $v['bcard']; ?></td>
                <td><?php echo $v['voucher']; ?></td>
                <td><?php echo $v['date_created']; ?></td>
              </tr>
              <?php
                }
              }
              ?>
              </table>

        </div>
      </section>
    </div>


    </main>

    <!-- FOOTER -->

	<?php include ('bottom.php');?>



   </body>
</html>
