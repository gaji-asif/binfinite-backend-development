<?php

ob_start();
require_once "../netallianz-config.php";
require_once "authentication.php";

if(!doAuth()){
    header("Location: login.php");
}

if (!empty($_GET['action'])&& $_GET['action']=='upload'){

      //Upload file
      $directory = "files/";

      $temp = explode(".", $_FILES["upload"]["name"]);
      $extension = end($temp);
      $rename_filename = "pocket.".$extension;

      if(file_exists($directory)) {
         move_uploaded_file($_FILES['upload']['tmp_name'], $directory . $rename_filename);
      }else{
         mkdir($directory, 0777);
         move_uploaded_file($_FILES['upload']['tmp_name'], $directory . $rename_filename);
      }

      $pocket = file('files/pocket.csv');
      foreach($pocket as $line){
          $array_pocket[] = explode(",", $line);
      }

      if(!empty($array_pocket)){
        foreach($array_pocket as $k => $v){
            mysql_query("
            REPLACE INTO voucher SET
            voucher_code     = '$v[0]',
            voucher_status     = '0'
            ");
        }
      }

      header("location: view-voucher.php");

}

?>

<!DOCTYPE html>
<html lang="en-US">
 <head>

 </head>

  <body class="size-1140">

	<?php include ('top.php');?>

    <!-- MAIN -->
    <main role="main">


      <!-- Section 3 -->
      <section class="section background-white">
        <div class="full-width text-center">
          <form enctype="multipart/form-data" name="contestForm" id="contestForm" method="post" action="import.php?action=upload" onsubmit="return validate();" novalidate>
          <div class="line">
            <h2 class="headline text-thin text-s-size-30">Upload Voucher List</h2>
            <p class="text-size-20 text-s-size-16 text-thin"><input type="file" id="upload" name="upload" class="input-md input-rounded form-control"/></p>
            <p>Please be aware that voucher import list must not have duplicates.</p>
            <p>Import in CSV with voucher in column 1 only.</p>
			      <br /><br />
			      <input class="button background-primary border-radius text-white text-size-22" type="submit" value="Upload">

          </div>
          </form>
        </div>
      </section>
      <hr class="break margin-top-bottom-0">

    </main>

    <!-- FOOTER -->

	<?php include ('bottom.php');?>



   </body>
</html>
