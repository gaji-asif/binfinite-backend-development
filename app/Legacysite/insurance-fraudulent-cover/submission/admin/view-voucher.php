<?php

ob_start();
require_once "../netallianz-config.php";
require_once "authentication.php";

if(!doAuth()){
    header("Location: login.php");
}


$result = array();

$q = mysql_query("SELECT * FROM voucher");
while($r = mysql_fetch_assoc($q)){
  array_push($result, $r);
}


function returnstatus($a){
  if($a == 0){
    return 'Active';
  }elseif($a == 1){
    return 'Used';
  }else{
    return 'Unknown Status. Consult Administrator';
  }

}

?>

<!DOCTYPE html>
<html lang="en-US">
 <head>

 </head>

  <style>
  table tr th {
	  background:#e42f46;
	  color:#fff;
	  text-align:center;
  }

   table tr td {
	  background: #ebebeb;
	  text-align:center;
	  color:#323232;
	  font-size: 14px;
  }
  </style>


  <body class="size-1140">

	<?php include ('top.php');?>

    <!-- MAIN -->
    <main role="main">



      <!-- Section 3 -->
      <section class="section background-white">

        <div class="full-width text-center">

          <div class="line">
            <h2 class="">Voucher <span class="text-primary">List</span></h2>
			<br /><br />

	  	<table class="hotels">


			  <tr>
			  <th colspan="6" style="font-size:18px;">Voucher</th>
			  </tr>

              <tr>
                <th>No</th>
                <th>Code</th>
                <th>Status</th>
                <th></th>
              </tr>
              <?php
              if($result){
                foreach($result as $k => $v){
              ?>
              <tr>
                <td><?php echo $v['voucher_id']; ?></td>
                <td><?php echo $v['voucher_code']; ?></td>
                <td><?php echo returnstatus($v['voucher_status']); ?></td>
                <td><a href="delete-voucher.php?id=<?php echo $v['voucher_id']; ?>">Delete</a></td>
              </tr>
              <?php
                }
              }
              ?>
              </table>

        </div>
      </section>
    </div>


    </main>

    <!-- FOOTER -->

	<?php include ('bottom.php');?>



   </body>
</html>
