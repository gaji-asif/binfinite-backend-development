<?php

ob_start();
require_once "../netallianz-config.php";
require_once "authentication.php";

if(!doAuth()){
    header("Location: login.php");
}

if (!empty($_GET['action'])&& $_GET['action']=='submit'){

  $member = array();

  $q1 = mysql_query("SELECT * FROM member");
  $total_header = mysql_num_rows($q1);
  while($r1 = mysql_fetch_assoc($q1)){
    array_push($member, $r1);
  }
  $i = 0;
  $filename = "../files/contestant.csv";
  $fp = fopen($filename,"w+");
  $invoice_header = "ENTRY,NAME,NRIC,MOBILE,EMAIL,VOUCHER,BCARD,DATE_CREATED\n";
  fwrite($fp,$invoice_header);
  foreach($member as $k => $v){
    $header_data = $v['member_id']." , ".$v['name']." , ".$v['ic']." , ".$v['mobile']." , ".$v['email']." , ".$v['voucher']." , ".$v['bcard']." , ".$v['date_created'];
    fwrite($fp,$header_data);
    $i++;
    if($i != $total_header){
    $next = "\n";
    fwrite($fp,$next);
    }
  }
  fclose($fp);

  header("Content-Type: application/octet-stream");
  header("Content-Transfer-Encoding: Binary");
  header("Content-disposition: attachment; filename=".basename($filename));
  readfile ($filename);
  exit();

}

?>

<!DOCTYPE html>
<html lang="en-US">
 <head>

 </head>

  <body class="size-1140">

	<?php include ('top.php');?>

    <!-- MAIN -->
    <main role="main">


      <!-- Section 3 -->
      <section class="section background-white">
        <div class="full-width text-center">

          <div class="line">
            <h2 class="headline text-thin text-s-size-30">File Download</h2>
            <p class="text-size-20 text-s-size-16 text-thin">Please download the submission list <a href="download.php?action=submit" style="color:#d50522;">HERE</a></p>
			<br /><br />

          </div>
        </div>
      </section>
      <hr class="break margin-top-bottom-0">

    </main>

    <!-- FOOTER -->

	<?php include ('bottom.php');?>



   </body>
</html>
