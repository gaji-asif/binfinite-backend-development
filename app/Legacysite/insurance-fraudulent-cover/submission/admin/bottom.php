
 <footer>

      <!-- Bottom Footer -->
      <section class="padding background-dark">
        <div class="line">
          <div class="s-12 l-6">
            <p class="text-size-12">Copyright <?php echo date("Y"); ?> B Infinite</p>
          </div>
          <div class="s-12 l-6">
            <a class="right text-size-12" href="#">Netallianz Technology</a>
          </div>
        </div>
      </section>
    </footer>
    <script type="text/javascript" src="js/responsee.js"></script>
    <script type="text/javascript" src="owl-carousel/owl.carousel.js"></script>
    <script type="text/javascript" src="js/template-scripts.js"></script>
