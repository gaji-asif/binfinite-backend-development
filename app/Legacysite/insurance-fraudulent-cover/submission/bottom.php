
<br><br><br><br><br>
			<footer id="footer">

				<div class="footer-copyright">
					<div class="container">
						<div class="row">

							<div class="col-md-8">
								<p>Copyright <?php echo date('Y'); ?> &#xA9; BLoyalty Sdn Bhd. All rights reserved.	<a href="/terms.php" target="_blank">Terms & Conditions</a> | <a href="/privacy.php" target="_blank">Privacy Policy</a></p>
							</div>
							<div class="col-md-4">
								<p style="float:right; font-size:12px;"><a href="http://www.netallianz.com" target="_blank" style="color:#A0A0A0 !important;">Netallianz Technology</p>
							</div>
						</div>
					</div>
				</div>
			</footer>

			<style>
			#footer p {
				font-size:14px;
			}
			</style>


	<!-- Vendor -->
		<script src="vendor/jquery/jquery.js"></script>
		<script src="vendor/bootstrap/bootstrap.js"></script>
		<script src="vendor/common/common.js"></script>

		<!-- Theme Base, Components and Settings -->
		<script src="js/theme.js"></script>

		<!-- Specific Page Vendor and Views -->

		<script src="js/views/view.home.js"></script>

		<!-- Theme Custom -->
		<script src="js/custom.js"></script>

		<!-- Theme Initialization Files -->
		<script src="js/theme.init.js"></script>

		<!-- BX Slider -->
		<script type="text/javascript" src="bxSlider/jquery.bxslider.js"></script>
		<script type="text/javascript" src="bxSlider/jquery.bxslider.min.js"></script>
		<link rel="stylesheet" href="bxSlider/jquery.bxslider.css" type="text/css" media="screen" />

		<script>
		$(document).ready(function(){
		  $('.bxslider').bxSlider({
			   pager: false,
			   auto: true,
			   speed: 3000,
			   pause: 7000,
				adaptiveHeight: true
		  });
		});

		</script>


		<style>
		#ui-datepicker-div .ui-datepicker-calendar,
#ui-datepicker-div .ui-datepicker-current
{
    display: none !important;
}

		</style>

		<!-- Date Picker -->

    <link href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css" />

    <link href="datepicker/MonthPicker.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="datepicker/examples.css" />


    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script src="https://cdn.rawgit.com/digitalBush/jquery.maskedinput/1.4.1/dist/jquery.maskedinput.min.js"></script>

    <script src="datepicker/MonthPicker.min.js"></script>
    <script src="datepicker/examples.js"></script>
