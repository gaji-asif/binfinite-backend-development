<!DOCTYPE html>
<?php include ('top.php');?>

<html>
	<head>

	</head>


	<body>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NJXXSGL"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		<div class="body">

			<div role="main" class="main">

			<div class="container" style="padding-top: 160px; padding-bottom:172px;">

					<div class="row">
						<div class="col-md-12">
						<p style="color:#083f88; text-align:center;">Your Fraudulent Charge Cover policy has been successfully activated.
 						<br><br>
						A confirmation email will be sent to your registered email within 7 working days. Thank you for your purchase! </p>
						<div style="font-size:14px; text-align:center;">You will be redirected to homepage in <span id="counter">10</span> seconds.</div>
						</div>
					</div>


					<script>
						setInterval(function() {
						var div = document.querySelector("#counter");
						var count = div.textContent * 1 - 1;
						div.textContent = count;
						if (count <= 0) {
						location.href="https://www.binfinite.com.my";
							}
						}, 1000);
					</script>

				</div>

			</div>
			<!-- FOOTER -->
			<?php include ('bottom.php');?>
		</div>



	</body>
</html>
