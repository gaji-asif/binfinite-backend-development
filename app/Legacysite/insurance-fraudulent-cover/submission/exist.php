<!DOCTYPE html>
<?php
include ('top.php');
$id = $_GET['id'];
?>

<html>
	<head>

	</head>


	<body>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NJXXSGL"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		<div class="body">

			<div role="main" class="main">

			<div class="container" style="padding-top: 80px; padding-bottom:200px;">

					<div class="row">
						<div class="col-md-12">


						<?php if($id == "existsubmissionyear"){ ?>
						<p style="text-align:center; font-size:18px;">You have an existing insurance policy that is still active.<br>
							Any enquiries, kindly contact B Infinite.<br>
							Thank you.
						</p><br />
						<?php } ?>

						<?php if($id == "invalidvoucher"){ ?>
						<p style="text-align:center; font-size:18px;">Sorry, the voucher code you entered is not valid.<br>
							Please enter a valid voucher code.</p><br>
						<?php } ?>



						<p style="text-align:center; font-size:18px;"><a href="javascript:history.back()">Go back</a></p>
						</div>
					</div>


				</div>

			</div>
			<!-- FOOTER -->
			<?php include ('bottom.php');?>
		</div>



	</body>
</html>
