<!doctype html>


<style>


@media(max-width:767px) {
   .desktop {
     display: none;

   }
}

@media (min-width: 767px){
   .mobile {
     display: none;
   }
   }


 @media(max-width:767px) {
   .frame {
     height: 900px;
   }
 }

 @media (min-width: 767px) {
   .frame {
     height: 550px;
   }
 }

 .navigationbar{
   font-size: 20px;
   color:black;
   padding-left: 40px;
   padding-right:40px;
   font-weight: bold;

 }

 .linkcol {
    color: #083f88 !important;
   }

 h5 {
   font-size: 1em !important;
   font-weight: 600 !important;
   letter-spacing: normal !important;
   line-height: 18px !important;
   margin: 0 0 14px 0 !important;
   text-transform: uppercase !important;
     text-align: center;
     color: #083f88 !important;

 }

 .box-content{
   text-align: center;
 }

 .insuretitle{
   text-align: center;
   color:#0073ae !important;
 }

 div .center{
   text-align: center;
 }

 .navbtn{
   text-align:left !important;
   margin-bottom:3px !important;
   font-size:15px !important;
 }

 .navbtntablet{
   padding: 0px 50px 0px 50px;
   margin-bottom:3px !important;
   font-size:20px !important;
 }

 .navbtntablet:hover {
   border-bottom: 2px solid #23a2dc;
 }


 .ui-accordion .ui-accordion-header {
     display: block;
     cursor: pointer;
     position: relative;
     margin: 2px 0 0 0;
     padding: .5em .5em .5em .7em;
     font-size: 100%;
 }

 .ui-state-active, .ui{
   color:#0073ae !important;
    font-weight: normal;
    color: #ffffff;
}

.ui-accordion .ui-accordion-content {
    padding: 1em 2.2em;
    border-top: 0;
}

td{
  padding-left:5px !important;
  }
th{
  padding-left:5px !important;
  }

body {
  background-color: white !important;
}


</style>

<html lang="en-US" class="no-js">

<?php include '../gtm-insurance.php';?>

  <link rel="stylesheet" href="/aig-assets/new-font.css" type="text/css" media="all">
  <link rel="stylesheet" href="/aig-assets/bootstrap.css" type="text/css" media="all">
  <link rel="stylesheet" href="/aig-assets/template.css" type="text/css" media="all">
  <script src="/aig-assets/jquery-1.11.1.min.js" async></script>


	<div id="page_wrapper">
		<?php // include 'top.php';?>
		<div id="page_header" class="page-subheader site-subheader-cst uh_zn_def_header_style maskcontainer--mask3">
			<div class="bgback">
			</div>
			<div class="kl-bg-source">
			<div class="kl-bg-source__overlay"></div>
			</div>
			<div class="th-sparkles">
			</div>
			<!-- DEFAULT HEADER STYLE -->
			<div class="ph-content-wrap ptop-0">
				<div class="ph-content-v-center ptop-25">
					<div class="container">
							<div class="row">
                <div class="col-sm-12">
									<div class="subheader-titles">
										<h4 class="subheader-subtitle">Fraudulent Cover</h4>
                    <img src="/aig-assets/insurance/aig-logo.png" style="margin-bottom:5px; height:50px;" align="right">
									</div>
								</div>

								<div class="col-sm-6">

									<div class="clearfix"></div>
								</div>

							</div>
							<!-- end row -->
						</div>
				</div>
			</div>

		</div>


        	<section style="padding-top:0px !important; padding-bottom:0px !important;">


            <a href="#renew"><img class="desktop" src="aig-assets/images/fraud-insurance-1.jpg" style="width:100%;" alt=""></a>
            <a href="#renew"><img class="mobile" src="aig-assets/images/fraud-mobile-1.jpg" style="width:100%;" alt=""></a>


        <div class="container hidden-xs" style="text-align:center;">
          <div class="tabletmenu" id="" style="z-index:100; padding-top:20px;">
            <a href="#overview" class="navbtntablet linkcol" title="Overview">Overview</a>

            <a href="#benefits" class="navbtntablet linkcol" title="Benefits">Benefits</a>

            <a href="aig-assets/pdf/fraudulent-cover-faq.pdf" target="_blank" class="navbtntablet linkcol" title="FAQ">FAQ</a>
          </div>
        </div>

            <div role="main" class="main">

              <div class="container" style="padding-top: 60px;" id="overview">

                <div class="row center">
                  <div class="col-md-12">
                    <h1 class="insuretitle">Overview</h1>
                    <hr>
                    <strong>Shop to your heart's content today with no worries with AIG Fraudulent Charge Cover.</strong>
                    <br><br>
                    AIG Fraudulent Charge Cover provides 24 hour worldwide coverage against any monetary loss suffered by you as a result of fraudulent charges incurred on your  eligible card. AIG will reimburse the unauthorized charges to you.
                  </div>
                </div>
              </div>

            <div class="container" style="padding-top: 30px;">
              <h2 class="insuretitle">Our most popular features include</h1>

              <div class="row featured-boxes">

                  <div class="col-md-3 col-md-offset-3 col-sm-6 col-xs-12" style="padding-top:25px;">

                      <div class="box-content">
                        <img src="aig-assets/images/card-protection.png" style="margin-bottom:15px;">
                        <h5 style="text-transform: initial !important;">Protection against card loss and card theft<br></h5>
                      </div>

                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12" style="padding-top:25px;">

                      <div class="box-content">
                        <img src="aig-assets/images/24hours.jpg" style="margin-bottom:15px;">
                        <h5 style="text-transform: initial !important;">24 Hours Worldwide Coverage</h5>
                      </div>

                  </div>


                </div>
                <br>
                <p><span style="color:red;">*</span> Coverage is subject to actual policy language</p>

              </div>


              <div id="benefits" class="container" style="padding-top:30px;">
                <div class="row center">
                  <div class="col-md-12">
                    <h1 class="insuretitle">Benefits</h1>
                    <hr>

                  </div>
                </div>
                <div class="row featured-boxes">
                    <div class="col-md-6" style="text-align:left;">
                        <div>
                          <p><strong>Card loss or card theft protection</strong></p>
                          <p>We will reimburse you for the unauthorized charges incurred on your card which is lost or stolen. </p>
                        </div>
                    </div>
                    <div class="col-md-6" style="text-align:left;">
                      <div>
                        <p><strong>Illegal or unauthorized use of card information</strong></p>
                        <p>We will reimburse you for the unauthorized charges incurred as a result of the illegal or unauthorized use of your card information<br>
                          i. in-store;<br>
                          ii. by telephone;<br>
                          iii. at an ATM; or<br>
                          iv. online.
                        </p>
                      </div>
                    </div>
                    <p><span style="color:red;">*</span> Coverage is subject to actual policy language.</p>
                  </div>
                </div>

                <div id="renew" class="container" style="padding-top:30px;">
                  <h1 class="insuretitle">Redeem/ Purchase on B Infinite Mobile App Guide</h1>
                  <div class="col-md-3 col-sm-6 col-xs-12" style="text-align:center;">
                      <h3>Step 1</h3>
                      <img style="width:80%;" src="aig-assets/images/step1.jpg">
                      <p>Find AIG Fraudulent Charge Cover under Redemptions in the B Infinite Mobile App.</p>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12" style="text-align:center;">
                      <h3>Step 2</h3>
                      <img style="width:80%;" src="aig-assets/images/step2.jpg">
                      <p>Buy or Redeem with BPoints</p>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12" style="text-align:center;">
                      <h3>Step 3</h3>
                      <img style="width:80%;" src="aig-assets/images/step3.jpg">
                      <p>Get your voucher code, and fill up this <a href="/insurance-fraudulent-cover/submission/" target="_blank"><u>form</u></a> to activate your policy.</p>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12" style="text-align:center;">
                      <h3>Step 4</h3>
                      <img style="width:80%;" src="aig-assets/images/step4.jpg">
                      <p>Done! You will receive the confirmation email from B Infinite within 7 working days.</p>
                  </div>
                </div>


            <div style="text-align:center; padding-bottom:25px;">
              <a class="zn_sub_button btn btn-fullcolor th-button-register" style="font-size:18px;" href="aig-assets/pdf/fraudulent-purchase.pdf" target="_blank">View Full Steps</a>
            </div>
            <hr class="tall">

            <div class="container" style="padding-bottom:50px;">

              <div class="row center">
                <div class="col-md-12">
                  <h2>More about Fraudulent Cover</h2>

                </div>

              </div>
      				<div class="row center">
      					<div class="col-md-3 col-md-offset-3">
      							<h4 style="font-size:18px;">Product Disclosure Sheet</h4>

      					</div>

      					<div class="col-md-3">
      							<a href="aig-assets/pdf/fraudulent-disclosure-sheet.pdf" target="_blank" class="btn btn-primary btn-icon" style="font-size:18px !important;">DOWNLOAD</a>
      					</div>
      				</div>
      				<br />
      				<div class="row center">
      					<div class="col-md-3 col-md-offset-3">
      							<h4 style="font-size:18px;">Policy Wording</h4>

      					</div>

      					<div class="col-md-3">
      							<a href="aig-assets/pdf/fraudulent-policy-wording.pdf" target="_blank" class="btn btn-primary btn-icon" style="font-size:18px !important;">DOWNLOAD</a>
      					</div>
      				</div>

      			</div>

            </div>
            </div>

            <div class="container">
              <a href="insurance.php"><p>Go Back</p></a>
            </div>

            <hr class="tall">


        		</section>

        		<?php // include 'bottom.php';?>

        	</div><!-- end page-wrapper -->


                            <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
                            <script>
                            $( function() {
                              // Accordion
                              $("#accordion").accordion({
                                  header: "h3",
                                  collapsible: true,
                                  heightStyle: false,
                                  navigation: true,
                                  active: false
                              });
                            } );
                            </script>

        </body>
        </html>
