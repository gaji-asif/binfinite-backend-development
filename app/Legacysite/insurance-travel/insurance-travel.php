<!doctype html>


<style>


@media(max-width:767px) {
   .desktop {
     display: none;

   }
}

@media (min-width: 767px){
   .mobile {
     display: none;
   }
   }


 @media(max-width:767px) {
   .frame {
     height: 900px;
   }
 }

 @media (min-width: 767px) {
   .frame {
     height: 550px;
   }
 }

 .navigationbar{
   font-size: 20px;
   color:black;
   padding-left: 40px;
   padding-right:40px;
   font-weight: bold;

 }

 .linkcol {
    color: #083f88 !important;
   }

 h5 {
   font-size: 1em !important;
   font-weight: 600 !important;
   letter-spacing: normal !important;
   line-height: 18px !important;
   margin: 0 0 14px 0 !important;
   text-transform: uppercase !important;
     text-align: center;
     color: #083f88 !important;

 }

 .box-content{
   text-align: center;
 }

 .insuretitle{
   text-align: center;
   color:#0073ae !important;
 }

 div .center{
   text-align: center;
 }


.navgrp{
  z-index: 10!important;
}

 .navbtn{
   text-align:left !important;
   margin-bottom:3px !important;
   font-size:15px !important;
 }

 .navbtntablet{
   padding: 0px 50px 0px 50px;
   margin-bottom:3px !important;
   font-size:20px !important;
 }

 .navbtntablet:hover {
   border-bottom: 2px solid #23a2dc;
 }

 .ui-accordion .ui-accordion-header {
     display: block;
     cursor: pointer;
     position: relative;
     margin: 2px 0 0 0;
     padding: .5em .5em .5em .7em;
     font-size: 100%;
 }

 .ui-state-active, .ui{
   color:#0073ae !important;
    font-weight: normal;
    color: #ffffff;
}

.ui-accordion .ui-accordion-content {
    padding: 1em 2.2em;
    border-top: 0;
}

td{
  padding-left:5px !important;
  }
th{
  padding-left:5px !important;
  }

body {
  background-color: white !important;
}

iframe {
  position: absolute;
}


</style>

<html lang="en-US" class="no-js">

<?php include '../gtm-insurance.php';?>

  <link rel="stylesheet" href="/aig-assets/new-font.css" type="text/css" media="all">
  <link rel="stylesheet" href="/aig-assets/bootstrap.css" type="text/css" media="all">
  <link rel="stylesheet" href="/aig-assets/template.css" type="text/css" media="all">
  <script src="/aig-assets/jquery-1.11.1.min.js" async></script>

	<div id="page_wrapper">
		<?php //include 'top.php';?>
		<div id="page_header" class="page-subheader site-subheader-cst uh_zn_def_header_style maskcontainer--mask3">
			<div class="bgback">
			</div>
			<div class="kl-bg-source">
			<div class="kl-bg-source__overlay"></div>
			</div>
			<div class="th-sparkles">
			</div>
			<!-- DEFAULT HEADER STYLE -->
			<div class="ph-content-wrap ptop-0">
				<div class="ph-content-v-center ptop-25">
					<div class="container">
							<div class="row">
								<div class="col-sm-12">
									<div class="subheader-titles">
										<h4 class="subheader-subtitle">Travel Insurance</h4>
                    <img src="/aig-assets/insurance/aig-logo.png" style="margin-bottom:5px; height:50px;" align="right">
									</div>
								</div>

								<div class="col-sm-6">

									<div class="clearfix"></div>
								</div>

							</div>
							<!-- end row -->
						</div>
				</div>
			</div>

		</div>

        	<section style="padding-top:0px !important; padding-bottom:0px !important;">


            <a href="https://www-424.aig.com.my/buytravel/binfinite" target="_blank" rel="nofollow"><img class="desktop" style="width:100%;" src="aig-assets/images/travel-insurance-1.jpg" alt=""></a>
            <a href="https://www-424.aig.com.my/buytravel/binfinite" target="_blank" rel="nofollow"><img class="mobile" style="width:100%;" src="aig-assets/images/travel-insurance-mobile-1.jpg" alt=""></a>


        <div class="container hidden-xs" style="text-align:center;">
          <div class="tabletmenu" id="" style="z-index:100; padding-top:20px;">
            <a href="#overview" class="navbtntablet linkcol" title="Overview">Overview</a>

            <a href="#benefits" class="navbtntablet linkcol" title="Benefits">Benefits</a>

            <a href="aig-assets/pdf/travel-insurance-faq.pdf" target="_blank" class="navbtntablet linkcol" title="FAQ">FAQ</a>
            <a href="http://aig.binfinite.com.my" class="navbtntablet linkcol" title="Submission" target="_blank">Submission</a>

          </div>
        </div>

            <div role="main" class="main">

              <div class="container" style="padding-top: 60px;" id="overview">

                <div class="row center">
                  <div class="col-md-12">
                    <h1 class="insuretitle">Overview</h1>
                    <hr>
                    <strong>Let us "Jaga" your travel experiences with AIG Travel Insurance, your trusted travel buddy.</strong>
                    <br><br>
                    We let you enjoy your trip with a peace of mind by giving you comprehensive travel insurance coverage. You don't have to worry about medical expenses incurred as a result of an accident or illness, trip cancellations, flight delays, lost baggage because all this and more are covered by us.
                  </div>
                </div>
              </div>

            <div class="container" style="padding-top:30px;">
              <h2 class="insuretitle">Our most popular features include</h1>

              <div class="row featured-boxes">
                  <div class="col-md-3 col-sm-6 col-xs-12" style="padding-top:25px;">
                      <div class="box-content">
                        <img src="aig-assets/images/travel-assistance.png" style="margin-bottom:15px;">
                        <h5 style="text-transform:initial !important;">24 Hours, 7 Days Worldwide Travel Assistance</h5>
                      </div>
                  </div>

                  <div class="col-md-3 col-sm-6 col-xs-12" style="padding-top:25px;">
                      <div class="box-content">
                        <img src="aig-assets/images/expenses-cover.png" style="margin-bottom:15px;">
                        <h5 style="text-transform:initial !important;">Medical & Personal Accident Cover <span style="color:red;">*</span><br></h5>
                        <p>up to RM1million medical expenses & up to RM300,000 in personal accident</p>
                      </div>
                  </div>

                  <div class="col-md-3 col-sm-6 col-xs-12" style="padding-top:25px;">
                      <div class="box-content">
                        <img src="aig-assets/images/evac-repat.png" style="margin-bottom:15px;">
                        <h5 style="text-transform:initial !important;">Medical Evacuation & Repatriation Service </h5>
                        <p>included in every policy</p>
                      </div>
                  </div>

                  <div class="col-md-3 col-sm-6 col-xs-12" style="padding-top:25px;">
                      <div class="box-content">
                        <img src="aig-assets/images/travel-frequency.png" style="margin-bottom:15px;">
                        <h5 style="text-transform:initial !important;">Flexible Travel Frequency</h5>
                        <p>Option of single trip & multiple trips in a year (annual)</p>
                      </div>
                  </div>

                </div>
                <br>
                <p><span style="color:red;">*</span> Coverage differs according to product & plan.</p>

              </div>




              <div id="benefits" class="container" style="padding-top:30px;">
                <div class="row center">
                  <div class="col-md-12">
                    <h1 class="insuretitle">Benefits</h1>
                    <hr>

                  </div>
                </div>
                <div class="row featured-boxes">
                    <div class="col-md-6 col-sm-6" style="text-align:left;">
                        <div>
                          <p><strong>Comprehensive coverage</strong></p>
                          <p>We provide you with coverage for overseas medical expenses, trip cancellation, travel interruptions and delays, emergency medical expenses, medical evacuation costs and lost, damaged or delayed baggage.</p>
                        </div>
                        <div>
                          <br>
                          <p><strong>Trip cancellation cover</strong></p>
                          <p>Don't worry about the costs of transport & accommodation due to your trip cancellation. We'll reimburse you the non-refundable expenses if your trip is cancelled due to specific events, within 60 days before your departure date.</p>
                        </div>
                        <div>
                          <p><strong>Travel Inconvenience</strong></p>
                          <p>We provide you coverage for travel inconveniences such as travel delays, loss or damage to your personal belongings, including your checked-in baggage.</p>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6" style="text-align:left;">
                      <div>
                        <p><strong>Medical & personal accident cover</strong></p>
                        <p>You can get up to RM1million for overseas medical expenses and up to RM300,000 for personal accidents, depending on the product and plan you purchase. Our travel assistance team is on call by phone to provide you emergency travel assistance, ensuring you receive appropriate medical care.</p>              </div>
                      <div>
                        <p><strong>Natural disasters</strong></p>
                        <p>We will pay for travel inconveniences related expenses against natural disasters such as earthquakes, floods and volcano eruptions, occuring at the country of travel.</p>
                      </div>
                      <div>
                        <p><strong>Our own global assistance service</strong></p>
                        <p>We have a fast and efficient 24-hour Emergency Assistance Team available to assist you in the event of injury following an accident or if you are ill and require hospital attention abroad.</p>
                      </div>

                    </div>
                  </div>
                  <p><span style="color:red;">*</span> Coverage differs according to product & plan.</p>
                </div>

                <div class="container" style="padding-top:20px;">
                  <h2 class="insuretitle">Let us "Jaga" you on your trip anytime, anywhere!</h2>
                    <br>
                    <div class="col-md-4">
                      <p style="text-align: center;">Eat all the yummy food you want.<br>
                        <b>We'll Jaga you if you fall sick.</b><br>
                        <img src="aig-assets/images/travel-1.png" alt="">
                      </p>

                    </div>
                    <div class="col-md-4">
                      <p style="text-align: center;">
                        Bring your gadgets along with you.<br>
                        <b>We'll Jaga you if it gets stolen.</b><br>
                        <img src="aig-assets/images/travel-2.png" alt="">
                      </p>
                    </div>

                    <div class="col-md-4">
                      <p style="text-align: center;">
                        Try all the outdoor activities you want.<br>
                        <b>We'll Jaga you if you get into accidents.</b><br>
                        <img src="aig-assets/images/travel-3.png" alt="">
                      </p>
                    </div>
                </div>

                <hr>

                <div class="container">

                  <div class="row center">
                    <div class="col-md-12">
                      <h2>More about Travel Insurance</h2>

                    </div>

                  </div>
                <div class="row center">
                  <div class="col-md-3 col-md-offset-3">
                      <h4 style="font-size:18px;">Product Disclosure Sheet</h4>

                  </div>

                  <div class="col-md-3">
                      <a href="insurance-travel/aig-assets/pdf/travel-insurance-disclsure-sheet.pdf" target="_blank" class="btn btn-primary btn-icon" style="font-size: 18px;">DOWNLOAD</a>

                  </div>
                </div>
                <br />
                <div class="row center">
                  <div class="col-md-3 col-md-offset-3">
                      <h4 style="font-size:18px;">Policy Wording</h4>

                  </div>

                  <div class="col-md-3">
                      <a href="insurance-travel/aig-assets/pdf/travel-insurance-policy-wording.pdf" target="_blank" class="btn btn-primary btn-icon" style="font-size: 18px;">DOWNLOAD</a>
                  </div>
                </div>
                <hr>

                </div>

            <div style="text-align:center; padding-bottom:25px;">
              <a class="zn_sub_button btn btn-fullcolor th-button-register" style="font-size:18px;" href="https://www-424.aig.com.my/buytravel/binfinite" target="_blank">Buy Now</a>
            </div>

            <div class="container">
              <a href="insurance.php"><p>Go Back</p></a>
            </div>

            <hr class="tall">


            </div>
            </div>

        		</section>

        		<?php //include 'bottom.php';?>

        	</div><!-- end page-wrapper -->


                            <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
                            <script>
                            $( function() {
                              // Accordion
                              $("#accordion").accordion({
                                  header: "h3",
                                  collapsible: true,
                                  heightStyle: false,
                                  navigation: true,
                                  active: false
                              });
                            } );
                            </script>

        </body>
        </html>
