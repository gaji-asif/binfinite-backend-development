<!DOCTYPE html>
<?php include ('top.php');?>

<html>
	<head>

	</head>


	<body>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NJXXSGL"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->

		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5P99F9L"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		<div class="body">

			<div role="main" class="main">

			<div class="container" style="padding-top: 60px; padding-bottom:30px;">

					<div class="row">
						<div class="col-md-12">
						<p style="text-align:center; font-size:18px;">You may qualify for AIG Malaysia Comprehensive Auto Insurance if:</p>
						<div class="col-md-2"></div>
						<div class="col-md-8">
						<p style="text-align:center; font-weight:600;">
							<ul >
							<li>You are between 24-65 years of age with more than 2 years driving experience</li>
							<li>You have only made a single claim on your vehicle for the past 3 years that is less than RM10k;</li>
							or<br>
							You have not made a claim on your vehicle for the past 3 years<br>
							Your driver's license is valid and not suspended due to road traffic offence(s)<br>
							<li>Your vehicle is not:</li>
							-Toyota Hilux<br>
							-Toyota Fortuner<br>
							-Toyota Vellfire with the sum insured of RM150k or more<br>
							-Motorcycle, Commercial Vehicle or for Commercial usage<br>
							-Sports or Performance vehicle<br>
							-Unregistered new / reconditioned vehicle<br>
							-Modified from manufacturer's specifications to increase engine performance from original<br>
							<li>Your vehicle is within 15 years old from the year of manufacture</li>
							<li>The sum insured for your vehicle is between RM11k and RM400k</li>
							</ul>
						</p>
						</div>
						<div class="col-md-2"></div>
				</div>

		<script>
		function closeWin() {
 		   myWindow.close();
}		</script>
						<p style="text-align:center; font-weight:600;">
							Do you wish to proceed ?
							<br /><br />
							<a href="https://www-400.aig.com.my/AutoDirect/login?compcode=71&agtcode=0560978001" target="_blank" rel="nofollow"><button class="btn btn-primary btn-icon">Yes</button></a>
							<a href="index.php"><button href="index.php" class="btn btn-primary btn-icon">No</button></a>
						</p>

						</div>
					</div>


				</div>

			</div>
			<!-- FOOTER -->
			<?php include ('bottom.php');?>
		</div>



	</body>
</html>
