<!DOCTYPE html>
<?php include ('top.php');?>

<html>
	<head>

</head>

<style>
.box-content {
   text-align:center;
    padding: 30px 30px 10px 30px;
}


</style>

	<body>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NJXXSGL"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		<div class="body">

			<div role="main" class="main">

				<div class="container" style="padding-top: 60px;">

					<div class="row center">
						<div class="col-md-12">
							<h2 class="short">Drive with ease of mind <br />knowing that your car is fully covered.
							</h2>
							<p style="font-size:18px;">
								We believe that your road trip should be worry-free. <br />Enjoy these benefits when you hit the road, with ease of mind knowing that <br />you're fully protected under the coverage of AIG.
							</p>
						</div>
					</div>

					<hr class="tall">
				</div>

			<div class="container">

				<div class="row featured-boxes">
						<div class="col-md-3 col-sm-3">

								<div class="box-content">
									<img src="images/renewal-online.png" style="margin-bottom:15px;">
									<h5>Instant Renewal Online</h5>
									<p>Including Road Tax Renewal Service with Delivery</p>
								</div>

						</div>
						<div class="col-md-3 col-sm-3">

								<div class="box-content">
									<img src="images/award-winning.png" style="margin-bottom:15px;">
									<h5>Award Winning Claims Service*</h5>
									<p>Fastest claims turnaround time</p>
									<a href="claims.php" target="_blank">How to claim</a>

								</div>

						</div>
						<div class="col-md-3 col-sm-3">

								<div class="box-content">
									<img src="images/road-assistance.png" style="margin-bottom:15px;">
									<h5>Emergency Road Assistance Included</h5>
									<p>Call 1300 88 3933 for towing, change of tyre & battery and more</p>
								</div>

						</div>

						<div class="col-md-3 col-sm-3">

								<div class="box-content">
									<img src="images/warranty.png" style="margin-bottom:15px;">
									<h5>1 Year Warranty</h5>
									<p>On repairs done at our panel workshop</p>

								</div>

						</div>

					</div>
					<br />
					<p style="font-size:14px; font-style:italic;"><span style="color:red;">*</span> Note : Road Tax Renewal with Delivery is currently available only for vehicles used in West Malaysia.
					<br>
					<span style="color:red;">*</span> AIG Malaysia is awarded "Best Automotive Claims Management Service" by Malaysia's <a href="http://www.mrc.com.my/" target="_blank" rel="nofollow">Motordata Research Consortium (MRC)</a></p>

				</div>


			<hr class="tall">

				<div class="container">

					<div class="row center">
						<div class="col-md-12">
							<h2>Online Renewal Services
							</h2>

						</div>

					</div>
				<div class="row">
					<div class="col-md-9">
							<h4>Car Insurance not expiring yet ?</h4>
							<p>
								Leave us your details to receive renewal reminder before your next expiry date.
							</p>
						</div>
						<div class="col-md-3">
							<a href="index.php" target="_blank" class="btn btn-primary push-top">Yes, send me a renewal reminder</a>
					</div>
				</div>
				<br />
				<div class="row">
					<div class="col-md-9">
							<h4>Car Insurance expiring soon ?</h4>
							<p>
								Renew your Car Insurance and Road Tax together with Delivery to your door step !
							</p>
						</div>
						<div class="col-md-3">
							<a href="renew-now.php" target="_blank" class="btn btn-primary push-top">Renew online now</a>
					</div>
				</div>

				<hr class="tall">

				</div>

				<div class="container">

					<div class="row center">
						<div class="col-md-12">
							<h2>Online Quick Quote & Renewal in 4 easy steps

							</h2>

						</div>

					</div>
					<div class="row center">

						<div class="col-md-12">
						<p>Click the button below to discover how to quick quote & renew online with us in just 4 easy steps.</p>

					</div>
<!--				<div class="row">
					<div class="col-md-6">
							<h4 style="font-size:18px;">Are you new and ready to renew your Car Insurance with us ?</h4>
							<p>Just follow the simple steps below for hassle free renewal.</p>
							<p style="font-size:12px;"><span style="color:red;">* </span>Note : Only for Malaysians aged between 24 and 65, who own a registered car.</p>
					</div>

					<div class="col-md-6">
							<h4 style="font-size:18px;">Are you an existing Car Insurance policyholder with us ?</h4>
							<p>All you need to do is input your 3 existing policy information and going through the steps below will be even faster.</p>

					</div>-->

				</div>

					<div class="row center">
						<div class="col-md-12">
							<a href="how-to-renew.php" target="_blank" class="btn btn-primary btn-lg btn-icon">4 EASY STEPS</a>

							<p><br />If you face difficulty renewing online, just email us at <a href="mailto:AIGMYCare@aig.com">AIGMYCare@aig.com</a> or call us at <a href="tel:1800 88 8811"> 1800 88 8811</a> for assistance.</p>
						</div>
					</div>

				</div>
			<hr class="tall">
			<div class="container">

					<div class="row center">
						<div class="col-md-12">
							<h2>More about Car Insurance</h2>

						</div>

					</div>
				<div class="row center">
					<div class="col-md-6">
							<h4 style="font-size:18px;">Product Disclosure Sheet</h4>

					</div>

					<div class="col-md-6">
							<a href="/aig-assets/pdf/car-product-disclosure-sheet.pdf" target="_blank" class="btn btn-primary btn-icon"><i class="fa fa-download"></i>DOWNLOAD</a>

					</div>
				</div>
				<br />
				<div class="row center">
					<div class="col-md-6">
							<h4 style="font-size:18px;">Policy Wording</h4>

					</div>

					<div class="col-md-6">
							<a href="/aig-assets/pdf/car-policy-wording.pdf" target="_blank" class="btn btn-primary btn-icon"><i class="fa fa-download"></i>DOWNLOAD</a>

					</div>
				</div>

			</div>
				<hr class="tall">

			<div class="container" style="padding-bottom:50px;">

					<div class="row center">
						<div class="col-md-12">
							<h2>More about Road Tax Renewal Service</h2>

						</div>

					</div>
				<div class="row center">
					<div class="col-md-12">
							<p style="font-size:18px;">We provide Road Tax Renewal Service with Delivery when you renew your Car Insurance online with AIG via B Infinite. </p>

						<a href="road-tax-renewal.php" target="_blank" class="btn btn-primary btn-icon">MORE INFO >></a>
					</div>
				</div>

				<div class="row center">
					<div class="col-md-12">
						<br><img src="images/aig-logo.png" style="width:80px;"><span style="font-size:12px;">underwritten by AIG Malaysia Insurance Berhad (795492-W)</span>
					</div>
				</div>

			</div>
			</div>
			<!-- FOOTER -->
			<?php include ('bottom.php');?>
		</div>

	</body>
</html>
