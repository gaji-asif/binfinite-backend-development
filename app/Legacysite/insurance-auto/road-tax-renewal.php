<!DOCTYPE html>
<?php include ('top.php');?>

<html>
	<head>

</head>

<style>
.box-content {
   text-align:center;
    padding: 30px 30px 10px 30px;
}

</style>

	<body>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NJXXSGL"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		<div class="body">

			<div role="main" class="main">

				<div class="container" style="padding-top: 60px; padding-bottom:60px;">

					<div class="row center">
						<div class="col-md-12">
							<h2 class="short">Road Tax Renewal Service
							</h2>
							<p style="font-size:18px;">We provide Road Tax Renewal Service with Delivery when you renew your Car Insurance online with AIG via B Infinite. </p>
						</div>
					</div>
				</div>



			<div class="container">

					<div class="row">
						<div class="col-md-12">

							<div class="toogle" data-plugin-toggle>
								<section class="toggle active">
									<label>Is the road tax renewal service applicable for vehicles used in West Malaysia & East Malaysia?</label>
									<div class="toggle-content">
										<p>This service is currently available only for vehicles used in West Malaysia.
										<br /><br />For vehicles used in East Malaysia, you can still renew your car insurance online (without selecting road tax).</p>
									</div>
								</section>
								<section class="toggle">
									<label>What type of charges are involved when I renew road tax with AIG Car Insurance online via B Infinite?</label>
									<div class="toggle-content">
										<p>You'll only be charged for road tax fee (West Malaysia rate applies). No charges will be imposed for delivery fee & service charge, as this is a complimentary service by AIG. </p>
									</div>
								</section>
								<section class="toggle">
									<label>When will I receive my road tax after I have renewed it here?</label>
									<div class="toggle-content">
										<p>Klang Valley: 3 working days <br />
											Peninsular/West Malaysia : within 3-4 working days <br />
											Sabah & Sarawak : within 6 working days </p>
									</div>
								</section>
								<section class="toggle">
									<label>Who will deliver the road tax to me?</label>
									<div class="toggle-content">
										<p>All deliveries are facilitated by MyEG Services Logistic Officers. </p>
									</div>
								</section>
								<section class="toggle">
									<label>I've renewed my car insurance with road tax renewal online with AIG via B Infinite. I haven't receive my road tax as per expected delivery date. What do I do?</label>
									<div class="toggle-content">
										<p>You can check your road tax delivery status via the following method : <br />
											<ul style="color:white">
											<li>Online via MyEG website <a href="https://www.myeg.com.my/delivery_status" target="_blank" rel="nofollow">here</a></li>
											<li>Call MyEG customer service at +603 7801 8888</li>
											<li>Call AIG Malaysia to enquire further at 1800 88 8811</li>
											</ul>
										</p>
									</div>
								</section>
							</div>
						</div>

					</div>
							<div class="row center">
					<div class="col-md-12">
						<br><img src="images/aig-logo.png" style="width:80px;"><span style="font-size:12px;">underwritten by AIG Malaysia Insurance Berhad (795492-W)</span>
					</div>
				</div>
			</div>
			<br /><br />
			</div>

			<!-- FOOTER -->
			<?php include ('bottom.php');?>
		</div>

	</body>
</html>
