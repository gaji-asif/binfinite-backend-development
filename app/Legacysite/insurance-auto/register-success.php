<!DOCTYPE html>
<?php include ('top.php');?>

<html>
	<head>

	</head>


	<body>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NJXXSGL"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		<div class="body">

			<div role="main" class="main">

			<div class="container" style="padding-top: 60px; padding-bottom:172px;">

					<div class="row">
						<div class="col-md-12">
						<h4 style="color:#083f88; text-align:center; text-transform:uppercase;">Your reminder request was successfully submitted!<br /></h4>
						<p style="text-align:center; font-size:18px; color:#313131;">You will receive a free car insurance renewal reminder from AIG prior to your car insurance expiry. Thank you.<br><br><br>
						</div>
					</div>



				</div>

			</div>
			<!-- FOOTER -->
			<?php include ('bottom.php');?>
		</div>



	</body>
</html>
