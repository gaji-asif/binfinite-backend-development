<!DOCTYPE html>
<?php include ('top.php');?>

<html>
	<head>

</head>

<style>
.box-content {
   text-align:center;
    padding: 30px 30px 10px 30px;
}

</style>

	<body>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NJXXSGL"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		<div class="body">

			<div role="main" class="main">

				<div class="container" style="padding-top: 60px; padding-bottom:60px;">

					<div class="row center">
						<div class="col-md-12">
							<h2 class="short">Steps to Renew Online
							</h2>

						</div>
					</div>
				</div>



			<div class="container">

					<div class="row">
						<div class="col-md-4 col-sm-4">
						<img src="images/step-1.png" style="margin-bottom:15px;">
						</div>

						<div class="col-md-8 col-sm-8">
							<h4>Quick Quote - Details of your car </h4>
							<p>We'll display your quote instantly. Just input your car details to get your next NCD (No Claims Discount) and our recommended sum insured for your car model. </p>
							<p style="font-size:12px;">Tips : The information below can be obtained from your current insurer's renewal notice, policy schedule or car registration card.</p>
							<ol style="font-size:12px;">
								<li>Chassis No</li>
								<li>Policy Start Date (Policy Start Date is next day of your Car Insurance expiry date)</li>
								<li>Car Make, Model, Year & Engine Capacity (Example : Proton, Saga, 2014, 1332cc)</li>
							</ol>
						</div>

					</div>

			</div>
			<hr class="tall">

			<div class="container">

					<div class="row">
						<div class="col-md-4 col-sm-4">
						<img src="images/step-2.png" style="margin-bottom:15px;">
						</div>

						<div class="col-md-8 col-sm-8">
							<h4>Details of yourself and your named drivers </h4>
							<p>Input your details as a policyholder and your named drivers.</p>
							<p style="font-size:12px;">Tips : A valid email address is important to receive your car insurance policy.</p>

						</div>

					</div>

			</div>
			<hr class="tall">

			<div class="container">

					<div class="row">
						<div class="col-md-4 col-sm-4">
						<img src="images/step-3.png" style="margin-bottom:15px;">
						</div>

						<div class="col-md-8 col-sm-8">
							<h4>Choose to include Road Tax Renewal, with Delivery (only for vehicles used in West Malaysia) </h4>
							<p>You can input your prefered delivery address and it'll be delivered to your doorstep.</p>
							<p style="font-size:12px;">Tips : We'll even tell you when will it be delivered to you.</p>

						</div>

					</div>

			</div>
			<hr class="tall">

			<div class="container">

					<div class="row">
						<div class="col-md-4 col-sm-4">
						<img src="images/step-4.png" style="margin-bottom:15px;">
						</div>

						<div class="col-md-8 col-sm-8">
							<h4>Review Quotation and Make Payment Online</h4>
							<p>We accept Malaysia issued credit/debit card for payment, from any banks.</p>
							<p style="font-size:12px;">Tips : After successful payment, you'll get an email notification on your policy schedule and JPJ notification with road tax delivery date.</p>

						</div>

					</div>

			</div>
			<br /><br />
				<div class="container">

					<div class="row center">
						<div class="col-md-12">

							<p><br />If you face difficulty renewing online, just email us at <a href="mailto:AIGMYCare@aig.com">AIGMYCare@aig.com</a> or call us at <a href="tel:1800 88 8811"> 1800 88 8811</a> for assistance.</p>
						</div>
					</div>
				</div>

			<br /><br/ >
			</div>

			<!-- FOOTER -->
			<?php include ('bottom.php');?>
		</div>

	</body>
</html>
