<!DOCTYPE html>
<?php include ('top.php');?>

<html>
	<head>

	</head>

	<style>

	ol > li {
		line-height:30px;
	}


	ol > li > ul >li {
		line-height:30px;
	}
	</style>

	<body>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NJXXSGL"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		<div class="body">

			<div role="main" class="main">

			<div class="container" style="padding-top: 60px;">

					<div class="row">
						<div class="col-md-12"><h2 style="color:#083f88;">How to Make A Claim</h2>
						</div>
					</div>
					<br />
					<div class="row">
						<div class="col-md-3">
							<!-- Side Bar -->
							<?php include ('side-bar.php');?>
						</div>
						<div class="col-md-9">

							<h2>Third Party Property Damage Claim</h2>

							<div class="row">
								<div class="col-md-12">

                                    <ol>
									<li>Notify the other driver and/or owner who were involved in the road accident with your insured vehicle in writing about your intention to make insurance claim against him. In addition, state that you hold him responsible, and ask him to notify his insurer;</li>
								    <li>Appoint a licensed adjuster to inspect your insured vehicle and thereafter provide you with their loss assessment/report; and</li>
                                    <li>If you do not have the other driver's contact information, you may conduct a search at Jabatan Pengangkutan Jalan / Road Transport Department based on the vehicle’s registration number to identify the insurer of the Third Party vehicle (“TP Insurer”).</li>
                                    <li>Submit the following documents to TP Insurer to facilitate your claim: </li>
                                    <ol type="a">
                                    <li>Police report;</li>
                                    <li>Police sketch plan and key;</li>
                                    <li>Police investigation report;</li>
                                    <li>Adjuster's report together with photographs of damaged insured vehicle;</li>
                                    <li>Original bill and receipt issued by the adjuster for their services;</li>
                                    <li>Copy of your own vehicle insurance policy; and</li>
                                    <li>Copy of your identity card, driving license and vehicle registration card.</li>
                                    </ol>
                                    </ol>
                                    <br /><br /><br />


                               <div class="row center">
                               <span style="font-size:18px;"><b>For further inquiries, please call us at <br />
                               <a href="tel:1 800 88 8811">1 800 88 8811</a> or email us at <a href="mailto:AIGMYCare@aig.com">AIGMYCare@aig.com</a></b></span>
                               <br /><br /><br />

                               <img src="images/aig-logo.png" style="width:80px;"><span style="font-size:12px;">underwritten by AIG Malaysia Insurance Berhad (795492-W)</span>
                             </div>


								</div>
							</div>


						</div>

					</div>

				</div>

			</div>
			<!-- FOOTER -->
			<?php include ('bottom.php');?>
		</div>



	</body>
</html>
