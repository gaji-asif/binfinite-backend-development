<!DOCTYPE html>
<?php include ('top.php');?>

<html>
	<head>

	</head>

	<style>

	ol > li {
		line-height:30px;
	}


	ol > li > ul >li {
		line-height:30px;
	}
	</style>

	<body>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NJXXSGL"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		<div class="body">

			<div role="main" class="main">

			<div class="container" style="padding-top: 60px;">

					<div class="row">
						<div class="col-md-12"><h2 style="color:#083f88;">How to Make A Claim</h2>
						</div>
					</div>
					<br />
					<div class="row">
						<div class="col-md-3">
							<!-- Side Bar -->
							<?php include ('side-bar.php');?>
						</div>
						<div class="col-md-9">

							<h2>Own Damage Claim - Accident</h2>

							<div class="row">
								<div class="col-md-12">
									<ol>
									<li>Notify us immediately after an accident. All accidents must be notified to us regardless of whether you intend to make a claim or otherwise.</li>
									<li>Only send your insured vehicle to our approved panel workshops.</li>
									<li>Ensure the following documents are provided to us:
										<ul>
											<li>Duly completed <a href="pdf/accident-claim-form.pdf" target="_blank">claim form</a></li>
											<li>A copy of:
											<ul>
												<li>Driving license and identity card of driver who was involved in the road accident</li>
												<li>Driving license and identity card of Insured</li>
												<li>Updated insured’s vehicle registration card (both sides);</li>
												<li>Police report (original or police certified true copy) </li>
												<li>For Commercial Vehicle: <br />i. Company Form 24 & 49<br /> ii. Driver's Good Driving License </li>
												<li>To be presented to the workshop (if any):
												<br />i. Daily Cash Allowance Policy
												<br />ii. Waiver of Betterment
												<br />iii. Option of spray painting of whole car</li>
											</ul>
                                            </li>
                                         </ul>
									  </li>

                                    <li>If pursuing Knock-For-Knock (“KFK”) Claim on your policy, kindly provide us the following:
                                        <ul>
											<li>All documents mentioned in item 3 above;</li>
											<li>Third party vehicle insurance information (by way of Jabatan Pengangkutan Jalan/ Road Transport Department) search; </li>
                                            <li>Duly completed KFK declaration letter that you are making a KFK Claim for damage to your vehicle on your policy;</li>
                                            <li>Police investigation result. </li>
                                            <li>Police Sketch Plan & Keys</li>
                                        </ul>
                                     </li>
							   </ol>

                               <br />

                               <b>Accident Guard Coverage</b> <br /><br />
                               If you have purchased this additional coverage, we only need to know the check-in and check-out dates for repair works carried out at our panel workshop to process your claim following benefits arising out of an own damage claim due to accident: <br /><br />
                               <ul>
                               <li>Daily cash allowance;</li>
                               <li>Waiver of betterment cost;</li>
                               <li>Option of spray painting of whole car.</li>
                               </ul>
                               <br />

                               <b>Note</b> : <i>Daily Cash Allowance is paid to you whilst Waiver of Betterment Cost and Spray Painting of Whole Car are stated in our offer letter for the own damage claim due to accident.</i>

                               <br /><br /><br />

                               <div class="row center">
                               <span style="font-size:18px;"><b>For further inquiries, please call us at <br />
                               <a href="tel:1 800 88 8811">1 800 88 8811</a> or email us at <a href="mailto:AIGMYCare@aig.com">AIGMYCare@aig.com</a></b></span>
                               <br /><br /><br />

                               <img src="images/aig-logo.png" style="width:80px;"><span style="font-size:12px;">underwritten by AIG Malaysia Insurance Berhad (795492-W)</span>
                               </div>

								</div>
							</div>


						</div>

					</div>

				</div>

			</div>
			<!-- FOOTER -->
			<?php include ('bottom.php');?>
		</div>



	</body>
</html>
