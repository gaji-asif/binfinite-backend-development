
	<head>
		<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NJXXSGL');</script>
<!-- End Google Tag Manager -->
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5P99F9L');</script>
<!-- End Google Tag Manager -->


		<!-- Basic -->
		<meta charset="utf-8">
		<title>BInfinite Car Insurance Renewal Campaign</title>
		<meta name="keywords" content="b infinite, car insurance, bcard, get free bpoints, free bpoints, free insurance reminder" />
		<meta name="description" content="Share your car insurance expiry month and get 500 BPoints plus a free renewal reminder before your next expiry date. Limited to first 1,000 members only! Hurry! ">
		<meta name="author" content="Netallianz">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="/insurance-auto/vendor/bootstrap/bootstrap.css">
		<link rel="stylesheet" href="/insurance-auto/vendor/fontawesome/css/font-awesome.css">
		<link rel="stylesheet" href="/insurance-auto/vendor/owlcarousel/owl.carousel.css" media="screen">
		<link rel="stylesheet" href="/insurance-auto/vendor/owlcarousel/owl.theme.css" media="screen">
		<link rel="stylesheet" href="/insurance-auto/vendor/magnific-popup/magnific-popup.css" media="screen">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="/insurance-auto/css/theme.css">
		<link rel="stylesheet" href="/insurance-auto/css/theme-elements.css">
		<link rel="stylesheet" href="/insurance-auto/css/theme-blog.css">
		<link rel="stylesheet" href="/insurance-auto/css/theme-shop.css">
		<link rel="stylesheet" href="/insurance-auto/css/theme-animate.css">

		<!-- Current Page CSS -->
		<link rel="stylesheet" href="/insurance-auto/vendor/rs-plugin/css/settings.css" media="screen">
		<link rel="stylesheet" href="/insurance-auto/vendor/circle-flip-slideshow/css/component.css" media="screen">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="/insurance-auto/css/skins/default.css">

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="/insurance-auto/css/custom.css">

		<!-- Head Libs -->
		<script src="vendor/modernizr/modernizr.js"></script>


		<!-- Analytic Code -->

<!--	<script>
	 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	  ga('create', 'UA-81045901-2', 'auto');
	 ga('send', 'pageview');
 </script> -->

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-81045901-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

	</head>

		<header id="header" class="colored flat-menu">

		<div class="container">
				<div class="row">
			<div class="col-md-2 col-sm-6 col-xs-6">

					<div class="logo">
						<a href="#">
							<img width="190" height="60" src="images/binfinite-logo.png" data-sticky-width="102" data-sticky-height="33" style="width:100%;">
						</a>
					</div>


			</div>

			<div class="col-md-8">
				<div class="navbar-collapse nav-main-collapse collapse">

						<nav class="nav-main mega-menu">
							<ul class="nav nav-pills nav-main" id="mainMenu">



								<li>
									<a href="index.php">Remind Me</a>
								</li>
								<!--<li>
									<a href="aig-car-insurance.php">Car Insurance</a>
								</li>-->

								<li>
									<a href="renew-now.php">Renew Now</a>
								</li>

								<!--<li>
									<a href="pdf/aig-tnc.pdf" target="_blank">Campaign T&C </a>
								</li>-->

							</ul>
						</nav>
				</div>

			</div>

			<div class="col-md-2 col-sm-6 col-xs-6">
					<div class="logo" style="right;">
						<a href="#">
							<img width="190" height="60" src="images/aig-logo.png" data-sticky-width="102" data-sticky-height="33">
						</a>
					</div>

			</div>
				</div>

					<!-- <button class="btn btn-responsive-nav btn-inverse" data-toggle="collapse" data-target=".nav-main-collapse">
						<i class="fa fa-bars"></i>
					</button> -->
					<br />
			<div class="row">
				<div class="col-sm-12 col-xs-12">
					<select class="mobile-menu" onchange="if (this.value) window.location.href=this.value" style="width:100%; margin-top:20px;">
						<option value="0">-- Menu --</option>
						<option value="index.php">Remind Me</option>
						<!--<option value="aig-car-insurance.php">AIG Car Insurance</option>-->
						<option value="renew-now.php">Renew Now</option>
						<!--<option value="pdf/aig-tnc.pdf">Disclaimer / Terms & Conditions</option>-->
					</select>
				</div>
			</div>
		</div>

		<style>
		@media (max-width:480px){
		.logo img {
			width:100%;
		}
		}

	@media (max-width:768px){
		body.sticky-menu-active #header {
		border-bottom: 1px solid #E9E9E9;
		position: fixed;
		top: -20px;
		width: 100%;
		z-index: 1;
		box-shadow: 0 0 3px rgba(0, 0, 0, 0.1);
		filter: none;
		min-height: 50px;}
	}

		@media only screen
and (min-device-width : 768px)
and (max-device-width : 1024px)
and (orientation : landscape) {

	#header nav ul.nav-main li a {
		font-size:12px;
	}
}

	@media (min-width:992px){
		.mobile-menu {display:none;}
	}


		</style>

	</header>
