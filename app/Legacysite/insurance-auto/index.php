<?php include ('top.php');?>

<?php
 require_once "netallianz-config.php";

if (!empty($_GET['action'])&& $_GET['action']=='submit'){

  if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])){
      //get verify response data
      $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$CONF['secret'].'&response='.$_POST['g-recaptcha-response']);
      $responseData = json_decode($verifyResponse);

      if($responseData->success){

          //$member_id = mysql_result(mysql_query("SELECT MAX(member_id) FROM member"),0)+1;
          $name = strtoupper($_POST['name']);
          $bcard = $_POST['bcard'];
          $ic = $_POST['ic'];
          $exp_date = $_POST['exp_date'];
          $car_reg_no = $string = preg_replace('/\s+/','',$_POST['car_reg_no']);
          $mobile = $_POST['mobile'];
          $email = $_POST['email'];
          $date_created = date('Y-m-d H:i:s');

          //Check Exist Registration

          $q1 = DB::select('SELECT * FROM aig_auto_members WHERE car_reg_no = :car_reg_no', ['car_reg_no' => $car_reg_no]);
          $r1 = sizeOf($q1);

          if($r1){
              echo "<script>";
              echo "window.location='exist-register.php'";
              echo "</script>";
          }else{
            DB::insert("INSERT INTO aig_auto_members SET
                         name = :name,
                         bcard = :bcard,
                         ic = :ic,
                         exp_date = :exp_date,
                         car_reg_no = :car_reg_no,
                         mobile = :mobile,
                         email = :email,
                         date_created = :date_created", [
                          'name' => $name,
                          'bcard' => $bcard,
                          'ic' => $ic,
                          'exp_date' => $exp_date,
                          'car_reg_no' => $car_reg_no,
                          'mobile' => $mobile,
                          'email' => $email,
                          'date_created' => $date_created
                         ]);

            echo "<script>";
            echo "window.location='register-success.php'";
            echo "</script>";
          }

      }else{
            echo "<script>";
            echo "window.location='robot-verification.php'";
            echo "</script>";
      }
  }else{
            echo "<script>";
            echo "window.location='recaptcha.php'";
            echo "</script>";
  }

}



?>

<!DOCTYPE html>
<html>
  <head>

 <script>
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
     alert('Only numbers are allowed.');
        return false;
    }
    return true;
}
  </script>

<script>
  function isNumberDot(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 46 || charCode > 57)) {
     alert('Only numbers are allowed.');
        return false;
    }
    return true;
}
</script>

  <script>
  function lettersOnly(evt) {
       evt = (evt) ? evt : event;
       var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
          ((evt.which) ? evt.which : 0));
       if (charCode > 32 && (charCode < 65 || charCode > 90) &&
          (charCode < 97 || charCode > 122)) {
          alert("Only letters are allowed.");
          return false;
       }
       return true;
     }
  </script>

  <script src="form-validation.js"></script>

</head>

<style>
.box-content {
   text-align:center;
    padding: 30px 30px 10px 30px;
}

.form-control:focus {
  border: 2px solid #b10320 !important;
}

/* Landscape */
@media only screen
  and (min-device-width: 768px)
  and (max-device-width: 1024px)
  and (orientation: landscape)
  and (-webkit-min-device-pixel-ratio: 1) {

  .bx-wrapper img {

  width:100%;
  display: block;
  height: 540px !important;
  }

}
</style>

  <body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NJXXSGL"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5P99F9L"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div class="body">

      <div role="main" class="main">
        <div class="slider-container">

          <div class="row">
              <div class="col-lg-7 col-md-6 col-sm-12">
                  <img src="images/form-image.jpg" style="width:100%" />
              </div>

              <div class="col-lg-5 col-md-6 col-sm-12"><br />
                <!--<div class="col-md-12"><h4 class="short" style="color:#1e1e1e; text-transform:uppercase;">Get 300 BPoints by sharing your car insurance expiry month with us</p></div>-->

                <div class="col-md-12"><h4 class="short" style="color:#1e1e1e; text-transform:uppercase;"><b>Car Insurance not expiring yet?</b></h4><p style="color:#1e1e1e; text-transform:uppercase; font-size:10px;">Leave us your details to receive renewal reminder before your next expiry date.</p></div>

                <form id="contestForm" action="index.php?action=submit" method="POST" onsubmit="return validate();">
                  <input type="hidden" name="_token" value="<?= csrf_token() ?>">
                  <div class="form-group">
                    <div class="col-md-6">
                      <label>Name </label>
                      <input type="text" value="" maxlength="100" class="form-control" name="name" id="name" onkeypress="return lettersOnly(event)">
                    </div>
                    <div class="col-md-6">
                      <label>BCard No</label>
                      <input type="text" value="" maxlength="16" class="form-control" name="bcard" id="bcard" onkeypress="return isNumber(event)" placeholder="Example : 6298435988888888">
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-md-12">
                      <label>IC No</label>
                      <input type="text" value="" maxlength="12" class="form-control" name="ic" id="ic" onkeypress="return isNumber(event)"placeholder="Example : 921212101234">
                    </div>
                  </div>


                  <div class="form-group">
                    <div class="col-md-6">
                      <label>Car Insurance Expiry Month</label>
                      <input type="text" id="NoIconDemo" value="" class="form-control" name="exp_date" id="exp_date" placeholder="MM/YYYY">
                    </div>

                    <div class="col-md-6">
                      <label>Car Registration Number</label>
                      <input type="text" value="" class="form-control" name="car_reg_no" id="car_reg_no">
                    </div>

                  </div>

                  <div class="form-group">
                    <div class="col-md-6">
                      <label>Mobile No</label>
                    <div class="input-group">
                            <span class="input-group-addon">
                              +60
                            </span>
                      <input type="text" value="" maxlength="10" class="form-control" name="mobile" id="mobile" onkeypress="return isNumber(event)" placeholder="123456789">
                    </div>
                  </div>


                    <div class="col-md-6">
                      <label>Email </label>
                      <input type="text" value="" maxlength="100" class="form-control" name="email" id="email" placeholder="example@gmail.com">
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-md-12">
                        <div class="checkbox">
                          <label style="font-size:12px;">
                            <input type="checkbox" value="" id="tnc">
                            I agree to the BInfinite's <a href="/privacy.php" target="_blank">Privacy Policy</a> and to be contacted by their Partner AIG Malaysia Insurance Berhad for marketing purposes.

                          </label>
                        </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <script src='https://www.google.com/recaptcha/api.js'></script>
                    <div class="g-recaptcha" data-sitekey="<?php echo $CONF['public'] ?>" style="transform:scale(0.85);-webkit-transform:scale(0.85);transform-origin:0 0;-webkit-transform-origin:0 0;"></div>
                  </div>

                  <div class="col-md-6">
                    <input type="submit" value="Submit" id="submit" class="btn btn-primary btn-lg" style="background-color: #424242; margin-left:20px;">
                  </div>
                <br /><br />
              </form>
              </div>
            </div>
          </div>
        <br /><br />




        <div class="container">

          <div class="row center">
            <div class="col-md-12">
              <h2 class="short">Earn 100 BPoints!
              </h2>
              <p style="font-size:18px;">
               Share your car insurance expiry month and get 100 BPoints plus a free renewal reminder before your next expiry date.
				<br /><br />
			   100 Bpoints reward limited to 1st 30 members daily. Hurry!<br><a href="/pdf/terms-aig-car-insurance-expiry.pdf" target="_blank">Terms & Conditions Apply</a>
              </p>
            </div>
          </div> 

          <hr class="tall">
        </div>

      <div class="container">
        <h3 class="short" style="color:#1e1e1e;"><b>Key Service Offerings</b></h3>
        <!--<div class="row featured-boxes">
            <div class="col-md-3 col-sm-3">

                <div class="box-content">
                  <i class="icon-featured fa fa-gift"></i>
                  <h5>Instant Renewal Online</h5>
                  <p>With 10% rebate on top of your NCD</p>
                </div>

            </div>
            <div class="col-md-3 col-sm-3">

                <div class="box-content">
                  <i class="icon-featured fa fa-send-o"></i>
                  <h5>Free Road Tax Delivery</h5>
                  <p>For vehicles used in West Malaysia</p>
                </div>

            </div>
            <div class="col-md-3 col-sm-3">

                <div class="box-content">
                  <i class="icon-featured fa fa-wrench"></i>
                  <h5>24 hours Road Assistance</h5>
                  <p>Call 1300 88 3933 for towing, change of tyre & battery and more.</p>
                </div>

            </div>

            <div class="col-md-3 col-sm-3">

                <div class="box-content">
                  <i class="icon-featured fa fa-wrench"></i>
                  <h5>Award Winning Claims Service*</h5>
                  <p>Fastest claims turnaround time.</p>
                  <a href="claims.php" target="_blank">How to claim</a>
                </div>

            </div>

          </div>-->
          <div class="row featured-boxes">
              <div class="col-md-3 col-sm-6 col-xs-12" style="padding-top:25px;">

                  <div class="box-content">
                    <img src="../aig-assets/images/award-winning.png" style="margin-bottom:15px;">
                    <h5 style="text-transform:Initial !important;">Award Winning<br>Claims Services*</h5>
                    <p>Fastest claims turnaround time</p>
                  </div>

              </div>
              <div class="col-md-3 col-sm-6 col-xs-12" style="padding-top:25px;">

                  <div class="box-content">
                    <img src="../aig-assets/images/warranty.png" style="margin-bottom:15px;">
                    <h5 style="text-transform:Initial !important;">1 Year Warranty<br></h5>
                    <p>on repairs done at our panel workshop</p>
                  </div>

              </div>
              <div class="col-md-3 col-sm-6 col-xs-12" style="padding-top:25px;">

                  <div class="box-content">
                    <img src="../aig-assets/images/road-assistance.png" style="margin-bottom:15px;">
                    <h5 style="text-transform:Initial !important;">Free Road<br>Assistance Service</h5>
                    <p>24 Hours, 7 Days<br>Just Call : 1800 88 6990</p>
                  </div>

              </div>

              <div class="col-md-3 col-sm-6 col-xs-12" style="padding-top:25px;">

                  <div class="box-content">
                    <img src="../aig-assets/images/renewal-online.png" style="margin-bottom:15px;">

                    <h5 style="text-transform:Initial !important;">Instant Renewal<br>Transfer Online</h5>
                    <p>Instant update of JPJ status & road tax renewal service included. Renew online <a href="/aig/renew-now.php" class="linkcol"><u>here ></u></a> </p>

                  </div>

              </div>

            </div>
          <br />

          <p><a href="/insurance-auto.php" target="_blank"><button class="btn btn-primary btn-icon">Find Out More</button></a></p>

          <p style="font-size:14px; font-style:italic;">Note : Road Tax Renewal with Delivery is currently available only for vehicles used in West Malaysia.<span style="color:red;">*</span>
          <br>
          AIG Malaysia is awarded "Best Automotive Claims Management Service" by Malaysia's <a href="http://www.mrc.com.my/" target="_blank" rel="nofollow">Motordata Research Consortium (MRC)</a></p>

        </div>
      </div>

     <div class="container">
          <hr class="tall">
          <span style="font-weight:bold; font-size:18px;">* This campaign is open to B Infinite members only.</span>

          <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
              <p style="font-size:18px;">
                <div class="col-md-12">
                  <img src="images/hand.png" style="width:100%; padding-bottom:10px;">
                  <div style="text-align:center; padding-bottom:10px;">Download Now</div>
                  <img src="images/b-infinite-icon.png" style="width:28px">
                  <a href="https://play.google.com/store/apps/details?id=com.bloyalty.app&hl=en" target="_blank" onclick="_gaq.push(['_trackEvent', 'AIG', 'Download', 'Android']);"><img src="images/google-app-store.png" style="width:90px"></a>
                  <a href="https://itunes.apple.com/my/app/b-infinite/id1100101801?mt=8" target="_blank"  onclick="_gaq.push(['_trackEvent', 'AIG', 'Download', 'App Store']);"><img src="images/apple-store.png" style="width:90px"></a>
                </div>
              </p>
            </div>
            <div class="col-md-7 col-sm-6 col-xs-12">
              <h2 class="short" style="color:#ea018c; padding-top:50px;">Not a B Infinite Member ?
              </h2>
              <p style="font-size:18px;">
                Membership is FREE, <a href="/get-started.php" onclick="_gaq.push(['_trackEvent', 'AIG', 'Membership', 'Learn More']);">learn more</a>.
              </p>
            </div>
      <div class="col-md-1"><a href="https://m.me/BInfiniteRewards" target="blank" class="btn btn-lg btn-primary" style="margin-top:55px; background:#ea018c; border: none;" onclick="_gaq.push(['_trackEvent', 'AIG', 'Membership', 'Join Now']);">Join Now</a></div>

    </div>

        </div>

      <!-- FOOTER -->
      <?php include ('bottom.php');?>
    </div>

  </body>
</html>
