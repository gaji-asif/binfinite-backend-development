<!DOCTYPE html>
<?php include ('top.php');?>

<html>
	<head>

	</head>

	<style>

	ol > li {
		line-height:30px;
	}


	ol > li > ul >li {
		line-height:30px;
	}
	</style>

	<body>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NJXXSGL"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		<div class="body">

			<div role="main" class="main">

			<div class="container" style="padding-top: 60px;">

					<div class="row">
						<div class="col-md-12"><h2 style="color:#083f88;">How to Make A Claim</h2>
						</div>
					</div>
					<br />
					<div class="row">
						<div class="col-md-3">
							<!-- Side Bar -->
							<?php include ('side-bar.php');?>
						</div>
						<div class="col-md-9">

							<h2>Own Damage Claim - Windsreen</h2>

							<div class="row">
								<div class="col-md-12">

                                    For windscreen claims, please provide the following: <br /><br />
                                    <ul>
									<li>Duly completed <a href="pdf/windscreen-claim-form.pdf" target="_blank">claim form</a></li>
								    <li>Photographs of damaged windscreen before repair (Vehicle registration number must be visible);</li>
                                    <li>Photographs of the replaced windscreen (Vehicle registration number must be visible);</li>
                                    <li>Updated copy of insured vehicle's registration card; and</li>
                                    <li>Original repair receipt. </li>
                                    <li>Photographs of Engine & Chassis Number</li>
                                    <li>Photographs of Speedo Meter Reading </li>
                                    </ul>
                               <br />
                              * All photographs will need to be time & date stamped
                              <br /><br /><br />


                              <div class="row center">
                               <span style="font-size:18px;"><b>For further inquiries, please call us at <br />
                               <a href="tel:1 800 88 8811">1 800 88 8811</a> or email us at <a href="mailto:AIGMYCare@aig.com">AIGMYCare@aig.com</a></b></span>
                               <br /><br /><br />

                               <img src="images/aig-logo.png" style="width:80px;"><span style="font-size:12px;">underwritten by AIG Malaysia Insurance Berhad (795492-W)</span>
                             </div>

								</div>
							</div>


						</div>

					</div>

				</div>

			</div>
			<!-- FOOTER -->
			<?php include ('bottom.php');?>
		</div>



	</body>
</html>
