<!DOCTYPE html>
<?php include ('top.php');?>

<html>
	<head>

	</head>

	<style>

	ol > li {
		line-height:30px;
	}


	ol > li > ul >li {
		line-height:30px;
	}
	</style>

	<body>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NJXXSGL"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		<div class="body">

			<div role="main" class="main">

			<div class="container" style="padding-top: 60px;">

					<div class="row">
						<div class="col-md-12"><h2 style="color:#083f88;">How to Make A Claim</h2>
						</div>
					</div>
					<br />
					<div class="row">
						<div class="col-md-3">
							<!-- Side Bar -->
							<?php include ('side-bar.php');?>
						</div>
						<div class="col-md-9">

							<h2>Third Party Bodily Injury Claim</h2>

							<div class="row">
								<div class="col-md-12">
                                    <b>In the event you are involved in a road accident causing personal injury or death to any individuals including passengers in your insured vehicle at the time of accident, please ensure you follow the following guidelines:</b><br /><br />
                                    <ul>
									<li>Accident must be reported to the police within 24 hours after the road accident;</li>
								    <li>Notify us of the accident immediately;</li>
                                    <li>Do not admit liability to anyone;</li>
                                    <li>Do not enter into any negotiations without our prior consent;</li>
                                    <li>Should you receive any letter and /or summons from the third party's solicitors, please inform us immediately;</li>
                                    <li>You may speak to your insurance broker or our authorised agent to seek clarification.</li>
                                    </ul>
                                    <br /><br /><br />

                               <div class="row center">
                               <span style="font-size:18px;"><b>For further inquiries, please call us at <br />
                               <a href="tel:1 800 88 8811">1 800 88 8811</a> or email us at <a href="mailto:AIGMYCare@aig.com">AIGMYCare@aig.com</a></b></span>
                               <br /><br /><br />

                               <img src="images/aig-logo.png" style="width:80px;"><span style="font-size:12px;">underwritten by AIG Malaysia Insurance Berhad (795492-W)</span>
                             </div>


								</div>
							</div>


						</div>

					</div>

				</div>

			</div>
			<!-- FOOTER -->
			<?php include ('bottom.php');?>
		</div>



	</body>
</html>
