<!doctype html>


<style>


@media(max-width:767px) {
   .desktop {
     display: none;

   }
}

@media (min-width: 767px){
   .mobile {
     display: none;
   }
   }


 @media(max-width:767px) {
   .frame {
     height: 900px;
   }
 }

 @media (min-width: 767px) {
   .frame {
     height: 550px;
   }
 }

 .navigationbar{
   font-size: 20px;
   color:black;
   padding-left: 40px;
   padding-right:40px;
   font-weight: bold;

 }

 .linkcol {
    color: #083f88 !important;
   }

 h5 {
   font-size: 1em !important;
   font-weight: 600 !important;
   letter-spacing: normal !important;
   line-height: 18px !important;
   margin: 0 0 14px 0 !important;
   text-transform: uppercase !important;
     text-align: center;
     color: #083f88 !important;

 }

 .box-content{
   text-align: center;
 }

 .insuretitle{
   text-align: center;
   color:#0073ae !important;
 }

 div .center{
   text-align: center;
 }


.navgrp{
  z-index: 10!important;
}

 .navbtn{
   text-align:left !important;
   margin-bottom:3px !important;
   font-size:15px !important;
 }

 .navbtntablet{
   padding: 0px 50px 0px 50px;
   margin-bottom:3px !important;
   font-size:20px !important;
 }

 .navbtntablet:hover {
   border-bottom: 2px solid #23a2dc;
 }


 .ui-accordion .ui-accordion-header {
     display: block;
     cursor: pointer;
     position: relative;
     margin: 2px 0 0 0;
     padding: .5em .5em .5em .7em;
     font-size: 100%;
 }

 .ui-state-active, .ui{
   color:#0073ae !important;
    font-weight: normal;
    color: #ffffff;
}

.ui-accordion .ui-accordion-content {
    padding: 1em 2.2em;
    border-top: 0;
}

td{
  padding-left:5px !important;
  }
th{
  padding-left:5px !important;
  }

body {
  background-color: white !important;
}


</style>

<html lang="en-US" class="no-js">

<?php include '../gtm-insurance.php';?>


  <link rel="stylesheet" href="/aig-assets/new-font.css" type="text/css" media="all">
  <link rel="stylesheet" href="/aig-assets/bootstrap.css" type="text/css" media="all">
  <link rel="stylesheet" href="/aig-assets/template.css" type="text/css" media="all">
  <script src="/aig-assets/jquery-1.11.1.min.js" async></script>

	<div id="page_wrapper">
		<?php // include 'top.php';?>
		<div id="page_header" class="page-subheader site-subheader-cst uh_zn_def_header_style maskcontainer--mask3">
			<div class="bgback">
			</div>
			<div class="kl-bg-source">
			<div class="kl-bg-source__overlay"></div>
			</div>
			<div class="th-sparkles">
			</div>
			<!-- DEFAULT HEADER STYLE -->
			<div class="ph-content-wrap ptop-0">
				<div class="ph-content-v-center ptop-25">
					<div class="container">
							<div class="row">
								<div class="col-sm-12">
									<div class="subheader-titles">
										<h4 class="subheader-subtitle">Car Insurance</h4>
                    <img src="/aig-assets/insurance/aig-logo.png" style="margin-bottom:5px; height:50px;" align="right">
									</div>
								</div>

								<div class="col-sm-6">

									<div class="clearfix"></div>
								</div>

							</div>
							<!-- end row -->
						</div>
				</div>
			</div>

		</div>

        	<section style="padding-top:0px !important; padding-bottom:0px !important;">


            <a href="/insurance-auto/renew-now.php" target="_blank" rel="nofollow"><img class="desktop" style="width:100%;" src="aig-assets/images/car-insurance-1.jpg" alt=""></a>
            <a href="/insurance-auto/renew-now.php" target="_blank" rel="nofollow"><img class="mobile" style="width:100%;" src="aig-assets/images/car-insurance-mobile-1.jpg" alt=""></a>


        <div class="container hidden-xs" style="text-align:center;">
          <div class="tabletmenu" id="" style="z-index:100; padding-top:20px;">
            <a href="#overview" class="navbtntablet linkcol" title="Overview">Overview</a>

            <a href="#benefits" class="navbtntablet linkcol" title="Benefits">Benefits</a>

            <a href="aig-assets/pdf/car-insurance-faq.pdf" target="_blank" class="navbtntablet linkcol" title="FAQ">FAQ</a>

            <a href="#services" class="navbtntablet linkcol" title="Online Renewal Services">Online Renewal Services</a>
            <a href="http://aig.binfinite.com.my" class="navbtntablet linkcol" title="Submission" target="_blank">Submission</a>

          </div>
        </div>

            <div role="main" class="main">

              <div class="container" style="padding-top: 60px;" id="overview">

                <div class="row center">
                  <div class="col-md-12">
                    <h1 class="insuretitle">Overview</h1>
                    <hr>
                    Life has plenty of journeys. Keep those journeys going with <a href="https://www.aig.my/news/aig-insurer-of-the-year" class="linkcol" target="_blank" rel="nofollow">"Insurer of the Year"</a> AIG Malaysia Insurance Berhad.
                    <br><br>
                    We provide comprehensive protection for you and your car, covering you against damage to your own vehicle due to accident, fire and theft. We also cover your liabilities to other parties such as damage, injury or death to the other party and their vehicle.
					<br><br>
					5 things you might not know about car insurance. Click <a href="aig-assets/images/5-things-about-car-insurance.jpg" target="_blank" style="text-decoration: none;border-bottom: 1px solid red;">here</a> to find out more!
				  </div>
                </div>
              </div>

            <div class="container" style="padding-top:30px;">
              <h2 class="insuretitle">Our most popular features include</h1>

              <div class="row featured-boxes">
                  <div class="col-md-3 col-sm-6 col-xs-12" style="padding-top:25px;">

                      <div class="box-content">
                        <img src="aig-assets/images/award-winning.png" style="margin-bottom:15px;">
                        <h5 style="text-transform:Initial !important;">Award Winning<br>Claims Services*</h5>
                        <p>Fastest claims turnaround time</p>
                      </div>

                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12" style="padding-top:25px;">

                      <div class="box-content">
                        <img src="aig-assets/images/warranty.png" style="margin-bottom:15px;">
                        <h5 style="text-transform:Initial !important;">1 Year Warranty<br></h5>
                        <p>on repairs done at our panel workshop</p>
                      </div>

                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12" style="padding-top:25px;">

                      <div class="box-content">
                        <img src="aig-assets/images/road-assistance.png" style="margin-bottom:15px;">
                        <h5 style="text-transform:Initial !important;">Free Road<br>Assistance Service</h5>
                        <p>24 Hours, 7 Days<br>Just Call : 1800 88 8811</p>
                      </div>

                  </div>

                  <div class="col-md-3 col-sm-6 col-xs-12" style="padding-top:25px;">

                      <div class="box-content">
                        <img src="aig-assets/images/renewal-online.png" style="margin-bottom:15px;">

                        <h5 style="text-transform:Initial !important;">Instant Renewal<br>Transfer Online</h5>
                        <p>Instant update of JPJ status & road tax renewal service included. Renew online <a href="/insurance-auto/renew-now.php" class="linkcol"><u>here ></u></a> </p>

                      </div>

                  </div>

                </div>
                <br />
                <p style="font-size:14px; font-style:italic;"><span style="color:red;">*</span> Note : Road Tax Renewal with Delivery is currently available only for vehicles used in West Malaysia.
                <br>
                <span style="color:red;">*</span> AIG Malaysia is awarded "Best Automotive Claims Management Service" by Malaysia's <a href="http://www.mrc.com.my/" class="linkcol" target="_blank" rel="nofollow">Motordata Research Consortium (MRC)</a></p>

              </div>

              <div id="video" class="container" style="padding-top:30px;">
                <div class="row center">
                  <div class="col-md-12">
                    <h1 class="insuretitle">#JagaTalks on Motor Insurance</h1>
                    <hr>
                            <p>Have you had difficulty understanding motor insurance? We are proud to introduce these educational series that serve to simplify what motor insurance is all about. Learn about motor insurance by watching these videos NOW! #AIGJagaYou</p>
                  </div>
                </div>
                <div id="benefits" class="container" style="padding-top:30px;">
                    <div class="row featured-boxes">
                        <div class="col-lg-4 col-md-4 col-sm-6">
                          <div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/308241950?title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>
                            <p style="text-align:center;"><b>Motor Insurance 101</b> (Part 1 of 6)</p>
                          <br><br>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6">
                          <div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/308241909?title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>
                            <p style="text-align:center;">What are <b>Third Party Claims</b> for motor insurance? (Part 2 of 6)</p>

                          <br>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6">
                          <div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/308241868?title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>
                            <p style="text-align:center;">Why <b>Betterment Cover</b> is important? (Part 3 or 6)</p>

                          <br><br>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6">
                          <div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/308241929?title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>
                            <p style="text-align:center;">Is <b>Windscreen Coverage</b> included in your motor insurance? (Part 4 of 6)</p>

                          <br>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6">
                          <div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/308241895?title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>
                            <p style="text-align:center;">Ensure your car is sufficiently covered and not <b>Under Insured</b> (Part 5 of 6)</p>

                          <br>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6">
                          <div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/308241877?title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>
                            <p style="text-align:center;">What is <b>No Claims Discount (NCD)</b>?  How to renew online in 4 easy steps ?(Part 6 of 6)</p>

                          <br><br>
                        </div>
                      </div>
                    </div>
                          </div>

              <div id="benefits" class="container" style="padding-top:30px;">
                <div class="row center">
                  <div class="col-md-12">
                    <h1 class="insuretitle">Benefits</h1>
                    <hr>

                  </div>
                </div>
                <div class="row featured-boxes">
                    <div class="col-md-6 col-sm-6" style="text-align:left;">
                        <div>
                          <p><strong>Vehicle damage due to accident</strong></p>
                          <p>If your vehicle is damaged after an accident, just bring it in for repairs to our panel workshop and we will take care of the bills. Our dedicated auto claim specialists will liaise with the workshop on accident-related repair costs.</p>
                        </div>
                        <div>
                          <br>
                          <p><strong>Coverage for vehicle fire & theft</strong></p>
                          <p>We'll reimburse you the market value/agreed value* of your car, in the event of theft or total loss caused by fire. Just make sure you lodge a police report immediately.</p>
                          <p style="font-size:14px;">*If your vehicle is covered with an agreed value policy. </p>
                        </div>
                        <div>
                          <p><strong>Privileges at our panel repair workshop</strong></p>
                          <p>For your vehicle repairs at our panel workshop, you'll be entitled to enjoy 1 year warranty on all repairs done.</p>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6" style="text-align:left;">
                      <div>
                        <p><strong>Customisable add-on coverage</strong></p>
                        <p>We offer a selection of add-on coverage, which is comprehensive for you and your vehicle while on the road. For an extra premium, you can choose to extend selected coverage such as windscreen, passenger protection plan, special perils (in case of flood or natural events) or accident guard.</p>              </div>
                      <div>
                        <p><strong>Full payout on total car damage</strong></p>
                        <p>If your vehicle is covered with an agreed value<sup>1</sup> policy with us, you'll get the full pay-out on total car damage. It's always recommended to get covered with us on agreed value<sup>1</sup> policy.</p>
                        <p style="font-size:14px;"><sup>1</sup> Initial agreed insured value instead of the depreciating market value of your car</p>
                      </div>
                      <div>
                        <p><strong>Road Assistance Service</strong></p>
                        <p>With every Comprehensive Car Insurance coverage, you'll be entitled for free road assistance, offering services such as change of punctured tyre, battery, towing service, accident assistance, out of petrol and more.</p>
                      </div>
                      <br>
                    </div>
                  </div>
                </div>




                <div id="services" class="container">

                  <div class="row center">
                    <div class="col-md-12">
                      <h1 class="insuretitle">Online Renewal Services
                      </h1>

                    </div>

                  </div>
                <div class="row">
                  <div class="col-md-7 col-md-offset-1">
                      <h4>Car Insurance not expiring yet ?</h4>
                      <p>
                        Leave us your details to receive renewal reminder before your next expiry date.
                      </p>
                    </div>
                    <div class="col-md-3" style="margin-top:20px;">
                      <a href="/insurance-auto" target="_blank" class="btn btn-primary push-top" style="font-size:17px;">Click Here</a>
                  </div>
                </div>
                <br />
                <div class="row">
                  <div class="col-md-7 col-md-offset-1">
                      <h4>Car Insurance expiring soon ?</h4>
                      <p>
                        Renew your Car Insurance and Road Tax together with Delivery to your door step !
                      </p>
                    </div>
                    <div class="col-md-3" style="margin-top:20px;">
                      <a href="/insurance-auto/renew-now.php" target="_blank" class="btn btn-primary push-top" style="font-size:17px;">Click Here To Renew</a>
                  </div>
                </div>

                <hr class="tall">

                </div>

                <div class="container">

                  <div class="row center">
                    <div class="col-md-12">
                      <h2>Online Quick Quote & Renewal in 4 easy steps

                      </h2>

                    </div>

                  </div>
                  <div class="row center">

                    <div class="col-md-12">
                    <p>Click the button below to discover how to quick quote & renew online with us in just 4 easy steps.</p>

                  </div>


                </div>

                  <div class="row center">
                    <div class="col-md-12">
                      <a href="/insurance-auto/how-to-renew.php" target="_blank" class="btn btn-primary btn-lg btn-icon">4 EASY STEPS</a>

                      <p><br />If you face difficulty renewing online, just email us at <a href="mailto:AIGMYCare@aig.com">AIGMYCare@aig.com</a> or call us at <a href="tel:1800 88 8811"> 1800 88 8811</a> for assistance.</p>
                    </div>
                  </div>

                </div>
                <hr class="tall">
                <div class="container">

                  <div class="row center">
                    <div class="col-md-12">
                      <h2>More about Car Insurance</h2>

                    </div>

                  </div>
                <div class="row center">
                  <div class="col-md-3 col-md-offset-3">
                      <h4 style="font-size:18px;">Product Disclosure Sheet</h4>

                  </div>

                  <div class="col-md-3">
                      <a href="insurance-auto/aig-assets/pdf/car-product-disclosure-sheet.pdf" target="_blank" class="btn btn-primary btn-icon" style="font-size: 18px;">DOWNLOAD</a>

                  </div>
                </div>
                <br />
                <div class="row center">
                  <div class="col-md-3 col-md-offset-3">
                      <h4 style="font-size:18px;">Policy Wording</h4>

                  </div>

                  <div class="col-md-3">
                      <a href="insurance-auto/aig-assets/pdf/car-policy-wording.pdf" target="_blank" class="btn btn-primary btn-icon" style="font-size: 18px;">DOWNLOAD</a>

                  </div>
                </div>

                </div>
                <hr class="tall">

                <div class="container" style="padding-bottom:50px;">

                  <div class="row center">
                    <div class="col-md-12">
                      <h2>More about Road Tax Renewal Service</h2>

                    </div>

                  </div>
                <div class="row center">
                  <div class="col-md-12">
                      <p style="font-size:18px;">We provide Road Tax Renewal Service with Delivery when you renew your Car Insurance online with AIG via B Infinite. </p>
                  </div>
                </div>

                <div style="text-align:center; padding-bottom:25px;">
                  <a class="zn_sub_button btn btn-fullcolor th-button-register" style="font-size:18px;" href="/insurance-auto/renew-now.php" target="_blank">Buy Now</a>
                </div>

                <div class="row center">
                  <div class="col-md-12">
                    <br>
                    <a href="/insurance-auto/road-tax-renewal.php" style="font-size:18px;" target="_blank" class="btn btn-primary btn-icon">MORE INFO >></a>
                    <br>
                    <br>
                    <img src="/insurance-auto/images/aig-logo.png" style="width:80px;"><span style="font-size:12px;">AIG Car Insurance is underwritten by AIG Malaysia Insurance Berhad (795492-W).</span>
                  </div>
                </div>
                </div>
                </div>






                <div class="container">
                  <a href="insurance.php"><p>Go Back</p></a>
                </div>

                <hr class="tall">
            </div>
            </div>

        		</section>

        		<?php // include 'bottom.php';?>

        	</div><!-- end page-wrapper -->


                            <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
                            <script>
                            $( function() {
                              // Accordion
                              $("#accordion").accordion({
                                  header: "h3",
                                  collapsible: true,
                                  heightStyle: false,
                                  navigation: true,
                                  active: false
                              });
                            } );
                            </script>
                            <script src="https://player.vimeo.com/api/player.js"></script>
        </body>
        </html>
