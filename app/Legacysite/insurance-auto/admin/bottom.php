

			<footer id="footer">
				
				<div class="footer-copyright">
					<div class="container">
						<div class="row">
							
							<div class="col-md-6">
								<p>AIG Malaysia © Copyright 2017. All Rights Reserved.</p>
							</div>
							<div class="col-md-6">
								<p style="float:right; color:#7f7e7e;">Netallianz Technology</p>
							</div>
						</div>
					</div>
				</div>
			</footer>

	<!-- Vendor -->
		<script src="/insurance-auto/vendor/jquery/jquery.js"></script>
		<script src="/insurance-auto/vendor/jquery.appear/jquery.appear.js"></script>
		<script src="/insurance-auto/vendor/jquery.easing/jquery.easing.js"></script>
		<script src="/insurance-auto/vendor/jquery-cookie/jquery-cookie.js"></script>
		<script src="/insurance-auto/vendor/bootstrap/bootstrap.js"></script>
		<script src="/insurance-auto/vendor/common/common.js"></script>
		<script src="/insurance-auto/vendor/jquery.validation/jquery.validation.js"></script>
		<script src="/insurance-auto/vendor/jquery.stellar/jquery.stellar.js"></script>
		<script src="/insurance-auto/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.js"></script>
		<script src="/insurance-auto/vendor/jquery.gmap/jquery.gmap.js"></script>
		<script src="/insurance-auto/vendor/isotope/jquery.isotope.js"></script>
		<script src="/insurance-auto/vendor/owlcarousel/owl.carousel.js"></script>
		<script src="/insurance-auto/vendor/jflickrfeed/jflickrfeed.js"></script>
		<script src="/insurance-auto/vendor/magnific-popup/jquery.magnific-popup.js"></script>
		<script src="/insurance-auto/vendor/vide/vide.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="js/theme.js"></script>
		
		<!-- Specific Page Vendor and Views -->
		<script src="/insurance-auto/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script src="/insurance-auto/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
		<script src="/insurance-auto/vendor/circle-flip-slideshow/js/jquery.flipshow.js"></script>
		<script src="js/views/view.home.js"></script>
		
		<!-- Theme Custom -->
		<script src="js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="js/theme.init.js"></script>
		
		<!-- BX Slider -->
		<script type="text/javascript" src="/insurance-auto/bxSlider/jquery.bxslider.js"></script>
		<script type="text/javascript" src="/insurance-auto/bxSlider/jquery.bxslider.min.js"></script>
		<link rel="stylesheet" href="/insurance-auto/bxSlider/jquery.bxslider.css" type="text/css" media="screen" />

		<script>
		$(document).ready(function(){
		  $('.bxslider').bxSlider({
			   pager: false,
			   auto: true,
			   speed: 3000,
			   pause: 7000,
				adaptiveHeight: true
		  });
		});

		</script>
