
		<!-- Basic -->
		<meta charset="utf-8">
		<title>AIG Malaysia</title>		
		<meta name="keywords" content="HTML5 Template" />
		<meta name="description" content="Porto - Responsive HTML5 Template">
		<meta name="author" content="okler.net">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="/insurance-auto/vendor/bootstrap/bootstrap.css">
		<link rel="stylesheet" href="/insurance-auto/vendor/fontawesome/css/font-awesome.css">
		<link rel="stylesheet" href="/insurance-auto/vendor/owlcarousel/owl.carousel.css" media="screen">
		<link rel="stylesheet" href="/insurance-auto/vendor/owlcarousel/owl.theme.css" media="screen">
		<link rel="stylesheet" href="/insurance-auto/vendor/magnific-popup/magnific-popup.css" media="screen">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="css/theme.css">
		<link rel="stylesheet" href="css/theme-elements.css">
		<link rel="stylesheet" href="css/theme-blog.css">
		<link rel="stylesheet" href="css/theme-shop.css">
		<link rel="stylesheet" href="css/theme-animate.css">

		<!-- Current Page CSS -->
		<link rel="stylesheet" href="/insurance-auto/vendor/rs-plugin/css/settings.css" media="screen">
		<link rel="stylesheet" href="/insurance-auto/vendor/circle-flip-slideshow/css/component.css" media="screen">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="css/skins/default.css">

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="css/custom.css">
		
		<!-- Head Libs -->
		<script src="vendor/modernizr/modernizr.js"></script>
		
		<header id="header" class="colored flat-menu">
				<div class="container">
					<div class="logo">
						<a href="#">
							<img width="190" height="60" src="../images/binfinite-logo.png" data-sticky-width="102" data-sticky-height="33">
						</a>
					</div>
						<button class="btn btn-responsive-nav btn-inverse" data-toggle="collapse" data-target=".nav-main-collapse">
						<i class="fa fa-bars"></i>
					</button>
				</div>
				
			
				
			</header>