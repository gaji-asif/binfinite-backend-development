<?php

ob_start();

//require_once "../netallianz-config.php";
require_once "authentication.php";

if (!empty($_GET['action'])&& $_GET['action']=='login' && isset($_POST['username']) && isset($_POST['password'])){

   
    $username = $_POST['username'];
    $password = hash('sha512', $_POST['password']);

    $r1 = DB::select("SELECT * FROM aig_auto_admins WHERE binary username = ? AND password = ?", [$username, $password]);

    if(empty($r1)){
        $message = array("type" => "error", "message" => "Authentication Failed!");
    }else{
    	$admin = $r1[0];
        $admin_id = $admin->admin_id;
        $session = session_get();
        setcookie('account:admin_id', $admin_id, time() + (86400 * 30), "/"); // 86400 = 1 day     
        setcookie('account:session', $session, time() + (86400 * 30), "/"); // 86400 = 1 day

        $q2 = DB::update("UPDATE aig_auto_admins SET session_id = ? WHERE admin_id = ?", [$session, $admin_id]);
        
        header("Location: download.php");
        die();
    }    
}
  
?>

<!DOCTYPE html>
<html>
	<head>

	</head>
	<body>

		<div class="body">
			<?php include ('top.php');?>

			<div role="main" class="main">
				
			<div class="container" style="padding-top:60px; padding-bottom:70px;">
					
					<div class="row">
						<div class="col-md-12"><h2 style="color:#083f88; text-align:center;">Admin Login</h2>
						</div>
					</div>
					<br />
					<div class="row">
						
						<div class="col-md-12">
							<form class="form-horizontal form-bordered" action="login.php?action=login" method="POST">
								<input type="hidden" name="_token" value="<?= csrf_token() ?>">
								
										<div class="form-group">
											<div class="col-md-3"></div>
											<div class="col-md-6">
												<div class="input-group input-group-icon">
													<span class="input-group-addon">
														<span class="icon"><i class="fa fa-user"></i></span>
													</span>
													<input type="text" name="username" class="form-control" placeholder="Username">
												</div>
											</div>
										</div>
										
										<div class="form-group">
											<div class="col-md-3"></div>
											<div class="col-md-6">
												<div class="input-group input-group-icon">
													<span class="input-group-addon">
														<span class="icon"><i class="fa fa-lock"></i></span>
													</span>
													<input type="password" name=password class="form-control" placeholder="Password">
												</div>
											</div>
										</div>
									<div class="form-group">
									<div class="col-md-3"></div>
											<div class="col-md-6">
											<input class="btn btn-primary appear-animation bounceIn appear-animation-visible" data-appear-animation="bounceIn" style="float:right;" type="submit" value="Login">
										
									</div>
							</form>

						</div>

					</div>

				</div>
			
			</div>
			<!-- FOOTER -->
			<?php include ('bottom.php');?>
		</div>

	
	
	</body>
</html>
