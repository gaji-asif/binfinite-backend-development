<?php

ob_start();
//require_once "../netallianz-config.php";
require_once "authentication.php";

if(!doAuth()){
    header("Location: login.php");
}

if (!empty($_GET['action'])&& $_GET['action']=='submit' && isset($_POST['month']) && isset($_POST['year'])) {



  function export_csv($filename, $header, $data) {
    // No point in creating the export file on the file-system. We'll stream
    // it straight to the browser. Much nicer.

    // Open the output stream
    $fh = fopen('php://output', 'w');

    // Start output buffering (to capture stream contents)
    ob_start();

    // CSV Header
    fputcsv($fh, $header);

    // CSV Data
    foreach ($data as $k => $v) {
      $line = array($v->name, $v->bcard, $v->ic, $v->exp_date, $v->car_reg_no, $v->mobile, $v->email, $v->date_created);
      fputcsv($fh, $line);
    }

    // Get the contents of the output buffer
    $string = ob_get_clean();

    // Output CSV-specific headers
    header('Pragma: public');
    header('Expires: 0');
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Cache-Control: private', false);
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="' . $filename . '";');
    header('Content-Transfer-Encoding: binary');

    // Stream the CSV data
    exit($string);
  }


  $month = $_POST['month'];
  $year = $_POST['year'];

  $filename = 'members_' . $month . '_' . $year . '.csv';
  $member = DB::select("SELECT * FROM aig_auto_members WHERE MONTH(date_created) = ? AND YEAR(date_created) = ?", [$month, $year]);
  $total_header = sizeOf($member);
  $headers = ["NAME","BCARD","NRIC","EXPDATE","CAR REGISTRAION NO","MOBILE","EMAIL","DATE CREATED"];
  export_csv($filename, $headers, $member);

}




?>

<!DOCTYPE html>
<html>
	<head>

	</head>
	<body>

		<div class="body">
			<?php include ('top.php');?>

			<div role="main" class="main">

			<div class="container" style="padding-top:60px; padding-bottom:100px;">

					<div class="row">
						<div class="col-md-12"><h2 style="color:#083f88; text-align:center;">File Download</h2>
						</div>
					</div>
					<br />
					<div class="row">

						<div class="col-md-12">

              <p style="text-align:center; font-size:20px;">Please download the participants list </p><br />
              <div class="container">
                <div class="col-md-6 col-md-offset-3">
                  <form class="" action="download.php?action=submit" method="post">
                    <input type="hidden" name="_token" value="<?= csrf_token() ?>">
                  <div class="input-group">


                    <div class="col-md-12">
                      <label>Month</label>
                      <input type="number" class="form-control" name="month" value=""><br>
                    </div>
                    <div class="col-md-12">
                      <label>Year</label>
                      <input type="number" class="form-control" name="year" value="">
                    </div>
                      <button class="btn btn-primary" action="submit">Download</button>
                  </div>


                  </form>
                </div>

              </div>


							<p style="text-align:center;"><a href="logout.php" class="btn btn-primary appear-animation bounceIn appear-animation-visible" data-appear-animation="bounceIn">Logout</a></p>
						</div>

					</div>

				</div>

			</div>
			<!-- FOOTER -->
			<?php include ('bottom.php');?>
		</div>



	</body>
</html>
