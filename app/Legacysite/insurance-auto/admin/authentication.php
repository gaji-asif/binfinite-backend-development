<?php
  
// require_once "../netallianz-config.php";
 

function doAuth(){           
  $session = $_COOKIE['account:session'];
  $admin_id = $_COOKIE['account:admin_id'];

  $result = DB::select("SELECT session_id FROM aig_auto_admins WHERE admin_id = ?", [$admin_id]);

  if(isset($result) && isset($result[0])) {
    
    $session_id = $result[0]->session_id;    
    if($session_id == $session){
      return 1;   
    }else{
      return 0;    
    }
  }else{
    return 0;   
  }
}

function session_get(){
  $size = 12;
  $chars = array_merge(range('a', 'z'), range('A', 'Z'), range(0, 9));
  $chars_size = sizeof($chars);

  $session = '';
  for($i=0; $i<$size; $i++){
    $session .= $chars[rand(0, $chars_size-1)];
  }

  return $session;
}


  
?>