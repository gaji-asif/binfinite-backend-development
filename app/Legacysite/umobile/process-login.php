<?php
	session_start(); require_once("config.php");  

	if (!empty($_POST["card_number"]) && !empty($_POST["card_pin"])) {
		$_SESSION["bcard_number"] = $_POST["card_number"];
		$_SESSION["bcard_password"] = $_POST["card_pin"];
		
		$result = VerifyLogin();

		if ($result == "ok") {
			GetMemberProfile();		
			$_SESSION["bcard_logged_in"] = true;
			header("Location: transaction-history.php");
			exit();
		} else {
			header("Location: login.php?error=2");
			exit();
		}
		
	} else {
		header("Location: login.php?error=1");
		exit();
	}
?>