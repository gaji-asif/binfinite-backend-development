<!doctype html>
<html lang="en-US" class="no-js">

<body class="">

	<div id="page_wrapper">
		<?php include ('top.php');?>
		<div id="page_header" class="page-subheader site-subheader-cst uh_zn_def_header_style maskcontainer--mask3">
			<div class="bgback">
			</div>
			<div class="kl-bg-source">
			<div class="kl-bg-source__overlay"></div>
			</div>
			<div class="th-sparkles">
			</div>


		</div>

		<style>.breadcrumb {
    padding: 10px 20px;
    background: #e36d1c;
    color: #fff;
}</style>

		<section class="hg_section">
			<div class="container">
				<div class="row">

				<div class="col-md-2 col-sm-2">

				</div>

				<div class="col-md-8 col-sm-8">

		<script>
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		 alert('Only numbers are allowed.');
        return false;
    }
    return true;
}
	</script>


		

						<h3>Step 1 : Connect </h3>
					<div class="row">
						<div class="col-md-3">&nbsp;</div>
									<div class="col-md-6"><img src="img/u-card.jpg"></div>
						<div class="col-md-3">&nbsp;</div>
					</div>

						<div class="fancy_register_form">
						<?php if (!empty($_GET['action']) && $_GET['action'] == 'submit') { 
								 if (empty($_POST['mobile']) || empty($_POST['ucard'])) {
									echo 'Please enter UMobile number and UCard<br>';
								 } else {
									$check_umobile_status = ucardStatusQuery($_POST['mobile'],$_POST['ucard']); 
									//var_dump($check_umobile_status);
									//die();
									if ($check_umobile_status == 1) {
										$_SESSION['umobile_number'] = $_POST['mobile'];
										$_SESSION['umobile_card'] = $_POST['ucard'];
										header('Location: register.php');
									} else {
										header('Location: connect.php?error=yes&error_code='.$check_umobile_status); 
									}
								 }
								 
							  } else {
						?>	
										<?php 
											if (!empty($_GET['error_code']) && $_GET['error_code'] == '2') {
												echo "<span style='color:red; font-weight:bold;'> [RC02] The mobile number keyed in is not available. Please try again.</span><br><br>";
											}  else if (!empty($_GET['error_code']) && $_GET['error_code'] == '3') {
												echo "<span style='color:red; font-weight:bold;'> [RC03] The BCARD number keyed in is not available. Please try again.</span><br><br>";
											} else if (!empty($_GET['error_code']) && $_GET['error_code'] == '4') {
												echo "<span style='color:red; font-weight:bold;'> [RC04] We are sorry, system is currently not available. Please try again later.</span><br><br>";
											} else if (!empty($_GET['error_code']) && $_GET['error_code'] == '5') {
												echo "<span style='color:red; font-weight:bold;'> [RC05] We are unable to process your request. The BCARD number keyed in has been registered. Please key in another BCARD number to proceed.</span><br><br>";
											} else if (!empty($_GET['error_code']) && $_GET['error_code'] == '6') {
												echo "<span style='color:red; font-weight:bold;'> [RC06] The mobile number and BCARD number keyed in do not match. Please try again.</span><br><br>";
											} else if (!empty($_GET['error']) && $_GET['error'] == 'yes') { 
												echo "<span style='color:red; font-weight:bold;'> The mobile number / UCard no keyed in is not available. Please try again.</span><br><br>";
											} 
										?>
										<!-- beginning of form -->
										<form method="post" class="th-register-form register_form_static form-horizontal " action="<?php echo $_SERVER["PHP_SELF"]; ?>?action=submit" id="connect">

											<div class="form-group">


												<label class="col-sm-3 control-label">UMobile No <span style="color:red;">*</span></label>

												<div class="col-sm-9">
													<input type="text" name="mobile" id="mobile" class="form-control inputbox" required="" placeholder="60181234567" maxlength="12" onkeypress="return isNumber(event)">
												</div>



											</div>

											<div class="form-group">


												<label class="col-sm-3 control-label">UCard No <span style="color:red;">*</span></label>

												<div class="col-sm-9">
													<input type="text" name="ucard" id="ucard" class="form-control inputbox" required="" placeholder="16 digits No" maxlength="16" onkeypress="return isNumber(event)">
												</div>



											</div>

											<div class="form-group">


												<label class="col-sm-3 control-label">Agent Code</label>

												<div class="col-sm-9">
													<input type="text" name="agent_code" id="agent_code" class="form-control inputbox" required="" placeholder="" maxlength="12">
												</div>



											</div>



				<br />
                <div class="form-group" style="font-size:12px;">

				Fields marked with <span style="color:red;"> * </span> are mandatory.

				</div>
				<button class="btn btn-fullcolor" type="submit" id="connect-to" style="float:right;">Next</button>
							</form>
							
							
				<?php } ?>
									</div>

				</div>

				<div class="col-md-2 col-sm-2">

				</div>

				</div>

			</div>
		</div>
	</section>

		<script>
            jQuery( document ).ready(function($) {

				//Sign Up form
                $("#connect-to").click(function (e) {
                    // this points to our form
                    e.preventDefault();

					if (!$('input[name=mobile]').val()) {
                        alert('Please enter your UMobile No.');
                        $('input[name=mobile]').focus();
                        return false;

                    } else if (!$('input[name=ucard]').val()) {
                        alert('Please enter your UCard No.');
                        $('input[name=ucard]').focus();
                        return false;

                    } else {
                    	$("#connect").submit();
                        return false;
                    }
                });
			});
	</script>


		<?php include ('bottom.php');?>

	</div><!-- end page-wrapper -->

</body>
</html>
