<!doctype html>
<html lang="en-US" class="no-js">

<?php include '../gtm.php'; ?>

	<div id="page_wrapper">
		<?php include ('top.php');?>
		<?php include ('../wordpress-config.php');?>
		<div id="page_header" class="page-subheader site-subheader-cst uh_zn_def_header_style maskcontainer--mask3">
			<div class="bgback">
			</div>
			<div class="kl-bg-source">
			<div class="kl-bg-source__overlay"></div>
			</div>
			<div class="th-sparkles">
			</div>

		</div>
<style>

.latest_posts.style2 ul.posts .details {
    float: left;
    width: 400px;
}

.latest_posts.style2 ul.posts .text {
    margin-left: 210px;
}


</style>
<section class="hg_section">
			<div class="hg_section_size container">
				<div class="row">

					<div class="col-md-12 col-sm-12">
						<div class="latest_posts style2">
							<h3 class="title">Latest Promotions</h3>
							<ul class="posts">
								<?php // Wordpress Starts
								$args = array(
									'posts_per_page'   => 100,
									'offset'           => 0,
									'category'         => '',
									//'category_name'    => '',
									'orderby'          => 'date',
									'order'            => 'DESC',
									//'include'          => '',
									//'exclude'          => '',
									//'meta_key'         => '',
									//'meta_value'       => '',
									'post_type'        => 'promotion_post',
									//'post_mime_type'   => '',
									//'post_parent'      => '',
									//'author'	   => '',
									//'author_name'	   => '',
									'post_status'      => 'publish',
									//'suppress_filters' => true
								);

								$myposts = get_posts( $args );
								foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
									<li class="post">
									<div class="details">
									<?php if (get_field('link_to_page_url') != '' && get_field('link_to_page_url') != '#' ) { ?>

										<a class="fancybox-button" rel="fancybox-button" href="<?php echo get_field('link_to_page_url'); ?>" ><img src="<?php if ( has_post_thumbnail() ) { the_post_thumbnail_url('full'); } ?>" /></a>

									<?php } else if (get_field('enlarged_image') != '') { ?>

										<a class="fancybox-button" rel="fancybox-button" href="<?php echo get_field('enlarged_image'); ?>" ><img src="<?php if ( has_post_thumbnail() ) { the_post_thumbnail_url('full'); } ?>" /></a>

									<?php } else { ?>

										<a class="fancybox-button" rel="fancybox-button" href="<?php if ( has_post_thumbnail() ) { the_post_thumbnail_url('full'); } ?>" ><img src="<?php if ( has_post_thumbnail() ) { the_post_thumbnail_url('full'); } ?>" /></a>

									<?php } ?>
									</div>
									<h4 class="title"><?php echo get_the_title(); ?></h4>
									<div class="text">
										<?php echo get_the_content(); ?><div style="clear:both">

									<!--
										<b>Partner</b> : GMO<br />
										<b>Promotion Date</b> : Valid until 30 November 2016<br />
										<b>Description:</b> <a href="survey-panelist.php">Sign up</a> as a survey panelist and get BPoints! <a href="survey-panelist.php">Sign up now!</a>
										-->
									</div>
									</li>
								<?php endforeach;
								wp_reset_postdata();
								// Wordpress ends
								?>



							</ul>
						</div>
						<!-- end // latest posts style 2 -->
					</div>

				</div>
			</div>
		</section>


		<?php include ('bottom.php');?>

	</div><!-- end page-wrapper -->

</body>
</html>
