

	<!-- JS FILES // These should be loaded in every page -->
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/kl-plugins.js"></script>
	<script type="text/javascript" src="addons/smoothscroll/smoothscroll.js"></script>
	<!-- JS FILES // Loaded on this page -->
	<script type="text/javascript" src="addons/scrollme/jquery.scrollme.js"></script>
	<!-- <script type="text/javascript" src="sliders/caroufredsel/jquery.carouFredSel-packed.js"></script> -->
	<script type="text/javascript" src="js/kl-recent-work-carousel2.js"></script>
	<script type="text/javascript" src="js/kl-testimonials-fader.js"></script>
	<script type="text/javascript" src="addons/flickrfeed/jquery.jflickrfeed.min.js"></script>
	<script type="text/javascript" src="addons/magnific_popup/jquery.magnific-popup.js"></script>
	<script type="text/javascript" src="js/kl-screenshot-box.js"></script>
	<script type="text/javascript" src="js/kl-logo-carousel.js"></script>

	<!-- JS codes -->
	<script type="text/javascript" src="js/kl-scripts.js"></script>
	<!-- JS codes -->
	<script type="text/javascript" src="js/kl-custom.js"></script>
	<script src="js/plugins.js"></script>
	<script src="js/app.js"></script>


<!-- jPages -->

  <link rel="stylesheet" href="css/jPages.css">
  <!-- <script type="text/javascript" src="js/jquery-1.8.2.min.js"></script> -->
  <script src="js/jPages.js"></script>



<script>
	/* when document is ready */
	$( document ).ready(function() {
		$("div.holder").jPages({
				containerID  : "history",
				perPage      : 20,
		//    startPage    : 1,
		//    startRange   : 1,
		//    midRange     : 15,
		//    endRange     : 1
		});
	});
</script>

 <!-- FancyBox -->
<link rel="stylesheet" href="fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>

<!-- Optionally add helpers - button, thumbnail and/or media -->
<link rel="stylesheet" href="fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
<script type="text/javascript" src="fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="fancybox/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

<link rel="stylesheet" href="fancybox/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
<script type="text/javascript" src="fancybox/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>


<script type="text/javascript">

$(document).ready(function() {
	$(".fancybox-button").fancybox({
		prevEffect		: 'none',
		nextEffect		: 'none',
		closeBtn		: false,
		helpers		: {
			title	: { type : 'inside' },
			buttons	: {}
		}
	});
});

</script>