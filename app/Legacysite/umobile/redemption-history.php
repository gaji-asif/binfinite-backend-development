<!doctype html>
<html lang="en-US" class="no-js">

<?php include '../gtm.php'; ?>

	<div id="page_wrapper">
		<?php include ('top.php');?>
		<?php /* if (empty($_SESSION["bcard_logged_in"]) ||  $_SESSION["bcard_logged_in"] == false) { die("Session timed out, please login again to access your account."); } */ ?>
		<div id="page_header" class="page-subheader site-subheader-cst uh_zn_def_header_style maskcontainer--mask3">
			<div class="bgback">
			</div>
			<div class="kl-bg-source">
			<div class="kl-bg-source__overlay"></div>
			</div>
			<div class="th-sparkles">
			</div>


		</div>

		<section class="hg_section">
			<div class="container">
				<div class="row">

				<div class="col-md-12 col-sm-12">
				 <?php include('menu-bar.php');?>


						<h3>Redemption History</h3>

						<table class="points">

							<tr>
								<th>Redeem ID</th>
								<th>Redeem Date</th>
								<th>Outlets</th>
								<th>Item</th>
								<th>Quantity</th>
								<th>Point</th>

							</tr>

							<tbody id="history">
							<!-- Jeff mod starts--->
							<?php echo GetRedeemHistory(); ?>
							<!-- Jeff mod ends -->

							</tbody>
						</table>
						<!-- Pagination Holder -->
						<div class="holder"></div>

				</div>



			</div>
		</div>
	</section>

		<?php include ('bottom.php');?>

	</div><!-- end page-wrapper -->

</body>
</html>
