<!doctype html>
<html lang="en-US" class="no-js">

<?php include '../gtm.php'; ?>

	<div id="page_wrapper">
		<?php include ('top.php');?>
		<?php /* if (empty($_SESSION["bcard_logged_in"]) ||  $_SESSION["bcard_logged_in"] == false) { die("Please login to continue."); } */?>

		<div id="page_header" class="page-subheader site-subheader-cst uh_zn_def_header_style maskcontainer--mask3">
			<div class="bgback">
			</div>
			<div class="kl-bg-source">
			<div class="kl-bg-source__overlay"></div>
			</div>
			<div class="th-sparkles">
			</div>


		</div>

<style>
@media (min-width: 991px) {
.th-accordion {width:49.6%; float:left;}
}


</style>
		<section class="hg_section">
			<div class="container">
				<div class="row">



				<div class="col-md-12 col-sm-12">
				<?php include('menu-bar.php');?>


				<h3>My Profile > Office Details</h3>
				<div class="tabbable tabs_style2">
				<ul class="nav fixclear">
								<li><a href="personal-details.php">Personal Details</a></li>
								<li><a href="home-details.php">Home Details</a></li>
								<li><a href="office-details.php" class="clicked">Office Details</a></li>
							</ul>
				</div>

			<div class="hg_accordion_element style2 ptop-25">
							<div class="th-accordion" style="margin-right: 5px;">

								<div class="acc-group editable">
									<button data-toggle="collapse" data-target="#acc8" aria-expanded="true" onClick="parent.location='edit-office-detail.php?field=office_address'">Address<span class="acc-icon"></span>

									<div class="details"><?php echo return_clean_value($_SESSION["profile"]["bcard_officeaddress1"]); ?></div>

									</button>

								</div>

								<div class="acc-group">
									<button data-toggle="collapse" data-target="#acc8" onClick="parent.location='edit-office-detail.php?field=office_address'">Country
									<div class="details"><?php echo return_clean_value($_SESSION["profile"]["bcard_officecountry"]); ?></div>
									</button>
								</div>

								<div class="acc-group">
									<button data-toggle="collapse" data-target="#acc8" onClick="parent.location='edit-office-detail.php?field=office_address'">State
									<div class="details"><?php echo return_clean_value($_SESSION["profile"]["bcard_officestate"]); ?></div>
									</button>
								</div>

								<div class="acc-group">
									<button data-toggle="collapse" data-target="#acc8" onClick="parent.location='edit-office-detail.php?field=office_address'">City
									<div class="details"><?php echo return_clean_value($_SESSION["profile"]["bcard_officecity"]); ?></div>
									</button>
								</div>

							</div>
						</div>
						<!-- end // accordion texts  -->

					<div class="hg_accordion_element style2">

							<div class="th-accordion">

								<div class="acc-group">
									<button data-toggle="collapse" data-target="#acc8" onClick="parent.location='edit-office-detail.php?field=office_address'">Postcode</span>
									<div class="details"><?php echo return_clean_value($_SESSION["profile"]["bcard_officezip"]); ?></div>
									</button>
								</div>

								<div class="acc-group editable">
									<button data-toggle="collapse" data-target="#acc8" onClick="parent.location='edit-office-detail.php?field=office_phone'">Office Phone<span class="acc-icon"></span>
									<div class="details"><?php echo return_clean_value($_SESSION["profile"]["bcard_officephone"]); ?></div>
									</button>
								</div>

								<div class="acc-group editable">
									<button data-toggle="collapse" data-target="#acc8" onClick="parent.location='edit-office-detail.php?field=office_fax'">Office Fax<span class="acc-icon"></span>
									<div class="details"><?php echo return_clean_value($_SESSION["profile"]["bcard_officefax"]); ?></div>
									</button>
								</div>

								<div class="acc-group editable">
									<button data-toggle="collapse" data-target="#acc8" onClick="parent.location='edit-office-detail.php?field=office_email'">Work Email<span class="acc-icon"></span>
									<div class="details"><?php echo return_clean_value($_SESSION["profile"]["bcard_officeemail"]); ?></div>
									</button>
								</div>




							</div>
						</div>
						<!-- end // accordion texts  -->

				</div>
				</div>

			</div>
		</div>
	</section>

		<?php include ('bottom.php');?>

	</div><!-- end page-wrapper -->

</body>
</html>
