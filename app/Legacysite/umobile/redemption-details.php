<!doctype html>
<html lang="en-US" class="no-js">

<?php include '../gtm.php'; ?>

	<div id="page_wrapper">
		<?php include ('top.php');?>
		<div id="page_header" class="page-subheader site-subheader-cst uh_zn_def_header_style maskcontainer--mask3">
			<div class="bgback">
			</div>
			<div class="kl-bg-source">
			<div class="kl-bg-source__overlay"></div>
			</div>
			<div class="th-sparkles">
			</div>

		</div>
<style>
table tr, td{  border: 1px solid black; padding: 10px 10px;}

table tr {border-bottom: 1px solid black;}

</style>
		<section class="hg_section">
			<div class="container">

				<?php
					if (!empty($_GET['gift_id'])) {

						if (!empty($_GET['type']) && $_GET['type'] == 'topup') {
							$the_result = GetTopUpGiftCatalogueSingle($_GET['gift_id']);
						} else {
							$the_result = GetGiftCatalogueSingle($_GET['gift_id']);
						}

						if (is_array($the_result)) {
				?>
							<div class="row">




						<div class="col-md-12 col-sm-12">
								<?php if (isset($_SESSION["bcard_logged_in"]) && $_SESSION["bcard_logged_in"] == true) { ?>
								<?php } else { ?>
								<div class="breadcrumb">
							<form method="post" action="process-login-redemption.php">
							  <?php
								if (isset($_GET['error'])) {
									echo '<div style="color:red;">'.$error_msg.'</div>';
								}
							  ?>

							<input type="text" onkeypress="return isNumber(event)" placeholder="Your 16 digits BCard No" name="card_number" value="" maxlength="16"/>

							<input type="password" placeholder="6-digit PIN" name="card_pin" value="" maxlength="6" onkeypress="return isNumber(event)"/>

							<input type="submit" value='Login' style="background: #454444; color: #fff; border-radius:5px;">

							</form> <?php } ?>

						</div>



								<div class="col-md-2"></div>

							<div class="col-md-8">
							<?php if (isset($_SESSION["bcard_logged_in"]) && $_SESSION["bcard_logged_in"] == true) { ?>
							<?php include('menu-bar.php');?>
						<?php } ?>
								<img src="<?php echo $the_result["Picture"]; ?>" />
									<div class="text_box">
									<h3 style="font-weight: 600;"><?php echo $the_result["GiftName"]; ?></h3>
										<p><?php echo $the_result["Description"]; ?></p>
										<table>
										<tr>
											<td width="40%">Gift ID </td>
											<td>:</td>
											<td width="50%"><?php echo $the_result["GiftID"]; ?></td>

										</tr>

										<tr>
											<td>Gift Name</td>
											<td>:</td>
											<td><?php echo $the_result["GiftName"]; ?></td>
										</tr>

										<tr>
											<td>Category</td>
											<td>:</td>
											<td><?php echo $the_result["Category"]; ?></td>
										</tr>

										</table>

									</div>





									<div class="text_box" style="margin-top:80px;">
										<h3 class="tbk__title fs-40 fw-extrabold" style="line-height:40px; text-align:center;"><span style="color:#d81e1e;"><?php echo $the_result["Point"]; ?></span>
										<br /><span style="font-size:28px;">POINTS</span></h3>
										<?php if (isset($_SESSION["bcard_logged_in"]) && $_SESSION["bcard_logged_in"] == true ) { ?>
											<a class="btn-element btn btn-fullcolor " href="#" style="margin:0 20px 30px 0; font-size: 14px; width:100%;" onclick="$('#the_redemption_form').show();">
												<span>REDEEM NOW</span>
											</a>
										<?php } else { ?>
											<a class="btn-element btn btn-fullcolor " href="#" style="margin:0 20px 30px 0; font-size: 14px; width:100%;" onclick="alert('Please login to redeem the item.');">
												<span>REDEEM NOW</span>
											</a>
										<?php } ?>
									</div>
							</div>

									<div class="col-md-2"></div>


						</div>


							</div>

							<?php if (!empty($_GET['action']) && $_GET['action'] == 'submit') { ?>
							<div class="row" id="the_redemption_form" style="display:block;">
							<?php } else { ?>
							<div class="row" id="the_redemption_form" style="display:none;">
							<?php } ?>

								<div class="col-md-8 col-sm-8 col-md-offset-2">
									<h3 style="font-weight: 600;">Redemption Form</h3>
										<?php if (!empty($_GET['type']) && $_GET['type'] == 'topup') {

												switch(trim($the_result["GiftID"])) {
													case "UMRM2":
													$value_name = "RM 2";
													break;
													case "UMRM5":
													$value_name = "RM 5";
													break;
													case "UMRM10":
													$value_name = "RM 10";
													break;
													case "UMRM15":
													$value_name = "RM 15";
													break;
													case "UMRM20":
													$value_name = "RM 20";
													break;
													default:
													$value_name = "";
												}

										?>
											<style>

											.form-module {
												float:left;
												max-width: 550px;
												background-color: #FF8300;
												border-top:0px;
											}

											.form-module input[type="button"]{
												background: #f7f7f7;
												color:#000;
												border-radius:5px;
											}

											</style>

											<?php

												if (!empty($_GET['action']) && $_GET['action'] == 'submit') {

													$submit_result = TopUpRedemption($_POST['redeem_gift_id'],$_POST['redeem_qty'],$now_date_time,$_POST['redeem_umobile_number']);

													echo $submit_result . '<br>';
													echo '<a href="'.$_SERVER['PHP_SELF'].'?type=topup&gift_id='.$the_result["GiftID"].'">Click here to redeem again</a>';

												} else {
											?>
											<!-- Form Module-->
											<div class="module form-module">
												<div class="toggle"><i class="fa fa-times fa-pencil"></i>
													<div class="tooltip">Click Me</div>
												</div>
											  <div class="form">
												<h2 align="center"><img src="<?php echo $the_result["Picture"]; ?>"></h2>
												<form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>?type=topup&action=submit&gift_id=<?php echo trim($the_result["GiftID"]); ?>">
												  <input type="text" placeholder="Enter Recipient's No (Eg. 60181234567)" name="redeem_umobile_number" />
												  <input type="text" placeholder="" value="<?php echo $value_name; ?>" DISABLED />
												  <input type="hidden" name="redeem_gift_id" value="<?php echo trim($the_result["GiftID"]); ?>">
												  <input type="hidden" name="redeem_qty" value="1">
													<div class="col-md-4">

													</div>
													<div class="col-md-4">
													<input type="submit" value='Top up Now'>
													</div>
													<div class="col-md-4">

													</div>


												</form>
											  </div>
											</div>
											<?php } ?>

										<?php } else { ?>

											<?php

												if (!empty($_GET['action']) && $_GET['action'] == 'submit') {

													if (empty($_POST['redemption_RecipientCountry'])) {
														$_POST['redemption_RecipientCountry'] = "Malaysia";
													}

													$submit_result = CatalogueRedemption(
																		$_POST['redemption_GiftID'],
																		$_POST['redemption_RedeemQty'],
																		$redemption_RedeemDate = $now_date_time,
																		$_SESSION["profile"]["bcard_fullname"],
																		$_SESSION["profile"]["bcard_mobilephone"],
																		$_SESSION["profile"]["bcard_primaryemail"],
																		$_POST['redemption_RecipientAddress1'],
																		$_POST['redemption_RecipientAddress2'],
																		$_POST['redemption_RecipientAddress3'],
																		$_POST['redemption_RecipientZip'],
																		$_POST['redemption_RecipientState'],
																		$_POST['redemption_RecipientCountry']
																	);

													echo $submit_result . '<br>';
													echo '<a href="'.$_SERVER['PHP_SELF'].'?gift_id='.trim($the_result["GiftID"]).'">Click here to redeem again</a>';

												} else {
											?>

											<script>
												$( document ).ready(function() {

													$( "input[name='redemption_RecipientAddress1']" ).val("<?php echo $_SESSION["profile"]["bcard_homeaddress1"]; ?>");
													$( "input[name='redemption_RecipientAddress2']" ).val("<?php echo $_SESSION["profile"]["bcard_homeaddress2"]; ?>");
													$( "input[name='redemption_RecipientAddress3']" ).val("<?php echo $_SESSION["profile"]["bcard_homeaddress3"]; ?>");
													$( "input[name='redemption_RecipientZip']" ).val("<?php echo $_SESSION["profile"]["bcard_homezip"]; ?>");
													$( "select[name='redemption_RecipientState']" ).val("<?php echo $_SESSION["profile"]["bcard_homestate"]; ?>");
													//$( "select[name='redemption_RecipientCountry']" ).val("<?php echo $_SESSION["profile"]["bcard_homecountry"]; ?>");

													$( "#choose_address" ).change(function() {
														if ($(this).val() == "home") {

															$( "input[name='redemption_RecipientAddress1']" ).val("<?php echo $_SESSION["profile"]["bcard_homeaddress1"]; ?>");
															$( "input[name='redemption_RecipientAddress2']" ).val("<?php echo $_SESSION["profile"]["bcard_homeaddress2"]; ?>");
															$( "input[name='redemption_RecipientAddress3']" ).val("<?php echo $_SESSION["profile"]["bcard_homeaddress3"]; ?>");
															$( "input[name='redemption_RecipientZip']" ).val("<?php echo $_SESSION["profile"]["bcard_homezip"]; ?>");
															$( "select[name='redemption_RecipientState']" ).val("<?php echo $_SESSION["profile"]["bcard_homestate"]; ?>");
															//$( "select[name='redemption_RecipientCountry']" ).val("<?php echo $_SESSION["profile"]["bcard_homecountry"]; ?>");

														} else if ($(this).val() == "office") {

															$( "input[name='redemption_RecipientAddress1']" ).val("<?php echo $_SESSION["profile"]["bcard_officeaddress1"]; ?>");
															$( "input[name='redemption_RecipientAddress2']" ).val("<?php echo $_SESSION["profile"]["bcard_officeaddress2"]; ?>");
															$( "input[name='redemption_RecipientAddress3']" ).val("<?php echo $_SESSION["profile"]["bcard_officeaddress3"]; ?>");
															$( "input[name='redemption_RecipientZip']" ).val("<?php echo $_SESSION["profile"]["bcard_officezip"]; ?>");
															$( "select[name='redemption_RecipientState']" ).val("<?php echo $_SESSION["profile"]["bcard_officestate"]; ?>");
															//$( "select[name='redemption_RecipientCountry']" ).val("<?php echo $_SESSION["profile"]["bcard_officecountry"]; ?>");

														} else {

															$( "input[name='redemption_RecipientAddress1']" ).val("");
															$( "input[name='redemption_RecipientAddress2']" ).val("");
															$( "input[name='redemption_RecipientAddress3']" ).val("");
															$( "input[name='redemption_RecipientZip']" ).val("");
															$( "select[name='redemption_RecipientState']" ).val("");
															//$( "select[name='redemption_RecipientCountry']" ).val("Malaysia");

														}
													});
												});
											</script>

											<form name="redeem-form" class="th-register-form register_form_static form-horizontal " id="redeem-form" action="<?php echo $_SERVER['PHP_SELF']; ?>?action=submit&gift_id=<?php echo trim($the_result["GiftID"]); ?>" method="post" >
													<input type="hidden" name="redemption_RecipientCountry" value="Malaysia" />
													<input type="hidden" name="redemption_GiftID" value="<?php echo trim($the_result["GiftID"]); ?>" />

													<div class="form-group">
													<label class="col-sm-3 control-label">Quantity</label>
													<div class="col-sm-9">
														<select class="form-control" name="redemption_RedeemQty">
														  <option value="1">1</option>
														  <option value="2">2</option>
														  <option value="3">3</option>
														  <option value="4">4</option>
														  <option value="5">5</option>
														  <option value="6">6</option>
														  <option value="7">7</option>
														  <option value="8">8</option>
														  <option value="9">9</option>
														  <option value="10">10</option>
														</select>

													</div>

												</div>


												<div class="form-group">
													<label class="col-sm-3 control-label">Delivery to</label>
													<div class="col-sm-9">
														<select class="form-control" id="choose_address">
														  <option value="home">Home Address</option>
														  <option value="office">Office Address</option>
														  <option value="new">Add New Address</option>

														</select>
													</div>

												</div>

												<div class="form-group">
													<label class="col-sm-3 control-label">Address 1</label>
													<div class="col-sm-9">
														<input type="text" name="redemption_RecipientAddress1" id="address1" class="form-control inputbox" required="yes" placeholder="Address Line 1">

													</div>
												</div>

												<div class="form-group">
													<label class="col-sm-3 control-label">Address 2</label>
													<div class="col-sm-9">
														<input type="text" name="redemption_RecipientAddress2" id="address2" class="form-control inputbox" required="" placeholder="Address Line 2">
													</div>

												</div>


												<div class="form-group">
													<label class="col-sm-3 control-label">Address 3</label>
													<div class="col-sm-9">
														<input type="text" name="redemption_RecipientAddress3" id="address3" class="form-control inputbox" required="" placeholder="Address Line 3">
													</div>
												</div>

												<div class="form-group">
													<label class="col-sm-3 control-label">Postcode</label>
													<div class="col-sm-9">
														<input type="text" name="redemption_RecipientZip" id="postcode" class="form-control inputbox" required="yes" placeholder="45800">
													</div>
												</div>

												<div class="form-group">
													<label class="col-sm-3 control-label">State</label>
													<div class="col-sm-9">
														<select class="form-control inputbox" name="redemption_RecipientState" required="yes" id="state">
															<option value="">-- Select one -- ( Malaysia states only )</option>
														<option value="Johor">Johor</option>
														<option value="Kedah">Kedah</option>
														<option value="Kelantan">Kelantan</option>
														<option value="Kuala Lumpur">Kuala Lumpur</option>
														<option value="Melaka">Melaka</option>
														<option value="Negeri Sembilan">Negeri Sembilan</option>
														<option value="Pahang">Pahang</option>
														<option value="Perak">Perak</option>
														<option value="Perlis">Perlis</option>
														<option value="Pulau Pinang">Pulau Pinang</option>
														<option value="Sabah">Sabah</option>
														<option value="Sarawak">Sarawak</option>
														<option value="Selangor">Selangor</option>
														<option value="Terengganu">Terengganu</option>
														<option value="Wilayah Persekutuan – Labuan">Wilayah Persekutuan – Labuan</option>
														<option value="Outside Malaysia">Outside Malaysia</option>
														</select>
												</div>

												</div>
<!--
												<div class="form-group">
													<label class="col-sm-3 control-label">Country</label>
													<div class="col-sm-9">
														<select class="form-control inputbox" name="redemption_RecipientCountry" required="yes" id="country">
														<?php
																foreach ($countries_list as $key => $value) {
																		//echo "<option value='".$value."'>".$value."</option>";
																}

														?>
														</select>

													</div>
												</div>
-->
										<div class="form-group">
										<div class="col-sm-6 col-sm-offset-6" style="margin-bottom:0;">
												<input type="submit" name="redeem-now" class="zn_sub_button btn btn-fullcolor th-button-register" value="CONFIRM REDEEM" id="redeem-now">
										</div>
										</div>


										</form>
										<?php } ?>

									<?php } ?>
								</div>
							</div>

					<?php } else { ?>
						<div class="row">
							Reward item not found.
						</div>
					<?php } ?>
				<?php } else { ?>
					<div class="row">
						No reward item selected.
					</div>
				<?php } ?>

			</div>
		</section>

		 <script>
            jQuery( document ).ready(function($) {

				//Sign Up form
                $("#redeem-now").click(function (e) {
                    // this points to our form
                    e.preventDefault();
					var country = $('#country');
					var state = $('#state');

                    if (!$('input[name=redemption_RecipientAddress1]').val()) {
                        alert('Please enter your address 1.');
                        $('input[name=redemption_RecipientAddress1]').focus();
                        return false;

                    } else if (!$('input[name=redemption_RecipientZip]').val()) {
                        alert('Please enter your postcode.');
                        $('input[name=redemption_RecipientZip]').focus();
                        return false;

					} else if (!$('select[name=redemption_RecipientState]').val()) {
						//If the "Please Select" option is selected display error.
						alert("Please select your state.");
						$('select[name=redemption_RecipientState]').focus();
						return false;


					}

                    else {
                    	$("#redeem-form").submit();
                        return false;
                    }
                });
			});
	</script>



		<?php include ('bottom.php');?>

	</div><!-- end page-wrapper -->

</body>
</html>
