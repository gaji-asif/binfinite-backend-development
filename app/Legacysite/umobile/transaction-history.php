<!doctype html>
<html lang="en-US" class="no-js">
<?php include '../gtm.php'; ?>

	<div id="page_wrapper">
		<?php include ('top.php');	?><br /><br />
		 <?php if (empty($_SESSION["bcard_logged_in"]) ||  $_SESSION["bcard_logged_in"] == false) { die("<p align='center'>Session timed out, please <a href='login.php'>login</a> again to access your account.</p>"); } ?>

		<div id="page_header" class="page-subheader site-subheader-cst uh_zn_def_header_style maskcontainer--mask3">
			<div class="bgback">
			</div>
			<div class="kl-bg-source">
			<div class="kl-bg-source__overlay"></div>
			</div>
			<div class="th-sparkles">
			</div>
		</div>

		<section class="hg_section">
			<div class="container">
				<div class="row">

				<div class="col-md-12 col-sm-12">
					<?php include('menu-bar.php');?>

					<!-- Jeff mod starts--->

					<h3>Transaction History</h3>
					<!-- Jeff mod ends -->
						<table class="points">
							<tr>
								<th colspan="5" class="main-th">Reward Transactions</td>
							</tr>
							<tr>
								<th>Transaction ID</th>
								<th>Sales Date</th>
								<th>Outlets</th>
								<th>Earned Points</th>



							</tr>

							<tbody id="history">
							<!-- Jeff mod starts--->
							<?php echo GetRewardHistory(); ?>
							<!-- Jeff mod ends -->



							</tbody>
						</table>

		<!-- Pagination holder -->
	     <div class="holder"></div>

				<!-- <a class="btn-element btn btn-fullcolor " href="redemption.php" style="float:right;"><span>REDEEM NOW</span></a> -->
				</div>

				</div>

			</div>


		</div><!-- End Page wrapper -->
	</section>
		<?php include ('bottom.php');?>
</body>
</html>
