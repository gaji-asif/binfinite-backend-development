<!doctype html>
<html lang="en-US" class="no-js">

<?php include '../gtm.php'; ?>

	<div id="page_wrapper">
		<?php include ('top.php');?>
		<div id="page_header" class="page-subheader site-subheader-cst uh_zn_def_header_style maskcontainer--mask3">
			<div class="bgback">
			</div>
			<div class="kl-bg-source">
			<div class="kl-bg-source__overlay"></div>
			</div>
			<div class="th-sparkles">
			</div>

			</div>

		</div>

<style>
@media (min-width: 991px) {
.th-accordion {width:49.6%; float:left;}
}


</style>
		<section class="hg_section">
			<div class="container">
				<div class="row">


				<div class="col-md-12 col-sm-12">
				<?php include('menu-bar.php');?>


				<h3>My Profile > Home Details</h3>
				<div class="tabbable tabs_style2">
				<ul class="nav fixclear">
								<li><a href="personal-details.php">Personal Details</a></li>
								<li><a href="home-details.php" class="clicked">Home Details</a></li>
								<li><a href="office-details.php">Office Details</a></li>
							</ul>
				</div>

			<div class="hg_accordion_element style2 ptop-25">
							<div class="th-accordion" style="margin-right: 5px;">
								<div class="acc-group editable">
									<button data-toggle="collapse" data-target="#acc8" aria-expanded="true" onClick="parent.location='edit-home-detail.php?field=home_address'">Address<span class="acc-icon"></span>

									<div class="details"><?php echo return_clean_value($_SESSION["profile"]["bcard_homeaddress1"]); ?></div>

									</button>

								</div>

								<div class="acc-group editable">
									<button data-toggle="collapse" data-target="#acc8" onClick="parent.location='edit-home-detail.php?field=home_address'">Postcode<span class="acc-icon"></span>
									<div class="details"><?php echo return_clean_value($_SESSION["profile"]["bcard_homezip"]); ?></div>
									</button>
								</div>

								<div class="acc-group editable">
									<button data-toggle="collapse" data-target="#acc8" onClick="parent.location='edit-home-detail.php?field=home_address'">City<span class="acc-icon"></span>
									<div class="details"><?php echo return_clean_value($_SESSION["profile"]["bcard_homecity"]); ?></div>
									</button>
								</div>

							</div>
						</div>
						<!-- end // accordion texts  -->

					<div class="hg_accordion_element style2">

							<div class="th-accordion">



								<div class="acc-group editable">
									<button data-toggle="collapse" data-target="#acc8" onClick="parent.location='edit-home-detail.php?field=home_address'">State<span class="acc-icon"></span>
									<div class="details"><?php echo return_clean_value($_SESSION["profile"]["bcard_homestate"]); ?></div>
									</button>
								</div>

								<div class="acc-group editable">
									<button data-toggle="collapse" data-target="#acc8" onClick="parent.location='edit-home-detail.php?field=home_address'">Country<span class="acc-icon"></span>
									<div class="details"><?php echo return_clean_value($_SESSION["profile"]["bcard_homecountry"]); ?></div>
									</button>
								</div>

								<div class="acc-group editable">
									<button data-toggle="collapse" data-target="#acc8" onClick="parent.location='edit-home-detail.php?field=home_phone'">Home Phone<span class="acc-icon"></span>
									<div class="details"><?php echo return_clean_value($_SESSION["profile"]["bcard_homephone"]); ?></div>
									</button>
								</div>

							</div>
						</div>
						<!-- end // accordion texts  -->

				</div>
				</div>

			</div>
		</div>
	</section>

		<?php include ('bottom.php');?>

	</div><!-- end page-wrapper -->

</body>
</html>
