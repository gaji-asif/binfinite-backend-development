<!doctype html>
<html lang="en-US" class="no-js">

<?php include '../gtm.php'; ?>

	<div id="page_wrapper">
		<?php include ('top.php');?>
		<div id="page_header" class="page-subheader site-subheader-cst uh_zn_def_header_style maskcontainer--mask3">
			<div class="bgback">
			</div>
			<div class="kl-bg-source">
			<div class="kl-bg-source__overlay"></div>
			</div>
			<div class="th-sparkles">
			</div>

		</div>

		<section class="hg_section">
			<div class="container">
				<div class="row">

				<div class="col-md-2 col-sm-2">

				</div>

				<div class="col-md-8 col-sm-8">
					 <?php include('menu-bar.php');?>

						<div class="tabbable tabs_style2">
				<ul class="nav fixclear">
								<li><a href="personal-details.php" class="clicked">Personal Details</a></li>
								<li><a href="home-details.php">Home Details</a></li>
								<li><a href="office-details.php">Office Details</a></li>
							</ul>
				</div>
									<div class="fancy_register_form">

									<?php if (!empty($_GET["field"]) && $_GET["field"] == 'title') { ?>

										<?php
											if (!empty($_GET["action"]) && $_GET["action"] == 'update') {
												 $_SESSION["profile"]["bcard_title"] = $_POST['the_input'];
												 $the_result = UpdateMemberProfile();
												 echo '<div>'.$the_result .'</div>';
											}
											$possible_selection = array("Mr","Mrs","Ms","Dr.");
										?>
										<!-- Edit Form starts -->
										<form name="login_form" method="post" class="th-register-form register_form_static form-horizontal" action="<?php echo $_SERVER['PHP_SELF']; ?>?field=<?php echo $_GET["field"]; ?>&action=update">
											<div class="form-group">
												<label class="col-sm-4 control-label" for="user_login">Salutation</label>
												<div class="col-sm-8">
													<select class="form-control inputbox" name="the_input">
														<?php
															foreach ($possible_selection as $value) {
																if ($value == $_SESSION["profile"]["bcard_title"]) {
																	echo '<option value="'.$value.'" SELECTED>'.$value.'</option>';
																} else {
																	echo '<option value="'.$value.'">'.$value.'</option>';
																}
															}
														?>

													</select>
												</div>
											</div>

											<button class="btn btn-fullcolor" type="submit" id="save" style="float:right;">Save</button>

										</form>
										<button class="btn" type="cancel" style="float:right; margin-right:10px;"  onclick="window.location='personal-details.php'">Cancel</button>
										<!-- Edit Form ends -->


									<?php } else if (!empty($_GET["field"]) && $_GET["field"] == 'nationality') { ?>

										<?php
											if (!empty($_GET["action"]) && $_GET["action"] == 'update') {
												 $_SESSION["profile"]["bcard_nationality"] = $_POST['the_input'];
												 $the_result = UpdateMemberProfile();
												 echo '<div>'.$the_result .'</div>';
											}
											$possible_selection = array("Malaysia","Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");

										?>
										<!-- Edit Form starts -->
										<form name="login_form" method="post" class="th-register-form register_form_static form-horizontal" action="<?php echo $_SERVER['PHP_SELF']; ?>?field=<?php echo $_GET["field"]; ?>&action=update">
											<div class="form-group">
												<label class="col-sm-4 control-label" for="user_login">Nationality</label>
												<div class="col-sm-8">
													<select class="form-control inputbox" name="the_input">
														<?php
															foreach ($possible_selection as $value) {
																if ($value == $_SESSION["profile"]["bcard_title"]) {
																	echo '<option value="'.$value.'" SELECTED>'.$value.'</option>';
																} else {
																	echo '<option value="'.$value.'">'.$value.'</option>';
																}
															}
														?>
													</select>
												</div>
											</div>

											<button class="btn btn-fullcolor" type="submit" id="save" style="float:right;">Save</button>
										</form>
											<button class="btn" type="cancel" style="float:right; margin-right:10px;" onClick="window.location='personal-details.php'">Cancel</button>
										<!-- Edit Form ends -->

									<?php } else if (!empty($_GET["field"]) && $_GET["field"] == 'maritalstatus') { ?>

										<?php
											if (!empty($_GET["action"]) && $_GET["action"] == 'update') {
												 $_SESSION["profile"]["bcard_maritalstatus"] = $_POST['the_input'];
												 $the_result = UpdateMemberProfile();
												 echo '<div>'.$the_result .'</div>';
											}
											$possible_selection = array("Single","Married");
										?>
										<!-- Edit Form starts -->
										<form name="login_form" method="post" class="th-register-form register_form_static form-horizontal" action="<?php echo $_SERVER['PHP_SELF']; ?>?field=<?php echo $_GET["field"]; ?>&action=update">
											<div class="form-group">
												<label class="col-sm-4 control-label" for="user_login">Marital Status</label>
												<div class="col-sm-8">
													<select class="form-control inputbox" name="the_input">
														<?php
															foreach ($possible_selection as $value) {
																if ($value == $_SESSION["profile"]["bcard_maritalstatus"]) {
																	echo '<option value="'.$value.'" SELECTED>'.$value.'</option>';
																} else {
																	echo '<option value="'.$value.'">'.$value.'</option>';
																}
															}
														?>
													</select>
												</div>
											</div>
											<button class="btn btn-fullcolor" type="submit" id="save" style="float:right;">Save</button>
										</form>
										<button class="btn" type="cancel" style="float:right; margin-right:10px;" onClick="window.location='personal-details.php'">Cancel</button>
										<!-- Edit Form ends -->

									<?php } else if (!empty($_GET["field"]) && $_GET["field"] == 'ownacar') { ?>

										<?php
											if (!empty($_GET["action"]) && $_GET["action"] == 'update') {
												 $_SESSION["profile"]["bcard_ownacar"] = $_POST['the_input'];
												 $the_result = UpdateMemberProfile();
												 echo '<div>'.$the_result .'</div>';
											}
											$possible_selection["Yes"] = "Y";
											$possible_selection["No"] = "N";
										?>
										<!-- Edit Form starts -->
										<form name="login_form" method="post" class="th-register-form register_form_static form-horizontal" action="<?php echo $_SERVER['PHP_SELF']; ?>?field=<?php echo $_GET["field"]; ?>&action=update">
											<div class="form-group">
												<label class="col-sm-4 control-label" for="user_login">Own a Car</label>
												<div class="col-sm-8">
													<select class="form-control inputbox" name="the_input">
														<?php
															foreach ($possible_selection as $key => $value) {
																if ($value == $_SESSION["profile"]["bcard_ownacar"]) {
																	echo '<option value="'.$value.'" SELECTED>'.$key.'</option>';
																} else {
																	echo '<option value="'.$value.'">'.$key.'</option>';
																}
															}
														?>
													</select>
												</div>
											</div>

											<button class="btn btn-fullcolor" type="submit" id="save" style="float:right;">Save</button>
										</form>
										<button class="btn" type="cancel" style="float:right; margin-right:10px;" onclick="parent.location='personal-details.php'">Cancel</button>

										<!-- Edit Form ends -->

									<?php } else if (!empty($_GET["field"]) && $_GET["field"] == 'ownacreditcard') { ?>

										<?php
											if (!empty($_GET["action"]) && $_GET["action"] == 'update') {
												 $_SESSION["profile"]["bcard_ownacreditcard"] = $_POST['the_input'];
												 $the_result = UpdateMemberProfile();
												 echo '<div>'.$the_result .'</div>';
											}
											$possible_selection["Yes"] = "Y";
											$possible_selection["No"] = "N";
										?>
										<!-- Edit Form starts -->
										<form name="login_form" method="post" class="th-register-form register_form_static form-horizontal" action="<?php echo $_SERVER['PHP_SELF']; ?>?field=<?php echo $_GET["field"]; ?>&action=update">
											<div class="form-group">
												<label class="col-sm-4 control-label" for="user_login">Own a Credit Card</label>
												<div class="col-sm-8">
													<select class="form-control inputbox" name="the_input">
														<?php
															foreach ($possible_selection as $key => $value) {
																if ($value == $_SESSION["profile"]["bcard_ownacreditcard"]) {
																	echo '<option value="'.$value.'" SELECTED>'.$key.'</option>';
																} else {
																	echo '<option value="'.$value.'">'.$key.'</option>';
																}
															}
														?>
													</select>
												</div>
											</div>

											<button class="btn btn-fullcolor" type="submit" id="save" style="float:right;">Save</button>

										</form>
										<button class="btn" type="cancel" style="float:right; margin-right:10px;" onclick="parent.location='personal-details.php'">Cancel</button>
										<!-- Edit Form ends -->

									<?php } else if (!empty($_GET["field"]) && $_GET["field"] == 'primaryemail') { ?>

										<?php
											if (!empty($_GET["action"]) && $_GET["action"] == 'update') {
												 $_SESSION["profile"]["bcard_primaryemail"] = $_POST['the_input'];
												 $the_result = UpdateMemberProfile();
												 echo '<div>'.$the_result .'</div>';
											}
										?>
										<!-- Edit Form starts -->
										<form name="edit_email" id="edit_email" method="post" class="th-register-form register_form_static form-horizontal" action="<?php echo $_SERVER['PHP_SELF']; ?>?field=<?php echo $_GET["field"]; ?>&action=update">
											<div class="form-group">
												<label class="col-sm-4 control-label" for="user_login">Email</label>
												<div class="col-sm-8">
													<input class="form-control inputbox" name="the_input" value="<?php echo $_SESSION["profile"]["bcard_primaryemail"]; ?>">
												</div>
											</div>

											<button class="btn btn-fullcolor" type="submit" id="save" style="float:right;">Save</button>
										</form>
										<button class="btn" type="cancel" style="float:right; margin-right:10px;" onclick="parent.location='personal-details.php'">Cancel</button>

										<script>

										jQuery( document ).ready(function($) {

											//Edit Email
											$("#save").click(function (e) {
												// this points to our form
												e.preventDefault();

												if (!$('input[name=the_input]').val()) {
													alert('Please enter your email.');
													$('input[name=the_input]').focus();
													return false;

												}else if (!$('input[name=the_input]').val() || $('input[name=the_input]').val().indexOf('@') == '-1' ) {
												alert('Please enter a valid email address.');
												$('input[name=the_input]').focus();
												return false;


												}else {
													$("#edit_email").submit();
													return false;
												}
											});
										});
									</script>
										<!-- Edit Form ends -->

									<?php } else if (!empty($_GET["field"]) && $_GET["field"] == 'mobilephone') { ?>

										<?php
											if (!empty($_GET["action"]) && $_GET["action"] == 'update') {
												 $_SESSION["profile"]["bcard_mobilephone"] = $_POST['the_input'];
												 $the_result = UpdateMemberProfile();
												 echo '<div>'.$the_result .'</div>';
											}
										?>
										<!-- Edit Form starts -->
										<form name="edit_mobile" id="edit_mobile" method="post" class="th-register-form register_form_static form-horizontal" action="<?php echo $_SERVER['PHP_SELF']; ?>?field=<?php echo $_GET["field"]; ?>&action=update">
											<div class="form-group">
												<label class="col-sm-4 control-label" for="user_login">Mobile Number</label>
												<div class="col-sm-2 control-label "><span style="color:#232222; font-size:14px;">+60</span></div>
												<div class="col-sm-6">
													<input class="form-control inputbox" name="the_input" value="<?php echo $_SESSION["profile"]["bcard_mobilephone"]; ?>">
												</div>
											</div>

											<button class="btn btn-fullcolor" type="submit" id="save" style="float:right;">Save</button>


											<script>

										jQuery( document ).ready(function($) {

											//Edit Mobile
											$("#save").click(function (e) {
												// this points to our form
												e.preventDefault();
												var z = document.forms["edit_mobile"]["the_input"].value;

												if (!$('input[name=the_input]').val()) {
													alert('Please enter your mobile no.');
													$('input[name=the_input]').focus();
													return false;

												}else if(!z.match(/^\d+/)){
												alert("Only numeric input is allowed.");

												}else {
													$("#edit_mobile").submit();
													return false;
												}
											});
										});
									</script>


										</form>
										<!-- Edit Form ends -->
										<button class="btn" type="cancel" style="float:right; margin-right:10px;" onclick="parent.location='personal-details.php'">Cancel</button>

									<?php } else if (!empty($_GET["field"]) && $_GET["field"] == 'birthdate') { ?>

										<?php
											if (!empty($_GET["action"]) && $_GET["action"] == 'update') {
												 $_SESSION["profile"]["bcard_birthdate"] = $_POST['the_input'];
												 $the_result = UpdateMemberProfile();
												 echo '<div>'.$the_result .'</div>';
											}
										?>
										<!-- Edit Form starts -->
										<form name="edit_dob" id="edit_dob" method="post" class="th-register-form register_form_static form-horizontal" action="<?php echo $_SERVER['PHP_SELF']; ?>?field=<?php echo $_GET["field"]; ?>&action=update">
											<div class="form-group">
												<label class="col-sm-4 control-label" for="user_login">Date of Birth</label>

												<div class="col-sm-6">
													<input type="date" name="the_input" id="dob" class="form-control inputbox" value="<?php $temp_dob = explode("T",$_SESSION["profile"]["bcard_birthdate"]); echo $temp_dob[0]; ?>">

													<?php /* <input class="form-control inputbox" name="the_input" value="<?php echo $_SESSION["profile"]["bcard_birthdate"]; ?>"> */?>
												</div>
											</div>

											<button class="btn btn-fullcolor" type="submit" id="save" style="float:right;">Save</button>


										</form>
										<button class="btn" type="cancek" style="float:right; margin-right:10px;" onclick="parent.location='personal-details.php'">Cancel</button>

										<!-- Edit Form ends -->

										<?php } else if (!empty($_GET["field"]) && $_GET["field"] == 'gender') { ?>

										<?php
											if (!empty($_GET["action"]) && $_GET["action"] == 'update') {
												 $_SESSION["profile"]["bcard_gender"] = $_POST['the_input'];
												 $the_result = UpdateMemberProfile();
												 echo '<div>'.$the_result .'</div>';
											}
											$possible_selection["Male"] = "M";
											$possible_selection["Female"] = "F";
										?>
										<!-- Edit Form starts -->
										<form name="edit_dob" id="edit_dob" method="post" class="th-register-form register_form_static form-horizontal" action="<?php echo $_SERVER['PHP_SELF']; ?>?field=<?php echo $_GET["field"]; ?>&action=update">
											<div class="form-group">
												<label class="col-sm-4 control-label" for="user_login">Gender</label>
												<div class="col-sm-8">
													<select class="form-control inputbox" name="the_input">
														<?php
															foreach ($possible_selection as $key => $value) {
																if ($value == $_SESSION["profile"]["bcard_gender"]) {
																	echo '<option value="'.$value.'" SELECTED>'.$key.'</option>';
																} else {
																	echo '<option value="'.$value.'">'.$key.'</option>';
																}
															}
														?>
													</select>
												</div>


												</div>


											<button class="btn btn-fullcolor" type="submit" id="save" style="float:right;">Save</button>

										</form>
										<button class="btn" type="cancel" style="float:right; margin-right:10px;" onclick="parent.location='personal-details.php'">Cancel</button>
										<!-- Edit Form ends -->

										<?php } else if (!empty($_GET["field"]) && $_GET["field"] == 'race') { ?>

										<?php
											if (!empty($_GET["action"]) && $_GET["action"] == 'update') {
												 $_SESSION["profile"]["bcard_race"] = $_POST['the_input'];
												 $the_result = UpdateMemberProfile();
												 echo '<div>'.$the_result .'</div>';
											}

											$possible_selection["Malay"] = "Malay";
											$possible_selection["Chinese"] = "Chinese";
											$possible_selection["Indian"] = "Indian";
											$possible_selection["Others"] = "Others";

										?>
										<!-- Edit Form starts -->
										<form name="edit_dob" id="edit_dob" method="post" class="th-register-form register_form_static form-horizontal" action="<?php echo $_SERVER['PHP_SELF']; ?>?field=<?php echo $_GET["field"]; ?>&action=update">
											<div class="form-group">
												<label class="col-sm-4 control-label" for="user_login">Race</label>
												<div class="col-sm-8">
													<select class="form-control inputbox" name="the_input">
														<?php
															foreach ($possible_selection as $key => $value) {
																$temp1 = strtolower($key);
																$temp2 = strtolower($_SESSION["profile"]["bcard_race"]);
																if ($temp1 == $temp2) {
																	echo '<option value="'.$value.'" SELECTED>'.$key.'</option>';
																} else {
																	echo '<option value="'.$value.'">'.$key.'</option>';
																}
															}
														?>
													</select>
												</div>


												</div>


											<button class="btn btn-fullcolor" type="submit" id="save" style="float:right;">Save</button>

										</form>
										<!-- Edit Form ends -->
										<button class="btn" type="cancel" style="float:right; margin-right:10px;" onclick="parent.location='personal-details.php'">Cancel</button>


									<?php } else if (!empty($_GET["field"]) && $_GET["field"] == 'changepassword') { ?>

										<?php
											if (!empty($_GET["action"]) && $_GET["action"] == 'update') {

												if ($_POST['new_password'] == $_POST['new_password_confirm']) {
													$the_result = ChangeMemberPassword($_POST['old_password'],$_POST['new_password']);
													echo '<div>'.$the_result .'</div>';
												} else {
													echo '<div>Error: Please make sure you have entered the new PIN correctly.</div>';
												}

											}
										?>
										<!-- Edit Form starts -->
										<form name="edit_pin" id="edit_pin" method="post" class="th-register-form register_form_static form-horizontal" action="<?php echo $_SERVER['PHP_SELF']; ?>?field=<?php echo $_GET["field"]; ?>&action=update">
											<div class="form-group">
												<label class="col-sm-4 control-label" for="user_login">Current PIN</label>
												<div class="col-sm-8">
													<input type="password" class="form-control inputbox" name="old_password" maxlength="6" onkeypress="return isNumber(event)">
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-4 control-label" for="user_login">New PIN</label>
												<div class="col-sm-8">
													<input type="password" class="form-control inputbox" name="new_password" maxlength="6" onkeypress="return isNumber(event)">
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-4 control-label" for="user_login">Confirm New PIN</label>
												<div class="col-sm-8">
													<input type="password" class="form-control inputbox" name="new_password_confirm" maxlength="6" onkeypress="return isNumber(event)">
												</div>
											</div>

											<button class="btn btn-fullcolor" type="submit" id="save" style="float:right;">Save</button>



											<script>
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		 alert('Only numbers are allowed.');
        return false;
    }
    return true;
}
	</script>

											<script>

										jQuery( document ).ready(function($) {

											//Edit PIN
											$("#save").click(function (e) {
												// this points to our form
												e.preventDefault();

												if (!$('input[name=old_password]').val()) {
													alert('Please enter your current password.');
													$('input[name=old_password]').focus();
													return false;

												} else if (!$('input[name=new_password]').val()) {
													alert('Please enter your new password.');
													$('input[name=new_password]').focus();
													return false;

												} else if (!$('input[name=new_password_confirm]').val()) {
													alert('Please confirm your new password.');
													$('input[name=new_password_confirm]').focus();
													return false;

												}else {
													$("#edit_pin").submit();
													return false;
												}
											});
										});
									</script>

										</form>
										<!-- Edit Form ends -->
										<button class="btn" type="cancel" style="float:right; margin-right:10px;" onclick="parent.location='personal-details.php'">Cancel</button>

									<?php } ?>

									</div>
				</div>
				</div>

			</div>
		</div>
	</section>

		<?php include ('bottom.php');?>

	</div><!-- end page-wrapper -->

</body>
</html>
