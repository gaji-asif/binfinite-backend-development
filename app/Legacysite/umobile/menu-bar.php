

<div class="breadcrumb">Welcome, <span style="color:#f4f5b5;"><?php echo $_SESSION["profile"]["bcard_fullname"]; ?></span> <br />Current Points Balance : <span style=" letter-spacing: 3px; color:#f4f5b5;"><?php echo PointInquiry(); ?></span>
				
			 <ul class="topnav navLeft topnav--lang">
							<li class="languages drop">
								<a href="redemption.php">
									<span class="globe glyphicon glyphicon-star icon-white"></span>
									REDEMPTIONS
								</a>
							</li>
							
							<li class="languages drop">
								<a href="#">
									<span class="globe glyphicon glyphicon-menu-hamburger icon-white"></span>
									MENU
								</a>
								<div class="pPanel">
									<ul class="inner">
										<li class="toplang-item">
											<a href="transaction-history.php">Transaction History</a>
										</li>
										<li class="toplang-item">
											<a href="redemption-history.php">Redemption History</a>
										</li>
										<li class="toplang-item">
											<a href="token-history.php">Token History</a>
										</li>
										<li class="toplang-item">
											<a href="bpoints-expiry.php">BPoints Expiry</a>
										</li>
										<li class="toplang-item">
											<a href="personal-details.php">My Profile</a>
										</li>
									</ul>
								</div>
							</li>
							
							<li class="languages drop">
								<a href="login.php?action=logout">
									<span class="globe glyphicon glyphicon-log-out icon-white"></span>
									LOGOUT
								</a>
							</li>
							
							
						</ul>
					
					
					
				</div>