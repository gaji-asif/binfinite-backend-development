<!doctype html>
<html lang="en-US" class="no-js">

<?php include '../gtm.php'; ?>

	<div id="page_wrapper">
		<?php include ('top.php');?>
		<div id="page_header" class="page-subheader site-subheader-cst uh_zn_def_header_style maskcontainer--mask3">
			<div class="bgback">
			</div>
			<div class="kl-bg-source">
			<div class="kl-bg-source__overlay"></div>
			</div>
			<div class="th-sparkles">
			</div>

		</div>

		<style>

		.hoverBorderWrapper {width:100px !important; min-height: 140px;}
		.latest_posts.style3 ul.posts li:last-child { border-bottom: 1px solid #E3E3E3; margin-bottom: 15px; padding-bottom: 15px; }
		</style>

	<section class="hg_section ptop-0">
			<div class="container">
				<div class="row">


					<div class="col-md-12 col-sm-12">


						<div id="sidebar-widget" class="sidebar">
							<div class="widget widget_recent_entries">
								<div class="latest_posts style3">
								<div class="row">

								<div class="col-md-12 col-sm-12">
									<h3>Redemption Offers</h3>
								</div>
								<br/><br /><br/>

						<?php if (isset($_SESSION["bcard_logged_in"]) && $_SESSION["bcard_logged_in"] == true) { ?>
						<?php } else { ?>
						<div class="breadcrumb">
							<form method="post" action="process-login-redemption.php">
							  <?php
								if (isset($_GET['error'])) {
									echo '<div style="color:red;">'.$error_msg.'</div>';
								}
							  ?>

							<input type="text" onkeypress="return isNumber(event)" placeholder="Your 16 digits BCard No" name="card_number" value="" maxlength="16"/>

							<input type="password" placeholder="6-digit PIN" name="card_pin" value="" maxlength="6" onkeypress="return isNumber(event)"/>

							<input type="submit" value='Login' style="background: #454444; color: #fff; border-radius:5px;">

							</form>

						</div><?php } ?>

						<?php if (isset($_SESSION["bcard_logged_in"]) && $_SESSION["bcard_logged_in"] == true) { ?>
								<?php include('menu-bar.php');?>
							<?php } ?>

								</div>

							<div class="row">
									<div class="col-md-12 col-sm-12">

									<ul class="posts">
									<?php
										//if (!empty($_GET['type']) && $_GET['type'] == 'topup') {
											echo GetTopUpGiftCatalogue();
										//} else {
											//echo GetGiftCatalogue();
										//}
									?>

									</ul>
									</div>



								</div>

								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</section>

		<?php include ('bottom.php');?>

	</div><!-- end page-wrapper -->

</body>
</html>
