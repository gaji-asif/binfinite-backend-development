<!doctype html>
<html lang="en-US" class="no-js">

<?php include '../gtm.php'; ?>

	<div id="page_wrapper">
		<?php include ('top.php');?>
		<?php if (empty($_SESSION["bcard_logged_in"]) ||  $_SESSION["bcard_logged_in"] == false) { die("Please login to continue."); } ?>
		<div id="page_header" class="page-subheader site-subheader-cst uh_zn_def_header_style maskcontainer--mask3">
			<div class="bgback">
			</div>
			<div class="kl-bg-source">
			<div class="kl-bg-source__overlay"></div>
			</div>
			<div class="th-sparkles">
			</div>

		</div>

		<section class="hg_section">
			<div class="container">
				<div class="row">

				<div class="col-md-2 col-sm-2">

				</div>

				<div class="col-md-8 col-sm-8">
				 <?php include('menu-bar.php');?>

						<div class="tabbable tabs_style2">
				<ul class="nav fixclear">
								<li><a href="personal-details.php">Personal Details</a></li>
								<li><a href="home-details.php">Home Details</a></li>
								<li><a href="office-details.php" class="clicked">Office Details</a></li>
							</ul>
				</div>
									<div class="fancy_register_form">

									<?php if (!empty($_GET["field"]) && $_GET["field"] == 'office_address') { ?>

										<?php
											if (!empty($_GET["action"]) && $_GET["action"] == 'update') {
												 $_SESSION["profile"]["bcard_officeaddress1"] = $_POST['the_input_address1'];
												 $_SESSION["profile"]["bcard_officeaddress2"] = $_POST['the_input_address2'];
												 $_SESSION["profile"]["bcard_officeaddress3"] = $_POST['the_input_address3'];
												 $_SESSION["profile"]["bcard_officecity"] = $_POST['the_input_city'];
												 $_SESSION["profile"]["bcard_officezip"] = $_POST['the_input_postcode'];
												 $_SESSION["profile"]["bcard_officestate"] = $_POST['the_input_state'];
												 $_SESSION["profile"]["bcard_officecountry"] = $_POST['the_input_country'];
												 $the_result = UpdateMemberProfile();
												 echo '<div>'.$the_result .'</div>';
											}
											$possible_selection_state = array("Johor","Kedah","Kelantan","Labuan","Melaka","Negeri Sembilan","Pahang","Perak","Perlis","Pulau Pinang","Putrajaya","Sabah","Sarawak","Selangor","Terengganu","Kuala Lumpur","Outside Malaysia");

										?>
										<!-- Edit Form starts -->
										<form name="login_form" method="post" class="th-register-form register_form_static form-horizontal" action="<?php echo $_SERVER['PHP_SELF']; ?>?field=<?php echo $_GET["field"]; ?>&action=update">

											<div class="form-group">
												<label class="col-sm-4 control-label" for="user_login">Address Line 1</label>
												<div class="col-sm-8">
													<input class="form-control inputbox" name="the_input_address1" value="<?php echo $_SESSION["profile"]["bcard_officeaddress1"]; ?>">
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-4 control-label" for="user_login">Address Line 2</label>
												<div class="col-sm-8">
													<input class="form-control inputbox" name="the_input_address2" value="<?php echo $_SESSION["profile"]["bcard_officeaddress2"]; ?>">
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-4 control-label" for="user_login">Address Line 3</label>
												<div class="col-sm-8">
													<input class="form-control inputbox" name="the_input_address3" value="<?php echo $_SESSION["profile"]["bcard_officeaddress3"]; ?>">
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-4 control-label" for="user_login">City</label>
												<div class="col-sm-8">
													<input class="form-control inputbox" name="the_input_city" value="<?php echo $_SESSION["profile"]["bcard_officecity"]; ?>">
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-4 control-label" for="user_login">PostCode</label>
												<div class="col-sm-8">
													<input class="form-control inputbox" name="the_input_postcode" value="<?php echo trim($_SESSION["profile"]["bcard_officezip"]); ?>">
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-4 control-label" for="user_login">State</label>
												<div class="col-sm-8">
													<select class="form-control inputbox" name="the_input_state">
														<?php
															foreach ($possible_selection_state as $value) {
																$temp_value1 = strtolower($value);
																$temp_value2 = strtolower(trim($_SESSION["profile"]["bcard_officestate"]));
																if ($temp_value1 == $temp_value2) {
																	echo '<option value="'.$value.'" SELECTED>'.$value.'</option>';
																} else {
																	echo '<option value="'.$value.'">'.$value.'</option>';
																}
															}
														?>
														<!--<option value="others">Others</option>-->
													</select>
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-4 control-label" for="user_login">Country</label>
												<div class="col-sm-8">
													<select class="form-control inputbox" name="the_input_country" sth="<?php echo strtolower($_SESSION["profile"]["bcard_officecountry"]); ?>">
														<?php
															foreach ($countries_list as $value) {
																$temp_value1 = strtolower($value);
																$temp_value2 = strtolower(trim($_SESSION["profile"]["bcard_officecountry"]));
																if ($temp_value1 == $temp_value2) {
																	echo '<option value="'.$value.'" SELECTED>'.$value.'</option>';
																} else {
																	echo '<option value="'.$value.'">'.$value.'</option>';
																}
															}
														?>
														<!--<option value="others">Others</option>-->
													</select>
												</div>
											</div>

											<button class="btn btn-fullcolor" type="submit" id="save" style="float:right;">Save</button>

										</form>
										<button class="btn" id="back" style="float:right; margin-right:10px;" onClick="parent.location='office-details.php'">Cancel</button>

										<!-- Edit Form ends -->

									<?php } else if (!empty($_GET["field"]) && $_GET["field"] == 'office_phone') { ?>

										<?php
											if (!empty($_GET["action"]) && $_GET["action"] == 'update') {
												 $_SESSION["profile"]["bcard_officephone"] = $_POST['the_input_phone'];
												 $_SESSION["profile"]["bcard_officeext"] = $_POST['the_input_ext'];
												 $the_result = UpdateMemberProfile();
												 echo '<div>'.$the_result .'</div>';
											}
										?>
										<!-- Edit Form starts -->
										<form name="login_form" method="post" class="th-register-form register_form_static form-horizontal" action="<?php echo $_SERVER['PHP_SELF']; ?>?field=<?php echo $_GET["field"]; ?>&action=update">
											<div class="form-group">
												<label class="col-sm-4 control-label" for="user_login">Office Phone</label>
												<div class="col-sm-8">
													<input class="form-control inputbox" name="the_input_phone" value="<?php echo trim($_SESSION["profile"]["bcard_officephone"]); ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-4 control-label" for="user_login">Office Phone Extension</label>
												<div class="col-sm-8">
													<input class="form-control inputbox" name="the_input_ext" value="<?php echo trim($_SESSION["profile"]["bcard_officeext"]); ?>">
												</div>
											</div>

											<button class="btn btn-fullcolor" type="submit" id="save" style="float:right;">Save</button>

										</form>

										<button class="btn" id="back" style="float:right; margin-right:10px;" onclick="parent.location='office-details.php'">Cancel</button>

										<!-- Edit Form ends -->


										<?php } else if (!empty($_GET["field"]) && $_GET["field"] == 'office_fax') { ?>

											<?php
												if (!empty($_GET["action"]) && $_GET["action"] == 'update') {
													 $_SESSION["profile"]["bcard_officefax"] = $_POST['the_input'];
													 $the_result = UpdateMemberProfile();
													 echo '<div>'.$the_result .'</div>';
												}
											?>
											<!-- Edit Form starts -->
											<form name="login_form" method="post" class="th-register-form register_form_static form-horizontal" action="<?php echo $_SERVER['PHP_SELF']; ?>?field=<?php echo $_GET["field"]; ?>&action=update">
												<div class="form-group">
													<label class="col-sm-4 control-label" for="user_login">Office Fax</label>
													<div class="col-sm-8">
														<input class="form-control inputbox" name="the_input" value="<?php echo trim($_SESSION["profile"]["bcard_officefax"]); ?>">
													</div>
												</div>

												<button class="btn btn-fullcolor" type="submit" id="save" style="float:right;">Save</button>

											</form>

											<button class="btn" id="back" style="float:right; margin-right:10px;" onclick="parent.location='office-details.php'">Cancel</button>


											<!-- Edit Form ends -->

											<?php } else if (!empty($_GET["field"]) && $_GET["field"] == 'office_email') { ?>

												<?php
													if (!empty($_GET["action"]) && $_GET["action"] == 'update') {
														 $_SESSION["profile"]["bcard_officeemail"] = $_POST['the_input'];
														 $the_result = UpdateMemberProfile();
														 echo '<div>'.$the_result .'</div>';
													}
												?>
												<!-- Edit Form starts -->
												<form name="login_form" method="post" class="th-register-form register_form_static form-horizontal" action="<?php echo $_SERVER['PHP_SELF']; ?>?field=<?php echo $_GET["field"]; ?>&action=update">
													<div class="form-group">
														<label class="col-sm-4 control-label" for="user_login">Office Email</label>
														<div class="col-sm-8">
															<input class="form-control inputbox" name="the_input" value="<?php echo trim($_SESSION["profile"]["bcard_officeemail"]); ?>">
														</div>
													</div>

													<button class="btn btn-fullcolor" type="submit" id="save" style="float:right;">Save</button>

												</form>
													<button class="btn" id="back" style="float:right; margin-right:10px;" onclick="parent.location='office-details.php'">Cancel</button>

												<!-- Edit Form ends -->


									<?php } ?>

									</div>
				</div>
				</div>

			</div>
		</div>
	</section>

		<?php include ('bottom.php');?>

	</div><!-- end page-wrapper -->

</body>
</html>
