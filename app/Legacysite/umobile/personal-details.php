<!doctype html>
<html lang="en-US" class="no-js">

<?php include '../gtm.php'; ?>

	<div id="page_wrapper">
		<?php include ('top.php');?><br /><br />
	<?php if (empty($_SESSION["bcard_logged_in"]) ||  $_SESSION["bcard_logged_in"] == false) { die("<p align='center'>Session timed out, please <a href='login.php'>login</a> again to access your account.</p>"); } ?>
		<div id="page_header" class="page-subheader site-subheader-cst uh_zn_def_header_style maskcontainer--mask3">
			<div class="bgback">
			</div>
			<div class="kl-bg-source">
			<div class="kl-bg-source__overlay"></div>
			</div>
			<div class="th-sparkles">
			</div>

		</div>

<style>
@media (min-width: 991px) {
.th-accordion {width:49.6%; float:left;}
}


</style>
		<section class="hg_section">
			<div class="container">
				<div class="row">


				<div class="col-md-12 col-sm-12">

				<?php include('menu-bar.php');?>

				<h3>My Profile > Personal Details</h3>

				<div class="tabbable tabs_style2">
					<ul class="nav fixclear">
								<li><a href="personal-details.php" class="clicked">Personal Details</a></li>
							<li><a href="home-details.php">Home Details</a></li>
								<li><a href="office-details.php">Office Details</a></li>
							</ul>
				</div>

				<div class="hg_accordion_element style2 ptop-25">
							<div class="th-accordion" style="margin-right: 5px;">
								<div class="acc-group editable">
									<button data-toggle="collapse" data-target="#acc8" aria-expanded="true" onClick="parent.location='edit-personal-detail.php?field=title'">Salutation<span class="acc-icon"></span>

									<div class="details"><?php echo $_SESSION["profile"]["bcard_title"]; ?></div>

									</button>

								</div>
								<div class="acc-group not-edit">
									<button data-toggle="collapse" data-target="#acc8">Full Name<span class="acc-icon"></span>
									<div class="details"><?php echo $_SESSION["profile"]["bcard_fullname"]; ?></div>
									</button>

								</div>
								<div class="acc-group not-edit">
									<button data-toggle="collapse" data-target="#acc8">IC / Passport<span class="acc-icon"></span>
									<div class="details"><?php echo $_SESSION["profile"]["bcard_ic"]; ?></div>
									</button>

								</div>
								<div class="acc-group editable">
									<button data-toggle="collapse" data-target="#acc8" onClick="parent.location='edit-personal-detail.php?field=birthdate'">Date of Birth<span class="acc-icon"></span>
									<div class="details"><?php $temp_dob = explode("T",$_SESSION["profile"]["bcard_birthdate"]); echo $temp_dob[0]; ?></div>
									</button>
								</div>

								<div class="acc-group editable">
									<button data-toggle="collapse" data-target="#acc8" onClick="parent.location='edit-personal-detail.php?field=gender'">Gender<span class="acc-icon"></span>
									<div class="details"><?php echo $_SESSION["profile"]["bcard_gender"]; ?></div>
									</button>
								</div>

								<div class="acc-group editable">
									<button data-toggle="collapse" data-target="#acc8" onClick="parent.location='edit-personal-detail.php?field=race'">Race<span class="acc-icon"></span>
									<div class="details"><?php echo $_SESSION["profile"]["bcard_race"]; ?></div>
									</button>
								</div>

								<div class="acc-group editable">
									<button data-toggle="collapse" data-target="#acc8" onClick="parent.location='edit-personal-detail.php?field=nationality'">Nationality<span class="acc-icon"></span>
									<div class="details"><?php echo $_SESSION["profile"]["bcard_nationality"]; ?></div>
									</button>
								</div>



							</div>
						</div>
						<!-- end // accordion texts  -->

					<div class="hg_accordion_element style2">

							<div class="th-accordion" style="margin-right: 5px;">

								<div class="acc-group editable">
									<button data-toggle="collapse" data-target="#acc8" onClick="parent.location='edit-personal-detail.php?field=maritalstatus'">Marital Status<span class="acc-icon"></span>
									<div class="details"><?php echo $_SESSION["profile"]["bcard_maritalstatus"]; ?></div>
									</button>
								</div>

								<div class="acc-group editable">
									<button data-toggle="collapse" data-target="#acc8" onClick="parent.location='edit-personal-detail.php?field=ownacar'">Own a Car<span class="acc-icon"></span>
									<div class="details"><?php echo $_SESSION["profile"]["bcard_ownacar"]; ?></div>
									</button>
								</div>

								<div class="acc-group editable">
									<button data-toggle="collapse" data-target="#acc8" onClick="parent.location='edit-personal-detail.php?field=ownacreditcard'">Own a Credit Card<span class="acc-icon"></span>
									<div class="details"><?php echo $_SESSION["profile"]["bcard_ownacreditcard"]; ?></div>
									</button>
								</div>

								<div class="acc-group editable">
									<button data-toggle="collapse" data-target="#acc8" onClick="parent.location='edit-personal-detail.php?field=primaryemail'">Email<span class="acc-icon"></span>
									<div class="details"><?php echo $_SESSION["profile"]["bcard_primaryemail"]; ?></div>
									</button>

								</div>


								<div class="acc-group editable">
									<button data-toggle="collapse" data-target="#acc8" onClick="parent.location='edit-personal-detail.php?field=mobilephone'">Mobile Phone<span class="acc-icon"></span>
									<div class="details"><?php echo $_SESSION["profile"]["bcard_mobilephone"]; ?></div>
									</button>
								</div>

								<div class="acc-group editable">
									<button data-toggle="collapse" data-target="#acc8" onClick="parent.location='edit-personal-detail.php?field=changepassword'">PIN<span class="acc-icon"></span>
									<div class="details">******</div>
									</button>
								</div>



							</div>
						</div>
						<!-- end // accordion texts  -->

				</div>
				</div>

			</div>
		</div>
	</section>

		<?php include ('bottom.php');?>

	</div><!-- end page-wrapper -->

</body>
</html>
