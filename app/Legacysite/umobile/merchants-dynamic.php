<!doctype html>

<html lang="en-US" class="no-js">

<?php include '../gtm.php'; ?>

	<div id="page_wrapper">
		<?php include ('top.php');?>
		<?php //include ('../wordpress-config.php');?>
		<div id="page_header" class="page-subheader site-subheader-cst uh_zn_def_header_style maskcontainer--mask3">
			<div class="bgback">
			</div>
			<div class="kl-bg-source">
			<div class="kl-bg-source__overlay"></div>
			</div>
			<div class="th-sparkles">
			</div>

		</div>

		<style>

		@media (max-width : 767px) {
			.element-item {width:50%;}
		}
		.form-module input, select {
			background: #ff840b;
			color:#fff;
			padding: 5px 15px;
		}

		@media only screen
  and (min-device-width: 768px)
  and (max-device-width: 1024px)
  and (orientation: landscape)
  and (-webkit-min-device-pixel-ratio: 1) {
	  .element-item {width:20%;}
  }

	@media (min-width: 768px) and (max-width: 991px) {
		.element-item {width:16%;}
	}
		</style>

	<section class="hg_section ptop-80">
			<div class="hg_section_size container">
				<div class="row">

				<div class="col-md-12 col-sm-12">

					<h3>Merchants</h3>
					<?php //dd(App\Category::forClass(\App\Merchant::class)); ?>
					<p>
					<select class="filters-select">
					  <option value="*">All</option>
					  <?php
					  	$cats = App\Category::forClass(\App\Merchant::class);


					 //  	$cats = [
					 //  		'.banking-finance' => 'Banking &amp; Finance',
						// 	'.e-commerce' => 'E-Commerce',
						// 	'.education' => 'Education',
						// 	'.fb' => 'F&amp;B',
						// 	'.health-beauty' => 'Health &amp; Beauty',
						// 	'.leisure-recreation' => 'Leisure &amp; Recreation',
						// 	'.motor-fuel' => 'Motor &amp; Fuel',
						// 	'.others' => 'Others',
						// 	'.retail' => 'Retail',
						// 	'.utilities' => 'Utilities'
						// ];

						// $tax = 'merchant_categories';

						// // get the terms of taxonomy
						// $terms = get_terms( $tax, [
						//   'hide_empty' => false, // do not hide empty terms
						// ]);



						// loop through all terms
						foreach( $cats as $cat ) {
							echo '<option value=".'.$cat->slug.'">'.$cat->name.'</option>';
						}

						?>


					</select>

					</p>

		<div class="row">
	<div class="col-md-12">
				<div class="grid">

				<?php // Wordpress Starts
					if (empty($_GET['category'])) {
					} else {
						$category = $_GET['category'];
					}
					$merchants = [
						'rakuten' => [
							'id' => '1',
					        'name' => 'Rakuten',
					        'category' => 'retail',
					        'logoImage' => asset('images/rkt.png'),
					        'bannerImage1' => asset('images/rkt1.png'),
					        'bannerImage2' => asset('images/rkt2.png'),
					    ],
					    '711' => [
							'id' => '2',
					        'name' => '7-11',
					        'category' => 'retail',
					        'logoImage' => asset('images/711-logo.png'),
					        'bannerImage1' => asset('images/rkt1.png'),
					        'bannerImage2' => asset('images/711-banner.jpg'),
					    ],
					    '11street' => [
							'id' => '3',
					        'name' => '11th Street',
					        'category' => 'retail',
					        'logoImage' => asset('images/11street-logo.png'),
					        'bannerImage1' => asset('images/rkt1.png'),
					        'bannerImage2' => asset('images/11street-banner.png'),
					    ],
					];

					//$myposts = get_posts( $args );
					foreach ( $merchants as $idx => $merchant ) : //setup_postdata( $post ); ?>

						<?php //$merchant_cat_slug = get_the_terms( get_the_ID(), 'merchant_categories' ); ?>
						<div class="col-md-2 element-item <?= $merchant['category'] ?>" data-category="<?= $merchant['category'] ?>">
							<div class="box image-boxes imgboxes_style1">
								<a class="hoverBorder imgboxes-wrapper" href="merchant-details.php?id=<?= $merchant['id'] ?>">
									<!-- Border image wrapper -->
									<span class="hoverBorderWrapper">
										<!-- Image -->
										<img src="<?= $merchant['logoImage'] ?>">
										<!--/ Image -->
									</span>
									<!--/ Border image wrapper -->

									<!-- Button
									<h6>7-ELEVEN</h6>-->
									<!--/ Button -->
								</a>

							</div>
							<!-- end span -->
						</div>

					<?php endforeach; ?>



				</div><!-- end grid -->
					</div>
			</div>

				</div>

				</div>


			</div>
		</section>

		<?php include ('bottom.php');?>


<script>

// init Isotope
var $grid = $('.grid').isotope({
  itemSelector: '.element-item',
  layoutMode: 'fitRows'
});
// filter functions
var filterFns = {
  // show if number is greater than 50
  numberGreaterThan50: function() {
    var number = $(this).find('.number').text();
    return parseInt( number, 10 ) > 50;
  },
  // show if name ends with -ium
  ium: function() {
    var name = $(this).find('.name').text();
    return name.match( /ium$/ );
  }
};
// bind filter on select change
$('.filters-select').on( 'change', function() {
  // get filter value from option value
  var filterValue = this.value;
  // use filterFn if matches value
  filterValue = filterFns[ filterValue ] || filterValue;
  $grid.isotope({ filter: filterValue });
});

</script>


	</div><!-- end page-wrapper -->

</body>

</html>
