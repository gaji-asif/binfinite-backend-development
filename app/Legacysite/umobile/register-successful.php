<!doctype html>
<html lang="en-US" class="no-js">

<?php include '../gtm.php'; ?>

	<div id="page_wrapper">
		<?php include ('top.php');?>
		<div id="page_header" class="page-subheader site-subheader-cst uh_zn_def_header_style maskcontainer--mask3">
			<div class="bgback">
			</div>
			<div class="kl-bg-source">
			<div class="kl-bg-source__overlay"></div>
			</div>
			<div class="th-sparkles">
			</div>


		</div>

	<style>.breadcrumb {
    padding: 10px 20px;
    background: #e36d1c;
    color: #fff;}
	</style>

		<script>
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		 alert('Only numbers are allowed.');
        return false;
    }
    return true;
}
	</script>


	<script>
	function lettersOnly(evt) {
       evt = (evt) ? evt : event;
       var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
          ((evt.which) ? evt.which : 0));
       if (charCode > 32 && (charCode < 65 || charCode > 90) &&
          (charCode < 97 || charCode > 122)) {
          alert("Only letters are allowed.");
          return false;
       }
       return true;
     }
	</script>

	<script>

	function isAlfa(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 32 && (charCode < 48 || charCode > 57) && (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122)) {
        alert("Special characters are not allowed.");
		return false;
    }
    return true;
}
	</script>

	<script>
	function isAlfaNoSpace(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57) && (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122)) {
        alert("Special characters and spaces are not allowed.");
		return false;
    }
    return true;
}
	</script>

		<section class="hg_section">
			<div class="container">
				<div class="row">



				<div class="col-md-12 col-sm-12">


						<h3>Step 2 : U Card Registration</h3>
						<div class="fancy_register_form">
								You have successfully registered for U Card - the exclusive rewards programme for U Mobile customers. <br />
								To start U Card Redemption, click <a href="https://u.com.my/ucard/redemption">here</a>
						</div>

				</div>

				</div>

			</div>
		</div>
	</section>
		<?php include('sign-up-form-validation.php');?>
		<?php include ('bottom.php');?>

	</div><!-- end page-wrapper -->

</body>
</html>
