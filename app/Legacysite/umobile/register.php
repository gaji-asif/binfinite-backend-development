<!doctype html>
<html lang="en-US" class="no-js">

<?php include '../gtm.php'; ?>

	<div id="page_wrapper">
		<?php include ('top.php');?>
		<div id="page_header" class="page-subheader site-subheader-cst uh_zn_def_header_style maskcontainer--mask3">
			<div class="bgback">
			</div>
			<div class="kl-bg-source">
			<div class="kl-bg-source__overlay"></div>
			</div>
			<div class="th-sparkles">
			</div>


		</div>

	<style>.breadcrumb {
    padding: 10px 20px;
    background: #e36d1c;
    color: #fff;}
	</style>

		<script>
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		 alert('Only numbers are allowed.');
        return false;
    }
    return true;
}
	</script>


	<script>
	function lettersOnly(evt) {
       evt = (evt) ? evt : event;
       var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
          ((evt.which) ? evt.which : 0));
       if (charCode > 32 && (charCode < 65 || charCode > 90) &&
          (charCode < 97 || charCode > 122)) {
          alert("Only letters are allowed.");
          return false;
       }
       return true;
     }
	</script>

	<script>

	function isAlfa(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 32 && (charCode < 48 || charCode > 57) && (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122)) {
        alert("Special characters are not allowed.");
		return false;
    }
    return true;
}
	</script>

	<script>
	function isAlfaNoSpace(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57) && (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122)) {
        alert("Special characters and spaces are not allowed.");
		return false;
    }
    return true;
}
	</script>

		<section class="hg_section">
			<div class="container">
				<div class="row">



				<div class="col-md-12 col-sm-12">


						<h3>Step 2 : U Card Registration</h3>
						<div class="fancy_register_form">

				<?php
					//$_SESSION['umobile_number'] = '601127039687';
					if (empty($_SESSION['umobile_number'])) {

						echo 'Something went wrong';

					} else {

						if (!empty($_GET['action']) && $_GET['action'] == "submit") {

							if (!empty($_POST['reg_Card']) && !empty($_POST['gender']) && !empty($_POST['reg_first_name']) ) {
							/*
								$_POST['reg_Title'] = '';
								$_POST['reg_MaritalStatus'] = '';
								$_POST['reg_OwnCar'] = '';
								$_POST['reg_OwnCreditCard'] = '';
								$_POST['reg_HomeCity'] = '';
								$_POST['reg_HomeCountry'] = 'Malaysia';
								$_POST['reg_HomePhone'] = '';
								$_POST['reg_OfficeAddress1'] = '';
								$_POST['reg_OfficeAddress2'] = '';
								$_POST['reg_OfficeAddress3'] = '';
								$_POST['reg_OfficeCity'] = '';
								$_POST['reg_OfficeState'] = '';
								$_POST['reg_OfficeCountry'] = '';
								$_POST['reg_OfficeZip'] = '';
								$_POST['reg_OfficeEmail'] = '';
								$_POST['reg_OfficePhone'] = '';
								$_POST['reg_OfficeExt'] = '';
								$_POST['reg_OfficeFax'] = '';
							*/

								$the_fullname = $_POST['reg_first_name'] . " " . $_POST['reg_last_name'];

								$the_result = RegisterMember(
									$_POST['reg_Card'],
									$_POST['reg_Title'],
									$the_fullname,
									$_POST['reg_IC'],
									$_POST['reg_BirthDate'],
									$_POST['gender'],
									$_POST['race'],
									$_POST['nationality'],
									$_POST['reg_MaritalStatus'],
									$_POST['reg_OwnCar'],
									$_POST['reg_OwnCreditCard'],
									$_POST['reg_HomeAddress1'],
									$_POST['reg_HomeAddress2'],
									$_POST['reg_HomeAddress3'],
									$_POST['reg_HomeCity'],
									$_POST['reg_HomeState'],
									$_POST['reg_HomeCountry'],
									$_POST['reg_HomeZip'],
									$_POST['reg_HomePhone'],
									$_POST['reg_HomeEmail'],
									$_POST['reg_MobilePhone'],
									$_POST['reg_OfficeAddress1'],
									$_POST['reg_OfficeAddress2'],
									$_POST['reg_OfficeAddress3'],
									$_POST['reg_OfficeCity'],
									$_POST['reg_OfficeState'],
									$_POST['reg_OfficeCountry'],
									$_POST['reg_OfficeZip'],
									$_POST['reg_OfficeEmail'],
									$_POST['reg_OfficePhone'],
									$_POST['reg_OfficeExt'],
									$_POST['reg_OfficeFax'],
									$_POST['reg_MSISDN']
								);


								echo $the_result;


							} else {

									echo "Error: Please fill in all required fields.";

							}

						}

				?>
										<!-- beginning of form -->
										<form name="sing-up-form" method="post" class="th-register-form register_form_static form-horizontal " action="<?php echo $_SERVER["PHP_SELF"]; ?>?action=submit" id="sign-up-form">
										<div class="breadcrumb">Personal Details</div>
											<input type="hidden" name="reg_MSISDN" value="<?php echo $_SESSION['umobile_number']; ?>" />

											<div class="form-group">

											<label class="col-sm-2 control-label">UCard Number <span style="color:red;">*</span></label>
											<div class="col-sm-4">
												<input type="text" name="reg_Card" id="ucard_number" class="form-control inputbox" required="yes" maxlength="16" placeholder="62984359066111234"  readonly="readonly" value="<?php echo $_SESSION['umobile_card']; ?>" onkeypress="return isNumber(event)">
											</div>

											<label class="col-sm-2 control-label">Salutation <span style="color:red;">*</span></label>
												<div class="col-sm-4">
														<select class="form-control inputbox" name="reg_Title">
														<option value="MR">Mr </option>
														<option value="MRS">Mrs</option>
														<option value="MISS">Miss</option>
													</select>
												</div>
										</div>


											<div class="form-group">
												<label class="col-sm-2 control-label">First Name <span style="color:red;">*</span></label>
												<div class="col-sm-4">
													<input type="text" name="reg_first_name" id="first_name" class="form-control inputbox" required="" placeholder="First Name" maxlength="30" onkeypress="return lettersOnly(event)">
												</div>

												<label class="col-sm-2 control-label">Last Name <span style="color:red;">*</span></label>
												<div class="col-sm-4">
													<input type="text" name="reg_last_name" id="last_name" class="form-control inputbox" required="" placeholder="Last Name" maxlength="30" onkeypress="return lettersOnly(event)">
												</div>


											</div>




											<div class="form-group">

												<label class="col-sm-2 control-label">Gender <span style="color:red;">*</span></label>
												<div class="col-sm-4">
														<select class="form-control inputbox" name="gender">
														<option value="male">Male</option>
														<option value="female">Female</option>
													</select>
												</div>

												<label class="col-sm-2 control-label">Marital Status <span style="color:red;">*</span></label>
												<div class="col-sm-4">
														<select class="form-control inputbox" name="reg_MaritalStatus">
														<option value="Single">Single</option>
														<option value="Married">Married</option>
													</select>
												</div>

											</div>


											<div class="form-group">
												<label class="col-sm-2 control-label">IC / Passport <span style="color:red;">*</span></label>
												<div class="col-sm-4">
													<input type="text" name="reg_IC" id="reg_IC" class="form-control inputbox" required="" placeholder="921111101234" maxlength="12" onkeypress="return isAlfaNoSpace(event)">
												</div>

												<label class="col-sm-2 control-label">Date of Birth <span style="color:red;">*</span></label>
												<div class="col-sm-4">
													<input type="date" name="reg_BirthDate" id="reg_BirthDate" class="form-control inputbox" required="" placeholder="1987-01-01">
												</div>

											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label">Race <span style="color:red;">*</span></label>
												<div class="col-sm-4">
														<select class="form-control inputbox" name="race">
														<option value="malay">Malay</option>
														<option value="chinese">Chinese</option>
														<option value="indian">Indian</option>
														<option value="others">Others</option>
													</select>
												</div>

												<label class="col-sm-2 control-label">Nationality <span style="color:red;">*</span></label>
												<div class="col-sm-4">
														<select class="form-control inputbox" name="nationality">
														<option value="Afghanistan">Afghanistan</option>
							<option value="Aland Islands">Aland Islands</option>
							<option value="Albania">Albania</option>
							<option value="Algeria">Algeria</option>
							<option value="American Samoa">American Samoa</option>
							<option value="Andorra">Andorra</option>
							<option value="Angola">Angola</option>
							<option value="Anguilla">Anguilla</option>
							<option value="Antarctica">Antarctica</option>
							<option value="Antigua and Barbuda">Antigua and Barbuda</option>
							<option value="Argentina">Argentina</option>
							<option value="Armenia">Armenia</option>
							<option value="Aruba">Aruba</option>
							<option value="Australia">Australia</option>
							<option value="Austria">Austria</option>
							<option value="Azerbaijan">Azerbaijan</option>
							<option value="Bahamas">Bahamas</option>
							<option value="Bahrain">Bahrain</option>
							<option value="Bangladesh">Bangladesh</option>
							<option value="Barbados">Barbados</option>
							<option value="Belarus">Belarus</option>
							<option value="Belgium">Belgium</option>
							<option value="Belize">Belize</option>
							<option value="Benin">Benin</option>
							<option value="Bermuda">Bermuda</option>
							<option value="Bhutan">Bhutan</option>
							<option value="Bolivia">Bolivia</option>
							<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
							<option value="Botswana">Botswana</option>
							<option value="Bouvet Island">Bouvet Island</option>
							<option value="Brazil">Brazil</option>
							<option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
							<option value="British Virgin Islands">British Virgin Islands</option>
							<option value="Brunei">Brunei</option>
							<option value="Bulgaria">Bulgaria</option>
							<option value="Burkina Faso">Burkina Faso</option>
							<option value="Burundi">Burundi</option>
							<option value="Cambodia">Cambodia</option>
							<option value="Cameroon">Cameroon</option>
							<option value="Canada">Canada</option>
							<option value="Cape Verde">Cape Verde</option>
							<option value="Cayman Islands">Cayman Islands</option>
							<option value="Central African Republic">Central African Republic</option>
							<option value="Chad">Chad</option>
							<option value="Chile">Chile</option>
							<option value="China">China</option>
							<option value="Christmas Island">Christmas Island</option>
							<option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
							<option value="Colombia">Colombia</option>
							<option value="Comoros">Comoros</option>
							<option value="Congo">Congo</option>
							<option value="Cook Islands">Cook Islands</option>
							<option value="Costa Rica">Costa Rica</option>
							<option value="Croatia">Croatia</option>
							<option value="Cuba">Cuba</option>
							<option value="Cyprus">Cyprus</option>
							<option value="Czech Republic">Czech Republic</option>
							<option value="Democratic Republic of Congo">Democratic Republic of Congo</option>
							<option value="Denmark">Denmark</option>
							<option value="Disputed Territory">Disputed Territory</option>
							<option value="Djibouti">Djibouti</option>
							<option value="Dominica">Dominica</option>
							<option value="Dominican Republic">Dominican Republic</option>
							<option value="East Timor">East Timor</option>
							<option value="Ecuador">Ecuador</option>
							<option value="Egypt">Egypt</option>
							<option value="El Salvador">El Salvador</option>
							<option value="Equatorial Guinea">Equatorial Guinea</option>
							<option value="Eritrea">Eritrea</option>
							<option value="Estonia">Estonia</option>
							<option value="Ethiopia">Ethiopia</option>
							<option value="Falkland Islands">Falkland Islands</option>
							<option value="Faroe Islands">Faroe Islands</option>
							<option value="Federated States of Micronesia">Federated States of Micronesia</option>
							<option value="Fiji">Fiji</option>
							<option value="Finland">Finland</option>
							<option value="France">France</option>
							<option value="French Guyana">French Guyana</option>
							<option value="French Polynesia">French Polynesia</option>
							<option value="French Southern Territories">French Southern Territories</option>
							<option value="Gabon">Gabon</option>
							<option value="Gambia">Gambia</option>
							<option value="Georgia">Georgia</option>
							<option value="Germany">Germany</option>
							<option value="Ghana">Ghana</option>
							<option value="Gibraltar">Gibraltar</option>
							<option value="Greece">Greece</option>
							<option value="Greenland">Greenland</option>
							<option value="Grenada">Grenada</option>
							<option value="Guadeloupe">Guadeloupe</option>
							<option value="Guam">Guam</option>
							<option value="Guatemala">Guatemala</option>
							<option value="Guinea">Guinea</option>
							<option value="Guinea-Bissau">Guinea-Bissau</option>
							<option value="Guyana">Guyana</option>
							<option value="Haiti">Haiti</option>
							<option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option>
							<option value="Honduras">Honduras</option>
							<option value="Hong Kong">Hong Kong</option>
							<option value="Hungary">Hungary</option>
							<option value="Iceland">Iceland</option>
							<option value="India">India</option>
							<option value="Indonesia">Indonesia</option>
							<option value="Iran">Iran</option>
							<option value="Iraq">Iraq</option>
							<option value="Iraq-Saudi Arabia Neutral Zone">Iraq-Saudi Arabia Neutral Zone</option>
							<option value="Ireland">Ireland</option>
							<option value="Israel">Israel</option>
							<option value="Italy">Italy</option>
							<option value="Ivory Coast">Ivory Coast</option>
							<option value="Jamaica">Jamaica</option>
							<option value="Japan">Japan</option>
							<option value="Jordan">Jordan</option>
							<option value="Kazakhstan">Kazakhstan</option>
							<option value="Kenya">Kenya</option>
							<option value="Kiribati">Kiribati</option>
							<option value="Kuwait">Kuwait</option>
							<option value="Kyrgyzstan">Kyrgyzstan</option>
							<option value="Laos">Laos</option>
							<option value="Latvia">Latvia</option>
							<option value="Lebanon">Lebanon</option>
							<option value="Lesotho">Lesotho</option>
							<option value="Liberia">Liberia</option>
							<option value="Libya">Libya</option>
							<option value="Liechtenstein">Liechtenstein</option>
							<option value="Lithuania">Lithuania</option>
							<option value="Luxembourg">Luxembourg</option>
							<option value="Macau">Macau</option>
							<option value="Macedonia">Macedonia</option>
							<option value="Madagascar">Madagascar</option>
							<option value="Malawi">Malawi</option>
							<option value="Malaysia" selected="">Malaysia</option>
							<option value="Maldives">Maldives</option>
							<option value="Mali">Mali</option>
							<option value="Malta">Malta</option>
							<option value="Marshall Islands">Marshall Islands</option>
							<option value="Martinique">Martinique</option>
							<option value="Mauritania">Mauritania</option>
							<option value="Mauritius">Mauritius</option>
							<option value="Mayotte">Mayotte</option>
							<option value="Mexico">Mexico</option>
							<option value="Moldova">Moldova</option>
							<option value="Monaco">Monaco</option>
							<option value="Mongolia">Mongolia</option>
							<option value="Montenegro">Montenegro</option>
							<option value="Montserrat">Montserrat</option>
							<option value="Morocco">Morocco</option>
							<option value="Mozambique">Mozambique</option>
							<option value="Myanmar">Myanmar</option>
							<option value="Namibia">Namibia</option>
							<option value="Nauru">Nauru</option>
							<option value="Nepal">Nepal</option>
							<option value="Netherlands Antilles">Netherlands Antilles</option>
							<option value="Netherlands">Netherlands</option>
							<option value="New Caledonia">New Caledonia</option>
							<option value="New Zealand">New Zealand</option>
							<option value="Nicaragua">Nicaragua</option>
							<option value="Niger">Niger</option>
							<option value="Nigeria">Nigeria</option>
							<option value="Niue">Niue</option>
							<option value="Norfolk Island">Norfolk Island</option>
							<option value="North Korea">North Korea</option>
							<option value="Northern Mariana Islands">Northern Mariana Islands</option>
							<option value="Norway">Norway</option>
							<option value="Oman">Oman</option>
							<option value="Pakistan">Pakistan</option>
							<option value="Palau">Palau</option>
							<option value="Palestinian Territories">Palestinian Territories</option>
							<option value="Panama">Panama</option>
							<option value="Papua New Guinea">Papua New Guinea</option>
							<option value="Paraguay">Paraguay</option>
							<option value="Peru">Peru</option>
							<option value="Philippines">Philippines</option>
							<option value="Pitcairn Islands">Pitcairn Islands</option>
							<option value="Poland">Poland</option>
							<option value="Portugal">Portugal</option>
							<option value="Puerto Rico">Puerto Rico</option>
							<option value="Qatar">Qatar</option>
							<option value="Reunion">Reunion</option>
							<option value="Romania">Romania</option>
							<option value="Russia">Russia</option>
							<option value="Rwanda">Rwanda</option>
							<option value="Saint Helena and Dependencies">Saint Helena and Dependencies</option>
							<option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
							<option value="Saint Lucia">Saint Lucia</option>
							<option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
							<option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
							<option value="Samoa">Samoa</option>
							<option value="San Marino">San Marino</option>
							<option value="Sao Tome and Principe">Sao Tome and Principe</option>
							<option value="Saudi Arabia">Saudi Arabia</option>
							<option value="Senegal">Senegal</option>
							<option value="Serbia">Serbia</option>
							<option value="Seychelles">Seychelles</option>
							<option value="Sierra Leone">Sierra Leone</option>
							<option value="Singapore">Singapore</option>
							<option value="Slovakia">Slovakia</option>
							<option value="Slovenia">Slovenia</option>
							<option value="Solomon Islands">Solomon Islands</option>
							<option value="Somalia">Somalia</option>
							<option value="South Africa">South Africa</option>
							<option value="South Georgia and South Sandwich Islands">South Georgia and South Sandwich Islands</option>
							<option value="South Korea">South Korea</option>
							<option value="Spain">Spain</option>
							<option value="Spratly Islands">Spratly Islands</option>
							<option value="Sri Lanka">Sri Lanka</option>
							<option value="Sudan">Sudan</option>
							<option value="Suriname">Suriname</option>
							<option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
							<option value="Swaziland">Swaziland</option>
							<option value="Sweden">Sweden</option>
							<option value="Switzerland">Switzerland</option>
							<option value="Syria">Syria</option>
							<option value="Taiwan">Taiwan</option>
							<option value="Tajikistan">Tajikistan</option>
							<option value="Tanzania">Tanzania</option>
							<option value="Thailand">Thailand</option>
							<option value="Togo">Togo</option>
							<option value="Tokelau">Tokelau</option>
							<option value="Tonga">Tonga</option>
							<option value="Trinidad and Tobago">Trinidad and Tobago</option>
							<option value="Tunisia">Tunisia</option>
							<option value="Turkey">Turkey</option>
							<option value="Turkmenistan">Turkmenistan</option>
							<option value="Turks And Caicos Islands">Turks And Caicos Islands</option>
							<option value="Tuvalu">Tuvalu</option>
							<option value="US Virgin Islands">US Virgin Islands</option>
							<option value="Uganda">Uganda</option>
							<option value="Ukraine">Ukraine</option>
							<option value="United Arab Emirates">United Arab Emirates</option>
							<option value="United Kingdom">United Kingdom</option>
							<option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
							<option value="United States">United States</option>
							<option value="Uruguay">Uruguay</option>
							<option value="Uzbekistan">Uzbekistan</option>
							<option value="Vanuatu">Vanuatu</option>
							<option value="Vatican City">Vatican City</option>
							<option value="Venezuela">Venezuela</option>
							<option value="Vietnam">Vietnam</option>
							<option value="Wallis and Futuna">Wallis and Futuna</option>
							<option value="Western Sahara">Western Sahara</option>
							<option value="Yemen">Yemen</option>
							<option value="Zambia">Zambia</option>
							<option value="Zimbabwe">Zimbabwe</option>
													</select>
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-2 control-label">Own A Car <span style="color:red;">*</span></label>
												<div class="col-sm-4">
														<select class="form-control inputbox" name="reg_OwnCar">
														<option value="yes">Yes</option>
														<option value="no">No</option>
													</select>
												</div>

												<label class="col-sm-2 control-label">Own A Credit Card <span style="color:red;">*</span></label>
												<div class="col-sm-4">
														<select class="form-control inputbox" name="reg_OwnCreditCard">
														<option value="yes">Yes</option>
														<option value="no">No</option>
													</select>
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-2 control-label">Email <span style="color:red;">*</span></label>
												<div class="col-sm-4">
													<input type="text" name="reg_HomeEmail" id="reg_HomeEmail" class="form-control inputbox" required="" placeholder="example@hotmail.com">
												</div>

												<label class="col-sm-2 control-label">Mobile Phone <span style="color:red;">*</span></label>
												<!--<div class="col-sm-1 control-label "><span style="color:#232222; font-size:14px;">+6</span></div>-->
												<div class="col-sm-4">
													<input type="text" name="reg_MobilePhone" id="reg_MobilePhone" class="form-control inputbox" required="" placeholder="0181234567" maxlength="12" onkeypress="return isNumber(event)">
												</div>



											</div>

											<br />
											<div class="breadcrumb">Home Details</div>
											<div class="form-group">
												<label class="col-sm-2 control-label">Address Line 1 <span style="color:red;">*</span></label>
												<div class="col-sm-4">
													<input type="text" name="reg_HomeAddress1" id="address1" class="form-control inputbox" required="" placeholder="Address">
												</div>

												<label class="col-sm-2 control-label">Country <span style="color:red;">*</span></label>
												<div class="col-sm-4">
														<select class="form-control inputbox" name="reg_HomeCountry">
														<option value="Afghanistan">Afghanistan</option>
							<option value="Aland Islands">Aland Islands</option>
							<option value="Albania">Albania</option>
							<option value="Algeria">Algeria</option>
							<option value="American Samoa">American Samoa</option>
							<option value="Andorra">Andorra</option>
							<option value="Angola">Angola</option>
							<option value="Anguilla">Anguilla</option>
							<option value="Antarctica">Antarctica</option>
							<option value="Antigua and Barbuda">Antigua and Barbuda</option>
							<option value="Argentina">Argentina</option>
							<option value="Armenia">Armenia</option>
							<option value="Aruba">Aruba</option>
							<option value="Australia">Australia</option>
							<option value="Austria">Austria</option>
							<option value="Azerbaijan">Azerbaijan</option>
							<option value="Bahamas">Bahamas</option>
							<option value="Bahrain">Bahrain</option>
							<option value="Bangladesh">Bangladesh</option>
							<option value="Barbados">Barbados</option>
							<option value="Belarus">Belarus</option>
							<option value="Belgium">Belgium</option>
							<option value="Belize">Belize</option>
							<option value="Benin">Benin</option>
							<option value="Bermuda">Bermuda</option>
							<option value="Bhutan">Bhutan</option>
							<option value="Bolivia">Bolivia</option>
							<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
							<option value="Botswana">Botswana</option>
							<option value="Bouvet Island">Bouvet Island</option>
							<option value="Brazil">Brazil</option>
							<option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
							<option value="British Virgin Islands">British Virgin Islands</option>
							<option value="Brunei">Brunei</option>
							<option value="Bulgaria">Bulgaria</option>
							<option value="Burkina Faso">Burkina Faso</option>
							<option value="Burundi">Burundi</option>
							<option value="Cambodia">Cambodia</option>
							<option value="Cameroon">Cameroon</option>
							<option value="Canada">Canada</option>
							<option value="Cape Verde">Cape Verde</option>
							<option value="Cayman Islands">Cayman Islands</option>
							<option value="Central African Republic">Central African Republic</option>
							<option value="Chad">Chad</option>
							<option value="Chile">Chile</option>
							<option value="China">China</option>
							<option value="Christmas Island">Christmas Island</option>
							<option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
							<option value="Colombia">Colombia</option>
							<option value="Comoros">Comoros</option>
							<option value="Congo">Congo</option>
							<option value="Cook Islands">Cook Islands</option>
							<option value="Costa Rica">Costa Rica</option>
							<option value="Croatia">Croatia</option>
							<option value="Cuba">Cuba</option>
							<option value="Cyprus">Cyprus</option>
							<option value="Czech Republic">Czech Republic</option>
							<option value="Democratic Republic of Congo">Democratic Republic of Congo</option>
							<option value="Denmark">Denmark</option>
							<option value="Disputed Territory">Disputed Territory</option>
							<option value="Djibouti">Djibouti</option>
							<option value="Dominica">Dominica</option>
							<option value="Dominican Republic">Dominican Republic</option>
							<option value="East Timor">East Timor</option>
							<option value="Ecuador">Ecuador</option>
							<option value="Egypt">Egypt</option>
							<option value="El Salvador">El Salvador</option>
							<option value="Equatorial Guinea">Equatorial Guinea</option>
							<option value="Eritrea">Eritrea</option>
							<option value="Estonia">Estonia</option>
							<option value="Ethiopia">Ethiopia</option>
							<option value="Falkland Islands">Falkland Islands</option>
							<option value="Faroe Islands">Faroe Islands</option>
							<option value="Federated States of Micronesia">Federated States of Micronesia</option>
							<option value="Fiji">Fiji</option>
							<option value="Finland">Finland</option>
							<option value="France">France</option>
							<option value="French Guyana">French Guyana</option>
							<option value="French Polynesia">French Polynesia</option>
							<option value="French Southern Territories">French Southern Territories</option>
							<option value="Gabon">Gabon</option>
							<option value="Gambia">Gambia</option>
							<option value="Georgia">Georgia</option>
							<option value="Germany">Germany</option>
							<option value="Ghana">Ghana</option>
							<option value="Gibraltar">Gibraltar</option>
							<option value="Greece">Greece</option>
							<option value="Greenland">Greenland</option>
							<option value="Grenada">Grenada</option>
							<option value="Guadeloupe">Guadeloupe</option>
							<option value="Guam">Guam</option>
							<option value="Guatemala">Guatemala</option>
							<option value="Guinea">Guinea</option>
							<option value="Guinea-Bissau">Guinea-Bissau</option>
							<option value="Guyana">Guyana</option>
							<option value="Haiti">Haiti</option>
							<option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option>
							<option value="Honduras">Honduras</option>
							<option value="Hong Kong">Hong Kong</option>
							<option value="Hungary">Hungary</option>
							<option value="Iceland">Iceland</option>
							<option value="India">India</option>
							<option value="Indonesia">Indonesia</option>
							<option value="Iran">Iran</option>
							<option value="Iraq">Iraq</option>
							<option value="Iraq-Saudi Arabia Neutral Zone">Iraq-Saudi Arabia Neutral Zone</option>
							<option value="Ireland">Ireland</option>
							<option value="Israel">Israel</option>
							<option value="Italy">Italy</option>
							<option value="Ivory Coast">Ivory Coast</option>
							<option value="Jamaica">Jamaica</option>
							<option value="Japan">Japan</option>
							<option value="Jordan">Jordan</option>
							<option value="Kazakhstan">Kazakhstan</option>
							<option value="Kenya">Kenya</option>
							<option value="Kiribati">Kiribati</option>
							<option value="Kuwait">Kuwait</option>
							<option value="Kyrgyzstan">Kyrgyzstan</option>
							<option value="Laos">Laos</option>
							<option value="Latvia">Latvia</option>
							<option value="Lebanon">Lebanon</option>
							<option value="Lesotho">Lesotho</option>
							<option value="Liberia">Liberia</option>
							<option value="Libya">Libya</option>
							<option value="Liechtenstein">Liechtenstein</option>
							<option value="Lithuania">Lithuania</option>
							<option value="Luxembourg">Luxembourg</option>
							<option value="Macau">Macau</option>
							<option value="Macedonia">Macedonia</option>
							<option value="Madagascar">Madagascar</option>
							<option value="Malawi">Malawi</option>
							<option value="Malaysia" selected="">Malaysia</option>
							<option value="Maldives">Maldives</option>
							<option value="Mali">Mali</option>
							<option value="Malta">Malta</option>
							<option value="Marshall Islands">Marshall Islands</option>
							<option value="Martinique">Martinique</option>
							<option value="Mauritania">Mauritania</option>
							<option value="Mauritius">Mauritius</option>
							<option value="Mayotte">Mayotte</option>
							<option value="Mexico">Mexico</option>
							<option value="Moldova">Moldova</option>
							<option value="Monaco">Monaco</option>
							<option value="Mongolia">Mongolia</option>
							<option value="Montenegro">Montenegro</option>
							<option value="Montserrat">Montserrat</option>
							<option value="Morocco">Morocco</option>
							<option value="Mozambique">Mozambique</option>
							<option value="Myanmar">Myanmar</option>
							<option value="Namibia">Namibia</option>
							<option value="Nauru">Nauru</option>
							<option value="Nepal">Nepal</option>
							<option value="Netherlands Antilles">Netherlands Antilles</option>
							<option value="Netherlands">Netherlands</option>
							<option value="New Caledonia">New Caledonia</option>
							<option value="New Zealand">New Zealand</option>
							<option value="Nicaragua">Nicaragua</option>
							<option value="Niger">Niger</option>
							<option value="Nigeria">Nigeria</option>
							<option value="Niue">Niue</option>
							<option value="Norfolk Island">Norfolk Island</option>
							<option value="North Korea">North Korea</option>
							<option value="Northern Mariana Islands">Northern Mariana Islands</option>
							<option value="Norway">Norway</option>
							<option value="Oman">Oman</option>
							<option value="Pakistan">Pakistan</option>
							<option value="Palau">Palau</option>
							<option value="Palestinian Territories">Palestinian Territories</option>
							<option value="Panama">Panama</option>
							<option value="Papua New Guinea">Papua New Guinea</option>
							<option value="Paraguay">Paraguay</option>
							<option value="Peru">Peru</option>
							<option value="Philippines">Philippines</option>
							<option value="Pitcairn Islands">Pitcairn Islands</option>
							<option value="Poland">Poland</option>
							<option value="Portugal">Portugal</option>
							<option value="Puerto Rico">Puerto Rico</option>
							<option value="Qatar">Qatar</option>
							<option value="Reunion">Reunion</option>
							<option value="Romania">Romania</option>
							<option value="Russia">Russia</option>
							<option value="Rwanda">Rwanda</option>
							<option value="Saint Helena and Dependencies">Saint Helena and Dependencies</option>
							<option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
							<option value="Saint Lucia">Saint Lucia</option>
							<option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
							<option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
							<option value="Samoa">Samoa</option>
							<option value="San Marino">San Marino</option>
							<option value="Sao Tome and Principe">Sao Tome and Principe</option>
							<option value="Saudi Arabia">Saudi Arabia</option>
							<option value="Senegal">Senegal</option>
							<option value="Serbia">Serbia</option>
							<option value="Seychelles">Seychelles</option>
							<option value="Sierra Leone">Sierra Leone</option>
							<option value="Singapore">Singapore</option>
							<option value="Slovakia">Slovakia</option>
							<option value="Slovenia">Slovenia</option>
							<option value="Solomon Islands">Solomon Islands</option>
							<option value="Somalia">Somalia</option>
							<option value="South Africa">South Africa</option>
							<option value="South Georgia and South Sandwich Islands">South Georgia and South Sandwich Islands</option>
							<option value="South Korea">South Korea</option>
							<option value="Spain">Spain</option>
							<option value="Spratly Islands">Spratly Islands</option>
							<option value="Sri Lanka">Sri Lanka</option>
							<option value="Sudan">Sudan</option>
							<option value="Suriname">Suriname</option>
							<option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
							<option value="Swaziland">Swaziland</option>
							<option value="Sweden">Sweden</option>
							<option value="Switzerland">Switzerland</option>
							<option value="Syria">Syria</option>
							<option value="Taiwan">Taiwan</option>
							<option value="Tajikistan">Tajikistan</option>
							<option value="Tanzania">Tanzania</option>
							<option value="Thailand">Thailand</option>
							<option value="Togo">Togo</option>
							<option value="Tokelau">Tokelau</option>
							<option value="Tonga">Tonga</option>
							<option value="Trinidad and Tobago">Trinidad and Tobago</option>
							<option value="Tunisia">Tunisia</option>
							<option value="Turkey">Turkey</option>
							<option value="Turkmenistan">Turkmenistan</option>
							<option value="Turks And Caicos Islands">Turks And Caicos Islands</option>
							<option value="Tuvalu">Tuvalu</option>
							<option value="US Virgin Islands">US Virgin Islands</option>
							<option value="Uganda">Uganda</option>
							<option value="Ukraine">Ukraine</option>
							<option value="United Arab Emirates">United Arab Emirates</option>
							<option value="United Kingdom">United Kingdom</option>
							<option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
							<option value="United States">United States</option>
							<option value="Uruguay">Uruguay</option>
							<option value="Uzbekistan">Uzbekistan</option>
							<option value="Vanuatu">Vanuatu</option>
							<option value="Vatican City">Vatican City</option>
							<option value="Venezuela">Venezuela</option>
							<option value="Vietnam">Vietnam</option>
							<option value="Wallis and Futuna">Wallis and Futuna</option>
							<option value="Western Sahara">Western Sahara</option>
							<option value="Yemen">Yemen</option>
							<option value="Zambia">Zambia</option>
							<option value="Zimbabwe">Zimbabwe</option>
													</select>

											</div>
										</div>

											<div class="form-group">

												<label class="col-sm-2 control-label">Address Line 2</label>
												<div class="col-sm-4">
													<input type="text" name="reg_HomeAddress2" id="address2" class="form-control inputbox" required="" placeholder="Address">
												</div>

												<label class="col-sm-2 control-label">State <span style="color:red;">*</span></label>
												<div class="col-sm-4">
														<select class="form-control inputbox" name="reg_HomeState">
														<option value="">-Select One- ( Malaysia states only )</option>
														<option value="Johor">Johor</option>
														<option value="Kedah">Kedah</option>
														<option value="Kelantan">Kelantan</option>
														<option value="Labuan">Labuan</option>
														<option value="Melaka">Melaka</option>
														<option value="Negeri Sembilan">Negeri Sembilan</option>
														<option value="Pahang">Pahang</option>
														<option value="Perak">Perak</option>
														<option value="Perlis">Perlis</option>
														<option value="Pulau Pinang">Pulau Pinang</option>
														<option value="Putrajaya">Putrajaya</option>
														<option value="Sabah">Sabah</option>
														<option value="Sarawak">Sarawak</option>
														<option value="Selangor">Selangor</option>
														<option value="Terengganu">Terengganu</option>
														<option value="Kuala Lumpur">Kuala Lumpur</option>
													</select>
												</div>
											</div>

												<div class="form-group">

											<label class="col-sm-2 control-label">Address Line 3</label>
												<div class="col-sm-4">
													<input type="text" name="reg_HomeAddress3" id="address3" class="form-control inputbox" required="" placeholder="Address" maxlength="45">
												</div>

												<label class="col-sm-2 control-label">City </label>
													<div class="col-sm-4">
														<input type="text" name="reg_HomeCity" id="home_city" class="form-control inputbox" required="" placeholder="Kuala Lumpur" maxlength="30" onkeypress="return lettersOnly(event)">
													</div>

												</div>

												<div class="form-group">
												<label class="col-sm-2 control-label">Home Phone <span style="color:red;">*</span></label>
												<div class="col-sm-1 control-label"><span style="color:#232222; font-size:14px;">+6</span></div>
												<div class="col-sm-3">
													<input type="text" name="reg_HomePhone" id="home_no" class="form-control inputbox" required="" placeholder="0312345678" maxlength="12" onkeypress="return isNumber(event)">
												</div>

												<label class="col-sm-2 control-label">Postcode <span  style="color:red;">*</span></label>
												<div class="col-sm-4">
													<input type="text" name="reg_HomeZip" id="postcode" class="form-control inputbox" required="" placeholder="55100" maxlength="5" onkeypress="return isNumber(event)">
												</div>

											</div>
										<br />
										<div class="breadcrumb">Office Details</div>

										<div class="form-group">
											<label class="col-sm-2 control-label">Address Line 1</label>
												<div class="col-sm-4">
													<input type="text" name="reg_OfficeAddress1" id="address1_office" class="form-control inputbox" required="" placeholder="Address">
												</div>


												<label class="col-sm-2 control-label">Country</label>
												<div class="col-sm-4">
														<select class="form-control inputbox" name="reg_OfficeCountry">

														<option value="Afghanistan">Afghanistan</option>
							<option value="Aland Islands">Aland Islands</option>
							<option value="Albania">Albania</option>
							<option value="Algeria">Algeria</option>
							<option value="American Samoa">American Samoa</option>
							<option value="Andorra">Andorra</option>
							<option value="Angola">Angola</option>
							<option value="Anguilla">Anguilla</option>
							<option value="Antarctica">Antarctica</option>
							<option value="Antigua and Barbuda">Antigua and Barbuda</option>
							<option value="Argentina">Argentina</option>
							<option value="Armenia">Armenia</option>
							<option value="Aruba">Aruba</option>
							<option value="Australia">Australia</option>
							<option value="Austria">Austria</option>
							<option value="Azerbaijan">Azerbaijan</option>
							<option value="Bahamas">Bahamas</option>
							<option value="Bahrain">Bahrain</option>
							<option value="Bangladesh">Bangladesh</option>
							<option value="Barbados">Barbados</option>
							<option value="Belarus">Belarus</option>
							<option value="Belgium">Belgium</option>
							<option value="Belize">Belize</option>
							<option value="Benin">Benin</option>
							<option value="Bermuda">Bermuda</option>
							<option value="Bhutan">Bhutan</option>
							<option value="Bolivia">Bolivia</option>
							<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
							<option value="Botswana">Botswana</option>
							<option value="Bouvet Island">Bouvet Island</option>
							<option value="Brazil">Brazil</option>
							<option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
							<option value="British Virgin Islands">British Virgin Islands</option>
							<option value="Brunei">Brunei</option>
							<option value="Bulgaria">Bulgaria</option>
							<option value="Burkina Faso">Burkina Faso</option>
							<option value="Burundi">Burundi</option>
							<option value="Cambodia">Cambodia</option>
							<option value="Cameroon">Cameroon</option>
							<option value="Canada">Canada</option>
							<option value="Cape Verde">Cape Verde</option>
							<option value="Cayman Islands">Cayman Islands</option>
							<option value="Central African Republic">Central African Republic</option>
							<option value="Chad">Chad</option>
							<option value="Chile">Chile</option>
							<option value="China">China</option>
							<option value="Christmas Island">Christmas Island</option>
							<option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
							<option value="Colombia">Colombia</option>
							<option value="Comoros">Comoros</option>
							<option value="Congo">Congo</option>
							<option value="Cook Islands">Cook Islands</option>
							<option value="Costa Rica">Costa Rica</option>
							<option value="Croatia">Croatia</option>
							<option value="Cuba">Cuba</option>
							<option value="Cyprus">Cyprus</option>
							<option value="Czech Republic">Czech Republic</option>
							<option value="Democratic Republic of Congo">Democratic Republic of Congo</option>
							<option value="Denmark">Denmark</option>
							<option value="Disputed Territory">Disputed Territory</option>
							<option value="Djibouti">Djibouti</option>
							<option value="Dominica">Dominica</option>
							<option value="Dominican Republic">Dominican Republic</option>
							<option value="East Timor">East Timor</option>
							<option value="Ecuador">Ecuador</option>
							<option value="Egypt">Egypt</option>
							<option value="El Salvador">El Salvador</option>
							<option value="Equatorial Guinea">Equatorial Guinea</option>
							<option value="Eritrea">Eritrea</option>
							<option value="Estonia">Estonia</option>
							<option value="Ethiopia">Ethiopia</option>
							<option value="Falkland Islands">Falkland Islands</option>
							<option value="Faroe Islands">Faroe Islands</option>
							<option value="Federated States of Micronesia">Federated States of Micronesia</option>
							<option value="Fiji">Fiji</option>
							<option value="Finland">Finland</option>
							<option value="France">France</option>
							<option value="French Guyana">French Guyana</option>
							<option value="French Polynesia">French Polynesia</option>
							<option value="French Southern Territories">French Southern Territories</option>
							<option value="Gabon">Gabon</option>
							<option value="Gambia">Gambia</option>
							<option value="Georgia">Georgia</option>
							<option value="Germany">Germany</option>
							<option value="Ghana">Ghana</option>
							<option value="Gibraltar">Gibraltar</option>
							<option value="Greece">Greece</option>
							<option value="Greenland">Greenland</option>
							<option value="Grenada">Grenada</option>
							<option value="Guadeloupe">Guadeloupe</option>
							<option value="Guam">Guam</option>
							<option value="Guatemala">Guatemala</option>
							<option value="Guinea">Guinea</option>
							<option value="Guinea-Bissau">Guinea-Bissau</option>
							<option value="Guyana">Guyana</option>
							<option value="Haiti">Haiti</option>
							<option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option>
							<option value="Honduras">Honduras</option>
							<option value="Hong Kong">Hong Kong</option>
							<option value="Hungary">Hungary</option>
							<option value="Iceland">Iceland</option>
							<option value="India">India</option>
							<option value="Indonesia">Indonesia</option>
							<option value="Iran">Iran</option>
							<option value="Iraq">Iraq</option>
							<option value="Iraq-Saudi Arabia Neutral Zone">Iraq-Saudi Arabia Neutral Zone</option>
							<option value="Ireland">Ireland</option>
							<option value="Israel">Israel</option>
							<option value="Italy">Italy</option>
							<option value="Ivory Coast">Ivory Coast</option>
							<option value="Jamaica">Jamaica</option>
							<option value="Japan">Japan</option>
							<option value="Jordan">Jordan</option>
							<option value="Kazakhstan">Kazakhstan</option>
							<option value="Kenya">Kenya</option>
							<option value="Kiribati">Kiribati</option>
							<option value="Kuwait">Kuwait</option>
							<option value="Kyrgyzstan">Kyrgyzstan</option>
							<option value="Laos">Laos</option>
							<option value="Latvia">Latvia</option>
							<option value="Lebanon">Lebanon</option>
							<option value="Lesotho">Lesotho</option>
							<option value="Liberia">Liberia</option>
							<option value="Libya">Libya</option>
							<option value="Liechtenstein">Liechtenstein</option>
							<option value="Lithuania">Lithuania</option>
							<option value="Luxembourg">Luxembourg</option>
							<option value="Macau">Macau</option>
							<option value="Macedonia">Macedonia</option>
							<option value="Madagascar">Madagascar</option>
							<option value="Malawi">Malawi</option>
							<option value="Malaysia" selected="">Malaysia</option>
							<option value="Maldives">Maldives</option>
							<option value="Mali">Mali</option>
							<option value="Malta">Malta</option>
							<option value="Marshall Islands">Marshall Islands</option>
							<option value="Martinique">Martinique</option>
							<option value="Mauritania">Mauritania</option>
							<option value="Mauritius">Mauritius</option>
							<option value="Mayotte">Mayotte</option>
							<option value="Mexico">Mexico</option>
							<option value="Moldova">Moldova</option>
							<option value="Monaco">Monaco</option>
							<option value="Mongolia">Mongolia</option>
							<option value="Montenegro">Montenegro</option>
							<option value="Montserrat">Montserrat</option>
							<option value="Morocco">Morocco</option>
							<option value="Mozambique">Mozambique</option>
							<option value="Myanmar">Myanmar</option>
							<option value="Namibia">Namibia</option>
							<option value="Nauru">Nauru</option>
							<option value="Nepal">Nepal</option>
							<option value="Netherlands Antilles">Netherlands Antilles</option>
							<option value="Netherlands">Netherlands</option>
							<option value="New Caledonia">New Caledonia</option>
							<option value="New Zealand">New Zealand</option>
							<option value="Nicaragua">Nicaragua</option>
							<option value="Niger">Niger</option>
							<option value="Nigeria">Nigeria</option>
							<option value="Niue">Niue</option>
							<option value="Norfolk Island">Norfolk Island</option>
							<option value="North Korea">North Korea</option>
							<option value="Northern Mariana Islands">Northern Mariana Islands</option>
							<option value="Norway">Norway</option>
							<option value="Oman">Oman</option>
							<option value="Pakistan">Pakistan</option>
							<option value="Palau">Palau</option>
							<option value="Palestinian Territories">Palestinian Territories</option>
							<option value="Panama">Panama</option>
							<option value="Papua New Guinea">Papua New Guinea</option>
							<option value="Paraguay">Paraguay</option>
							<option value="Peru">Peru</option>
							<option value="Philippines">Philippines</option>
							<option value="Pitcairn Islands">Pitcairn Islands</option>
							<option value="Poland">Poland</option>
							<option value="Portugal">Portugal</option>
							<option value="Puerto Rico">Puerto Rico</option>
							<option value="Qatar">Qatar</option>
							<option value="Reunion">Reunion</option>
							<option value="Romania">Romania</option>
							<option value="Russia">Russia</option>
							<option value="Rwanda">Rwanda</option>
							<option value="Saint Helena and Dependencies">Saint Helena and Dependencies</option>
							<option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
							<option value="Saint Lucia">Saint Lucia</option>
							<option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
							<option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
							<option value="Samoa">Samoa</option>
							<option value="San Marino">San Marino</option>
							<option value="Sao Tome and Principe">Sao Tome and Principe</option>
							<option value="Saudi Arabia">Saudi Arabia</option>
							<option value="Senegal">Senegal</option>
							<option value="Serbia">Serbia</option>
							<option value="Seychelles">Seychelles</option>
							<option value="Sierra Leone">Sierra Leone</option>
							<option value="Singapore">Singapore</option>
							<option value="Slovakia">Slovakia</option>
							<option value="Slovenia">Slovenia</option>
							<option value="Solomon Islands">Solomon Islands</option>
							<option value="Somalia">Somalia</option>
							<option value="South Africa">South Africa</option>
							<option value="South Georgia and South Sandwich Islands">South Georgia and South Sandwich Islands</option>
							<option value="South Korea">South Korea</option>
							<option value="Spain">Spain</option>
							<option value="Spratly Islands">Spratly Islands</option>
							<option value="Sri Lanka">Sri Lanka</option>
							<option value="Sudan">Sudan</option>
							<option value="Suriname">Suriname</option>
							<option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
							<option value="Swaziland">Swaziland</option>
							<option value="Sweden">Sweden</option>
							<option value="Switzerland">Switzerland</option>
							<option value="Syria">Syria</option>
							<option value="Taiwan">Taiwan</option>
							<option value="Tajikistan">Tajikistan</option>
							<option value="Tanzania">Tanzania</option>
							<option value="Thailand">Thailand</option>
							<option value="Togo">Togo</option>
							<option value="Tokelau">Tokelau</option>
							<option value="Tonga">Tonga</option>
							<option value="Trinidad and Tobago">Trinidad and Tobago</option>
							<option value="Tunisia">Tunisia</option>
							<option value="Turkey">Turkey</option>
							<option value="Turkmenistan">Turkmenistan</option>
							<option value="Turks And Caicos Islands">Turks And Caicos Islands</option>
							<option value="Tuvalu">Tuvalu</option>
							<option value="US Virgin Islands">US Virgin Islands</option>
							<option value="Uganda">Uganda</option>
							<option value="Ukraine">Ukraine</option>
							<option value="United Arab Emirates">United Arab Emirates</option>
							<option value="United Kingdom">United Kingdom</option>
							<option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
							<option value="United States">United States</option>
							<option value="Uruguay">Uruguay</option>
							<option value="Uzbekistan">Uzbekistan</option>
							<option value="Vanuatu">Vanuatu</option>
							<option value="Vatican City">Vatican City</option>
							<option value="Venezuela">Venezuela</option>
							<option value="Vietnam">Vietnam</option>
							<option value="Wallis and Futuna">Wallis and Futuna</option>
							<option value="Western Sahara">Western Sahara</option>
							<option value="Yemen">Yemen</option>
							<option value="Zambia">Zambia</option>
							<option value="Zimbabwe">Zimbabwe</option>
													</select>
											</div>

							</div>

											<div class="form-group">

												<label class="col-sm-2 control-label">Address Line 2</label>
												<div class="col-sm-4">
													<input type="text" name="reg_OfficeAddress2" id="address2_office" class="form-control inputbox" required="" placeholder="Address" maxlength="45">
												</div>

												<label class="col-sm-2 control-label">State</label>
												<div class="col-sm-4">
														<select class="form-control inputbox" name="reg_OfficeState">
														<option value="">-Select One- ( Malaysia states only )</option>
														<option value="Johor">Johor</option>
														<option value="Kedah">Kedah</option>
														<option value="Kelantan">Kelantan</option>
														<option value="Labuan">Labuan</option>
														<option value="Melaka">Melaka</option>
														<option value="Negeri Sembilan">Negeri Sembilan</option>
														<option value="Pahang">Pahang</option>
														<option value="Perak">Perak</option>
														<option value="Perlis">Perlis</option>
														<option value="Pulau Pinang">Pulau Pinang</option>
														<option value="Putrajaya">Putrajaya</option>
														<option value="Sabah">Sabah</option>
														<option value="Sarawak">Sarawak</option>
														<option value="Selangor">Selangor</option>
														<option value="Terengganu">Terengganu</option>
														<option value="Kuala Lumpur">Kuala Lumpur</option>
													</select>
												</div>

											</div>


											<div class="form-group">

												<label class="col-sm-2 control-label">Address Line 3</label>
												<div class="col-sm-4">
													<input type="text" name="reg_OfficeAddress3" id="address3_office" class="form-control inputbox" required="" placeholder="Address">
												</div>

												<label class="col-sm-2 control-label">City</label>
												<div class="col-sm-4">
													<input type="text" name="reg_OfficeCity" id="office_city" class="form-control inputbox" required="" placeholder="" maxlength="30" onkeypress="return lettersOnly(event)">
												</div>
											</div>

											<div class="form-group">

											<label class="col-sm-2 control-label">Office Phone</label>
												<div class="col-sm-1 control-label "><span style="color:#232222; font-size:14px;">+6</span></div>
												<div class="col-sm-2">
													<input type="text" name="reg_OfficePhone" id="office_no" class="form-control inputbox" required="" placeholder="0312345678" maxlength="12" onkeypress="return isNumber(event)">
												</div>
												<div class="col-sm-1">
													<input type="text" name="reg_OfficeExt" id="office_no_ext" class="form-control inputbox" required="" placeholder="Ext" maxlength="3" onkeypress="return isNumber(event)">
												</div>

											<label class="col-sm-2 control-label">Postcode</label>
											<div class="col-sm-4">
													<input type="text" name="reg_OfficeZip" id="postcode_office" class="form-control inputbox" required="" placeholder="55100" maxlength="5" onkeypress="return isNumber(event)">
												</div>
											</div>

											<div class="form-group">

												<label class="col-sm-2 control-label">Office Fax</label>
												<div class="col-sm-1 control-label "><span style="color:#232222; font-size:14px;">+6</span></div>
												<div class="col-sm-3">
													<input type="text" name="reg_OfficeFax" id="office_fax" class="form-control inputbox" required="" placeholder="0312345678" maxlength="12" onkeypress="return isNumber(event)">
												</div>


												<label class="col-sm-2 control-label">Work Email</label>
												<div class="col-sm-4">
													<input type="text" name="reg_OfficeEmail" id="second-email" class="form-control inputbox" required="" placeholder="example@hotmail.com">
												</div>
											</div>

											<br />

                <div class="form-group" style="font-size:12px;">
                  <input type="checkbox" id="tnc" name="tnc" value="1" required>&nbsp;&nbsp;I agree to the UCard <a href="/terms.php" target="_blank">Terms & Conditions</a><br /><br />
				Fields marked with <span style="color:red;"> * </span> are mandatory.

				</div>
				<button class="btn btn-fullcolor" type="submit" id="sign-up" style="float:right;">Submit</button>
							</form>

				<?php } ?>
									</div>

				</div>

				</div>

			</div>
		</div>
	</section>
		<?php include('sign-up-form-validation.php');?>
		<?php include ('bottom.php');?>

	</div><!-- end page-wrapper -->

</body>
</html>
