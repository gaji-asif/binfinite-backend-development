<?php
	ob_start();
	session_start();
	require_once('config.php');
	if (!empty($_GET['action']) && $_GET['action'] == 'logout') {
		$_SESSION["bcard_logged_in"] = false;
		unset($_SESSION["bcard_number"]);
		unset($_SESSION["bcard_password"]);
		unset($_SESSION["profile"]);
	}
?>

	<title>U Mobile iFrame</title>

	<!-- meta -->
	<meta charset="UTF-8">
	<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1">

	<link rel="stylesheet" type="text/css" href="fonts/open-sans.css" />

	<meta name="keywords" content="Malaysia Loyalty Card, Loyalty Program in Malaysia, Online Redemption, BCard, B Infinite, Redemption Programme, Berjaya Corporation, Berjaya Card, BLoyalty" />
	<meta name="description" content="B Infinite (formerly BCARD) is a Loyalty Program in Malaysia by BLoyalty Sdn Bhd, a subsidiary of Berjaya Corporation Bhd">


	<!-- Google Fonts CSS Stylesheet // More here http://www.google.com/fonts#UsePlace:use/Collection:Open+Sans
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400italic,400,600,600italic,700,800,800italic" rel="stylesheet" type="text/css">
	<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>-->

	<!-- ***** Boostrap Custom / Addons Stylesheets ***** -->
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/sliders/ios/style.css" type="text/css" media="all">

	<link rel="stylesheet" href="css/template.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/responsive.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/base-sizing.css" type="text/css" media="all">
	<!-- Updates CSS Stylesheet -->
	<link rel="stylesheet" href="css/updates.css" type="text/css" />
	<!-- Custom CSS Stylesheet (where you should add your own css rules) -->
	<link rel="stylesheet" href="css/custom.css" type="text/css" />


	<!-- Modernizr Library -->
	<script type="text/javascript" src="js/vendor/modernizr.min.js"></script>

	<!-- jQuery Library -->
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src= "js/vendor/jquery-1.10.1.min.js"></script>
	<script type="text/javascript" src="js/jquery-migrate-1.2.1.js"></script>




	<script src="js/platform.js" async defer></script>
	<script type="text/javascript" async defer src="//assets.pinterest.com/js/pinit.js"></script>
