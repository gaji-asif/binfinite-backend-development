<!doctype html>

<html lang="en-US" class="no-js">

<?php include '../gtm.php'; ?>
	
	<div id="page_wrapper">
		<?php include ('top.php');?>
		<div id="page_header" class="page-subheader site-subheader-cst uh_zn_def_header_style maskcontainer--mask3">
			<div class="bgback">
			</div>
			<div class="kl-bg-source">
			<div class="kl-bg-source__overlay"></div>
			</div>
			<div class="th-sparkles">
			</div>

		</div>

	<section class="hg_section ptop-40">
			<div class="hg_section_size container">
				<div class="row">


						<div class="latest_posts default-style kl-style-2 container">
							<div class="row gutter-sm">

								<div class="col-md-12 col-sm-12">
							<div class="text_box" style="text-align:justify;">
							<!-- Form Module-->
						<div class="module form-module">
							<div class="toggle"><i class="fa fa-times fa-pencil"></i>
								<div class="tooltip">Click Me</div>
							</div>
						  <div class="form">
							<!-- Jeff mod starts -->
							<h2>MEMBER LOGIN</h2>
							<form method="post" action="process-login.php">
							  <?php
								if (isset($_GET['error'])) {
									echo '<div style="color:red;">'.$error_msg.'</div>';
								}
							  ?>
							  <input type="text" onkeypress="return isNumber(event)" placeholder="Your 16 digits BCard No" name="card_number" value="" maxlength="16"/>
							  <input type="password" placeholder="6-digit PIN" name="card_pin" value="" maxlength="6" onkeypress="return isNumber(event)"/>
							  <input type="submit" value='Login' style="background: #ea6f25; color: #fff; border-radius:5px;">
								<!-- <div class="ctb"><a href="sign-up.php">First time login? <br />Register your BCard here.</a></div> -->
							</form>
							<!-- Jeff mod ends -->
						  </div>

							<div class="cta"><a href="forgot-pin.php">Forgot your PIN?</a></div>
						</div>

							</div>

				</div>
			</div>
						<!-- end // latest posts style 2 -->

				</div>
			</div>
		</section>

	<script>
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		 alert('Only numbers are allowed.');
        return false;
    }
    return true;
}
	</script>
		<?php include ('bottom.php');?>

	</div><!-- end page-wrapper -->

</body>
</html>
