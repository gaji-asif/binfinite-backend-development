<?php

	session_start();
	$config_api_url = 'https://dev.bcard.com.my/bcardwsapi/service.asmx?wsdl';
	$config_ws_key = '345185882';
	$config_company_code = 'P0007';
	$config_brand_code = 'WEBSITE';

	$_SESSION["bcard_number"] = "6298430000010469";
	$_SESSION["bcard_password"] = "111111";
	
	
	echo '<pre>';
	echo GetMemberProfile();
	echo '<pre>';
	
	/* -----------------------------------------------------------------------------------------*/
	
	
	// Retrieve Transaction History
	function GetRewardHistory() {

		global $config_api_url, $config_ws_key, $config_company_code, $config_brand_code;

		$client = new SoapClient($config_api_url);

		$result = $client->GetRewardHistory(array(
				"WSKey" => $config_ws_key,
				"WSCompanyCode" => $config_company_code,
				"WSBranchCode" => $config_brand_code ,
				"Card" => $_SESSION["bcard_number"],
				"Password" => $_SESSION["bcard_password"]));
		
		
		//var_dump($result);
		//var_dump($result->GetGiftCatalogueResult->GetGiftCatalogueResultDetails);
		
		$output = '';
	
		if (isset($result->GetRewardHistoryResult->ResponseHeader->ErrorCode) && $result->GetRewardHistoryResult->ResponseHeader->ErrorCode == '00') {
			
			$the_result = $result->GetRewardHistoryResult->GetRewardHistoryResultDetails;
		
			// Check if loop is available
			if (!empty($the_result) && count($the_result) == 1) {
				
				$output.= $the_result->RewardID . '<br>';
			    $output.= $the_result->RewardDate . '<br>';
				$output.= $the_result->TotalAmount . '<br>';
			    $output.= $the_result->TotalPoint . '<br>';
				$output.= $the_result->MerchantName . '<br>';
			    $output.= $the_result->CompanyCode . '<br>';
				$output.= $the_result->BranchCode . '<br>';
			    $output.= $the_result->POSID . '<br>';
			    $output.= '<br><br>';
								
			} else if (!empty($the_result) && count($the_result) > 1) {
								
				foreach ($the_result as $obj) {
					$output.= $obj->RewardID . '<br>';
					$output.= $obj->RewardDate . '<br>';
					$output.= $obj->TotalAmount . '<br>';
					$output.= $obj->TotalPoint . '<br>';
					$output.= $obj->MerchantName . '<br>';
					$output.= $obj->CompanyCode . '<br>';
					$output.= $obj->BranchCode . '<br>';
					$output.= $obj->POSID . '<br>';
					$output.= '<br><br>';
				}
				
			} else {
			
				$output = 'No record found.';
				
			}
		
		} else {
			
			$output = 'Something went wrong.';
			
		}
		
		return $output;
	}
	
	/* -----------------------------------------------------------------------------------------*/
	
	
	// Retrieve Redemption History
	function GetRedeemHistory() {

		global $config_api_url, $config_ws_key, $config_company_code, $config_brand_code;

		$client = new SoapClient($config_api_url);

		$result = $client->GetRedeemHistory(array(
				"WSKey" => $config_ws_key,
				"WSCompanyCode" => $config_company_code,
				"WSBranchCode" => $config_brand_code ,
				"Card" => $_SESSION["bcard_number"],
				"Password" => $_SESSION["bcard_password"]));
		
		
		//var_dump($result);
		//var_dump($result->GetGiftCatalogueResult->GetGiftCatalogueResultDetails);
		
		$output = '';
	
		if (isset($result->GetRedeemHistoryResult->ResponseHeader->ErrorCode) && $result->GetRedeemHistoryResult->ResponseHeader->ErrorCode == '00') {
			
			$the_result = $result->GetRedeemHistoryResult->GetRedeemHistoryResultDetails;
		
			// Check if loop is available
			if (!empty($the_result) && count($the_result) == 1) {
				
				$output.= $the_result->RewardID . '<br>';
			    $output.= $the_result->RewardDate . '<br>';
				$output.= $the_result->TotalAmount . '<br>';
			    $output.= $the_result->TotalPoint . '<br>';
				$output.= $the_result->MerchantName . '<br>';
			    $output.= $the_result->CompanyCode . '<br>';
				$output.= $the_result->BranchCode . '<br>';
			    $output.= $the_result->POSID . '<br>';
			    $output.= '<br><br>';
								
			} else if (!empty($the_result) && count($the_result) > 1) {
								
				foreach ($the_result as $obj) {
					$output.= $obj->RewardID . '<br>';
					$output.= $obj->RewardDate . '<br>';
					$output.= $obj->TotalAmount . '<br>';
					$output.= $obj->TotalPoint . '<br>';
					$output.= $obj->MerchantName . '<br>';
					$output.= $obj->CompanyCode . '<br>';
					$output.= $obj->BranchCode . '<br>';
					$output.= $obj->POSID . '<br>';
					$output.= '<br><br>';
				}
				
			} else {
			
				$output = 'No record found.';
				
			}
		
		} else {
			
			$output = 'Something went wrong.';
			
		}
		
		return $output;
	}
	
	/* -----------------------------------------------------------------------------------------*/
	
	// Retrieve Point History
	function GetPointHistory() {

		global $config_api_url, $config_ws_key, $config_company_code, $config_brand_code;

		$client = new SoapClient($config_api_url);

		$result = $client->GetPointHistory(array(
				"WSKey" => $config_ws_key,
				"WSCompanyCode" => $config_company_code,
				"WSBranchCode" => $config_brand_code ,
				"Card" => $_SESSION["bcard_number"],
				"Password" => $_SESSION["bcard_password"]));
		
		
		//var_dump($result);
		//var_dump($result->GetGiftCatalogueResult->GetGiftCatalogueResultDetails);
		
		$output = '';
	
		if (isset($result->GetPointHistoryResult->ResponseHeader->ErrorCode) && $result->GetPointHistoryResult->ResponseHeader->ErrorCode == '00') {
			
			$the_result = $result->GetPointHistoryResult->GetPointHistoryResultDetails;
		
			// Check if loop is available
			if (!empty($the_result) && count($the_result) == 1) {
				
				$output.= $the_result->Point . '<br>';
			    $output.= $the_result->ExpiryDate . '<br>';
				$output.= $the_result->Status . '<br>';
			    $output.= $the_result->MerchantName . '<br>';
				$output.= $the_result->CompanyCode . '<br>';
			    $output.= $the_result->BranchCode . '<br>';
				$output.= $the_result->POSID . '<br>';
			    $output.= $the_result->SalesDate . '<br>';
			    $output.= '<br><br>';
								
			} else if (!empty($the_result) && count($the_result) > 1) {
								
				foreach ($the_result as $obj) {
					$output.= $obj->Point . '<br>';
					$output.= $obj->ExpiryDate . '<br>';
					$output.= $obj->Status . '<br>';
					$output.= $obj->MerchantName . '<br>';
					$output.= $obj->CompanyCode . '<br>';
					$output.= $obj->BranchCode . '<br>';
					$output.= $obj->POSID . '<br>';
					$output.= $obj->SalesDate . '<br>';
					$output.= '<br><br>';
				}
				
			} else {
			
				$output = 'No record found.';
				
			}
		
		} else {
			
			$output = 'Something went wrong.';
			
		}
		
		return $output;
	}
	
	/* -----------------------------------------------------------------------------------------*/
	
	// Retrieve Member's Profile
	function GetMemberProfile() {

		global $config_api_url, $config_ws_key, $config_company_code, $config_brand_code;

		$client = new SoapClient($config_api_url);

		$result = $client->GetMemberProfile(array(
				"WSKey" => $config_ws_key,
				"WSCompanyCode" => $config_company_code,
				"WSBranchCode" => $config_brand_code ,
				"Card" => $_SESSION["bcard_number"],
				"Password" => $_SESSION["bcard_password"]));
		
		
		//var_dump($result);
		//var_dump($result->GetGiftCatalogueResult->GetGiftCatalogueResultDetails);
		
		$output = '';
	
		if (isset($result->GetMemberProfileResult->ResponseHeader->ErrorCode) && $result->GetMemberProfileResult->ResponseHeader->ErrorCode == '00') {
			
			$the_result = $result->GetMemberProfileResult->GetMemberProfileResultDetails;
		
			// Check if loop is available
			if (!empty($the_result) && count($the_result) == 1) {
				
				$output.= $the_result->Title . '<br>';
			    $output.= $the_result->FullName . '<br>';
				$output.= $the_result->IC . '<br>';
			    $output.= $the_result->BirthDate . '<br>';
				$output.= $the_result->Gender . '<br>';
			    $output.= $the_result->Race . '<br>';
				$output.= $the_result->Nationality . '<br>';
			    $output.= $the_result->MaritalStatus . '<br>';
				$output.= $the_result->OwnCar . '<br>';
			    $output.= $the_result->OwnCreditCard . '<br>';
				$output.= $the_result->BalancePoint . '<br>';
			    $output.= $the_result->HomeAddress1 . '<br>';
				$output.= $the_result->HomeAddress2 . '<br>';
			    $output.= $the_result->HomeAddress3 . '<br>';
				$output.= $the_result->HomeCity . '<br>';
			    $output.= $the_result->HomeState . '<br>';
				$output.= $the_result->HomeCountry . '<br>';
			    $output.= $the_result->HomeZip . '<br>';
				$output.= $the_result->HomePhone . '<br>';
			    $output.= $the_result->HomeEmail . '<br>';
				$output.= $the_result->MobilePhone . '<br>';
			    $output.= $the_result->OfficeAddress1 . '<br>';
				$output.= $the_result->OfficeAddress2 . '<br>';
			    $output.= $the_result->OfficeAddress3 . '<br>';
				$output.= $the_result->OfficeCity . '<br>';
			    $output.= $the_result->OfficeState . '<br>';
				$output.= $the_result->OfficeCountry . '<br>';
			    $output.= $the_result->OfficeZip . '<br>';
				$output.= $the_result->OfficeEmail . '<br>';
			    $output.= $the_result->OfficePhone . '<br>';
				$output.= $the_result->OfficeExt . '<br>';
			    $output.= $the_result->OfficeFax . '<br>';
				$output.= $the_result->MSISDN . '<br>';
			    $output.= $the_result->CreateDate . '<br>';
				$output.= $the_result->ActivationDate . '<br>';
			    $output.= '<br><br>';
								
			} else if (!empty($the_result) && count($the_result) > 1) {
								
				// This record can't be looped
				
			} else {
			
				$output = 'No record found.';
				
			}
		
		} else {
			
			$output = 'Something went wrong.';
			
		}
		
		return $output;
	}
	
	/* -----------------------------------------------------------------------------------------*/
	
	// List Reward Catalogue
	function GetGiftCatalogue() {

		global $config_api_url, $config_ws_key, $config_company_code, $config_brand_code;

		$client = new SoapClient($config_api_url);

		$result = $client->GetGiftCatalogue(array(
				"WSKey" => $config_ws_key,
				"WSCompanyCode" => $config_company_code,
				"WSBranchCode" => $config_brand_code ,
				"Card" => $_SESSION["bcard_number"],
				"Password" => $_SESSION["bcard_password"]));
		
		
		//var_dump($result);
		//var_dump($result->GetGiftCatalogueResult->GetGiftCatalogueResultDetails);
		
		$output = '';
	
		if (isset($result->GetGiftCatalogueResult->ResponseHeader->ErrorCode) && $result->GetGiftCatalogueResult->ResponseHeader->ErrorCode == '00') {
			
			$the_result = $result->GetGiftCatalogueResult->GetGiftCatalogueResultDetails;
		
			// Check if loop is available
			if (!empty($the_result) && count($the_result) == 1) {
				
				$output.= $the_result->GiftID . '<br>';
			    $output.= $the_result->GiftName . '<br>';
				$output.= $the_result->Category . '<br>';
			    $output.= $the_result->Description . '<br>';
				$output.= $the_result->Point . '<br>';
			    $output.= $the_result->Picture . '<br>';
				$output.= $the_result->GiftQty . '<br>';
			    $output.= $the_result->GiftCompanyCode . '<br>';
				$output.= $the_result->GiftCompanyName . '<br>';
				$output.= $the_result->TotalOutlets . '<br>';
			    $output.= '<br><br>';
								
			} else if (!empty($the_result) && count($the_result) > 1) {
								
				foreach ($the_result as $obj) {
					$output.= $obj->GiftID . '<br>';
					$output.= $obj->GiftName . '<br>';
					$output.= $obj->Category . '<br>';
					$output.= $obj->Description . '<br>';
					$output.= $obj->Point . '<br>';
					$output.= $obj->Picture . '<br>';
					$output.= $obj->GiftQty . '<br>';
					$output.= $obj->GiftCompanyCode . '<br>';
					$output.= $obj->GiftCompanyName . '<br>';
					$output.= $obj->TotalOutlets . '<br>';
					$output.= '<br><br>';
				}
				
			} else {
			
				$output = 'No record found.';
				
			}
		
		} else {
			
			$output = 'Something went wrong.';
			
		}
		
		return $output;
		
	}
	
	




	

?>
