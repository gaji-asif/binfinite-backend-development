 <script>
            jQuery( document ).ready(function($) {

				//Sign Up form
                $("#sign-up").click(function (e) {
                    // this points to our form
                    e.preventDefault();

                    if (!$('input[name=reg_Card]').val()) {
                        alert('Please enter your 16 digits UCard No.');
                        $('input[name=reg_Card]').focus();
						return false; 
						
					} else if($('input[name=reg_Card]').val().length < 15){
						alert("UCard No must be 16 digits.");
						$('input[name=reg_Card]').focus();
						return false;
						
					} else if (!$('input[name=reg_first_name]').val()) {
                        alert('Please enter your first name.');
                        $('input[name=reg_first_name]').focus();
                        return false;

					} else if (!$('input[name=reg_last_name]').val()) {
                        alert('Please enter your last name.');
                        $('input[name=reg_last_name]').focus();
                        return false;
					
						
                    } else if (!$('input[name=reg_IC]').val()) {
                        alert('Please enter your IC or Passport No.');
                        $('input[name=reg_IC]').focus();
                        return false;

					} else if (!$('input[name=reg_BirthDate]').val()) {
                        alert('Please enter your date of birth.');
                        $('input[name=reg_BirthDate]').focus();
                        return false;

					} else if (!$('input[name=reg_HomeEmail]').val() || $('input[name=reg_HomeEmail]').val().indexOf('@') == '-1' ) {
                        alert('Please enter a valid email address.');
                        $('input[name=reg_HomeEmail]').focus();
                        return false;

					} else if (!$('input[name=reg_MobilePhone]').val()) {
                        alert('Please enter your mobile phone no.');
                        $('input[name=reg_MobilePhone]').focus();
                        return false;
					
					} else if (!$('input[name=reg_HomeAddress1]').val()) {
                        alert('Please enter your address 1.');
                        $('input[name=reg_HomeAddress1]').focus();
                        return false;
						
					/*} else if (!$('input[name=reg_HomeCity]').val()) {
                        alert('Please enter your city.');
                        $('input[name=reg_HomeCity]').focus();
                        return false; */

					} else if (!$('input[name=reg_HomePhone]').val()) {
                        alert('Please enter your home phone no.');
                        $('input[name=reg_HomePhone]').focus();
                        return false;
					
					} else if (!$('input[name=reg_HomeZip]').val()) {
                        alert('Please enter postcode.');
                        $('input[name=reg_HomeZip]').focus();
                        return false;
					
					}else if(!$('#tnc').is(':checked')){
					alert("Please tick on the checkbox to indicate your acceptance of BCard terms & conditions.");       
					return false;    

                    } else {
                    	$("#sign-up-form").submit();
                        return false;
                    }
                });
			});
	</script>
