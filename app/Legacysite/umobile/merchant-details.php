<!doctype html>

<html lang="en-US" class="no-js">

<?php include '../gtm.php'; ?>

	<div id="page_wrapper">
		<?php include ('top.php');?>
		<?php include ('../wordpress-config.php');?>
				<?php // Wordpress Starts
				$args = array(
					'p' => $_GET['id'],
					'posts_per_page'   => 1,
					'offset'           => 0,
					'category'         => '',
					//'category_name'    => '',
					'orderby'          => 'date',
					'order'            => 'DESC',
					//'include'          => '',
					//'exclude'          => '',
					//'meta_key'         => '',
					//'meta_value'       => '',
					'post_type'        => 'merchant_post',
					//'post_mime_type'   => '',
					//'post_parent'      => '',
					//'author'	   => '',
					//'author_name'	   => '',
					'post_status'      => 'publish',
					//'suppress_filters' => true
				);

				$post_counter = 0;

				$myposts = get_posts( $args );
				foreach ( $myposts as $post ) : setup_postdata( $post );

					$the_merchant_name = get_the_title();
					$the_merchant_logo = get_the_post_thumbnail($_GET['id'], 'full' );
					$the_merchant_point_earning = get_field('merchant_points_earning_rate');
					$the_merchant_partner_website = get_field('merchant_partner_website');
					$the_merchant_full_or_partial_redemption = get_field('full_or_partial_redemption');
					$the_merchant_slider_1 = get_field('merchant_slider_1');
					$the_merchant_slider_2 = get_field('merchant_slider_2');
					$the_merchant_slider_3 = get_field('merchant_slider_3');
					$the_merchant_first_paragraph = get_field('merchant_first_paragraph');
					$the_merchant_content = get_the_content();
					$the_merchant_example = get_field('merchant_example');

				endforeach;
				wp_reset_postdata();
				// Wordpress ends
		?>
	<style>
.bx-wrapper .bx-viewport {
	border-radius:10px;
}
</style>

	<section class="hg_section bg-lightgray ptop-50 pbottom-65">
			<div class="container">
				<div class="row">

				<div class="col-md-12 col-sm-12">
					<div class="col-md-3 col-sm-3">
						<?php echo $the_merchant_logo ;?>
							</div>


					<div class="col-md-9 col-sm-9">

					<div class="hg_accordion_element default-style">

							<div class="th-accordion">
								<?php if (!empty($the_merchant_point_earning)) { ?>
								<div class="acc-group">
									<button data-toggle="collapse" data-target="#acc4" class="collapsed" aria-expanded="true">Points Earning Rate: <span style="color:#e30e27;"><?php echo $the_merchant_point_earning; ?></span></button>
								</div>
								<?php } ?>

								<?php if (!empty($the_merchant_partner_website)) { ?>
								<div class="acc-group">
									<button data-toggle="collapse" data-target="#acc5" class="collapsed" aria-expanded="false">Merchant Partner Website : <a href="<?php echo $the_merchant_partner_website; ?>" target="_blank" rel="nofollow"><?php echo $the_merchant_partner_website; ?></a></button>
								</div>
								<?php } ?>

								<?php if (!empty($the_merchant_full_or_partial_redemption)) { ?>
								<div class="acc-group">
									<button data-toggle="collapse" data-target="#acc5" class="collapsed" aria-expanded="false"><a href="<?php echo $the_merchant_full_or_partial_redemption; ?>">Full or Partial Redemption</a></button>
								</div>
								<?php } ?>

							</div>
						</div>
					</div>

				</div>


				</div>


				<div class="row">


					<div class="col-md-12 col-sm-12">
						<div class="col-md-3 col-sm-3">&nbsp;</div>

						<div class="col-md-9 col-sm-9">
						<p align="justify">
							<?php echo $the_merchant_first_paragraph ;?>

						</p>

						<p align="justify">
							<?php echo $the_merchant_content; ?>
						</p>

							<?php if (!empty($the_merchant_example)) { ?>
						<div class="ib2-style2 ib2-text-color-light-theme ib2-custom" style="background-color:#f5f6f7;">
							<div class="ib2-inner">
								<h4 class="ib2-info-message">EXAMPLE</h4>
								<div class="ib2-content">

									<div class="ib2-content--text">
										<?php echo $the_merchant_example; ?>
									</div>
								</div>
							</div>
						</div>
						<?php } ?>


						</div>

					</div>


				</div>

			</div>
		</section>



		<?php include ('bottom.php');?>


	</div><!-- end page-wrapper -->

</body>

</html>
