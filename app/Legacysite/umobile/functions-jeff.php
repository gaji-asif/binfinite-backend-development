<?php
	error_reporting(E_ALL); ini_set('display_errors', 1);
	
	$now_date_time = date('Y-m-d'). 'T' . date('H:i:s');

	/* -----------------------------------------------------------------------------------------*/

	// If null / empty return N/A
	function return_clean_value($the_value) {
			$the_value = trim($the_value);
			if (empty($the_value)){
					return "N/A";
			} else {
					return $the_value;
			}
	}

	// Retrieve Transaction History
	function GetRewardHistory() {

		global $config_api_url, $config_ws_key, $config_company_code, $config_brand_code;

		$client = new SoapClient($config_api_url);

		$result = $client->GetRewardHistory(array(
				"WSKey" => $config_ws_key,
				"WSCompanyCode" => $config_company_code,
				"WSBranchCode" => $config_brand_code ,
				"Card" => $_SESSION["bcard_number"],
				"Password" => $_SESSION["bcard_password"]));

		//var_dump($result->GetGiftCatalogueResult->GetGiftCatalogueResultDetails);

		$output = '';

		if (isset($result->GetRewardHistoryResult->ResponseHeader->ErrorCode) && $result->GetRewardHistoryResult->ResponseHeader->ErrorCode == '00' && isset($result->GetRewardHistoryResult->GetRewardHistoryResultDetails)) {

			$the_result = $result->GetRewardHistoryResult->GetRewardHistoryResultDetails;

			// Check if loop is available
			if (!empty($the_result) && count($the_result) == 1) {
				$reward_date = explode("T",$the_result->RewardDate);
				$output.= "<tr>";
				$output.= "<td>".$the_result->RewardID."</td>";
				$output.= "<td>".$reward_date[0]."</td>";
				$output.= "<td>".$the_result->MerchantName."</td>";
				$output.= "<td>".number_format($the_result->TotalPoint)."</td>";
				$output.= "</tr>";
			/*
				$output.= $the_result->RewardID . '<br>';
			    $output.= $the_result->RewardDate . '<br>';
				$output.= $the_result->TotalAmount . '<br>';
			    $output.= $the_result->TotalPoint . '<br>';
				$output.= $the_result->MerchantName . '<br>';
			    $output.= $the_result->CompanyCode . '<br>';
				$output.= $the_result->BranchCode . '<br>';
			    $output.= $the_result->POSID . '<br>';
			    $output.= '<br><br>';
			*/
			} else if (!empty($the_result) && count($the_result) > 1) {

				foreach ($the_result as $obj) {
					$reward_date = explode("T",$obj->RewardDate);
					$output.= "<tr>";
					$output.= "<td>".$obj->RewardID."</td>";
					$output.= "<td>".$reward_date[0]."</td>";
					$output.= "<td>".$obj->MerchantName."</td>";
					$output.= "<td>".number_format($obj->TotalPoint)."</td>";
					$output.= "</tr>";
				/*
					$output.= $obj->RewardID . '<br>';
					$output.= $obj->RewardDate . '<br>';
					$output.= $obj->TotalAmount . '<br>';
					$output.= $obj->TotalPoint . '<br>';
					$output.= $obj->MerchantName . '<br>';
					$output.= $obj->CompanyCode . '<br>';
					$output.= $obj->BranchCode . '<br>';
					$output.= $obj->POSID . '<br>';
					$output.= '<br><br>';
				*/
				}

			} else {

				$output = '<tr><td colspan=4>No record found.</td></tr>';

			}

		} else {

			$output = '<tr><td colspan=4>No record found.</td></tr>';

		}

		return $output;
	}

	/* -----------------------------------------------------------------------------------------*/


	// Retrieve Redemption History
	function GetRedeemHistory() {

		global $config_api_url, $config_ws_key, $config_company_code, $config_brand_code;

		$client = new SoapClient($config_api_url);

		$result = $client->GetRedeemHistory(array(
				"WSKey" => $config_ws_key,
				"WSCompanyCode" => $config_company_code,
				"WSBranchCode" => $config_brand_code ,
				"Card" => $_SESSION["bcard_number"],
				"Password" => $_SESSION["bcard_password"]));

		//echo "<pre>";
		//var_dump($result);
		//echo "</pre>";
		//var_dump($result->GetGiftCatalogueResult->GetGiftCatalogueResultDetails);

		$output = '';

		if (isset($result->GetRedeemHistoryResult->ResponseHeader->ErrorCode) && $result->GetRedeemHistoryResult->ResponseHeader->ErrorCode == '00' && isset($result->GetRedeemHistoryResult->GetRedeemHistoryResultDetails)) {

			$the_result = $result->GetRedeemHistoryResult->GetRedeemHistoryResultDetails;

			// Check if loop is available
			if (!empty($the_result) && count($the_result) == 1) {
				$the_date = explode('T',$the_result->RedemptionDate);
				$output.= "<tr>";
				$output.= "<td>".$the_result->RedemptionID."</td>";
				$output.= "<td>".$the_date[0]."</td>";
				$output.= "<td>".$the_result->MerchantName ."</td>";
				$output.= "<td>".$the_result->GiftName."</td>";
				$output.= "<td>".$the_result->Quantity."</td>";
				$output.= "<td>".$the_result->Point."</td>";
				$output.= "<tr>";


			} else if (!empty($the_result) && count($the_result) > 1) {

				foreach ($the_result as $obj) {
					$the_date = explode('T',$obj->RedemptionDate);
					$output.= "<tr>";
					$output.= "<td>".$obj->RedemptionID."</td>";
					$output.= "<td>".$the_date[0]."</td>";
					$output.= "<td>".$obj->MerchantName ."</td>";
					$output.= "<td>".$obj->GiftName."</td>";
					$output.= "<td>".$obj->Quantity."</td>";
					$output.= "<td>".$obj->Point."</td>";
					$output.= "<tr>";

				}

			} else {

				$output = '<tr><td colspan=6>No record found</td></tr>';

			}

		} else {
			$output = '<tr><td colspan=6>No record found</td></tr>';
		}

		return $output;
	}

	/* -----------------------------------------------------------------------------------------*/

	// Retrieve Point History
	function GetPointHistory() {

		global $config_api_url, $config_ws_key, $config_company_code, $config_brand_code;

		$client = new SoapClient($config_api_url);

		$result = $client->GetPointHistory(array(
				"WSKey" => $config_ws_key,
				"WSCompanyCode" => $config_company_code,
				"WSBranchCode" => $config_brand_code ,
				"Card" => $_SESSION["bcard_number"],
				"Password" => $_SESSION["bcard_password"]));


		//var_dump($result);
		//var_dump($result->GetGiftCatalogueResult->GetGiftCatalogueResultDetails);

		$output = '';

		if (isset($result->GetPointHistoryResult->ResponseHeader->ErrorCode) && $result->GetPointHistoryResult->ResponseHeader->ErrorCode == '00' && isset($result->GetPointHistoryResult->GetPointHistoryResultDetails)) {

			$the_result = $result->GetPointHistoryResult->GetPointHistoryResultDetails;

			// Check if loop is available
			if (!empty($the_result) && count($the_result) == 1) {
				$the_date = explode('T',$the_result->ExpiryDate);
				$output.= "<tr>";
				$output.= "<td>".$the_result->Point."</td>";
				$output.= "<td>".$the_date[0]."</td>";
				$output.= "<td>".$the_result->Status."</td>";
				$output.= "</tr>";

			} else if (!empty($the_result) && count($the_result) > 1) {

				foreach ($the_result as $obj) {
					$the_date = explode('T',$obj->ExpiryDate);
					$the_array[] = "<tr><td>".$obj->Point."</td><td>".$the_date[0]."</td><td>".$obj->Status."</td></tr>";
				}
				
				foreach (array_reverse($the_array,true) as $value) {
					$output.= $value;
				}

			} else {

				$output = '<tr><td colspan=3>No record found.</td></tr>';

			}

		} else {

			$output = '<tr><td colspan=3>No record found.</td></tr>';

		}

		return $output;
	}

	/* -----------------------------------------------------------------------------------------*/

	// Retrieve Member's Profile
	function GetMemberProfile() {

		global $config_api_url, $config_ws_key, $config_company_code, $config_brand_code;

		$client = new SoapClient($config_api_url);

		$result = $client->GetMemberProfile(array(
				"WSKey" => $config_ws_key,
				"WSCompanyCode" => $config_company_code,
				"WSBranchCode" => $config_brand_code ,
				"Card" => $_SESSION["bcard_number"],
				"Password" => $_SESSION["bcard_password"]));


		//var_dump($result); die();
		//var_dump($result->GetGiftCatalogueResult->GetGiftCatalogueResultDetails);

		$output = '';

		if (isset($result->GetMemberProfileResult->ResponseHeader->ErrorCode) && $result->GetMemberProfileResult->ResponseHeader->ErrorCode == '00' && isset($result->GetMemberProfileResult->GetMemberProfileResultDetails)) {

			$the_result = $result->GetMemberProfileResult->GetMemberProfileResultDetails;

			// Check if loop is available
			if (!empty($the_result) && count($the_result) == 1) {

				$_SESSION["profile"]["bcard_title"] = $the_result->Title;
			    $_SESSION["profile"]["bcard_fullname"] = $the_result->FullName;
				$_SESSION["profile"]["bcard_ic"] = $the_result->IC;
			    $_SESSION["profile"]["bcard_birthdate"] = $the_result->BirthDate;
				$_SESSION["profile"]["bcard_gender"] = $the_result->Gender;
			    $_SESSION["profile"]["bcard_race"] = trim($the_result->Race);
				$_SESSION["profile"]["bcard_nationality"] = $the_result->Nationality;
			    $_SESSION["profile"]["bcard_maritalstatus"] = $the_result->MaritalStatus;
				$_SESSION["profile"]["bcard_ownacar"] = $the_result->OwnCar;
			    $_SESSION["profile"]["bcard_ownacreditcard"] = $the_result->OwnCreditCard;
				$_SESSION["profile"]["bcard_balancepoint"] = $the_result->BalancePoint;
			    $_SESSION["profile"]["bcard_homeaddress1"] = $the_result->HomeAddress1;
				$_SESSION["profile"]["bcard_homeaddress2"] = $the_result->HomeAddress2;
			    $_SESSION["profile"]["bcard_homeaddress3"] = $the_result->HomeAddress3;
				$_SESSION["profile"]["bcard_homecity"] = $the_result->HomeCity;
			    $_SESSION["profile"]["bcard_homestate"]  = $the_result->HomeState;
				$_SESSION["profile"]["bcard_homecountry"] = $the_result->HomeCountry;
			    $_SESSION["profile"]["bcard_homezip"] = $the_result->HomeZip;
				$_SESSION["profile"]["bcard_homephone"] = $the_result->HomePhone;
			    $_SESSION["profile"]["bcard_primaryemail"] = $the_result->HomeEmail;
				$_SESSION["profile"]["bcard_mobilephone"] = $the_result->MobilePhone;
			    $_SESSION["profile"]["bcard_officeaddress1"] = $the_result->OfficeAddress1;
				$_SESSION["profile"]["bcard_officeaddress2"] = $the_result->OfficeAddress2;
			    $_SESSION["profile"]["bcard_officeaddress3"] = $the_result->OfficeAddress3;
				$_SESSION["profile"]["bcard_officecity"] = $the_result->OfficeCity;
			    $_SESSION["profile"]["bcard_officestate"] = $the_result->OfficeState;
				$_SESSION["profile"]["bcard_officecountry"] = $the_result->OfficeCountry;
			    $_SESSION["profile"]["bcard_officezip"]= $the_result->OfficeZip;
				$_SESSION["profile"]["bcard_officeemail"] = $the_result->OfficeEmail;
			    $_SESSION["profile"]["bcard_officephone"]= $the_result->OfficePhone;
				$_SESSION["profile"]["bcard_officeext"] = $the_result->OfficeExt;
			    $_SESSION["profile"]["bcard_officefax"] = $the_result->OfficeFax;
				$_SESSION["profile"]["bcard_officemsisdn"] = $the_result->MSISDN;
			    $_SESSION["profile"]["bcard_officecreatedate"] = $the_result->CreateDate;
				$_SESSION["profile"]["bcard_activationdate"] = $the_result->ActivationDate;

			} else if (!empty($the_result) && count($the_result) > 1) {

				// This record can't be looped

			} else {

				$output = 'No record found.';

			}

		} else {

			$output = 'Something went wrong.';

		}

		return $output;
	}

	/* -----------------------------------------------------------------------------------------*/

	// List Reward Catalogue
	function GetGiftCatalogue() {

		global $config_api_url, $config_ws_key, $config_company_code, $config_brand_code;

		$client = new SoapClient($config_api_url);

		$result = $client->GetGiftCatalogue(array(
				"WSKey" => $config_ws_key,
				"WSCompanyCode" => $config_company_code,
				"WSBranchCode" => $config_brand_code
				));


		//var_dump($result); die();
		//var_dump($result->GetGiftCatalogueResult->GetGiftCatalogueResultDetails);

		$output = '';

		if (isset($result->GetGiftCatalogueResult->ResponseHeader->ErrorCode) && $result->GetGiftCatalogueResult->ResponseHeader->ErrorCode == '00' && isset($result->GetGiftCatalogueResult->GetGiftCatalogueResultDetails)) {

			$the_result = $result->GetGiftCatalogueResult->GetGiftCatalogueResultDetails;

			// Check if loop is available
			if (!empty($the_result) && count($the_result) == 1) {

				$output.= '<li class="lp-post col-md-12 col-sm-12">';
				$output.= '<a href="#" class="hoverBorder pull-left">';
				$output.= '<span class="hoverBorderWrapper">';
				$output.= '<img src="'.$the_result->Picture.'">';
				$output.= '<span class="theHoverBorder"></span>';
				$output.= '</span>';
				$output.= '</a>';
				$output.= '<h4 class="title"><a href="redemption-details.php?gift_id='.$the_result->GiftID.'">'.$the_result->GiftName.'</a></h4>';
				$output.= '<div class="lp-post-comments-num">';
				$output.= $the_result->Point . ' points <br />';
				if (strlen($the_result->Description) < 101) {
						$output.= $the_result->Description;
				} else {
						$output.= substr($the_result->Description,0,100)."...";
				}
				$output.= '</div>';
				$output.= '<a class="deals" href="redemption-details.php?gift_id='.$the_result->GiftID.'">Read More >></a>';
				$output.= '</li>';


			/*
				$output.= $the_result->GiftID . '<br>';
			    $output.= $the_result->GiftName . '<br>';
				$output.= $the_result->Category . '<br>';
			    $output.= $the_result->Description . '<br>';
				$output.= $the_result->Point . '<br>';
			    $output.= $the_result->Picture . '<br>';
				$output.= $the_result->GiftQty . '<br>';
			    $output.= $the_result->GiftCompanyCode . '<br>';
				$output.= $the_result->GiftCompanyName . '<br>';
				$output.= $the_result->TotalOutlets . '<br>';
			    $output.= '<br><br>';
			*/
			} else if (!empty($the_result) && count($the_result) > 1) {

				foreach ($the_result as $obj) {
					$output.= '<li class="lp-post col-md-12 col-sm-12">';
					$output.= '<a href="#" class="hoverBorder pull-left">';
					$output.= '<span class="hoverBorderWrapper">';
					$output.= '<img src="'.$obj->Picture.'">';
					$output.= '<span class="theHoverBorder"></span>';
					$output.= '</span>';
					$output.= '</a>';
					$output.= '<h4 class="title"><a href="redemption-details.php?gift_id='.$obj->GiftID.'">'.$obj->GiftName.'</a></h4>';
					$output.= '<div class="lp-post-comments-num">';
					$output.= $obj->Point . ' points <br />';
					if (strlen($obj->Description) < 101) {
						$output.= $obj->Description;
					} else {
						$output.= substr($obj->Description,0,100)."...";
					}
					$output.= '</div>';
					$output.= '<a class="deals" href="redemption-details.php?gift_id='.$obj->GiftID.'">Read More >></a>';
					$output.= '</li>';

			/*
					$output.= $obj->GiftID . '<br>';
					$output.= $obj->GiftName . '<br>';
					$output.= $obj->Category . '<br>';
					$output.= $obj->Description . '<br>';
					$output.= $obj->Point . '<br>';
					$output.= $obj->Picture . '<br>';
					$output.= $obj->GiftQty . '<br>';
					$output.= $obj->GiftCompanyCode . '<br>';
					$output.= $obj->GiftCompanyName . '<br>';
					$output.= $obj->TotalOutlets . '<br>';
					$output.= '<br><br>';
			*/
				}

			} else {

				$output = 'No record found.';

			}

		} else {

			$output = 'Something went wrong.';

		}

		return $output;

	}


	/* -----------------------------------------------------------------------------------------*/

	// View Single Reward Catalogue
	function GetGiftCatalogueSingle($the_gift_id = '') {

		global $config_api_url, $config_ws_key, $config_company_code, $config_brand_code;

		$client = new SoapClient($config_api_url);

		$result = $client->GetGiftCatalogue(array(
				"WSKey" => $config_ws_key,
				"WSCompanyCode" => $config_company_code,
				"WSBranchCode" => $config_brand_code
				));

		//var_dump($result); die();
		//var_dump($result->GetGiftCatalogueResult->GetGiftCatalogueResultDetails);

		$output = '';

		if (isset($result->GetGiftCatalogueResult->ResponseHeader->ErrorCode) && $result->GetGiftCatalogueResult->ResponseHeader->ErrorCode == '00' && isset($result->GetGiftCatalogueResult->GetGiftCatalogueResultDetails)) {

			$the_result = $result->GetGiftCatalogueResult->GetGiftCatalogueResultDetails;

			// Check if loop is available
			if (!empty($the_result) && count($the_result) == 1) {

				if ($the_gift_id == trim($the_result->GiftID)) {
					$output["GiftID"] = $the_result->GiftID;
					$output["GiftName"] = $the_result->GiftName;
					$output["Category"] = $the_result->Category;
					$output["Description"] = $the_result->Description;
					$output["Point"] = $the_result->Point;
					$output["Picture"] = $the_result->Picture;
					$output["GiftQty"] = $the_result->GiftQty;
					$output["GiftCompanyCode"] = $the_result->GiftCompanyCode;
					$output["GiftCompanyName"] = $the_result->GiftCompanyName;
					$output["TotalOutlets"] = $the_result->TotalOutlets;
				}

			/*
				$output.= $the_result->GiftID . '<br>';
			    $output.= $the_result->GiftName . '<br>';
				$output.= $the_result->Category . '<br>';
			    $output.= $the_result->Description . '<br>';
				$output.= $the_result->Point . '<br>';
			    $output.= $the_result->Picture . '<br>';
				$output.= $the_result->GiftQty . '<br>';
			    $output.= $the_result->GiftCompanyCode . '<br>';
				$output.= $the_result->GiftCompanyName . '<br>';
				$output.= $the_result->TotalOutlets . '<br>';
			    $output.= '<br><br>';
			*/
			} else if (!empty($the_result) && count($the_result) > 1) {

				foreach ($the_result as $obj) {
					if ($the_gift_id == trim($obj->GiftID)) {
						$output["GiftID"] = $obj->GiftID;
						$output["GiftName"] = $obj->GiftName;
						$output["Category"] = $obj->Category;
						$output["Description"] = $obj->Description;
						$output["Point"] = $obj->Point;
						$output["Picture"] = $obj->Picture;
						$output["GiftQty"] = $obj->GiftQty;
						$output["GiftCompanyCode"] = $obj->GiftCompanyCode;
						$output["GiftCompanyName"] = $obj->GiftCompanyName;
						$output["TotalOutlets"] = $obj->TotalOutlets;
					}

			/*
					$output.= $obj->GiftID . '<br>';
					$output.= $obj->GiftName . '<br>';
					$output.= $obj->Category . '<br>';
					$output.= $obj->Description . '<br>';
					$output.= $obj->Point . '<br>';
					$output.= $obj->Picture . '<br>';
					$output.= $obj->GiftQty . '<br>';
					$output.= $obj->GiftCompanyCode . '<br>';
					$output.= $obj->GiftCompanyName . '<br>';
					$output.= $obj->TotalOutlets . '<br>';
					$output.= '<br><br>';
			*/
				}

			} else {

				$output = 'No record found.';

			}

		} else {

			$output = 'Something went wrong.';

		}

		return $output;

	}


	/* -----------------------------------------------------------------------------------------*/

	// Retrieve Card Details
	function GetCard() {

		global $config_api_url, $config_ws_key, $config_company_code, $config_brand_code;

		$client = new SoapClient($config_api_url);

		$result = $client->GetCard(array(
				"WSKey" => $config_ws_key,
				"WSCompanyCode" => $config_company_code,
				"WSBranchCode" => $config_brand_code ,
				"Card" => $_SESSION["bcard_number"],
				"Password" => $_SESSION["bcard_password"]));


		//var_dump($result); die();
		//var_dump($result->GetGiftCatalogueResult->GetGiftCatalogueResultDetails);

		$output = '';

		if (isset($result->GetCardResult->ResponseHeader->ErrorCode) && $result->GetCardResult->ResponseHeader->ErrorCode == '00') {

			$the_result = $result->GetCardResult->GetCardResultDetails;

			// Check if loop is available
			if (!empty($the_result) && count($the_result) == 1) {

				$output.= $the_result->CardTypeName . '<br>';
			    $output.= $the_result->CardImage . '<br>';
				$output.= $the_result->Status . '<br>';
			    $output.= $the_result->PrincipalPoint . '<br>';
				$output.= $the_result->TokenPoint . '<br>';
			    $output.= $the_result->MinToken . '<br>';
				$output.= $the_result->TotalPoint . '<br>';
			    $output.= $the_result->CreateDate . '<br>';
				$output.= $the_result->ActivationDate . '<br>';
			    $output.= '<br><br>';

			} else if (!empty($the_result) && count($the_result) > 1) {

				foreach ($the_result as $obj) {
					$output.= $obj->CardTypeName . '<br>';
					$output.= $obj->CardImage . '<br>';
					$output.= $obj->Status . '<br>';
					$output.= $obj->PrincipalPoint . '<br>';
					$output.= $obj->TokenPoint . '<br>';
					$output.= $obj->MinToken . '<br>';
					$output.= $obj->TotalPoint . '<br>';
					$output.= $obj->CreateDate . '<br>';
					$output.= $obj->ActivationDate . '<br>';
					$output.= '<br><br>';
				}

			} else {

				$output = 'No record found.';

			}

		} else {

			$output = 'Something went wrong.';

		}

		return $output;
	}

	/* -----------------------------------------------------------------------------------------*/


	// Member Login / Card Verification
	function VerifyLogin() {
		global $config_api_url, $config_ws_key, $config_company_code, $config_brand_code;

		$client = new SoapClient($config_api_url);

		$result = $client->VerifyLogin(array(
				"WSKey" => $config_ws_key,
				"WSCompanyCode" => $config_company_code,
				"WSBranchCode" => $config_brand_code ,
				"Card" => $_SESSION["bcard_number"],
				"Password" => $_SESSION["bcard_password"]));


		//var_dump($result); die();
		//var_dump($result->GetGiftCatalogueResult->GetGiftCatalogueResultDetails);

		$output = '';

		if (isset($result->VerifyLoginResult->ResponseHeader->ErrorCode) && $result->VerifyLoginResult->ResponseHeader->ErrorCode == '00') {

			$output = 'ok';


		} else {

			$output = 'Login invalid - Incorrect Username or Password';

		}

		return $output;
	}

	/* -----------------------------------------------------------------------------------------*/

	// Point Inquiry
	function PointInquiry() {

		global $config_api_url, $config_ws_key, $config_company_code, $config_brand_code;

		$client = new SoapClient($config_api_url);

		$result = $client->PointInquiry(array(
				"WSKey" => $config_ws_key,
				"WSCompanyCode" => $config_company_code,
				"WSBranchCode" => $config_brand_code ,
				"Card" => $_SESSION["bcard_number"],
				"Password" => $_SESSION["bcard_password"]));

		//var_dump($result->GetGiftCatalogueResult->GetGiftCatalogueResultDetails);

		$output = '';

		if (isset($result->PointInquiryResult->ResponseHeader->ErrorCode) && $result->PointInquiryResult->ResponseHeader->ErrorCode == '00' && isset($result->PointInquiryResult->PointInquiryResultDetails)) {

			$the_result = $result->PointInquiryResult->PointInquiryResultDetails;

			$output = number_format($the_result->PrincipalPoint);


		} else {

			$output = 'Something went wrong.';

		}

		return $output;
	}

	/* -----------------------------------------------------------------------------------------*/

	// Check Card is active or inactive
	function CheckCard() {

		global $config_api_url, $config_ws_key, $config_company_code, $config_brand_code;

		$client = new SoapClient($config_api_url);

		$result = $client->CheckCard(array(
				"WSKey" => $config_ws_key,
				"WSCompanyCode" => $config_company_code,
				"WSBranchCode" => $config_brand_code ,
				"Card" => $_SESSION["bcard_number"],
				"Password" => $_SESSION["bcard_password"]));


		//var_dump($result); die();
		//var_dump($result->GetGiftCatalogueResult->GetGiftCatalogueResultDetails);

		$output = '';

		if (isset($result->CheckCardResult->ResponseHeader->ErrorCode) && $result->CheckCardResult->ResponseHeader->ErrorCode == '00') {

			$the_result = $result->CheckCardResult->CheckCardResultDetails;

			// Check if loop is available
			if (!empty($the_result) && count($the_result) == 1) {

				$output.= $the_result->Status . '<br>';
			    $output.= '<br><br>';

			} else if (!empty($the_result) && count($the_result) > 1) {

				foreach ($the_result as $obj) {
					$output.= $obj->Status . '<br>';
					$output.= '<br><br>';
				}

			} else {

				$output = 'No record found.';

			}

		} else {

			$output = 'Something went wrong.';

		}

		return $output;
	}

	/* -----------------------------------------------------------------------------------------*/

	// Retrieve token point history
	function GetTokenPointHistory() {

		global $config_api_url, $config_ws_key, $config_company_code, $config_brand_code;

		$client = new SoapClient($config_api_url);

		$result = $client->GetTokenPointHistory(array(
				"WSKey" => $config_ws_key,
				"WSCompanyCode" => $config_company_code,
				"WSBranchCode" => $config_brand_code ,
				"Card" => $_SESSION["bcard_number"],
				"Password" => $_SESSION["bcard_password"]));


		//var_dump($result); die();
		//var_dump($result->GetGiftCatalogueResult->GetGiftCatalogueResultDetails);

		$output = '';

		if (isset($result->GetTokenPointHistoryResult->ResponseHeader->ErrorCode) && $result->GetTokenPointHistoryResult->ResponseHeader->ErrorCode == '00' && isset($result->GetTokenPointHistoryResult->GetTokenPointHistoryResultDetails)) {

			$the_result = $result->GetTokenPointHistoryResult->GetTokenPointHistoryResultDetails;

			// Check if loop is available
			if (!empty($the_result) && count($the_result) == 1) {
				$date1 = explode("T",$the_result->RewardDate);
				$date2 = explode("T",$the_result->ExpiryDate);
				$date3 = explode("T",$the_result->RedeemDate);
				$output.= "<tr>";
				$output.= "<td>".$the_result->TokenID."</td>";
				$output.= "<td>".$the_result->RewardPoint."</td>";
				$output.= "<td>".$the_result->MinToken."</td>";
				$output.= "<td>".$date1[0]."</td>";
				$output.= "<td>".$date2[0]."</td>";
				$output.= "<td>".$date3[0]."</td>";
				$output.= "<td>".$the_result->MerchantName."</td>";
				$output.= "</tr>";
/*
				$output.= $the_result->Card . '<br>';
			    $output.= $the_result->TokenID . '<br>';
				$output.= $the_result->RewardDate . '<br>';
			    $output.= $the_result->ExpiryDate . '<br>';
				$output.= $the_result->RewardPoint . '<br>';
				$output.= $the_result->MinToken . '<br>';
			    $output.= $the_result->RedeemDate . '<br>';
				$output.= $the_result->MerchantName . '<br>';
			    $output.= $the_result->CompanyCode . '<br>';
				$output.= $the_result->BranchCode . '<br>';
				$output.= $the_result->POSID . '<br>';
			    $output.= '<br><br>';
*/
			} else if (!empty($the_result) && count($the_result) > 1) {

				foreach ($the_result as $obj) {
					$date1 = explode("T",$obj->RewardDate);
					$date2 = explode("T",$obj->ExpiryDate);
					$date3 = explode("T",$obj->RedeemDate);
					$output.= "<tr>";
					$output.= "<td>".$obj->TokenID."</td>";
					$output.= "<td>".$obj->RewardPoint."</td>";
					$output.= "<td>".$obj->MinToken."</td>";
					$output.= "<td>".$date1[0]."</td>";
					$output.= "<td>".$date2[0]."</td>";
					$output.= "<td>".$date3[0]."</td>";
					$output.= "<td>".$obj->MerchantName."</td>";
					$output.= "</tr>";
/*
					$output.= $obj->Card . '<br>';
					$output.= $obj->TokenID . '<br>';
					$output.= $obj->RewardDate . '<br>';
					$output.= $obj->ExpiryDate . '<br>';
					$output.= $obj->RewardPoint . '<br>';
					$output.= $obj->MinToken . '<br>';
					$output.= $obj->RedeemDate . '<br>';
					$output.= $obj->MerchantName . '<br>';
					$output.= $obj->CompanyCode . '<br>';
					$output.= $obj->BranchCode . '<br>';
					$output.= $obj->POSID . '<br>';
					$output.= '<br><br>';
*/
					}

			} else {

				$output = '<tr><td colspan=7>No record found.</td></tr>';

			}

		} else {

			$output = '<tr><td colspan=7>No record found.</td></tr>';

		}

		return $output;
	}

	/* -----------------------------------------------------------------------------------------*/

	// Retrieve merchant HQ information
	function GetMerchantHQ() {

		global $config_api_url, $config_ws_key, $config_company_code, $config_brand_code;

		$client = new SoapClient($config_api_url);

		$result = $client->GetMerchantHQ(array(
				"WSKey" => $config_ws_key,
				"WSCompanyCode" => $config_company_code,
				"WSBranchCode" => $config_brand_code ,
				"Card" => $_SESSION["bcard_number"],
				"Password" => $_SESSION["bcard_password"]));


		//var_dump($result); die();
		//var_dump($result->GetGiftCatalogueResult->GetGiftCatalogueResultDetails);

		$output = '';

		if (isset($result->GetMerchantHQResult->ResponseHeader->ErrorCode) && $result->GetMerchantHQResult->ResponseHeader->ErrorCode == '00' && isset($result->GetMerchantHQResult->GetMerchantHQResultDetails)) {

			$the_result = $result->GetMerchantHQResult->GetMerchantHQResultDetails;

			// Check if loop is available
			if (!empty($the_result) && count($the_result) == 1) {

				$output.= $the_result->CompanyCode . '<br>';
			    $output.= $the_result->CompanyName . '<br>';
				$output.= $the_result->Address . '<br>';
			    $output.= $the_result->PostCode . '<br>';
				$output.= $the_result->State . '<br>';
			    $output.= $the_result->Country . '<br>';
				$output.= $the_result->Latitude . '<br>';
			    $output.= $the_result->Longitude . '<br>';
				$output.= $the_result->PhoneNumber . '<br>';
			    $output.= $the_result->Fax . '<br>';
				$output.= $the_result->Email . '<br>';
			    $output.= $the_result->WebSite . '<br>';
				$output.= $the_result->Category . '<br>';
				$output.= $the_result->TotalOutlets . '<br>';
			    $output.= $the_result->Logo . '<br>';
			    $output.= '<br><br>';

			} else if (!empty($the_result) && count($the_result) > 1) {

				foreach ($the_result as $obj) {
					$output.= $obj->CompanyCode . '<br>';
					$output.= $obj->CompanyName . '<br>';
					$output.= $obj->Address . '<br>';
					$output.= $obj->PostCode . '<br>';
					$output.= $obj->State . '<br>';
					$output.= $obj->Country . '<br>';
					$output.= $obj->Latitude . '<br>';
					$output.= $obj->Longitude . '<br>';
					$output.= $obj->PhoneNumber . '<br>';
					$output.= $obj->Fax . '<br>';
					$output.= $obj->Email . '<br>';
					$output.= $obj->WebSite . '<br>';
					$output.= $obj->Category . '<br>';
					$output.= $obj->TotalOutlets . '<br>';
					$output.= $obj->Logo . '<br>';
					$output.= '<br><br>';
				}

			} else {

				$output = 'No record found.';

			}

		} else {

			$output = 'Something went wrong.';

		}

		return $output;
	}

	/* -----------------------------------------------------------------------------------------*/

	// Retrieve merchant branch information
	function GetMerchantBranch($company_code) {

		global $config_api_url, $config_ws_key, $config_company_code, $config_brand_code;

		$client = new SoapClient($config_api_url);

		$result = $client->GetMerchantBranch(array(
				"WSKey" => $config_ws_key,
				"WSCompanyCode" => $config_company_code,
				"WSBranchCode" => $config_brand_code ,
				"CompanyCode" => $company_code ,
				"Card" => $_SESSION["bcard_number"],
				"Password" => $_SESSION["bcard_password"]));


		//var_dump($result); die();
		//var_dump($result->GetGiftCatalogueResult->GetGiftCatalogueResultDetails);

		$output = '';

		if (isset($result->GetMerchantBranchResult->ResponseHeader->ErrorCode) && $result->GetMerchantBranchResult->ResponseHeader->ErrorCode == '00' && isset($result->GetMerchantBranchResult->GetMerchantBranchResultDetails)) {

			$the_result = $result->GetMerchantBranchResult->GetMerchantBranchResultDetails;

			// Check if loop is available
			if (!empty($the_result) && count($the_result) == 1) {

				$output.= $the_result->BranchCode . '<br>';
			    $output.= $the_result->BranchName . '<br>';
				$output.= $the_result->Address . '<br>';
			    $output.= $the_result->PostCode . '<br>';
				$output.= $the_result->State . '<br>';
			    $output.= $the_result->Country . '<br>';
				$output.= $the_result->Latitude . '<br>';
			    $output.= $the_result->Longitude . '<br>';
				$output.= $the_result->PhoneNumber . '<br>';
			    $output.= $the_result->Fax . '<br>';
				$output.= $the_result->Email . '<br>';
			    $output.= '<br><br>';

			} else if (!empty($the_result) && count($the_result) > 1) {

				foreach ($the_result as $obj) {
					$output.= $obj->BranchCode . '<br>';
					$output.= $obj->BranchName . '<br>';
					$output.= $obj->Address . '<br>';
					$output.= $obj->PostCode . '<br>';
					$output.= $obj->State . '<br>';
					$output.= $obj->Country . '<br>';
					$output.= $obj->Latitude . '<br>';
					$output.= $obj->Longitude . '<br>';
					$output.= $obj->PhoneNumber . '<br>';
					$output.= $obj->Fax . '<br>';
					$output.= $obj->Email . '<br>';
					$output.= '<br><br>';
				}

			} else {

				$output = 'No record found.';

			}

		} else {

			$output = 'Something went wrong.';

		}

		return $output;
	}

	/* -----------------------------------------------------------------------------------------*/

	// Retrieve merchant HQ's category information
	function GetMerchantHQCategory() {

		global $config_api_url, $config_ws_key, $config_company_code, $config_brand_code;

		$client = new SoapClient($config_api_url);

		$result = $client->GetMerchantHQCategory(array(
				"WSKey" => $config_ws_key,
				"WSCompanyCode" => $config_company_code,
				"WSBranchCode" => $config_brand_code ,
				"Card" => $_SESSION["bcard_number"],
				"Password" => $_SESSION["bcard_password"]));


		//var_dump($result); die();
		//var_dump($result->GetGiftCatalogueResult->GetGiftCatalogueResultDetails);

		$output = '';

		if (isset($result->GetMerchantHQCategoryResult->ResponseHeader->ErrorCode) && $result->GetMerchantHQCategoryResult->ResponseHeader->ErrorCode == '00' && isset($result->GetMerchantHQCategoryResult->GetMerchantHQCategoryResultDetails)) {

			$the_result = $result->GetMerchantHQCategoryResult->GetMerchantHQCategoryResultDetails;

			// Check if loop is available
			if (!empty($the_result) && count($the_result) == 1) {

				$output.= $the_result->CategoryID . '<br>';
			    $output.= $the_result->CategoryName . '<br>';
			    $output.= '<br><br>';

			} else if (!empty($the_result) && count($the_result) > 1) {

				foreach ($the_result as $obj) {
					$output.= $obj->CategoryID . '<br>';
					$output.= $obj->CategoryName . '<br>';
					$output.= '<br><br>';
				}

			} else {

				$output = 'No record found.';

			}

		} else {

			$output = 'Something went wrong.';

		}

		return $output;
	}

	/* -----------------------------------------------------------------------------------------*/

	// Retrieve merchant HQ's information based on category
	function GetMerchantHQByCategory($CategoryID) {

		global $config_api_url, $config_ws_key, $config_company_code, $config_brand_code;

		$client = new SoapClient($config_api_url);

		$result = $client->GetMerchantHQByCategory(array(
				"WSKey" => $config_ws_key,
				"WSCompanyCode" => $config_company_code,
				"WSBranchCode" => $config_brand_code ,
				"CategoryID" => $CategoryID ,
				"Card" => $_SESSION["bcard_number"],
				"Password" => $_SESSION["bcard_password"]));


		//var_dump($result); die();
		//var_dump($result->GetGiftCatalogueResult->GetGiftCatalogueResultDetails);

		$output = '';

		if (isset($result->GetMerchantHQByCategoryResult->ResponseHeader->ErrorCode) && $result->GetMerchantHQByCategoryResult->ResponseHeader->ErrorCode == '00' && isset($result->GetMerchantHQByCategoryResult->GetMerchantHQByCategoryResultDetails)) {

			$the_result = $result->GetMerchantHQByCategoryResult->GetMerchantHQByCategoryResultDetails;

			// Check if loop is available
			if (!empty($the_result) && count($the_result) == 1) {

				$output.= $the_result->CompanyCode . '<br>';
			    $output.= $the_result->CompanyName . '<br>';
				$output.= $the_result->Address . '<br>';
			    $output.= $the_result->PostCode . '<br>';
				$output.= $the_result->State . '<br>';
			    $output.= $the_result->Country . '<br>';
				$output.= $the_result->Latitude . '<br>';
			    $output.= $the_result->Longitude . '<br>';
				$output.= $the_result->PhoneNumber . '<br>';
			    $output.= $the_result->Fax . '<br>';
				$output.= $the_result->Email . '<br>';
			    $output.= $the_result->WebSite . '<br>';
				$output.= $the_result->TotalOutlets . '<br>';
			    $output.= $the_result->Logo . '<br>';
			    $output.= '<br><br>';

			} else if (!empty($the_result) && count($the_result) > 1) {

				foreach ($the_result as $obj) {
					$output.= $obj->CompanyCode . '<br>';
					$output.= $obj->CompanyName . '<br>';
					$output.= $obj->Address . '<br>';
					$output.= $obj->PostCode . '<br>';
					$output.= $obj->State . '<br>';
					$output.= $obj->Country . '<br>';
					$output.= $obj->Latitude . '<br>';
					$output.= $obj->Longitude . '<br>';
					$output.= $obj->PhoneNumber . '<br>';
					$output.= $obj->Fax . '<br>';
					$output.= $obj->Email . '<br>';
					$output.= $obj->WebSite . '<br>';
					$output.= $obj->TotalOutlets . '<br>';
					$output.= $obj->Logo . '<br>';
					$output.= '<br><br>';
				}

			} else {

				$output = 'No record found.';

			}

		} else {

			$output = 'Something went wrong.';

		}

		return $output;
	}

	/* -----------------------------------------------------------------------------------------*/


	// Retrieve gift catalogue for top up
	function GetTopUpGiftCatalogue() {

		global $config_api_url, $config_ws_key, $config_company_code, $config_brand_code;

		$client = new SoapClient($config_api_url);

		$result = $client->GetTopUpGiftCatalogue(array(
				"WSKey" => $config_ws_key,
				"WSCompanyCode" => $config_company_code,
				"WSBranchCode" => $config_brand_code));


		//var_dump($result); die();
		//var_dump($result->GetGiftCatalogueResult->GetGiftCatalogueResultDetails);

		$output = '';

		if (isset($result->GetTopUpGiftCatalogueResult->ResponseHeader->ErrorCode) && $result->GetTopUpGiftCatalogueResult->ResponseHeader->ErrorCode == '00' && isset($result->GetTopUpGiftCatalogueResult->GetTopUpGiftCatalogueResultDetails)) {

			$the_result = $result->GetTopUpGiftCatalogueResult->GetTopUpGiftCatalogueResultDetails;

			// Check if loop is available
			if (!empty($the_result) && count($the_result) == 1) {

				$output.= '<li class="lp-post col-md-12 col-sm-12">';
				$output.= '<a href="#" class="hoverBorder pull-left">';
				$output.= '<span class="hoverBorderWrapper">';
				$output.= '<img src="'.$the_result->Picture.'">';
				$output.= '<span class="theHoverBorder"></span>';
				$output.= '</span>';
				$output.= '</a>';
				$output.= '<h4 class="title"><a href="redemption-details.php?gift_id='.trim($the_result->GiftID).'&type=topup">'.$the_result->GiftName.'</a></h4>';
				$output.= '<div class="lp-post-comments-num">';
				$output.= $the_result->Point . ' points <br />';
				if (strlen($the_result->Description) < 101) {
						$output.= $the_result->Description;
				} else {
						$output.= substr($the_result->Description,0,100)."...";
				}
				$output.= '</div>';
				$output.= '<a class="deals" href="redemption-details.php?gift_id='.trim($the_result->GiftID).'&type=topup">Read More >></a>';
				$output.= '</li>';			
				
				
			} else if (!empty($the_result) && count($the_result) > 1) {

				foreach ($the_result as $obj) {

					$output.= '<li class="lp-post col-md-12 col-sm-12">';
					$output.= '<a href="#" class="hoverBorder pull-left">';
					$output.= '<span class="hoverBorderWrapper">';
					$output.= '<img src="'.$obj->Picture.'">';
					$output.= '<span class="theHoverBorder"></span>';
					$output.= '</span>';
					$output.= '</a>';
					$output.= '<h4 class="title"><a href="redemption-details.php?gift_id='.trim($obj->GiftID).'&type=topup">'.$obj->GiftName.'</a></h4>';
					$output.= '<div class="lp-post-comments-num">';
					$output.= $obj->Point . ' points <br />';
					if (strlen($obj->Description) < 101) {
							$output.= $obj->Description;
					} else {
							$output.= substr($obj->Description,0,100)."...";
					}
					$output.= '</div>';
					$output.= '<a class="deals" href="redemption-details.php?gift_id='.trim($obj->GiftID).'&type=topup">Read More >></a>';
					$output.= '</li>';
/*
					$output.= $the_result->GiftID . '<br>';
					$output.= $obj->GiftName . '<br>';
					$output.= $obj->Category . '<br>';
					$output.= $obj->Description . '<br>';
					$output.= $obj->Point . '<br>';
					$output.= $obj->Picture . '<br>';
					$output.= $obj->GiftQty . '<br>';
					$output.= $obj->GiftCompanyCode . '<br>';
					$output.= $obj->GiftCompanyName . '<br>';
					$output.= $obj->TotalOutlets . '<br>';
					$output.= '<br><br>';
*/
				}

			} else {

				$output = 'No record found.';

			}

		} else {

			$output = 'Something went wrong.';

		}

		return $output;
	}

	/* -----------------------------------------------------------------------------------------*/

	// View Single Topup Reward Catalogue
	function GetTopUpGiftCatalogueSingle($the_gift_id = '') {

		global $config_api_url, $config_ws_key, $config_company_code, $config_brand_code;

		$client = new SoapClient($config_api_url);

		$result = $client->GetTopUpGiftCatalogue(array(
				"WSKey" => $config_ws_key,
				"WSCompanyCode" => $config_company_code,
				"WSBranchCode" => $config_brand_code
				));

		//var_dump($result); die();
		//var_dump($result->GetGiftCatalogueResult->GetGiftCatalogueResultDetails);

		$output = '';

		if (isset($result->GetTopUpGiftCatalogueResult->ResponseHeader->ErrorCode) && $result->GetTopUpGiftCatalogueResult->ResponseHeader->ErrorCode == '00' && isset($result->GetTopUpGiftCatalogueResult->GetTopUpGiftCatalogueResultDetails)) {

			$the_result = $result->GetTopUpGiftCatalogueResult->GetTopUpGiftCatalogueResultDetails;

			// Check if loop is available
			if (!empty($the_result) && count($the_result) == 1) {

				if ($the_gift_id == trim($the_result->GiftID)) {
					$output["GiftID"] = $the_result->GiftID;
					$output["GiftName"] = $the_result->GiftName;
					$output["Category"] = $the_result->Category;
					$output["Description"] = $the_result->Description;
					$output["Point"] = $the_result->Point;
					$output["Picture"] = $the_result->Picture;
					$output["GiftQty"] = $the_result->GiftQty;
					$output["GiftCompanyCode"] = $the_result->GiftCompanyCode;
					$output["GiftCompanyName"] = $the_result->GiftCompanyName;
					$output["TotalOutlets"] = $the_result->TotalOutlets;
				}

			/*
				$output.= $the_result->GiftID . '<br>';
			    $output.= $the_result->GiftName . '<br>';
				$output.= $the_result->Category . '<br>';
			    $output.= $the_result->Description . '<br>';
				$output.= $the_result->Point . '<br>';
			    $output.= $the_result->Picture . '<br>';
				$output.= $the_result->GiftQty . '<br>';
			    $output.= $the_result->GiftCompanyCode . '<br>';
				$output.= $the_result->GiftCompanyName . '<br>';
				$output.= $the_result->TotalOutlets . '<br>';
			    $output.= '<br><br>';
			*/
			} else if (!empty($the_result) && count($the_result) > 1) {

				foreach ($the_result as $obj) {
					if ($the_gift_id == trim($obj->GiftID)) {
						$output["GiftID"] = $obj->GiftID;
						$output["GiftName"] = $obj->GiftName;
						$output["Category"] = $obj->Category;
						$output["Description"] = $obj->Description;
						$output["Point"] = $obj->Point;
						$output["Picture"] = $obj->Picture;
						$output["GiftQty"] = $obj->GiftQty;
						$output["GiftCompanyCode"] = $obj->GiftCompanyCode;
						$output["GiftCompanyName"] = $obj->GiftCompanyName;
						$output["TotalOutlets"] = $obj->TotalOutlets;
					}

			/*
					$output.= $obj->GiftID . '<br>';
					$output.= $obj->GiftName . '<br>';
					$output.= $obj->Category . '<br>';
					$output.= $obj->Description . '<br>';
					$output.= $obj->Point . '<br>';
					$output.= $obj->Picture . '<br>';
					$output.= $obj->GiftQty . '<br>';
					$output.= $obj->GiftCompanyCode . '<br>';
					$output.= $obj->GiftCompanyName . '<br>';
					$output.= $obj->TotalOutlets . '<br>';
					$output.= '<br><br>';
			*/
				}

			} else {

				$output = 'No record found.';

			}

		} else {

			$output = 'Something went wrong.';

		}

		return $output;

	}


	/*========================= INPUT API ========================*/

	// Register a member
	function RegisterMember(
		$reg_Card = "",
		$reg_Title = "",
		$reg_FullName = "",
		$reg_IC = "",
		$reg_BirthDate = "",
		$reg_Gender = "",
		$reg_Race = "",
		$reg_Nationality = "",
		$reg_MaritalStatus = "",
		$reg_OwnCar = "",
		$reg_OwnCreditCard = "",
		$reg_HomeAddress1 = "",
		$reg_HomeAddress2 = "",
		$reg_HomeAddress3 = "",
		$reg_HomeCity = "",
		$reg_HomeState = "",
		$reg_HomeCountry = "",
		$reg_HomeZip = "",
		$reg_HomePhone = "",
		$reg_HomeEmail = "",
		$reg_MobilePhone = "",
		$reg_OfficeAddress1 = "",
		$reg_OfficeAddress2 = "",
		$reg_OfficeAddress3 = "",
		$reg_OfficeCity = "",
		$reg_OfficeState = "",
		$reg_OfficeCountry = "",
		$reg_OfficeZip = "",
		$reg_OfficeEmail = "",
		$reg_OfficePhone = "",
		$reg_OfficeExt = "",
		$reg_OfficeFax = "",
		$reg_MSISDN = "") {

		global $config_api_url, $config_ws_key, $config_company_code, $config_brand_code;

		$client = new SoapClient($config_api_url);

		$result = $client->RegisterMember(array(
				"WSKey" => $config_ws_key,
				"WSCompanyCode" => $config_company_code,
				"WSBranchCode" => $config_brand_code,
				"Card" => $reg_Card,
				"Title" => $reg_Title ,
				"FullName" => $reg_FullName ,
				"IC" => $reg_IC ,
				"BirthDate" => $reg_BirthDate ,
				"Gender" => $reg_Gender ,
				"Race" => $reg_Race ,
				"Nationality" => $reg_Nationality ,
				"MaritalStatus" => $reg_MaritalStatus ,
				"OwnCar" => $reg_OwnCar ,
				"OwnCreditCard" => $reg_OwnCreditCard ,
				"HomeAddress1" => $reg_HomeAddress1 ,
				"HomeAddress2" => $reg_HomeAddress2 ,
				"HomeAddress3" => $reg_HomeAddress3 ,
				"HomeCity" => $reg_HomeCity ,
				"HomeState" => $reg_HomeState ,
				"HomeCountry" => $reg_HomeCountry ,
				"HomeZip" => $reg_HomeZip ,
				"HomePhone" => $reg_HomePhone ,
				"HomeEmail" => $reg_HomeEmail ,
				"MobilePhone" => $reg_MobilePhone ,
				"OfficeAddress1" => $reg_OfficeAddress1 ,
				"OfficeAddress2" => $reg_OfficeAddress2 ,
				"OfficeAddress3" => $reg_OfficeAddress3 ,
				"OfficeCity" => $reg_OfficeCity ,
				"OfficeState" => $reg_OfficeState ,
				"OfficeCountry" => $reg_OfficeCountry ,
				"OfficeZip" => $reg_OfficeZip ,
				"OfficeEmail" => $reg_OfficeEmail ,
				"OfficePhone" => $reg_OfficePhone ,
				"OfficeExt" => $reg_OfficeExt ,
				"OfficeFax" => $reg_OfficeFax ,
				"MSISDN" => $reg_MSISDN
			));


		//var_dump($result); die();
		//var_dump($result->GetGiftCatalogueResult->GetGiftCatalogueResultDetails);

		$output = '';

		if (isset($result->RegisterMemberResult->ResponseHeader->ErrorCode) && $result->RegisterMemberResult->ResponseHeader->ErrorCode == '00') {

			$output.= 'Registration successful';

		} else {

			$output.= 'Registration not successful';
			$output.= '<br>Reason: '. $result->RegisterMemberResult->ResponseHeader->ErrorMessage;

		}

		return $output;
	}

	/* -----------------------------------------------------------------------------------------*/

	// Update member profile
	function UpdateMemberProfile() {

		/*
		$upd_Card = "",
		$upd_Password = "",
		$upd_Title = "",
		$upd_FullName = "",
		$upd_IC = "",
		$upd_BirthDate = "",
		$upd_Gender = "",
		$upd_Race = "",
		$upd_Nationality = "",
		$upd_MaritalStatus = "",
		$upd_OwnCar = "",
		$upd_OwnCreditCard = "",
		$upd_HomeAddress1 = "",
		$upd_HomeAddress2 = "",
		$upd_HomeAddress3 = "",
		$upd_HomeCity = "",
		$upd_HomeState = "",
		$upd_HomeCountry = "",
		$upd_HomeZip = "",
		$upd_HomePhone = "",
		$upd_HomeEmail = "",
		$upd_MobilePhone = "",
		$upd_OfficeAddress1 = "",
		$upd_OfficeAddress2 = "",
		$upd_OfficeAddress3 = "",
		$upd_OfficeCity = "",
		$upd_OfficeState = "",
		$upd_OfficeCountry = "",
		$upd_OfficeZip = "",
		$upd_OfficeEmail = "",
		$upd_OfficePhone = "",
		$upd_OfficeExt = "",
		$upd_OfficeFax = "" */

		global $config_api_url, $config_ws_key, $config_company_code, $config_brand_code;

		$client = new SoapClient($config_api_url);

		/* For testing only. Please remove */
/*		if (empty($_SESSION["profile"]["bcard_fullname"])) {
			$_SESSION["profile"]["bcard_fullname"] = "Random Name ".rand(0,1000);
		}

		if (empty($_SESSION["profile"]["bcard_ic"])) {
			$_SESSION["profile"]["bcard_ic"] = rand(100000000000,900000000000);
		}

		if (empty($_SESSION["profile"]["bcard_mobilephone"])) {
			$_SESSION["profile"]["bcard_mobilephone"] = "601".rand(1000000,9000000);
		}

		if (empty($_SESSION["profile"]["bcard_primaryemail"])) {
			$_SESSION["profile"]["bcard_primaryemail"] = "testing-".rand(10000,99999)."@netallianz.com";
		}
*/

		$result = $client->UpdateMemberProfile(array(
				"WSKey" => $config_ws_key,
				"WSCompanyCode" => $config_company_code,
				"WSBranchCode" => $config_brand_code,
				"Card" => $_SESSION["bcard_number"],
				"Password" => $_SESSION["bcard_password"],
				"Title" => $_SESSION["profile"]["bcard_title"] ,
				"FullName" => $_SESSION["profile"]["bcard_fullname"] ,
				"IC" => $_SESSION["profile"]["bcard_ic"] ,
				"BirthDate" => $_SESSION["profile"]["bcard_birthdate"] ,
				"Gender" => $_SESSION["profile"]["bcard_gender"] ,
				"Race" => trim($_SESSION["profile"]["bcard_race"]) ,
				"Nationality" => $_SESSION["profile"]["bcard_nationality"] ,
				"MaritalStatus" => $_SESSION["profile"]["bcard_maritalstatus"] ,
				"OwnCar" => $_SESSION["profile"]["bcard_ownacar"] ,
				"OwnCreditCard" => $_SESSION["profile"]["bcard_ownacreditcard"] ,
				"HomeAddress1" => $_SESSION["profile"]["bcard_homeaddress1"] ,
				"HomeAddress2" => $_SESSION["profile"]["bcard_homeaddress2"] ,
				"HomeAddress3" => $_SESSION["profile"]["bcard_homeaddress3"] ,
				"HomeCity" => $_SESSION["profile"]["bcard_homecity"] ,
				"HomeState" => $_SESSION["profile"]["bcard_homestate"] ,
				"HomeCountry" => $_SESSION["profile"]["bcard_homecountry"] ,
				"HomeZip" => $_SESSION["profile"]["bcard_homezip"] ,
				"HomePhone" => $_SESSION["profile"]["bcard_homephone"] ,
				"HomeEmail" => $_SESSION["profile"]["bcard_primaryemail"] ,
				"MobilePhone" => $_SESSION["profile"]["bcard_mobilephone"] ,
				"OfficeAddress1" => $_SESSION["profile"]["bcard_officeaddress1"] ,
				"OfficeAddress2" => $_SESSION["profile"]["bcard_officeaddress2"] ,
				"OfficeAddress3" => $_SESSION["profile"]["bcard_officeaddress3"] ,
				"OfficeCity" => $_SESSION["profile"]["bcard_officecity"] ,
				"OfficeState" => $_SESSION["profile"]["bcard_officestate"] ,
				"OfficeCountry" => $_SESSION["profile"]["bcard_officecountry"],
				"OfficeZip" => $_SESSION["profile"]["bcard_officezip"] ,
				"OfficeEmail" => $_SESSION["profile"]["bcard_officeemail"] ,
				"OfficePhone" => $_SESSION["profile"]["bcard_officephone"] ,
				"OfficeExt" => $_SESSION["profile"]["bcard_officeext"],
				"OfficeFax" => $_SESSION["profile"]["bcard_officefax"]));

	/*			"Title" => $upd_Title ,
				"FullName" => $upd_FullName ,
				"IC" => $upd_IC ,
				"BirthDate" => $upd_BirthDate ,
				"Gender" => $upd_Gender ,
				"Race" => $upd_Race ,
				"Nationality" => $upd_Nationality ,
				"MaritalStatus" => $upd_MaritalStatus ,
				"OwnCar" => $upd_OwnCar ,
				"OwnCreditCard" => $upd_OwnCreditCard ,
				"HomeAddress1" => $upd_HomeAddress1 ,
				"HomeAddress2" => $upd_HomeAddress2 ,
				"HomeAddress3" => $upd_HomeAddress3 ,
				"HomeCity" => $upd_HomeCity ,
				"HomeState" => $upd_HomeState ,
				"HomeCountry" => $upd_HomeCountry ,
				"HomeZip" => $upd_HomeZip ,
				"HomePhone" => $upd_HomePhone ,
				"HomeEmail" => $upd_HomeEmail ,
				"MobilePhone" => $upd_MobilePhone ,
				"OfficeAddress1" => $upd_OfficeAddress1 ,
				"OfficeAddress2" => $upd_OfficeAddress2 ,
				"OfficeAddress3" => $upd_OfficeAddress3 ,
				"OfficeCity" => $upd_OfficeCity ,
				"OfficeState" => $upd_OfficeState ,
				"OfficeCountry" => $upd_OfficeCountry ,
				"OfficeZip" => $upd_OfficeZip ,
				"OfficeEmail" => $upd_OfficeEmail ,
				"OfficePhone" => $upd_OfficePhone ,
				"OfficeExt" => $upd_OfficeExt ,
				"OfficeFax" => $upd_OfficeFax
	*/



		//var_dump($result); //die();
		//var_dump($result->GetGiftCatalogueResult->GetGiftCatalogueResultDetails);

		$output = '';

		if (isset($result->UpdateMemberProfileResult->ResponseHeader->ErrorCode) && $result->UpdateMemberProfileResult->ResponseHeader->ErrorCode == '00') {

			$output.= 'Update profile successful';
			GetMemberProfile();

		} else {

			$output.= 'Update profile not successful';
			$output.= '<br>Reason: '. $result->UpdateMemberProfileResult->ResponseHeader->ErrorMessage;
			GetMemberProfile();
		}

		return $output;
	}

	/* -----------------------------------------------------------------------------------------*/


	// Reset Password
	function ResetPassword($the_card_number = "") {

		global $config_api_url, $config_ws_key, $config_company_code, $config_brand_code;

		$client = new SoapClient($config_api_url);

		$result = $client->ResetPassword(array(
				"WSKey" => $config_ws_key,
				"WSCompanyCode" => $config_company_code,
				"WSBranchCode" => $config_brand_code ,
				"Card" => $the_card_number));


		//var_dump($result); die();
		//var_dump($result->GetGiftCatalogueResult->GetGiftCatalogueResultDetails);

		$output = '';

		if (isset($result->ResetPasswordResult->ResponseHeader->ErrorCode) && $result->ResetPasswordResult->ResponseHeader->ErrorCode == '00') {

			$output = '<p style="font-size:14px;">PIN Reset Successfully. We’ve sent the new PIN via SMS and to your registered email address. </p>';

		} else {

			$output = 'Something went wrong.';
			$output.= '<br>Reason: '. $result->ResetPasswordResult->ResponseHeader->ErrorMessage;
			$output.= '<br><br><a href="forgot-pin.php"><input type="submit" value="Try Again" style="background: #ea6f25; color: #fff; border-radius: 5px;"></a>';
		}

		return $output;
	}

	/* -----------------------------------------------------------------------------------------*/

	// Reset Password MSISDN
	function ResetPasswordMSISDN($the_card_number = "") {

		global $config_api_url, $config_ws_key, $config_company_code, $config_brand_code;

		$client = new SoapClient($config_api_url);

		$result = $client->ResetPasswordMSISDN(array(
				"WSKey" => $config_ws_key,
				"WSCompanyCode" => $config_company_code,
				"WSBranchCode" => $config_brand_code ,
				"Password" => "hello123",
				"Card" => $the_card_number));


		var_dump($result); die();
		//var_dump($result->GetGiftCatalogueResult->GetGiftCatalogueResultDetails);

		$output = '';

		if (isset($result->ResetPasswordMSISDNResult->ResponseHeader->ErrorCode) && $result->ResetPasswordMSISDNResult->ResponseHeader->ErrorCode == '00') {

			$output = '<p style="font-size:14px;">Your have requested to reset your login PIN. A link will be sent to your registered email address.</p>';

		} else {

			$output = 'Something went wrong.';
			$output.= '<br>Reason: '. $result->ResetPasswordMSISDNResult->ResponseHeader->ErrorMessage;
		}

		return $output;
	}

	/* -----------------------------------------------------------------------------------------*/

	// Change member's password
	function ChangeMemberPassword($the_old_password = "",$the_new_password = "") {

		global $config_api_url, $config_ws_key, $config_company_code, $config_brand_code;

		$client = new SoapClient($config_api_url);

		$result = $client->ChangeMemberPassword(array(
				"WSKey" => $config_ws_key,
				"WSCompanyCode" => $config_company_code,
				"WSBranchCode" => $config_brand_code ,
				"Card" => $_SESSION["bcard_number"],
				"OldPassword" => $the_old_password,
				"NewPassword" => $the_new_password
				));


		//var_dump($result); die();
		//var_dump($result->GetGiftCatalogueResult->GetGiftCatalogueResultDetails);

		$output = '';

		if (isset($result->ChangeMemberPasswordResult->ResponseHeader->ErrorCode) && $result->ChangeMemberPasswordResult->ResponseHeader->ErrorCode == '00') {

			$output = "PIN updated successfully.";
			$_SESSION["bcard_password"] = $the_new_password;

		} else {

			$output = 'Unable to update PIN';
			$output.= '<br>Reason: '. $result->ChangeMemberPasswordResult->ResponseHeader->ErrorMessage;
		}

		return $output;
	}

	/* -----------------------------------------------------------------------------------------*/

	// Reward
	function Reward(
			$reward_RedeemDate = "2016-07-31T00:00:00",
			$reward_BillNo = "",
			$reward_TotalAmount = "",
			$reward_TotalPoint = ""
		) {

		global $config_api_url, $config_ws_key, $config_company_code, $config_brand_code, $config_postid;

		$client = new SoapClient($config_api_url);

		$result = $client->Reward(array(
				"WSKey" => $config_ws_key,
				"WSCompanyCode" => $config_company_code,
				"WSBranchCode" => $config_brand_code ,
				"Card" => $_SESSION["bcard_number"],
				"POSID" => $config_postid ,
				"TranxDate" => $reward_RedeemDate,
				"BillNo" => $reward_BillNo,
				"TotalAmount" => $reward_TotalAmount,
				"TotalPoint" => $reward_TotalPoint
				));


		//var_dump($result); die();
		//var_dump($result->GetGiftCatalogueResult->GetGiftCatalogueResultDetails);

		$output = '';

		if (isset($result->RewardResult->ResponseHeader->ErrorCode) && $result->RewardResult->ResponseHeader->ErrorCode == '00') {

			$output = "Successful";

		} else {

			$output = 'Something went wrong.';
			$output.= '<br>Reason: '. $result->RewardResult->ResponseHeader->ErrorMessage;
		}

		return $output;
	}

	/* -----------------------------------------------------------------------------------------*/


	// Redemption
	function CatalogueRedemption(
			$redemption_GiftID = "",
			$redemption_RedeemQty = "",
			$redemption_RedeemDate = "2016-07-31T00:00:00",
			$redemption_RecipientName = "",
			$redemption_RecipientHp = "",
			$redemption_RecipientEmail = "",
			$redemption_RecipientAddress1 = "",
			$redemption_RecipientAddress2 = "",
			$redemption_RecipientAddress3 = "",
			$redemption_RecipientZip = "",
			$redemption_RecipientState = "",
			$redemption_RecipientCountry = ""
		) {

		global $config_api_url, $config_ws_key, $config_company_code, $config_brand_code, $config_postid;

		$client = new SoapClient($config_api_url);

		$result = $client->CatalogueRedemption(array(
				"WSKey" => $config_ws_key,
				"WSCompanyCode" => $config_company_code,
				"WSBranchCode" => $config_brand_code ,
				"Password" => $_SESSION["bcard_password"],
				"Card" => $_SESSION["bcard_number"],
				"GiftID" => $redemption_GiftID,
				"RedeemQty" => $redemption_RedeemQty,
				"RedeemDate" => $redemption_RedeemDate,
				"RecipientName" => $redemption_RecipientName,
				"RecipientHp" => $redemption_RecipientHp,
				"RecipientEmail" => $redemption_RecipientEmail,
				"RecipientAddress1" => $redemption_RecipientAddress1,
				"RecipientAddress2" => $redemption_RecipientAddress2,
				"RecipientAddress3" => $redemption_RecipientAddress3,
				"RecipientZip" => $redemption_RecipientZip,
				"RecipientState" => $redemption_RecipientState,
				"RecipientCountry" => $redemption_RecipientCountry
				));


		//var_dump($result); die();
		//var_dump($result->GetGiftCatalogueResult->GetGiftCatalogueResultDetails);

		$output = '';

		if (isset($result->CatalogueRedemptionResult->ResponseHeader->ErrorCode) && $result->CatalogueRedemptionResult->ResponseHeader->ErrorCode == '00') {

			$output = "Successful";

		} else {

			$output = 'Something went wrong.';
			$output.= '<br>Reason: '. $result->CatalogueRedemptionResult->ResponseHeader->ErrorMessage;
		}

		return $output;
	}

	/* -----------------------------------------------------------------------------------------*/


	// Top Up Redemption
	function TopUpRedemption(
			$redemption_GiftID = "",
			$redemption_RedeemQty = "",
			$redemption_RedeemDate = "2012-12-31T00:00:00",
			$redemption_MSISDN = ""
		) {

		global $config_api_url, $config_ws_key, $config_company_code, $config_brand_code, $config_postid;

		$client = new SoapClient($config_api_url);

		$result = $client->TopUpRedemption(array(
				"WSKey" => $config_ws_key,
				"WSCompanyCode" => $config_company_code,
				"WSBranchCode" => $config_brand_code ,
				"Password" => $_SESSION["bcard_password"],
				"Card" => $_SESSION["bcard_number"],
				"GiftID" => $redemption_GiftID ,
				"RedeemQty" => $redemption_RedeemQty,
				"RedeemDate" => $redemption_RedeemDate,
				"MSISDN" => $redemption_MSISDN
				));


		//var_dump($result); die();
		//var_dump($result->GetGiftCatalogueResult->GetGiftCatalogueResultDetails);

		$output = '';

		if (isset($result->TopUpRedemptionResult->ResponseHeader->ErrorCode) && $result->TopUpRedemptionResult->ResponseHeader->ErrorCode == '00') {

			$output = "Successful";

		} else {

			$output = 'Something went wrong.';
			$output.= '<br>Reason: '. $result->TopUpRedemptionResult->ResponseHeader->ErrorMessage;
		}

		return $output;
	}

	/* -----------------------------------------------------------------------------------------*/

//	echo '<pre>';
//	echo ChangeMemberPassword('111111','Ab5372635');

/*
	echo CatalogueRedemption(
			$redeem_Password = "111111",
			$redeem_GiftID = "1",
			$redeem_RedeemQty = "1",
			$redeem_RedeemDate = "2016-07-31T00:00:00",
			$redeem_RecipientName = "Netallianz",
			$redeem_RecipientHp = "12873613",
			$redeem_RecipientEmail = "akldjhad@asdmasd.com",
			$redeem_RecipientAddress1 = "Address 1",
			$redeem_RecipientAddress2 = "2",
			$redeem_RecipientAddress3 = "3",
			$redeem_RecipientZip = "58200",
			$redeem_RecipientState = "KL",
			$redeem_RecipientCountry = "Malaysia"
		);
*/

/*
echo Reward(
			$reward_RedeemDate = "2016-07-31T00:00:00",
			$reward_BillNo = "1298383",
			$reward_TotalAmount = "1000",
			$reward_TotalPoint = "1000"
		);
*/
/*
echo Redemption(
			$redemption_password = "111111",
			$redemption_RedeemDate = "2016-07-31T00:00:00",
			$redemption_point = "10"
		);
*/
/*	echo RegisterMember("6298430000010014",		"Mr",		"Something",		"870706141111","1987-01-01",		"M",		"Chinese",		"Malaysia",		"Y",		"Y",		"Y",		"Street 1",		"reg_HomeAddress2",		"reg_HomeAddress3",		"reg_HomeCity",		"JOHOR",		"Malaysia",		"58200",		"5555555",	"somethinghahahehelol@gmail.com",		"0125555555",		"reg_OfficeAddress1",		"reg_OfficeAddress2",		"reg_OfficeAddress3",		"KL",		"JOHOR",		"reg_OfficeCountry",		"83423",		"somethinghahahehelol@gmail.com",		"821372387",		"123",		"128736123",		"123456"	);
*/

//	echo UpdateMemberProfile("6298430000010014",	"111112",	"Mr",		"Something",		"870706141111","1987-01-01",		"M",		"Chinese",		"Malaysia",		"Y",		"Y",		"Y",		"Street 1",		"reg_HomeAddress2",		"reg_HomeAddress3",		"reg_HomeCity",		"JOHOR",		"Malaysia",		"58200",		"182361232",	"somethinghahahehelol@gmail.com",		"0125555555",		"reg_OfficeAddress1",		"reg_OfficeAddress2",		"reg_OfficeAddress3",		"KL",		"JOHOR",		"reg_OfficeCountry",		"83423",		"somethinghahahehelol@gmail.com",		"821372387",		"123",		"128736123",		"123456"	);

//	echo GetMemberProfile();
//	echo '<pre>';
?>
