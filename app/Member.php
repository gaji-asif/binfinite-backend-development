<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $table = 'lms_members';

    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * Relation to lms_member_cards
     *
     */
    public function cards()
    {
        return $this->hasMany('App\MemberCard', 'lms_MemberID', 'lms_MemberID');
    }

    /**
     * Sets primary card for this member
     *
     */
    public function setPrimaryCard($cardNo)
    {
        // Get the intended card by cardNo
        $card = $this->cards()->where('card_no', $cardNo)->first();

        if ($card) {
            // Set current primary as inactive first
            $this->cards()->where('is_primary', true)->update(['is_primary' => false]);

            $card->is_primary = true;
            $card->save();
            return true;
        }
        return false;
    }

    /**
     * Attribute for primary card for this member
     *
     */
    public function getPrimaryCardAttribute()
    {
        return $this->cards()->where('is_primary', true)->first();
    }

    /**
     * Attribute for checking if the user has registered in SSO member
     *
     */
    public function getIsRegisteredAttribute()
    {
        return $this->lms_RegisteredDateTime != null;
    }
}
