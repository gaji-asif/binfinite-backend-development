<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Rinvex\Categories\Traits\Categorizable;
use Illuminate\Support\Str;
use App\Category;
use \Storage;
use App\Traits\StorageImageResolver;

class Merchant extends Model
{
	use Categorizable, StorageImageResolver;

	protected $appends = ['profile_url'];

	/**
	 * Relation to merchant_offers
	 *
	 */
	public function offers()
	{
		return $this->hasMany('App\MerchantOffer', 'merchant_id');
	}

	/**
	 * Relation to merchant_offers for active offers only
	 *
	 */
	public function activeOffers()
	{
		return $this->offers()->active(true);
	}

	/**
	 * Relation to merchant_outlets
	 *
	 */
	public function outlets()
	{
		return $this->hasMany('App\MerchantOutlet', 'merchant_id');
	}

	/**
	 * Attribute for full url for merchant logo
	 *
	 */
	public function getLogoImageAttribute()
	{
		return $this->resolveImage($this->logo_url);
	}

	/**
	 * Attribute for full url for merchant profile top banner
	 *
	 */
	public function getProfileBannerImageAttribute()
	{
		return $this->resolveImage($this->profile_banner_url);
	}

	/**
	 * Attribute for full url for merchant profile footer banner
	 *
	 */
	public function getProfileFooterBannerImageAttribute()
	{
		return $this->resolveImage($this->profile_footer_banner_url);
	}

	/**
	 * Attribute merchant name slugified
	 *
	 */
	public function getSlugAttribute()
	{
		return Str::slug($this->name);
	}

	/**
	 * Attribute for merchant slug for better SEO url
	 *
	 */
	public function getSlugUrlAttribute()
	{
		return $this->id . '/' . $this->slug;
	}

	/**
	 * Attribute for merchant slug for better SEO url
	 *
	 */
	public function getProfileUrlAttribute()
	{
		return route('partner_profile', [ $this->id, $this->slug ]);
	}

	/**
	 * Attribute for merchant slug for better SEO url
	 *
	 */
	public function getConditionalUrlAttribute()
	{
		$activeOffers = $this->activeOffers;
		$offerCount = $activeOffers->count();

		if ($offerCount == 1) {
			return route('partner_offer', [$this->id, $this->slug, $activeOffers->first()->id]);
		}

		if ($offerCount > 1) {
			return route('partner_profile', [$this->id, $this->slug]);
		}

		return route('partner_detail', [$this->id, $this->slug]);
	}
}
