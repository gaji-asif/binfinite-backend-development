<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NetworkPartner extends Model
{
    protected $fillable = [
        'business_name','website','brand_name','brand_website','brand_category','contact_name','office_contact','mobile_contact','contact_regards'
    ];
}
