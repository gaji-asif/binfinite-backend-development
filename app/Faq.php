<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;


class Faq extends Model
{
    public function getCategorySlugAttribute() {
    	return Str::slug($this->category);
    }
}
