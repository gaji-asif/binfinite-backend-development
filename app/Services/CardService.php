<?php

namespace App\Services;

use App\Exceptions\GeneralException;
use App\Adapters\MYLMS\Exceptions\LMSErrorException;
use App\User;
use App\Member;
use App\MemberCard;
use Auth;

use MYLMS;

/**
 * This class handles all logic for LMS interaction
 */

class CardService
{

    /**
     * Logs in user and save SSO details in user table
     *
     * @param string $email
     * @param string $password
     * @return App\User $user
     * @throws GeneralException|MYLMSException
     */
    public function ssoLogin($email, $password) {
        $loginResult = MYLMS::ssoLogin($email, $password);

        if ($loginResult && $loginResult->data) {

            $ssoId = $loginResult->data['SSOID'];
            $sessionId = $loginResult->data['sessionID'];

            $user = User::where('email', $email)->first();

            // User doesnt exist? Create new.
            if (! $user) {
                $user = User::create([
                    'email' => $email,
                ]);
            }

            // Set sso details here
            $user->lms_sso_id = $ssoId;
            $user->lms_session_id = $sessionId;

            $user->save();

            // Set Laravel auth for user
            Auth::login($user);

            return Auth::user();
        }

        return false;
    }

    /**
     * Sync user's profle with member profile from LMS
     *
     * @param App\User $user
     * @param string $cardNo
     * @return App\Member
     * @throws GeneralException|MYLMSException
     */
    public function syncMemberProfile($user)
    {
        $lmsMemberProfile = MYLMS::ssoGetMemberProfile($user->email, $user->lms_sso_id)->data;

        $member = $user->lmsMember;

        if (! isset($member) && isset($lmsMemberProfile['MemberID'])) {
            $existingMember = Member::where('lms_MemberID', $lmsMemberProfile['MemberID'])->first();

            if ($existingMember) {
                // Found existing member! If its not tied to any user lets tie it up
                if ($existingMember->user_id == null) {
                    $existingMember->user_id = $user->id;
                    $member = $user->lmsMember()->first();
                } else {
                    throw new GeneralException('This member has already been registered before');
                }
            }
        }

        if (! $member) {
            $member = new Member;
            $member->user_id = $user->id;
        }

        $member = $this->_populateLMSMemberInfo($member, $lmsMemberProfile);
        $member->save();

        return $member;
    }

    /**
     * Sync member and member's card list with LMS. Will remove cards in db if no longer in LMS' list
     * and add new ones. Will call syncMemberProfile() to make sure member object is available.
     *
     * @param App\User $user
     * @return null
     * @throws GeneralException|MYLMSException
     */
    public function syncMemberCards($user)
    {
        // Get all cards for this member
        $lmsCards = MYLMS::ssoGetCard($user->email, $user->lms_sso_id)->data;
        $member = $user->lmsMember;

        // Populate member profile
        $member = $this->syncMemberProfile($user);

        if (! $member->lms_MemberID) {
            // No member id yet! Need to bail
            return;
        }

        // Remove unlisted cards
        if ($lmsCards) {
            $availableCardNos = $lmsCards->pluck('Card')->toArray();
            $unlistedMembersCardsEloquent = MemberCard::whereNotIn('card_no', $availableCardNos)->where('lms_MemberID', $member->lms_MemberID);

            // Or should we unlink through LMS API? Not sure yet.
            $unlistedMembersCardsEloquent->delete();
        }

        // Create new card if not exists, if already exists update all card info.
        $lmsCards->each(function($lmsCard) use ($user, $member) {
            $cardNo = $lmsCard['Card'];
            $card = MemberCard::where('lms_MemberID', $member->lms_MemberID)->where('card_no', $cardNo)->first();

            if (! $card) {
                $card = new MemberCard;
                $card->card_no = $cardNo;
                $card->lms_MemberID = $member->lms_MemberID;
            }

            $card = $this->_populateLMSMemberCardInfo($card, $lmsCard);
            $card->save();
        });

        // Set card with highest balance as default primary card
        if (! $member->primaryCard) {
            $card = $member->cards()->orderBy('lms_BalancePoint', 'desc')->first();

            if ($card) {
                $member->setPrimaryCard($card->card_no);
            }
        }
    }

    /**
     * Link member card to SSO user through member (SSO).
     *
     * @param App\User $user
     * @param string $cardNo
     * @param string $cardPin
     * @return null
     * @throws GeneralException|MYLMSException
     */
    public function linkUserToCard($user, $cardNo, $cardPin)
    {
        // Check pin first
        $this->_checkCard($cardNo, $cardPin);

        if ($user) {

            $member = $user->lmsMember;

            // Peculiar case, has the user registered SSO?
            if (! $member) {
                throw new GeneralException('Sorry, are unable to fetch your information');
            }

            $existingCard = \App\MemberCard::where('card_no', $cardNo)->first();

            // Check if already exists in db
            if ($existingCard) {
                if ($existingCard->lms_MemberID == null) {
                    
                    $existingCard->lms_MemberID = $member->lms_MemberID;
                    $existingCard->save();
                } else if ($existingCard->lms_MemberID != $member->lms_MemberID) {
                    // Card is already linked based on lms_MemberID
                    throw new GeneralException('Card is already linked by someone else');
                } else {
                    // Card is already linked by this user
                    throw new GeneralException('Card is already linked');
                }
            }

            try {
                $result = MYLMS::ssoAddCard($member->lms_Email, $cardNo, $cardPin);
            } catch (LMSErrorException $e) {
                if ($e->is(LMSErrorException::SERVER_EXCEPTION)) {
                    // Card is already linked in server side
                    throw new GeneralException('Card is already linked by someone else');
                }
            }

            $this->syncMemberCards($user);
        }
    }

    /**
     * Update SSO member contact details (email & mobile)
     *
     * @param App\User $user
     * @param string $email
     * @param string $mobile
     * @return null
     * @throws GeneralException|MYLMSException
     */
    public function updateMemberContact($user, $email, $mobile)
    {
        //TODO: Send to LMS. Currently API doesnt support micro-updates and we dont have full user info
    }

    /**
     * Register SSO member
     *
     * @param App\User $user
     * @param string ...
     * @return null
     * @throws GeneralException|MYLMSException
     */
    public function registerSSO($user, $name, $nationality, $country,
        $ic = null, $passport = null, $dob = null, $address1 = null, $address2 = null, 
        $address3 = null, $postcode = null, $city = null, $state = null, 
        $paymentRegistered = null, $masterRegistered = null)
    {
        $member = $user->lmsMember;

        $params = [
            'SSOID' => $user->lms_sso_id,
            'Email' => $member->lms_Email,
            'Name' => $name,
            'IC' => $ic,
            'Passport' => $passport,
            'DOB' => $dob,
            'Nationality' => $nationality,
            'Mobile' => $member->lms_Mobile,
            'Address1' => $address1,
            'Address2' => $address2,
            'Address3' => $address3,
            'Postcode' => $postcode,
            'City' => $city,
            'State' => $state,
            'Country' => $country,
            'PaymentRegistered' => $paymentRegistered,
            'MasterRegistered' => $masterRegistered,
        ];

        return MYLMS::ssoRegister($params);
    }

    /**
     * Populate card object with data from LMS
     *
     * @param App\MemberCard $card
     * @param array $dataFromLMS
     * @return App\MemberCard
     */
    public function _populateLMSMemberCardInfo($card, $dataFromLMS)
    {
        $card->lms_BalancePoint = $dataFromLMS['BalancePoint'];
        $card->lms_TotalTokenPoint = $dataFromLMS['TotalTokenPoint'];
        return $card;
    }

    /**
     * Populate member object with data from LMS
     *
     * @param App\Member $member
     * @param array $dataFromLMS
     * @return App\Member
     */
    public function _populateLMSMemberInfo($member, $dataFromLMS)
    {
        $member->lms_CreatedDateTime = $dataFromLMS['CreatedDateTime'];
        $member->lms_Email = $dataFromLMS['Email'];
        $member->lms_Mobile = $dataFromLMS['Mobile'];

        // If null the member has not run ssoRegister() yet
        $member->lms_MemberID = $dataFromLMS['MemberID'] ?? null;
        $member->lms_Name = $dataFromLMS['Name'] ?? null;
        $member->lms_RegisteredDateTime = $dataFromLMS['RegisteredDateTime'] ?? null;
        return $member;
    }

    /**
     * Check if card credentials are valid
     *
     * @param string $cardNo
     * @param string $cardPin
     * @return array
     * @throws GeneralException|MYLMSException
     */
    public function _checkCard($cardNo, $cardPin)
    {
        return MYLMS::GetCard($cardNo, $cardPin)->data;   
    }

    // Guarantees a created card
    // public function _getOrCreateCard($cardNo, $cardPin)
    // {
    //     $card = MemberCard::where('card_no', $cardNo)->first();

    //     if (! $card) {
    //         $card = $this->createCard($cardNo, $cardPin);
    //     }

    //     return $card;
    // }


    // public function getCardById($cardNo)
    // {
    //     return MemberCard::where('card_no', $cardNo)->first();
    // }

    // // Old function without SSO
    // public function createCard($cardNo, $cardPin)
    // {
    //     $lmsCard = MYLMS::GetCard($cardNo, $cardPin)->data;
    //     $lmsMember = MYLMS::GetMemberProfile($cardNo, $cardPin)->data;
    //     $card = $this->getCardById($cardNo);

    //     if ($card) {
    //         return $card;
    //     }

    //     $card = new MemberCard();
    //     $member = new Member();

    //     \DB::transaction(function() use ($card, $member, $lmsCard, $lmsMember, $cardNo, $cardPin) {
    //         $member = $this->_populateLMSMemberInfo($member, $lmsMember);
    //         $member->card_no = $cardNo;
    //         $member->user_id = null;
    //         $member->save();

    //         $card = $this->_populateLMSMemberCardInfo($card, $lmsCard);
    //         $card->card_no = $cardNo;
    //         $card->card_pin = $cardPin;
    //         $card->status = '';
    //         $card->save();
    //     });

    //     if ((! $card) || (! $member)) throw new GeneralException('Unable to create member and card');
        
    //     return $card;
    // }


    // Old function without SSO


    // Old function without SSO
    // public function refreshCardInfo($cardNo, $cardPin)
    // {
    //     $lmsCard = MYLMS::GetCard($cardNo, $cardPin)->data;
    //     $card = MemberCard::where('card_no', $cardNo)->first();

    //     if ($card) {
    //         $card = $this->_populateLMSMemberCardInfo($card, $lmsCard);

    //         // Update pin too
    //         $card->card_pin = $cardPin;
    //         $card->save();
    //     }
    // }

}
