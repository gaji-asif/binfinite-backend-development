<?php

namespace App\Services;

use App\Exceptions\GeneralException;
use App\Category;

class ShoponlineService
{
	/**
	 * If logged in & has bcard, returns url. Else null.
	 *
	 * @param $storeId Store identifier by shoponline
	 * @return String|null
	 */
	public static function getAffiliateUrlDefaultMember($storeId)
	{
		$user = \Auth::user();
		if ($user) {
			$member = $user->defaultMember()->first();
			if ($member) {
		        return self::getAffiliateUrl($storeId, $member->card_no);
			}
		}
	}

	/**
	 * If logged in & has bcard, returns url. Else null.
	 *
	 * @param $storeId Store identifier by shoponline
	 * @return String|null
	 */
	public static function getAffiliateUrl($storeId, $cardId)
	{
		$user = \Auth::user();
		$affiliateId = config('services.shoponline.affiliate_id');
        return "https://invol.co/aff_m?offer_id={$storeId}&aff_id={$affiliateId}&source=deeplink_generator&aff_sub={$cardId}";
	}
}