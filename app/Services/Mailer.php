<?php

namespace App\Services;

use Mail;
use Exception;
use Carbon;

class Mailer
{
    public function confirmationActivation($atts)
    {
        Mail::send('emails.confirmation', $atts, function ($message) use ($atts) {
            $message->to($atts['email'], $atts['name'])->subject('Confirmation For BInfinite Account Registration');
        });
    }
}