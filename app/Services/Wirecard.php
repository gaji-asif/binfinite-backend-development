<?php

namespace App\Services;

use Carbon\Carbon;

class Wirecard
{
    public function generateSignature($invoiceNo, $price)
    {
        $data = array();
        $data['request_time_stamp']         = Carbon::now('UTC')->format('YmdHis');
        $data['request_id']                 = $invoiceNo;
        $data['merchant_account_id']        = config('payment.merchant_account_id');
        $data['transaction_type']            = config('payment.transaction_type');
        $data['requested_amount']            = $price;
        $data['requested_amount_currency']   = config('payment.requested_amount_currency');
        $data['redirect_url']               = config('payment.redirect_url');
        $data['secret_key']                  = config('payment.secret_key');

        $signatureSourceString = implode($data, '');
        $data['pre_signature'] = $signatureSourceString;
        $data['signature'] = hash('sha256', $signatureSourceString);

        return $data;
    }
}