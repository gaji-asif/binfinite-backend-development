<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;

use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;

use App\MerchantOffer as MerchantOfferModel;

use NovaAttachMany\AttachMany;
use Ctessier\NovaAdvancedImageField\AdvancedImage;

class MerchantOffer extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\MerchantOffer';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 
        'title',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            Text::make('Title', 'offer_text'),
            Text::make('Subtext', 'offer_subtext'),
            Text::make('Point ratio', 'conversion_text'),
            // AdvancedImage::make('Banner image', 'offer_banner_url')->disk('public')->croppable(3/2),
            AdvancedImage::make('Thumbnail image', 'thumbnail_image_url')->disk('public')->croppable(3/2),
            Date::make('Expire at', 'expire_at'),
            Select::make('Offer type', 'offer_type')->options([
                MerchantOfferModel::DISCOUNT_PURCHASE_PERCENT => ['label' => 'Purchase discount percent', 'group' => 'Discount'],
                MerchantOfferModel::DISCOUNT_REDEMPTION_PERCENT => ['label' => 'Redemption discount percent', 'group' => 'Discount'],
                MerchantOfferModel::DISCOUNT_PURCHASE_VALUE => ['label' => 'Purchase discount value', 'group' => 'Discount'],
                MerchantOfferModel::DISCOUNT_REDEMPTION_VALUE => ['label' => 'Redemption discount value', 'group' => 'Discount'],
                MerchantOfferModel::EARN_PURCHASE_MULTIPLIER => ['label' => 'Purchase points multiplier', 'group' => 'Earn'],
                MerchantOfferModel::EARN_BONUS_POINTS => ['label' => 'Bonus points', 'group' => 'Earn'],
            ]),
            Text::make('Button Text', 'button_text'),

            BelongsTo::make('Merchant', 'merchant'),
            AttachMany::make('Categories'),
            AttachMany::make('Outlets', 'outlets', 'App\Nova\MerchantOutlet'),

            Trix::make('Terms and Conditions', 'terms_html'),
            // AjaxSelect::make('Outlets')
            //     ->get('/api/merchant/{merchant}/outlets')
            //     ->parent('merchant'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
