<?php

namespace App\Exceptions;

class GeneralException extends \Exception
{
	/**
	 * Default error message
	 *
	 */
	const DEFAULT_MESSAGE = 'Server error';

	/**
	 * Gets public friendly error message
	 *
	 * @return string
	 */
	public function getFriendlyMessage()
	{
		return $this->message != '' ? $this->message : self::DEFAULT_MESSAGE;
	}
}