<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
        'card_pin',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        return parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        $hasFriendlyMessage = is_a($exception, GeneralException::class) || is_subclass_of($exception, GeneralException::class);

        if ($hasFriendlyMessage) {
            if ($request->expectsJson()) {
                return jsend_fail($exception->getFriendlyMessage());
            }

            // Friendly message only for production-like environment
            if (! \App::environment(['local', 'development'])) {
                return redirect()->back()->with('error', $exception->getFriendlyMessage());
            }
        }
        return parent::render($request, $exception);
    }

}
