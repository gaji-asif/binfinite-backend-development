<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Adapters\MYLMS\MYLMSService;

class MYLMSServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // $this->app->singleton(MYLMSService::class, function ($app) {
        //     return new Connection($app['config']['riak']);
        // });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
