<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Blade;

class BladeHelperServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('cssImage', function ($url, $base='/images') {
            return 'background-image:url(' . $base . '/' . $url . ');' ;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
