<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Rinvex\Categories\Traits\Categorizable;
use Illuminate\Support\Str;
use App\Category;
use \Storage;
use App\Traits\StorageImageResolver;

class Announcements extends Model
{
    use Categorizable, StorageImageResolver;

    //protected $table = 'announcements';
    
    /**
     * Attribute announcements name slugified
     *
     */
    public function getSlugAttribute()
    {
        return Str::slug($this->announcements_title);
    }

    /**
     * Attribute for announcements slug for better SEO url
     *
     */
    public function getSlugUrlAttribute()
    {
        return route('announcements_detail', [$this->id, $this->slug]);
    }
}
