<?php

namespace App\Helpers;

use App\Order;
use App\User;

use Carbon\Carbon;
use \DB;

class OrderHelper
{
    public static function getLatestInvoiceNo()
    {
        $order = Order::orderBy('id', 'DESC')->first();
        $orderNo = !empty($order) ? $order->id: 0;
        $orderNo = str_pad($orderNo, 6, "0", STR_PAD_LEFT);

        return str_replace("{NO}", $orderNo, config('payment.order_prefix'));
    }

    public static function createOrder($userId, $price, $paymentMethod = 'creditcard')
    {
        $invoiceNo =  self::getLatestInvoiceNo();
        
        $amount = $price * (1 + config('payment.sst_rate'));
        $sstAmount = $price * config('payment.sst_rate');
        $netAmount = $amount - $sstAmount;

        $user = User::where('id',$userId)->first();

        if(empty($user))
            return false;

        //Create pre - order
        $order = [
            'invoice_no' => $invoiceNo,
            'user_id' => $user->id,
            'full_name' => $user->name,
            'address' => '', //get from user address?,
            'amount' => $amount,
            'net_amount' => $netAmount,
            'sst_amount' => $sstAmount,
            'currency' => config('payment.requested_amount_currency'),
            'category' => config('payment.transaction_type'),
            'payment_method' => $paymentMethod
        ];

        return Order::create($order);
    }

    public static function log($data) 
    {
        $log = [
            'invoice_no' => $data['request_id'],
            'masked_account_number' => $data['masked_account_number'],
            'amount' => $data['requested_amount'],
            'currency' => $data['requested_amount_currency'],
            'transaction_state' => $data['transaction_state'],
            'status_code_1' => $data['status_code_1'],
            'status_code_2' => $data['status_code_2'],
            'status_code_3' => $data['status_code_3'],
            'status_description_1' => $data['status_description_1'],
            'status_description_2' => $data['status_description_2'],
            'status_description_3' => $data['status_description_3'],
            'completion_time_stamp' => array_not_empty('completion_time_stamp', $data) ? Carbon::createFromFormat('YmdHis', $data['completion_time_stamp']) : Carbon::now(),
            'source' => 'wirecard'
        ];

        DB::table('order_log')->insert($log);
    }

    public static function getLogs($limit = 30)
    {
        return DB::table('order_log')->orderBy('id', 'DESC')->limit($limit)->get();
    }
}
