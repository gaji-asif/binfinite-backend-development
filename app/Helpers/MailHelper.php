<?php

namespace App\Helpers;
use Mail;

class MailHelper
{
    /**
     * Sends contact page form to email
     */
    public static function sendContactForm($data)
    {
        Mail::send(['html' => 'emails.contactus'], $data, function ($message) {
            $message->to(config('mail.support_email_to'), 'BInfinite')->subject
                ('Contact Form');
        });
        return true;
    }

    /**
     * Send an e-mail for bpoints conver request to binfinite admin.
     *
     */

    public static function sendBPointsConvertRequest($data)
    {
        // HTML template - emails.convert_point_form
        // Plain Text template - emails.convert_point_form_plain
        $result = Mail::send(['emails.convert_point_form', 'emails.convert_point_form_plain'], $data, function ($message) {
            $message->to(config('mail.support_email_to'), 'BInfinite')->subject('Convert to BPoints Request');
        });
    }

    public static function sendNetworkPartnerDetails($data)
    {
        // HTML template - emails.convert_point_form
        // Plain Text template - emails.convert_point_form_plain
        Mail::send(['html' => 'emails.send_partner_details'], $data, function ($message) {
            $message->to('asif01665@yahoo.com', 'BInfinite')->subject
                ('Network Partners');
        });
        return true;
    }
}
