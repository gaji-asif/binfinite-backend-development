<?php

namespace App\Helpers;

class BladeHelper
{
    /**
    * Prints out css style wrap for background image
    * We assume that http means url is already properly formatted
    *
    */
    public static function backgroundImage($url, $base='')
    {
    	$fullPath = $url;

    	if (! str_is_url($url)) {
            if (substr($url, 0, 1) == '/') {
                // Assuming absolute url, truncate to standardize
                $url = substr($url, 1);
            } else {
                // Relative url
                if ($base == '') {
                    // Default base
                    $base = '/images/';
                }

                if (strpos($url, '/') === false) {
                    // Most likely a direct filename, encode
                    $url = rawurlencode($url);
                }
            }

    		$fullPath = asset($base . '/' . $url);
    	}

        return 'background-image:url(' . $fullPath . ');';
    }
}
