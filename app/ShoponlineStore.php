<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShoponlineStore extends Model
{

    protected $table = 'shoponlinestores';
	/**
	 * Attribute for full url for logo image
	 *
	 */
    public function getLogoUrlAttribute()
    {
    	return '/images/shoponline/stores/' . $this->image;
    	// // If future we want admin editable, this probably will need to be changed to storage
    	// return $this->resolveImage($this->image);
    }

    /**
     * Scope for only active stores with link available
     * 
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
    	return $query->where('status', 1)
    		->where('link', '!=', '');
    }

    /**
     * Attribute for affiliate url. 
     * 
     */
    public function getAffiliateUrl($cardId)
    {
        return Services\ShoponlineService::getAffiliateUrl($this->link, $cardId);
    }

}
