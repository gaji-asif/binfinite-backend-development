<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailActivation extends Model
{
    protected $table = 'email_activation';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'activation_code', 'user_id', 'created_at', 'updated_at'
    ];

}
