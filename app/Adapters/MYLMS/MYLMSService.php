<?php

namespace App\Adapters\MYLMS;

use App\Adapters\MYLMS\SoapClient\SoapClientInterface as SoapClient;
use App\Adapters\MYLMS\SoapClient\SoapClient as DefaultSoapClient;
use App\Adapters\MYLMS\Exceptions\NotImplementedException;

class MYLMSService
{
	public $soapClient;
	public $key;
	public $companyCode;
	public $branchCode;
	public $isDebug;

	/* Constants */
	const API_BASE_PATH = __NAMESPACE__ . '\Api\\';

	/**
	 * Constructs MYLMSService with wsdl URL
	 * 
	 * @param string $wsdl
	 */
	public function __construct(SoapClient $soapClient = null)
	{
		$this->soapClient = $soapClient;
	}

	/**
	 * Set WSkey for MYLMS auth
	 * 
	 * @param string $key
	 * @return MYLMSService
	 */
	public function setKey($key)
	{
		$this->key = $key;
		return $this;
	}
	
	/**
	 * Set WSCompanyCode for MYLMS auth
	 * 
	 * @param string $companyCode
	 * @return MYLMSService
	 */
	public function setCompanyCode($companyCode)
	{
		$this->companyCode = $companyCode;
		return $this;
	}

	/**
	 * Set WSBranchCode for MYLMS auth
	 * 
	 * @param string $branchCode
	 * @return MYLMSService
	 */
	public function setBranchCode($branchCode)
	{
		$this->branchCode = $branchCode;
		return $this;
	}

	/**
	 * Calls soap function
	 * 
	 * @param string $function
	 * @param mixed $arguments
	 * @return mixed
	 */
	public function callSoap($function, $arguments)
	{
		$client = $this->soapClient;
		$result = $client->__call($function, $arguments);

		if ($this->isDebug) {
			dump($client->__getLastRequest(), $client->__getLastResponse());
		}

		return $result;
	}

	/**
	 * Gets soap client
	 * 
	 * @return SoapClient
	 */
	public function getClient()
	{
		return $this->soapClient;
	}

	/**
	 * Setup default configuration of MYLMSService
	 * 
	 * @return MYLMSService
	 */
	public function setDefaultConfig()
	{
		$this->setKey(config('services.mylms.wskey'))
			->setCompanyCode(config('services.mylms.wscompanycode'))
			->setBranchCode(config('services.mylms.wsbranchcode'));

		return $this;
	}

	/**
	 * Enable debugging for SoapClient
	 * 
	 * @return MYLMSService
	 */
	public function debug()
	{
		$this->isDebug = true;
		return $this;
	}

	/**
	 * Magic function for shorthanding everything to API subclasses,
	 * autoloading classmap style.
	 * 
	 * @param string $function
	 * @param mixed $arguments
	 * @return mixed Result from subclass run call
	 */
	public function __call($function, $arguments)
	{
		$classPath = static::API_BASE_PATH . $function;
		if (class_exists($classPath)) {

			// If the only argument is array, directly run the submit function
			if (sizeOf($arguments) == 1 && is_array($arguments[0])) {
				return 	(new $classPath($this))->send($arguments[0]);
			}

			// Else run the parameterized function
			return (new $classPath($this))->run(...$arguments);
		}
		throw new NotImplementedException(
			sprintf("MYLMS API class '%s' is not defined. Tried class '%s'", $function, $classPath)
		);
	}

	/**
	 * Magic function for shorthanding creation of instance 
	 * using default configuration and calls API subclass.
	 * 
	 * @param string $function
	 * @param mixed $arguments
	 * @return mixed Result from subclass run call
	 */
	public static function __callStatic($function, $arguments)
	{
		$client = static::getDefaultInstance();
		return $client->$function(...$arguments);
	}

	/**
	 * Gets instance with default configuration
	 * 
	 * @return MYLMSService
	 */
	public static function getDefaultInstance()
	{
		$soapClient = new DefaultSoapClient(config('services.mylms.wsdl'));

		$client = (new static($soapClient))->setDefaultConfig();
		return $client;
	}

	/**
	 * Gets instance with custom WSDL default configuration
	 * 
	 * @return SoapClient
	 */
	public static function customInstanceWSDL($url)
	{
		$soapClient = new DefaultSoapClient($url);

		$client = (new static($soapClient))->setDefaultConfig();
		return $client;
	}
}