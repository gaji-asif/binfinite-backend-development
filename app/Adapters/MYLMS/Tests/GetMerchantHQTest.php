<?php

namespace App\Adapters\MYLMS\Tests;

use Tests\TestCase;
use App\Adapters\MYLMS\MYLMSService;
use App\Adapters\MYLMS\Exceptions\LMSErrorException;

class GetMerchantHQTest extends TestCase
{
	public function testDataShouldBeCollection()
	{
		$service = TestHelper::getMockService();
		$client = $service->getClient();
		$client->withSuccess()
			->withData([[
				"CompanyCode" => "B0006",
				"CompanyName" => "WENDY'S",
				"Address" => "LOT 09-27 & 28, LEVEL 9,BERJAYA TIMES SQUARE,NO. 1, JALAN IMBI,",
				"PostCode" => "55100",
				"State" => "KUALA LUMPUR",
				"Country" => "MALAYSIA ",
				"Latitude" => "0",
				"Longitude" => "0",
				"PhoneNumber" => "03-21451800 ",
				"Fax" => "03-21432304 ",
				"Email" => "ESTHERYIM@WEN-BERJAYA.COM; AMIN.BAHARIN@WEN-BERJAY",
				"WebSite" => "WWW.FACEBOOK.COM/WENDYS.MALAYSIA",
				"Category" => "FOOD & BEVERAGE",
				"TotalOutlets" => "18",
				"Logo" => "https://www.bcard.com.my/BCARDLMS/images/merchant/1/B0006.png",
			]]);

		$data = $service->GetMerchantHQ()->data;

		$this->assertInstanceOf(\Illuminate\Support\Collection::class, $data);
		$this->assertEquals($data->first()['CompanyCode'], 'B0006');
	}

	public function testEmptyDataShouldntThrow()
	{
		$service = TestHelper::getMockService();
		$client = $service->getClient();
		$client->withSuccess();
		
		try {
			$data = $service->GetMerchantHQ()->data;
		} catch (LMSErrorException $error) {
			$this->fail('Shouldnt throw error');
		}
		
		$this->assertTrue(true);
	}
}