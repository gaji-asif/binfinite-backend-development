<?php

namespace App\Adapters\MYLMS\Tests;

use Tests\TestCase;
use App\Adapters\MYLMS\MYLMSService;
use App\Adapters\MYLMS\SoapClient\MockSoapClient;
use App\Adapters\MYLMS\SoapClient\SoapClient;

class TestHelper extends TestCase
{
	/**
	* Get MYLMS development environment client
	*
	* @return MYLMSService
	*/
	public static function getDevSoapClient()
	{
		$wsdl = 'https://dev.binfinite.com.my/bcardwsapi/service.asmx?wsdl';
		$options = [];
		$soapClient = new SoapClient($wsdl, $options);
		return $soapClient;
	}

	/**
	* Get dummy client
	*
	* @return MYLMSService
	*/
	public static function getMockSoapClient()
	{
		$wsdl = '';
		$options = [];
		$soapClient = new MockSoapClient($wsdl, $options);
		return $soapClient;
	}

	public static function setupService($soapClient)
	{
		$key = '190536604';
		$cc = 'Z9999';
		$bc = 'TEST064';

		$service = new \App\Adapters\MYLMS\MYLMSService($soapClient);
		$service->setKey($key)
			->setCompanyCode($cc)
			->setBranchCode($bc);
		return $service;
	}

	public static function getMockService($soapClient = null)
	{
		if (! $soapClient) {
			$soapClient = static::getMockSoapClient();
		}
		
		return static::setupService($soapClient);
	}

	public static function getDevService($soapClient = null)
	{
		if (! $soapClient) {
			$soapClient = static::getDevSoapClient();
		}

		return static::setupService($soapClient);
	}


	/**
	 * Gets service based on environment. Tests should run using mock by default.
	 *
	 */
	public static function getServiceBasedOnEnvironment()
	{
		if (\App::environment(['testing'])) {
			return static::getMockService();
		}

		return static::getDevService();
	}
}
