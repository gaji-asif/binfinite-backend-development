<?php

namespace App\Adapters\MYLMS\Tests;

use Tests\TestCase;
use App\Adapters\MYLMS\MYLMSService;
use App\Adapters\MYLMS\Exceptions\LMSErrorException;

class MYLMSTest extends TestCase
{
	/**
	* Test should automatically run API using magic __call function
	* with arguments
	*
	* @return void
	*/
	public function testAbstractBaseShouldRunAPIWithArguments()
	{
		$arg1 = 'arg1';
		$arg2 = 'arg2';

		$service = TestHelper::getMockService();
		$api = $service->MockAPI($arg1, $arg2);
		$this->assertEquals($api->getDummyArg1(), $arg1);
		$this->assertEquals($api->getDummyArg2(), $arg2);
	}

	/**
	* Test for invalid input
	*
	* @return void
	*/
	public function testShouldThrowInvalidInput()
	{
		$service = TestHelper::getMockService();
		$client = $service->getClient();
		$client->withError(LMSErrorException::INVALID_INPUT);

		try {
			$service->GetCard('ASDASDASDASDASDASDASDASDASD', 'KF(#JSIDJO@IO!@#!@');

			// Trigger fail because it should've caught error
			$this->fail('Should have caught error');
		} catch (LMSErrorException $e) {
			$this->assertTrue($e->is(LMSErrorException::INVALID_INPUT));
		}
	}
}

/*** DUMMY API CLASS ***/

namespace App\Adapters\MYLMS\Api;

use App\Adapters\MYLMS\Exceptions\InvalidArgumentException;

/*
 * Dummy API
 */
class MockAPI extends AbstractBase
{
	protected $mandatoryFields = [];
	public $dummyArg1;
	public $dummyArg2;

	/**
	 * Parameterized function
	 * 
	 * @param string $cardId
	 * @param string $cardPin
	 * @return mixed Result from subclass run call
	 */
	public function run($dummyArg1, $dummyArg2)
	{
		$this->dummyArg1 = $dummyArg1;
		$this->dummyArg2 = $dummyArg2;
		return $this;
	}

	public function getDummyArg1()
	{
		return $this->dummyArg1;
	}

	public function getDummyArg2()
	{
		return $this->dummyArg2;
	}
}