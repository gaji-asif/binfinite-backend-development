<?php

namespace App\Adapters\MYLMS\Exceptions;

class InvalidArgumentException extends MYLMSException implements ExceptionInterface
{
	//
}