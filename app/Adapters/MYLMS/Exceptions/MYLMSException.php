<?php

namespace App\Adapters\MYLMS\Exceptions;
use App\Exceptions\GeneralException;

class MYLMSException extends GeneralException implements ExceptionInterface
{
	/**
	 * Default error message
	 *
	 */
	const DEFAULT_MESSAGE = 'LMS Server error';
}