<?php

namespace App\Adapters\MYLMS\Exceptions;

class NotImplementedException extends MYLMSException implements ExceptionInterface
{
	//
}