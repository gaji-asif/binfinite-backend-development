<?php

namespace App\Adapters\MYLMS\Exceptions;

class LMSErrorException extends MYLMSException implements ExceptionInterface
{
	/**
	 * Error types
	 *
	 */
	const TRANSACTION_DONE_WITHOUT_ERROR = '00';
	const INVALID_CARD = '01';
	const INVALID_MERCHANT = '02';
	const INVALID_PIN = '03';
	const INSUFFICIENT_POINT = '04';
	const PIN_CHANGED_FAIL = '05';
	const CARD_IS_BLOCKED = '06';
	const CARD_IS_EXPIRED = '07';
	const PRINCIPAL_IS_CURRENTLY_A_SUPPLEMENTARY = '08';
	const SUPPLEMENTARY_IS_CURRENTLY_A_PRINCIPAL = '09';
	const SUPPLEMENTARY_IS_ALREADY_A_SUPPLEMENTARY = '10';
	const MERGE_OWN_CARD = '11';
	const TRANSACTION_RECORD_DOES_NOT_EXISTS_OR_INVALID_TRANSACTION_ID = '12';
	const TRANSACTION_ALREADY_VOIDED = '13';
	const TRANSACTION_ALREADY_REVERSED = '14';
	const TRANSACTION_RECORD_MORE_THAN_ONE_OR_NOT_FOUND = '15';
	const INVALID_POINT_OR_AMOUNT_OR_BILL_NO = '16';
	const INVALID_CATEGORY = '18';
	const CARD_NOT_REGISTERED = '19';
	const DUPLICATE_DATA_OR_TRANSACTION = '20';
	const WEB_SERVICE_KEY_ALREADY_SETUP = '21';
	const INVALID_WEB_SERVICE_KEY = '22';
	const INVALID_IC = '23';
	const INVALID_NAME = '24';
	const INVALID_OPERATION_OR_ADJUSTMENT = '25';
	const INVALID_STAN_NO_OR_OFFLINE_ID_OR_ONLINE_ID = '26';
	const INVALID_STAFF = '27';
	const INVALID_DATE = '29';
	const INVALID_CONTACT_NO_OR_PHONE_NUMBER_OR_MSISDN = '32';
	const INVALID_EMAIL_ADDRESS = '33';
	const MEMBER_ALREADY_REGISTERED = '34';
	const INSUFFICIENT_QUANTITY = '35';
	const INVALID_CATEGORY_ID = '36';
	const SERVER_TIME_OUT_OR_OPERATION_TIME_OUT = '40';
	const INVALID_INPUT = '80';
	const INVALID_PERMISSION = '81';
	const TOP_UP_FAIL = '82';
	const UNEXPECTED_ERROR = '98';
	const SERVER_EXCEPTION = '99';

	/**
	 * Undocumented error codes
	 *
	 */
	const FAIL_TO_DELIVER_CONFIRMATION_EMAIL = '-09';
	const EMAIL_ALREADY_EXISTS = '-21';

	/**
	 * Error for no headers
	 *
	 */
	const NO_HEADERS = '99999';

	/**
	 * Public friendly error messages
	 *
	 */
	const MESSAGES = [
		self::TRANSACTION_DONE_WITHOUT_ERROR => 'Success',
		self::INVALID_CARD => 'Invalid card or PIN',
		self::INVALID_PIN => 'Invalid password',
		self::INVALID_INPUT => 'Invalid input',
		self::INVALID_MERCHANT => 'Invalid merchant',
		self::FAIL_TO_DELIVER_CONFIRMATION_EMAIL => 'Failed to deliver confirmation email',
		self::EMAIL_ALREADY_EXISTS => 'Email is already registered',
		self::NO_HEADERS => 'Something went wrong',
	];

	/**
	 * Original message needs to be stored because some errors just gives SERVER_EXCEPTION
	 * but has context in error message
	 *
	 */
	public $originalMessage;

	/**
	 * Extend the original construct to have original message
	 *
	 * @param int $code
	 * @return boolean
	 */
	public function __construct($message, $code = 0, Exception $previous = null, $originalMessage = null) {
        parent::__construct($message, $code, $previous);
        $this->originalMessage = $originalMessage;
    }

	/**
	 * For shorthand comparison of error type, ie: 
	 * $e->is(LMSErrorException::INVALID_INPUT)
	 *
	 * @param int $code
	 * @return boolean
	 */
	public function is($code)
	{
		return $this->getCode() == $code;
	}

	/**
	 * String code with prepend single zero
	 *
	 * @return string
	 */
	public function getStringCode()
	{
		return $this->getCode() < 10 ? '0' . (string)$this->getCode() : (string)$this->getCode();
	}

	/**
	 * Case insensitive comparison of original error message
	 *
	 * @return string
	 */
	public function isMessage($string)
	{
		return strcasecmp($this->originalMessage, $string) == 0;
	}

	/**
	 * Gets public friendly error message
	 *
	 * @return string
	 */
	public function getFriendlyMessage()
	{
		return self::MESSAGES[$this->getStringCode()] ?? 
			sprintf('%s(%s)', self::DEFAULT_MESSAGE, $this->getStringCode());
	}
}