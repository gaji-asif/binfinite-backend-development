<?php

namespace App\Adapters\MYLMS\Api;

/*
 * Retrieve all gift catalogue
 */
class GetGiftCatalogue extends AbstractBase
{
	/**
	 * Parameterized function
	 * 
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function run()
	{
		return $this->send([]);
	}

	/**
	 * Function that should implement some business logic, 
	 * transformations, or validation to parameters
	 * prior submitting request to LMS.
	 * 
	 * @param string $args
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function send($args)
	{
		return $this->authenticatedSubmit('GetGiftCatalogue', $args);
	}
}