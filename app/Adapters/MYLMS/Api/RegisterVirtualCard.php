<?php

namespace App\Adapters\MYLMS\Api;

/*
 * Retrieve member profile
 */
class RegisterVirtualCard extends AbstractBase
{
	protected $mandatoryFields = ['Card', 'Password'];

	/**
	 * Parameterized function
	 * 
	 * @param string $cardNo
	 * @param string $cardPin
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function run()
	{
		return $this->send([
			'Merchant' => '',
			'Title' => '',
			'FullName' => '',
			'IC' => '',
			'BirthDate' => '',
			'Gender' => '',
			'Race' => '',
			'Nationality' => '',
			'MaritalStatus' => '',
			'OwnCar' => '',
			'OwnCreditCard' => '',
			'HomeAddress1' => '',
			'HomeAddress2' => '',
			'HomeAddress3' => '',
			'HomeCity' => '',
			'HomeState' => '',
			'HomeCountry' => '',
			'HomeZip' => '',
			'HomePhone' => '',
			'HomeEmail' => '',
			'MobilePhone' => '',
			'OfficeAddress1' => '',
			'OfficeAddress2' => '',
			'OfficeAddress3' => '',
			'OfficeState' => '',
			'OfficeCountry' => '',
			'OfficeZip' => '',
			'OfficeEmail' => '',
			'OfficePhone' => '',
			'OfficeExt' => '',
			'OfficeFax' => '',
			'CardType' => '',
			'MSISDN' => '',
		]);
	}

	/**
	 * Function that should implement some business logic, 
	 * transformations, or validation to parameters
	 * prior submitting request to LMS.
	 * 
	 * @param string $args
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function send($args)
	{
		return $this->authenticatedSubmit('RegisterVirtualCard', $args);
	}

}