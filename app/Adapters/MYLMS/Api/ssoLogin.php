<?php

namespace App\Adapters\MYLMS\Api;

use App\Adapters\MYLMS\Exceptions\LMSErrorException;

/*
 * Register SSO. Will trigger email & mobile OTP.
 */
class ssoLogin extends AbstractBase
{
	protected $mandatoryFields = ['Email', 'Password'];

	/**
	 * Parameterized function
	 * 
	 * @param string $email
	 * @param string $mobile
	 * @param string $password
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function run($email, $password)
	{
		return $this->send([
			'Email' => $email,
			'Password' => $password,
		]);
	}

	/**
	 * Function that should implement some business logic, 
	 * transformations, or validation to parameters
	 * prior submitting request to LMS.
	 * 
	 * @param string $args
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function send($args)
	{
		try {
			return $this->authenticatedSubmit('ssoLogin', $args);
		} catch (LMSErrorException $e) {
			if ($e->is(LMSErrorException::INVALID_PIN)) {
				return false;
			}
		}
		
	}

}