<?php

namespace App\Adapters\MYLMS\Api;

/*
 * Retrieves all merchant HQ
 */
class GetMerchantHQ extends AbstractBase
{
	/**
	 * Parameterized function
	 * 
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function run()
	{
		return $this->send([]);
	}

	/**
	 * Function that should implement some business logic, 
	 * transformations, or validation to parameters
	 * prior submitting request to LMS.
	 * 
	 * @param string $args
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function send($args)
	{
		return $this->authenticatedSubmit('GetMerchantHQ', $args);
	}

	/**
	 * Transforms response data into usable format
	 * 
	 * @param array $data
	 * @return Collection
	 */
	public function transformResponseData($data)
	{
		if (! $data) return collect([]);

		return collect($data)->map(function ($item, $index) {
			return (array) $item;
		});
	}

}