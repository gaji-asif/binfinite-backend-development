<?php

namespace App\Adapters\MYLMS\Api;

/*
 * This is in documentation but getting error 99 Server Exception. Not sure why.
 */
class CheckWebServiceKeyStatus extends AbstractBase
{
	/**
	 * Parameterized function
	 * 
	 * @param string $posId
	 * @param string $cardPin
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function run($posId = '1234')
	{
		return $this->authenticatedSubmit('CheckWebServiceKeyStatus', [
			'POSID' => $posId
		]);
	}

}