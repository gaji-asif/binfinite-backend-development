<?php

namespace App\Adapters\MYLMS\Api;

/*
 * Verify OTP & email
 */
class ssoCheckEmailOtp extends AbstractBase
{
	/**
	 * Parameterized function
	 * 
	 * @param string $email
	 * @param string $mobile
	 * @param string $otp
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function run($email, $mobile, $otp)
	{
		return $this->send([
			'Email' => $email,
			'Mobile' => $mobile,
			'OTP' => $otp
		]);
	}

	/**
	 * Function that should implement some business logic, 
	 * transformations, or validation to parameters
	 * prior submitting request to LMS.
	 * 
	 * @param string $args
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function send($args)
	{
		return $this->authenticatedSubmit('ssoCheckEmailOtp', $args);
	}
}