<?php

namespace App\Adapters\MYLMS\Api;

/*
 * Retrieve member profile
 */
class GetMemberProfile extends AbstractBase
{
	protected $mandatoryFields = ['Card', 'Password'];

	/**
	 * Parameterized function
	 * 
	 * @param string $cardNo
	 * @param string $cardPin
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function run($cardNo, $cardPin)
	{
		return $this->send([
			'Card' => $cardNo,
			'Password' => $cardPin,
		]);
	}

	/**
	 * Function that should implement some business logic, 
	 * transformations, or validation to parameters
	 * prior submitting request to LMS.
	 * 
	 * @param string $args
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function send($args)
	{
		return $this->authenticatedSubmit('GetMemberProfile', $args);
	}

}