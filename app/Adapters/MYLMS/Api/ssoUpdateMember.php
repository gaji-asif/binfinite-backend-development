<?php

namespace App\Adapters\MYLMS\Api;

/*
 * SSO Get redeem history
 */
class ssoUpdateMember extends AbstractBase
{
	// protected $mandatoryFields = ['SSOID', 'Email', 'NewEmail', 'Name', 'Nationality', 'Mobile', 'NewMobile', 'Country'];

	/**
	 * Parameterized function (all params are string)
	 * 
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function run($SSOID, $Email, $NewEmail, $Name, 
		$IC, $Passport, $DOB, $Nationality, $Mobile, 
		$NewMobile, $Address1, $Address2, $Address3, 
		$Postcode, $City, $State, $Country, 
		$PyamentRegistered, $MastercardRegistered)
	{

		return $this->send([
			'SSOID' => $SSOID,
			'Email' => $Email,
			'NewEmail' => $NewEmail,
			'Name' => $Name,
			'IC' => $IC,
			'Passport' => $Passport,
			'DOB' => $DOB,
			'Nationality' => $Nationality,
			'Mobile' => $Mobile,
			'NewMobile' => $NewMobile,
			'Address1' => $Address1,
			'Address2' => $Address2,
			'Address3' => $Address3,
			'Postcode' => $Postcode,
			'City' => $City,
			'State' => $State,
			'Country' => $Country,
			// LMS API HAVE A TYPO.
			'PyamentRegistered' => $PyamentRegistered,
			'MastercardRegistered' => $MastercardRegistered,
		]);
	}

	/**
	 * Function that should implement some business logic, 
	 * transformations, or validation to parameters
	 * prior submitting request to LMS.
	 * 
	 * @param string $args
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function send($args)
	{
		return $this->authenticatedSubmit('ssoUpdateMember', $args);
	}
}