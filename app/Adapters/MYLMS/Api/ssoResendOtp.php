<?php

namespace App\Adapters\MYLMS\Api;

/*
 * Resend OTP for registration.
 */
class ssoResendOtp extends AbstractBase
{
	/**
	 * Parameterized function
	 * 
	 * @param string $ssoId
	 * @param string $mobile
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function run($ssoId, $mobile)
	{
		return $this->send([
			'SSOID' => $ssoId,
			'Mobile' => $mobile
		]);
	}

	/**
	 * Function that should implement some business logic, 
	 * transformations, or validation to parameters
	 * prior submitting request to LMS.
	 * 
	 * @param string $args
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function send($args)
	{
		return $this->authenticatedSubmit('ssoResendOtp', $args);
	}
}