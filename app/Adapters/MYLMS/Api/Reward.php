<?php

namespace App\Adapters\MYLMS\Api;

/*
 * Reward a member card with points
 */
class Reward extends AbstractBase
{
	protected $mandatoryFields = ['Card', 'TranxDate'];

	/**
	 * Parameterized function
	 * 
	 * @param string $cardNo
	 * @param string $totalPoint
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function run($cardNo, $totalPoint)
	{
		return $this->send([
			'Card' => $cardNo,
			// 'POSID' => '1234612356',
			'TranxDate' => now(),
			'BillNo' => 'test12345',
			'TotalAmount' => 1000,
			'TotalPoint' => 1000,
		]);
	}

	/**
	 * Function that should implement some business logic, 
	 * transformations, or validation to parameters
	 * prior submitting request to LMS.
	 * 
	 * @param string $args
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function send($args)
	{
		// TranxDate supports Carbon
		if ($args['TranxDate'] instanceof \Carbon\Carbon) {
			$args['TranxDate'] = $args['TranxDate']->format('Y-m-d H:i:s');
		}

		return $this->authenticatedSubmit('Reward', $args);
	}

}