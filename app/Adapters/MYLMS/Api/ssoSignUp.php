<?php

namespace App\Adapters\MYLMS\Api;

/*
 * Request for SSO signup. Will trigger email & mobile OTP. Dont confuse with ssoRegister
 */
class ssoSignUp extends AbstractBase
{
	protected $mandatoryFields = ['Email', 'Mobile', 'Password'];

	/**
	 * Parameterized function
	 * 
	 * @param string $email
	 * @param string $mobile
	 * @param string $password
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function run($email, $mobile, $password)
	{
		return $this->send([
			'Email' => $email,
			'Mobile' => $mobile,
			'Password' => $password,
		]);
	}

	/**
	 * Function that should implement some business logic, 
	 * transformations, or validation to parameters
	 * prior submitting request to LMS.
	 * 
	 * @param string $args
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function send($args)
	{
		return $this->authenticatedSubmit('ssoSignUp', $args);
	}
}