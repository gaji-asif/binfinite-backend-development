<?php

namespace App\Adapters\MYLMS\Api;

/*
 * SSO Update member's password.
 */
class ssoUpdatePassword extends AbstractBase
{
	/**
	 * Parameterized function
	 * 
	 * @param string $email
	 * @param string $oldPassword
	 * @param string $newPassword
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function run($email, $oldPassword, $newPassword)
	{
		return $this->send([
			'Email' => $email,
			'OldPassword' => $oldPassword,
			'NewPassword' => $newPassword,
		]);
	}

	/**
	 * Function that should implement some business logic, 
	 * transformations, or validation to parameters
	 * prior submitting request to LMS.
	 * 
	 * @param string $args
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function send($args)
	{
		return $this->authenticatedSubmit('ssoUpdatePassword', $args);
	}
}