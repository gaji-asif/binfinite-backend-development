<?php

namespace App\Adapters\MYLMS\Api;

/*
 * Check login session
 */
class ssoCheckLoginSession extends AbstractBase
{
	/**
	 * Parameterized function
	 * 
	 * @param string $sessionId
	 * @param string $email
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function run($sessionId, $email)
	{
		return $this->send([
			'SessionID' => $sessionId,
			'Email' => $email
		]);
	}

	/**
	 * Function that should implement some business logic, 
	 * transformations, or validation to parameters
	 * prior submitting request to LMS.
	 * 
	 * @param string $args
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function send($args)
	{
		return $this->authenticatedSubmit('ssoCheckLoginSession', $args);
	}
}