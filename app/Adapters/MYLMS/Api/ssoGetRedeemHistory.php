<?php

namespace App\Adapters\MYLMS\Api;

/*
 * SSO Get redeem history
 */
class ssoGetRedeemHistory extends AbstractBase
{
	protected $mandatoryFields = ['Card', 'SSOID', 'Email'];

	/**
	 * Parameterized function
	 * 
	 * @param string $email
	 * @param string $ssoId
	 * @param string $cardNo
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function run($email, $ssoId, $cardNo)
	{
		return $this->send([
			'Email' => $email,
			'SSOID' => $ssoId,
			'Card' => $cardNo,
		]);
	}

	/**
	 * Function that should implement some business logic, 
	 * transformations, or validation to parameters
	 * prior submitting request to LMS.
	 * 
	 * @param string $args
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function send($args)
	{
		return $this->authenticatedSubmit('ssoGetRedeemHistory', $args);
	}
	
	/**
	 * Gets response result's details
	 * 
	 * @return mixed
	 */
	public function __getResponseDetails()
	{
		$result = $this->__getResponseResult();

		if (isset($result->GetRedeemHistoryResultDetails))
			return $result->GetRedeemHistoryResultDetails;

		return null;
	}

	/**
	 * Transforms response data into usable format
	 * 
	 * @param array $data
	 * @return Collection
	 */
	public function transformResponseData($data)
	{
		if (! $data) return collect([]);

		// Just one
		if (isset($data['RedemptionID'])) {
			return collect([ (array) $data]);
		}

		// Multiple
		return collect($data)->map(function ($item, $index) {
			return (array) $item;
		});
	}
}