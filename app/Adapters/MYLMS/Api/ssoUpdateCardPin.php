<?php

namespace App\Adapters\MYLMS\Api;

/*
 * SSO update card's password
 */
class ssoUpdateCardPin extends AbstractBase
{
	protected $mandatoryFields = ['Card', 'SSOID', 'Email', 'OldPin', 'NewPin'];

	/**
	 * Parameterized function
	 * 
	 * @param string $email
	 * @param string $ssoId
	 * @param string $cardNo
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function run($email, $ssoId, $cardNo, $oldPin, $newPin)
	{
		return $this->send([
			'Email' => $email,
			'SSOID' => $ssoId,
			'Card' => $cardNo,
			'OldPin' => $oldPin,
			'NewPin' => $newPin,
		]);
	}

	/**
	 * Function that should implement some business logic, 
	 * transformations, or validation to parameters
	 * prior submitting request to LMS.
	 * 
	 * @param string $args
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function send($args)
	{
		return $this->authenticatedSubmit('ssoUpdateCardPin', $args);
	}
}