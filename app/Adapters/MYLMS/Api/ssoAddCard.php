<?php

namespace App\Adapters\MYLMS\Api;

/*
 * SSO add card.
 * Throws 99 if card already linked
 */
class ssoAddCard extends AbstractBase
{
	/**
	 * Parameterized function
	 * 
	 * @param string $email
	 * @param string $cardNo
	 * @param string $pin
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function run($email, $cardNo, $pin)
	{
		return $this->send([
			'Email' => $email,
			'CardNo' => $cardNo,
			'Pin' => $pin,
		]);
	}

	/**
	 * Function that should implement some business logic, 
	 * transformations, or validation to parameters
	 * prior submitting request to LMS.
	 * 
	 * @param string $args
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function send($args)
	{
		return $this->authenticatedSubmit('ssoAddCard', $args);
	}

	/**
	 * Transforms response data into usable format
	 * 
	 * @param array $data
	 * @return Collection
	 */
	public function transformResponseData($data)
	{
		if (! $data) return collect([]);

		return collect($data);

		// return collect($data)->map(function ($item, $index) {
		// 	return (array) $item;
		// });
	}

}