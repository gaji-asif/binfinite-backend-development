<?php

namespace App\Adapters\MYLMS\Api;

/*
 * SSO Logout.
 */
class ssoLogout extends AbstractBase
{
	/**
	 * Parameterized function
	 * 
	 * @param string $email
	 * @param string $mobile
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function run($email, $sessionId)
	{
		return $this->send([
			'Email' => $email,
			'SessionID' => $sessionId,
		]);
	}

	/**
	 * Function that should implement some business logic, 
	 * transformations, or validation to parameters
	 * prior submitting request to LMS.
	 * 
	 * @param string $args
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function send($args)
	{
		return $this->authenticatedSubmit('ssoLogout', $args);
	}

}