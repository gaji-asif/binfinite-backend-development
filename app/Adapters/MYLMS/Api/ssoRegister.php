<?php

namespace App\Adapters\MYLMS\Api;

/*
 * Register for member profile & virtual card. (Has to login first!)
 */
class ssoRegister extends AbstractBase
{
	protected $mandatoryFields = ['SSOID', 'Email', 'Name', 'Nationality', 'Mobile', 'Country'];

	/**
	 * Parameterized function (all params are string)
	 * 
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function run($ssoId, $email, $name, $ic, $passport, $dob, $nationality, 
		$mobile, $address1, $address2, $address3, $postcode, $city, $state, 
		$country, $paymentRegistered, $masterRegistered)
	{
		return $this->send([
			'SSOID' => $ssoId,
			'Email' => $email,
			'Name' => $name,
			'IC' => $ic,
			'Passport' => $passport,
			'DOB' => $dob,
			'Nationality' => $nationality,
			'Mobile' => $mobile,
			'Address1' => $address1,
			'Address2' => $address2,
			'Address3' => $address3,
			'Postcode' => $postcode,
			'City' => $city,
			'State' => $state,
			'Country' => $country,
			'PaymentRegistered' => $paymentRegistered,
			'MasterRegistered' => $masterRegistered,
		]);
	}

	/**
	 * Function that should implement some business logic, 
	 * transformations, or validation to parameters
	 * prior submitting request to LMS.
	 * 
	 * @param string $args
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function send($args)
	{
		return $this->authenticatedSubmit('ssoRegister', $args);
	}

}