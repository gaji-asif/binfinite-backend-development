<?php

namespace App\Adapters\MYLMS\Api;

use App\Adapters\MYLMS\Exceptions\NotImplementedException;
use App\Adapters\MYLMS\Exceptions\InvalidArgumentException;
use App\Adapters\MYLMS\Exceptions\LMSErrorException;
use App\Adapters\MYLMS\Exceptions\MYLMSException;

abstract class AbstractBase
{
	protected $mylmsService;
	protected $response;
	protected $parsedResponse;
	protected $function;

	protected $mandatoryFields = [];
	public $header;
	public $data;

	/**
	 * Construct with instance of mylmsService.
	 * 
	 * @param MYLMSService $mylmsService
	 */
	public function __construct($mylmsService)
	{
		$this->mylmsService = $mylmsService;
	}

	/**
	 * Gets response result
	 * 
	 * @return StdClass
	 */
	public function __getResponseResult()
	{
		return $this->response->{$this->function . 'Result'};
	}

	/**
	 * Gets response result's header
	 * 
	 * @return StdClass
	 */
	public function __getResponseHeader()
	{
		$result = $this->__getResponseResult();
		if (isset($result->{'ResponseHeader'})) {
			return $result->{'ResponseHeader'};
		}

		// Wierd case where its just:
		//   #response: {#767 ▼
		//     +"ssoAddCardResult": {#768 ▼
		//       +"ErrorCode": "99"
		//     }
		//   }
		if ($result) {
			return $result;
		}
	}

	/**
	 * Gets response result's details
	 * 
	 * @return mixed
	 */
	public function __getResponseDetails()
	{
		$result = $this->__getResponseResult();
		if (isset($result->{$this->function . 'ResultDetails'}))
			return $result->{$this->function . 'ResultDetails'};

		if (isset($result->{$this->function . 'Result'}))
			return $result->{$this->function . 'Result'};

		return null;
	}

	/**
	 * Parses the dynamic response into a standard form.
	 * 
	 * @return array
	 */
	public function parseResponse()
	{
		return [
			'header' => (array) $this->__getResponseHeader(),
			'details' => (array) $this->__getResponseDetails()
		];
	}

	/**
	 * Validates response header.
	 * 
	 * @return boolean
	 * @throws LMSErrorException
	 */
	public function validateResponseHeader()
	{
		$headers = $this->__getResponseHeader();
		
		// No headers from LMS
		if (is_null($headers)) {
			throw new LMSErrorException(
				'No header response',
				LMSErrorException::NO_HEADERS
			);
		}

		if (isset($headers->ErrorCode) && $headers->ErrorCode != '00') {
			$ex = new LMSErrorException(
				sprintf('LMS Error: "%s" (%s)', 
					$headers->ErrorMessage ?? '', 
					$headers->ErrorCode), 
					$headers->ErrorCode,
					null,
					$headers->ErrorMessage ?? ''
				);
			throw $ex;
		}

		// There will be case where no response code but theres header,
		// we'll handle that in API classes.
		return true;
	}

	/**
	 * Gets raw response
	 * 
	 * @return mixed response
	 */
	public function getResponse()
	{
		return $this->response;
	}

	/**
	 * Gets parsed response
	 * 
	 * @return array parsedResponse
	 */
	public function getParsedResponse()
	{
		return $this->parsedResponse;
	}

	/**
	 * Gets transformed data
	 * 
	 * @return mixed
	 */
	public function getData()
	{
		return $this->data;
	}

	/**
	 * Uses submit with appended WSKey, WSCompanyCode, and WSBranchCode as argument.
	 * 
	 * @param string $function
	 * @param StdClass $arguments
	 * @return array
	 */
	public function authenticatedSubmit($function, $arguments = [])
	{
		$arguments['WSKey'] = $this->mylmsService->key;
		$arguments['WSCompanyCode'] = $this->mylmsService->companyCode;
		$arguments['WSBranchCode'] = $this->mylmsService->branchCode;
		return $this->submit($function, $arguments);
	}

	/**
	 * Validates parameters for mandatory fields.
	 * Should throw exception if fail
	 * 
	 * @param array $arguments
	 * @return void
	 * @throws InvalidArgumentException
	 */
	public function validateArguments($arguments)
	{
		for ($i = 0; $i < sizeOf($this->mandatoryFields); $i++) {
			$field = $this->mandatoryFields[$i];
			if (!isset($arguments[$field])) {
				throw new InvalidArgumentException(
					sprintf('Field "%s" is mandatory', $field)
				);
			}
		}
	}

	/**
	 * Transforms response data into usable format
	 * 
	 * @param array $data
	 * @return array
	 */
	public function transformResponseData($data)
	{
		return $data;
	}

	/**
	 * Submit request to SoapClient, parse and validate after.
	 * 
	 * @param string $function
	 * @param StdClass $arguments
	 * @return array
	 * @throws MYLMSException
	 */
	public function submit($function, $arguments = [])
	{
		$this->function = $function;

		// The web service is expecting an array with first index as data, 
		// i.e $data[0] thus wrapping is required
		$wrappedArguments = array($arguments);
		$this->validateArguments($arguments);
		
		// TODO: Catch soap/network errors
		try {
			$response = $this->mylmsService->callSoap($function, $wrappedArguments);
		} catch (\SoapFault $e) {
			throw new MYLMSException($e->getMessage(), $e->getCode(), $e);
		}

		$this->response = $response;

		$this->validateResponseHeader();

		$parsedResponse = $this->parseResponse();
		$this->parsedResponse = $parsedResponse;
		$this->header = $parsedResponse['header'];
		$this->data = $this->transformResponseData($parsedResponse['details']);

		return $this;
	}

	/**
	 * Catch that run thats not implemented from child classes.
	 * Wish this could've been done in an abstract function instead,
	 * but overloading argument is not allowed.
	 * 
	 * @param string $function
	 * @param mixed $arguments
	 * @return void
	 * @throws NotImplementedException|\BadMethodCallException
	 */
	public function __call($function, $arguments)
	{
		if ($function == 'run') {
			throw new NotImplementedException('Missing implementation for run function');
		}

		if ($function == 'send') {
			throw new NotImplementedException('Missing implementation for send function');
		}

		throw new \BadMethodCallException(sprintf(
			'Method %s::%s does not exist.', static::class, $function
		));
	}
}