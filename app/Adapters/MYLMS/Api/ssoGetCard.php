<?php

namespace App\Adapters\MYLMS\Api;

/*
 * Get all cards owned by the user.
 */
class ssoGetCard extends AbstractBase
{
	protected $mandatoryFields = ['Email', 'SSOID'];

	/**
	 * Parameterized function
	 * 
	 * @param string $email
	 * @param string $ssoId
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function run($email, $ssoId)
	{
		return $this->send([
			'Email' => $email,
			'SSOID' => $ssoId,
		]);
	}

	/**
	 * Function that should implement some business logic, 
	 * transformations, or validation to parameters
	 * prior submitting request to LMS.
	 * 
	 * @param string $args
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function send($args)
	{
		return $this->authenticatedSubmit('ssoGetCard', $args);
	}

	/**
	 * Transforms response data into usable format
	 * 
	 * @param array $data
	 * @return Collection
	 */
	public function transformResponseData($data)
	{
		if (! $data) return collect([]);

		// Just one card
		if (isset($data['BalancePoint'])) {
			return collect([ (array) $data]);
		}

		// Multiple cards
		return collect($data)->map(function ($item, $index) {
			return (array) $item;
		});
	}
}