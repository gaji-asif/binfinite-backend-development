<?php

namespace App\Adapters\MYLMS;

use App\Adapters\MYLMS\SoapClient\SoapClient as DefaultSoapClient;

class MYLMSServiceVirtualCard extends MYLMSService
{
	/* Constants */
	const API_BASE_PATH = __NAMESPACE__ . '\ApiVirtualCard\\';

	/**
	 * Gets instance with default configuration
	 * 
	 * @return MYLMSService
	 */
	public static function getDefaultInstance()
	{
		$soapClient = new DefaultSoapClient(config('services.mylms.wsdl_virtualcard'));

		$client = (new static($soapClient))->setDefaultConfig();
		return $client;
	}
}