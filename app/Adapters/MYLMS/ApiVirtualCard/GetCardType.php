<?php

namespace App\Adapters\MYLMS\ApiVirtualCard;

use  App\Adapters\MYLMS\Api\AbstractBase;

/*
 * Get all cards owned by the user.
 */
class GetCardType extends AbstractBase
{
	protected $mandatoryFields = ['CardType', 'WSCompanyCode', 'WSBranchCode'];

	/**
	 * Parameterized function
	 * 
	 * @param string $email
	 * @param string $ssoId
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function run($cardType, $wsCompanyCode, $wsBranchCode)
	{
		return $this->send([
			'CardType' => $cardType,
			'WSCompanyCode' => $wsCompanyCode,
			'WSBranchCode' => $wsBranchCode,
		]);
	}

	/**
	 * Function that should implement some business logic, 
	 * transformations, or validation to parameters
	 * prior submitting request to LMS.
	 * 
	 * @param string $args
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function send($args)
	{
		return $this->submit('GetCardType', $args);
	}

	/**
	 * Validates response header.
	 * 
	 * @return boolean
	 * @throws LMSErrorException
	 */
	public function validateResponseHeader()
	{
		parent::validateResponseHeader();
		$headers = $this->__getResponseHeader();

		if (! is_null($headers)) {
			return true;
		}

		return false;
	}

	/**
	 * Parses the dynamic response into a standard form.
	 * 
	 * @return array
	 */
	public function parseResponse()
	{
		$headers = $this->__getResponseHeader();
		$data = $headers->CardTypeList ?? [];

		return [
			'header' => (array) $headers,
			'details' => (array) $data,
		];
	}

	/**
	 * Transforms response data into usable format
	 * 
	 * @param array $data
	 * @return Collection
	 */
	public function transformResponseData($data)
	{
		if (! $data) return collect([]);

		// Just one
		if (isset($data['cardtype'])) {
			return collect([ (array) $data]);
		}

		// Multiple
		return collect($data)->map(function ($item, $index) {
			return (array) $item;
		});
	}

}