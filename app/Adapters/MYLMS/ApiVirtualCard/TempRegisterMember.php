<?php

namespace App\Adapters\MYLMS\ApiVirtualCard;

use  App\Adapters\MYLMS\Api\AbstractBase;

/*
 * Register Virtual Card for member
 */
class TempRegisterMember extends AbstractBase
{
	protected $mandatoryFields = [
		'Merchant', 'Title', 'FullName', 'IC', 'BirthDate', 'Gender', 
		'Race', 'Nationality', 'MaritalStatus', 'OwnCar', 'OwnCreditCard', 
		'HomeAddress1', 'HomeCity', 'HomeState', 'HomeCountry', 'HomeZip', 
		'HomeEmail', 'MobilePhone', 'CardType'];

	/**
	 * Parameterized function
	 * 
	 * @param string $cardNo
	 * @param string $cardPin
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function run($Merchant, $Title, $FullName, $IC, $BirthDate, $Gender, $Race, 
			$Nationality, $MaritalStatus, $OwnCar, $OwnCreditCard, $HomeAddress1, 
			$HomeAddress2, $HomeAddress3, $HomeCity, $HomeState, $HomeCountry, 
			$HomeZip, $HomePhone, $HomeEmail, $MobilePhone, $OfficeAddress1, $OfficeAddress2, 
			$OfficeAddress3, $OfficeState, $OfficeCountry, $OfficeZip, $OfficeEmail, 
			$OfficePhone, $OfficeExt, $OfficeFax, $CardType)
	{
		return $this->send([
			'Merchant' => $Merchant,
			'Title' => $Title,
			'FullName' => $FullName,
			'IC' => $IC,
			'BirthDate' => $BirthDate,
			'Gender' => $Gender,
			'Race' => $Race,
			'Nationality' => $Nationality,
			'MaritalStatus' => $MaritalStatus,
			'OwnCar' => $OwnCar,
			'OwnCreditCard' => $OwnCreditCard,
			'HomeAddress1' => $HomeAddress1,
			'HomeAddress2' $HomeAddress2,
			'HomeAddress3' $HomeAddress3,
			'HomeCity' => $HomeCity,
			'HomeState' => $HomeState,
			'HomeCountry' => $HomeCountry,
			'HomeZip' => $HomeZip,
			'HomePhone' $HomePhone,
			'HomeEmail' => $HomeEmail,
			'MobilePhone' => $MobilePhone,
			'OfficeAddress1' $OfficeAddress1,
			'OfficeAddress2' $OfficeAddress2,
			'OfficeAddress3' $OfficeAddress3,
			'OfficeState' $OfficeState,
			'OfficeCountry' $OfficeCountry,
			'OfficeZip' $OfficeZip,
			'OfficeEmail' $OfficeEmail,
			'OfficePhone' $OfficePhone,
			'OfficeExt' $OfficeExt,
			'OfficeFax' $OfficeFax,
			'CardType' => $CardType,
			'MSISDN' => '',
		]);

		// The following is sample:
		// return $this->send([
		// 	'Merchant' => 'B0062',
		// 	'Title' => 'MR',
		// 	'FullName' => 'Testing user here',
		// 	'IC' => '900111111111',
		// 	'BirthDate' => '1990-01-01',
		// 	'Gender' => 'Male',
		// 	'Race' => 'Malay',
		// 	'Nationality' => 'Malaysia',
		// 	'MaritalStatus' => 'Married',
		// 	'OwnCar' => 'Yes',
		// 	'OwnCreditCard' => 'Yes',
		// 	'HomeAddress1' => 'No. 138, Jalan Jalan Cari Makan',
		// 	'HomeAddress2' => '',
		// 	'HomeAddress3' => '',
		// 	'HomeCity' => 'Seremban',
		// 	'HomeState' => 'Negeri Sembilan',
		// 	'HomeCountry' => 'Malaysia',
		// 	'HomeZip' => '70300',
		// 	'HomePhone' => '',
		// 	'HomeEmail' => 'ilyas@web2square.com',
		// 	'MobilePhone' => '0144444444',
		// 	'OfficeAddress1' => '',
		// 	'OfficeAddress2' => '',
		// 	'OfficeAddress3' => '',
		// 	'OfficeState' => '',
		// 	'OfficeCountry' => '',
		// 	'OfficeZip' => '',
		// 	'OfficeEmail' => '',
		// 	'OfficePhone' => '',
		// 	'OfficeExt' => '',
		// 	'OfficeFax' => '',
		// 	'CardType' => '3',
		// 	'MSISDN' => '',
		// ]);
	}

	/**
	 * Gets response result
	 * 
	 * @return StdClass
	 */
	public function __getResponseResult()
	{
		return $this->response->RegisterMemberResult;
	}

	/**
	 * Function that should implement some business logic, 
	 * transformations, or validation to parameters
	 * prior submitting request to LMS.
	 * 
	 * @param string $args
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function send($args)
	{
		return $this->authenticatedSubmit('TempRegisterMember', $args);
	}

}