<?php

namespace App\Adapters\MYLMS\ApiVirtualCard;

use  App\Adapters\MYLMS\Api\AbstractBase;

/*
 * Verify virtual card from code given in email
 */
class GetVirtualCard extends AbstractBase
{
	protected $mandatoryFields = [];

	/**
	 * Parameterized function
	 * 
	 * @param string $email
	 * @param string $code
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function run($email, $code)
	{
		return $this->send([
			'homeemail' => $email,
			'RandomCode' => $code,
		]);
	}

	/**
	 * Function that should implement some business logic, 
	 * transformations, or validation to parameters
	 * prior submitting request to LMS.
	 * 
	 * @param string $args
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function send($args)
	{
		return $this->authenticatedSubmit('GetVirtualCard', $args);
	}

	/**
	 * Parses the dynamic response into a standard form.
	 * 
	 * @return array
	 */
	public function parseResponse()
	{
		$headers = $this->__getResponseHeader();

		// What a wierd way to store the data in header..
		$data = [ 'card' => $headers->virtualCard ] ?? [];

		return [
			'header' => (array) $headers,
			'details' => (array) $data,
		];
	}

	// Example success:
	// #parsedResponse: array:2 [▼
	//     "header" => array:3 [▼
	//       "ErrorCode" => "00"
	//       "ErrorMessage" => "SUCCESS"
	//       "virtualCard" => "6298435910000054"
	//     ]
	//     "details" => []
	//   ]
}