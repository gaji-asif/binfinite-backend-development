<?php

namespace App\Adapters\MYLMS\ApiVirtualCard;

use  App\Adapters\MYLMS\Api\AbstractBase;

/*
 * Change password for new Virtual Card
 */
class ChangePassword extends AbstractBase
{
	protected $mandatoryFields = [];

	/**
	 * Parameterized function
	 * 
	 * @param string $cardNo
	 * @param string $cardPin
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function run()
	{
		return $this->send([
			'Card' => '6298435910000054',
			'NewPassword' => '112112',
		]);
	}

	/**
	 * Function that should implement some business logic, 
	 * transformations, or validation to parameters
	 * prior submitting request to LMS.
	 * 
	 * @param string $args
	 * @return mixed Result from subclass run call
	 * @throws MYLMSException
	 */
	public function send($args)
	{
		return $this->authenticatedSubmit('ChangePassword', $args);
	}

	// Example success:
	//  #response: {#765 ▼
	//   +"ChangePasswordResult": {#766 ▼
	//     +"ResponseHeader": {#767 ▼
	//       +"ErrorCode": "00"
	//       +"ErrorMessage": "Success Email"
	//     }
	//   }
	// }
	// Note: If already changed can be changed again.. This might be a loophole too
	// LMS will also send email..
}