<?php

namespace App\Adapters\MYLMS\SoapClient;

interface SoapClientInterface
{
	/**
	 * For mock testing purposes
	 *
	 */
	public function once();
	public function withError($code, $message = '');
	public function withSuccess($message = 'Success');
	public function withData($data);
}