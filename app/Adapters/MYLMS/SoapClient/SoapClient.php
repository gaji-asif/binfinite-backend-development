<?php

namespace App\Adapters\MYLMS\SoapClient;
use App\Adapters\MYLMS\Exceptions\MYLMSException;

class SoapClient extends \SoapClient implements SoapClientInterface
{
	public function __construct($wsdl, $options = [])
	{
		if (\App::environment(['local', 'development', 'testing', 'staging'])) {
			$streamContext = stream_context_create([
				'ssl' => [
					'verify_peer' => false,
					'verify_peer_name' => false,
					'allow_self_signed' => true,
				],
			]);

			$options['stream_context'] = $streamContext;
			$options['trace'] = 1;
			$options['soap_version'] = SOAP_1_2;

			// Temp disable cache config for development, please revise when deploying
			ini_set('soap.wsdl_cache_enabled', 0);
			ini_set('soap.wsdl_cache_ttl', 0);
		}

		try {
			parent::__construct($wsdl, $options);
		} catch (\Exception $e) {
			if ($e->faultcode == 'WSDL') {
				throw new MYLMSException('WSDL server error');
			}

			throw $e;
		}
	}
	
	public function once()
	{
		return $this;
	}

	public function withError($code, $message = '')
	{
		return $this;
	}

	public function withSuccess($message = 'Success')
	{
		return $this;
	}

	public function withData($data)
	{
		return $this;
	}
}