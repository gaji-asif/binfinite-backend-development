<?php

namespace App\Adapters\MYLMS\SoapClient;

use App\Adapters\MYLMS\Exceptions\LMSErrorException;

class MockSoapClient implements SoapClientInterface
{
	public $wsdl;
	public $options;

	private $errorCode;
	private $errorMessage;
	private $data;
	private $isOnce = false;

	public function __construct($wsdl, $options)
	{
		$this->wsdl = $wsdl;
		$this->options = $options;
	}

	public function __call($function, $argument)
	{
		$objectName = $function . 'Result';
		$responseHeader = 'ResponseHeader';
		$detailName = $function . 'ResultDetails';
		$result = [];
		$result[$objectName] = [];
		$result[$objectName][$responseHeader] = [
			'ErrorCode' => $this->errorCode,
			'ErrorMessage' => $this->errorMessage,
		];

		if ($this->data) {
			$result[$objectName][$detailName] = $this->data;
		}

		$resultObject = json_decode(json_encode($result));

		if ($this->isOnce) {
			$this->errorCode = null;
			$this->errorMessage = null;
			$this->data = null;
		}

		return $resultObject;
	}

	public function once()
	{
		$this->isOnce = true;
		return $this;
	}

	public function withError($code, $message = '')
	{
		$this->errorCode = $code;
		$this->errorMessage = $message;
		return $this;
	}

	public function withSuccess($message = 'Success')
	{
		$this->errorCode = LMSErrorException::TRANSACTION_DONE_WITHOUT_ERROR;
		$this->errorMessage = $message;
		return $this;
	}

	public function withData($data)
	{
		$this->data = $data;
		return $this;
	}
}