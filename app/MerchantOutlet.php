<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\StorageImageResolver;

class MerchantOutlet extends Model
{
	use StorageImageResolver;
	
	/**
	 * Relation to merchant
	 *
	 */
	public function merchant()
	{
		return $this->belongsTo('App\Merchant', 'merchant_id');
	}

	/**
	 * Relation to merchant_offers through merchant_offers_outlets
	 *
	 */
	public function offers()
	{
		return $this->belongsToMany('App\MerchantOffer', 'merchant_offers_outlets');
	}

	/**
	 * Scope for getting outlets that are directed to the merchant only
	 *
	 */
	public function scopeMerchantId($query, $merchantId)
	{
		return $query->where('merchant_id', $merchantId);
	}

	/**
	 * Attribute for full url for outlet thumbnail
	 *
	 */
	public function getThumbnailImageAttribute()
	{
		return $this->resolveImage($this->thumbnail_url);
	}

	/**
	 * Temporary attribute to generate array for partner offer page with outlets
	 *
	 */
	public function getPartnerOfferArrayAttribute()
	{
		return [
            'image' => $this->thumbnail_image,
            'title' => $this->name,
            'bio_html' => 'T: ' . $this->phone . '<br>E: ' . $this->email,
        ];
	}
}