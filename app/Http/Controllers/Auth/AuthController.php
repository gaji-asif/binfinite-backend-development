<?php
namespace App\Http\Controllers\Auth;
use App\User;
use App\EmailActivation;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;

use App\Services\Mailer;

use App\Adapters\MYLMS\Exceptions\LMSErrorException;
use \MYLMS;

use LVR\Phone\Phone;

class AuthController extends Controller
{
    protected $mailer;
    protected $cardService;

    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
        $this->cardService = new \App\Services\CardService;
    }

    // public function redirectToProvider($provider)
    // {
    //     return Socialite::driver($provider)->redirect();
    // }

    // /**
    //  * Obtain the user information from provider.  Check if the user already exists in our
    //  * database by looking up their provider_id in the database.
    //  * If the user exists, log them in. Otherwise, create a new user then log them in. After that
    //  * redirect them to the authenticated users homepage.
    //  *
    //  * @return Response
    //  */
    // public function handleProviderCallback($provider)
    // {
    //     $socialiteUser = Socialite::driver($provider)->user();

    //     //dd($socialiteUser);

    //     $user = User::where('email', $socialiteUser->email)->first();

    //     // Duplicates/conflict checks
    //     if ($user) {
    //         // User with this email exists, check if this is the same provider

    //         if ($user->provider == null && $user->provider_id == null) {
    //             // This user has never logged in with any providers before, should we allow it?
    //             $authUser = $this->assignExistingUserToProvider($user, $socialiteUser, $provider);
    //         } else if ($user->provider != $provider) {
    //             // Disallow different provider
    //             return redirect('/')->with('error','Your email has already been used with a different social login in our system. Please contact support for further action.');
    //         }   
    //     }

    //     $authUser = $this->findOrCreateUser($socialiteUser, $provider);

    //     Auth::login($authUser, true);
        
    //     if (! $authUser->defaultMember) {
    //         //flag all user have no bcard
    //         return redirect('/')->with('no_bcard', 'true');
    //     }
        
    //     return redirect('/');
    // }

    // /**
    //  * If a user has registered email but not with a provider, link to the provider
    //  * with existing account.
    //  * @param  $user User object (User model)
    //  * @param  $user Socialite user object
    //  * @param $provider Social auth provider
    //  * @return  User
    //  */
    // public function assignExistingUserToProvider($user, $socialiteUser, $provider)
    // {
    //     // Only for users who are not already assigned
    //     if ($user->provider != null || $user->provider_id != null) {
    //         return $user;
    //     }

    //     if ($socialiteUser->user['verified_email'] === true && $user->email_verified_at == null) {
    //         $user->email_verified_at = now();
    //     }

    //     $user->provider = $provider;
    //     $user->provider_id = $socialiteUser->user['id'];
    //     $user->save();

    //     return $user;
    // }


    // /**
    //  * If a user has registered before using social auth, return the user
    //  * else, create a new user object.
    //  * @param  $socialiteUser Socialite user object
    //  * @param $provider Social auth provider
    //  * @return  User
    //  */
    // public function findOrCreateUser($socialiteUser, $provider)
    // {
    //     $authUser = User::where('provider_id', $socialiteUser->id)->first();
    //     if ($authUser) {
    //         return $authUser;
    //     } else {

    //         $display_name = ellipsize_text($socialiteUser->name, 20); // set as default
    //         if ($provider == 'google') {
    //             $display_name = $socialiteUser->user['given_name'];
    //         }

    //         $user = User::create([
    //             'name'     => $socialiteUser->name,
    //             'display_name'  => $display_name,
    //             'email'    => $socialiteUser->email ?? '',
    //             'provider' => $provider,
    //             'provider_id' => $socialiteUser->id,
    //             'role' => User::$roleMember,
    //             'status' => User::$statusActive
    //         ]);

    //         if ($socialiteUser->user['verified_email'] === true) {
    //             $user->email_verified_at = now();
    //             $user->save();
    //         }
    //         return $user;
    //     }
    // }

    public function logout() {
        $user = Auth::user();

        if ($user) {
            MYLMS::ssoLogout($user->email, $user->lms_session_id);
            $user->lms_session_id = null;
            $user->lms_sso_id = null;
            $user->save();
            Auth::logout();
        }

		return redirect('/');
	}

	public function memberLogin(LoginRequest $request) {
		$inputs = $request->all();
        $email = $inputs['email'];
        $password = $inputs['password'];

        // $credentials = [
        //     'email' => $email, 
        //     'password' => $password, 
        //     'status' => User::$statusActive, 
        //     'role' => User::$roleMember
        // ];

        $user = $this->cardService->ssoLogin($email, $password);

        // Failed login
        if (! $user) {
            return redirect(route('home'))->with('error', 'You have entered an invalid email or password');
        }

        // Success login here on out
        // Sync member cards from lms
        $this->cardService->syncMemberCards($user);
        $user->fresh();

        // Got bcard
        if ($user->lmsMember()->first()->isRegistered) {
            return redirect(route('home'));
        }

        // No bcard, prompt user to create new
        // return redirect(route('home'))->with('no_bcard', 'true');

        // Not registered LMS, register first
        return redirect(route('member_register_sso'));
	}

    public function memberRegister(Request $request) {

        $validator = \Validator::make($request->all(), [
            'email' => 'required|email|max:255',
            'mobile' => ['required', new Phone],
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
        ]);

        // Just give the first message..
        $error = $validator->errors()->first();
        if ($error && $error != '') {
            return redirect('/member/register')->with('register_error', $error);
        }

        $inputs = $request->all();

        $email = $inputs['email'];
        $mobile = $inputs['mobile'];
        $password = $inputs['password'];
        $passwordConfirm = $inputs['password_confirmation'];

        $user = User::where('email', $email)->orWhere('mobile', $mobile)->first();

        if ($user) {
            // Already exists. Bail?
            return redirect('/member/register')
                ->with('register_error', 'Your email or mobile number already exists in our system. Please use another email and mobile number, or contact support for help.')
                ->withInput();
        }
        
        // if(User::isEmailExists($inputs['email'])) {
        //     return redirect('/member/register')->with('register_error','Your email is already exists in our system. Please use other emails to register.');
        // }

        // $data = User::create([
        //     'email'    => ,
        //     'password'  => bcrypt(),
        //     'role' => User::$roleMember,
        //     'status' => User::$statusPendingActive
        // ]);

        // Signup at SSO first
        try {
            $result = MYLMS::ssoSignup($email, $mobile, $password);
            
            if ($result) {
                $user = User::create([
                    'email'    => $email,
                    'mobile'    => $mobile,
                    'role' => User::$roleMember,
                    'status' => User::$statusPendingActive,
                ]);

                // Attempt to fill up member info into our db
                $user = $this->cardService->ssoLogin($email, $password);
                $this->cardService->syncMemberProfile($user);

                // return redirect('/member/register')->with('register_success','Thank you for sign up! We have send an email with an activation link to your email address. In order to complete the sign up process, please click the activation link.');
                // return redirect('/member/register')->with('register_success','Thank you for signing up! You will receive verification number on your mobile phone shortly.');
                return redirect(route('verify_mobile'))->with('success', 'Thank you for sign up! We have send an email with an activation link to your email address. In order to complete the sign up process, please click the activation link.');
            }
        } catch (LMSErrorException $e) {
            $redirect = redirect('/member/register')->withInput();
            // Catch couple of error messages
            if ($e->isMessage('Email Exist')) {
                return $redirect->with('register_error', 'Your email already exists. Please try to login using that email instead.');
            }

            if ($e->isMessage('Mobile Exist')) {
                return $redirect->with('register_error', 'Your mobile number already exists. Please try to use another one.');
            }

            return $redirect->with('register_error', $e->getFriendlyMessage());
        }


        //create email activation
        // $code = get_token();

        // $activation_link = config('app.url').'/account/activation/'.$data->id.'/'.$code;

        // EmailActivation::create([
        //     'user_id' => $data->id,
        //     'activation_code' => $code
        // ]);
        
        // $emails = array(
        //     'email' => $inputs['email'],
        //     'activation_link' => $activation_link,
        //     'name' => $inputs['display_name'],
        // );

        // $this->mailer->confirmationActivation($emails);

        return redirect('/member/register')->with('register_error','There was an error registering your account.');
    }

    public function memberActivation($user_id, $activation_code) {
        $user = User::find($user_id);

        $user->status = User::$statusActive;

        $user->save();

        Auth::login($user, true);

        //flag all user have no bcard
        return redirect('/')->with('success','Thank you activate your account.')->with('no_bcard', 'true');
    }
}