<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\TestFormRequest;
use App\TestTable;

class TestFormController extends Controller
{
    public function __construct()
    {

    }
    
    public function test(TestFormRequest $request) {
        $this->validate(request(),[
            //put fields to be validated here
            ]);         
       
        $data = new TestTable;
        $data->test1 = $request['name'];
        $data->test2 = $request['display_name'];
        // add other fields
        $data->save();

        return redirect('/');
    }
}
