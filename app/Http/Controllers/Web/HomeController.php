<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use App\Helpers\MailHelper;
use App\ContactForm;

class HomeController extends Controller
{
    public function index() {
    	return view('web.pages.home');
    }

    public function termsPage() {
    	return view('web.pages.terms');
    }

    public function privacyPage() {
    	return view('web.pages.privacy');
    }

    public function supportPage(Request $request) {
        $faqs = \App\Faq::all();
        $categories = [];

        // Slugged categories
        // Should change to use FAQ categorySlug attribute, but rn too dizzy head
        $cats = $faqs->pluck('category')->unique();
        foreach ($cats as $cat) {
            $categories[Str::slug($cat)] = $cat;
        }

        $sortedFaqs = $faqs->groupBy('category');

        $sentContact = false;

        if($request->all()) {
            MailHelper::sendContactForm($request->all());

            ContactForm::create([
                'full_name' => $request->get('full_name'),
                'email' => $request->get('email'),
                'mobile_number' => $request->get('mobile_number'),
                'reason' => $request->get('reason'),
                'binf_number' => $request->get('binf_number'),
                'details' => $request->get('details'),
            ]);
            
            $sentContact = true;
        }

    	return view('web.pages.support', [
            'categories' => $categories,
            'sortedFaqs' => $sortedFaqs,
            'sentContact' => $sentContact
        ]);
    }

    public function pointsExchangePage() {
    	return view('web.pages.points_exchange');
    }

    public function announcementsPage() {
        $announcements = \App\Announcements::all();

        return view('web.pages.announcements.announcements', [
            'announcementsListing' => $announcements
        ]);
    }
}
