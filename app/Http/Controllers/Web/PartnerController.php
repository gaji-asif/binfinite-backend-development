<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Merchant;
use App\MerchantOffer;

class PartnerController extends Controller
{
    /**
     * Partner detail page
     *
     */
    public function earnPartnerDetailPage(Request $request, $partnerId, $partnerSlug = null) {
        $merchant = Merchant::find($partnerId);

        if (! $merchant) {
            return redirect(route('home'));
        }

        if ($merchant->slug != $partnerSlug) {
            return redirect(route('partner_detail', [$merchant->id, $merchant->slug]));
        }

    	return view('web.pages.partners.earn_partner_detail', ['partner' => $merchant]);
    }

    /**
     * Partner profile page
     *
     */
    public function earnPartnerProfilePage(Request $request, $partnerId, $partnerSlug = null) {
        $merchant = Merchant::find($partnerId);

        if (! $merchant) {
            return redirect(route('home'));
        }

        if ($merchant->slug != $partnerSlug) {
            return redirect(route('partner_profile', [$merchant->id, $merchant->slug]));
        }

        $offers = $merchant->activeOffers->map(function($offer) use ($merchant) {
            return $offer->partnerPromoArray;
        });

        $collection = [
            'name' => 'Offers from ' . $merchant->name,
            'company' => $merchant->name,
            'items' => $offers,
        ];

        return view('web.pages.partners.earn_partner_profile', ['partner' => $merchant, 'collection' => $collection]);
    }

    /**
     * Partner offer detail page
     *
     */
    public function earnPartnerOfferPage(Request $request, $partnerId, $partnerSlug, $offerId) {
        $merchant = Merchant::find($partnerId);

        if (! $merchant) {
            return redirect(route('home'));
        }

        if (! $offerId) {
            return redirect(route('home'));
        }

        if ($merchant->slug != $partnerSlug) {
            return redirect(route('partner_offer', [$merchant->id, $merchant->slug, $offerId]));
        }

        $offer = MerchantOffer::find($offerId);
        
        if (! $offer) {
            return redirect(route('home'));
        }

        $outlets = $offer->outlets->map(function($outlet) use ($merchant) {
            return $outlet->partnerOfferArray;
        });

        $collection = [
            'name' => 'Participating Outlets',
            'company' => $merchant->name,
            'isMoreButtonUrl' => $merchant->profileUrl,
            'items' => $outlets,
        ];

    	return view('web.pages.partners.earn_partner_offer', ['partner' => $merchant, 'offer' => $offer, 'collection' => $collection]);
    }
}
