<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Payment;

use App\Helpers\OrderHelper;
use App\User;
use Carbon\Carbon;

use App\Services\Wirecard;

class PaymentController extends Controller
{   
    protected $wirecard;

    public function __construct()
    {
        $this->wirecard = new Wirecard();
    }

    public function dummy(Request $request)
    {
    	$data = array();
        $data['request_time_stamp']          = Carbon::now('UTC')->format('YmdHis');
        $data['request_id']                 = "ORDER-".time();
        $data['merchant_account_id']        = config('payment.merchant_account_id');
        $data['transaction_type']            = config('payment.transaction_type');
        $data['requested_amount']            = '1.00';
        $data['requested_amount_currency']   = config('payment.requested_amount_currency');
        $data['redirect_url']               = config('payment.redirect_url');
        //$data['ip_address']                 = $request->ip();
        $data['secret_key']                  = config('payment.secret_key');

        $signatureSourceString = implode($data, '');
        $signature =  hash('sha256', $signatureSourceString);

    	return view('web.pages.payments.dummy', compact(
    		'data', 'signature', 'signatureSourceString'));
    }

    public function order(Request $request)
    {
        $order = OrderHelper::createOrder(1,5, config('payment.payment_method'));

        $user = User::where('id', $order->user_id)->first();

        $data = $this->wirecard->generateSignature($order->invoice_no, $order->amount);

        return view('web.pages.order.summary', compact(
            'data', 'order', 'user'));
    }

    public function capture(Request $request)
    {

        $inputs = $request->all();
        
        if(!empty($inputs))
            OrderHelper::log($inputs);

        $results = OrderHelper::getLogs();

        return view('web.pages.order.payment_status', compact(
            'results'));
    }

    public function paymentProductDetail() {
        return view('web.pages.payment.payment_product_detail');
    }
    public function paymentPaymentDetail() {
        return view('web.pages.payment.payment_payment_detail');
    }
    public function paymentComplete() {
        return view('web.pages.payment.payment_complete');
    }
}
