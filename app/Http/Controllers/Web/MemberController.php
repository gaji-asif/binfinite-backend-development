<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\MerchantFormRequest;
use App\Helpers\MailHelper;
use App\PointConvertForm;
use App\User;
use App\NetworkPartner;
use App\BrandCategory;

use App\Adapters\MYLMS\Exceptions\LMSErrorException;

class MemberController extends Controller
{
    protected $cardService;

    /**/
    public function __construct()
    {
        $this->cardService = new \App\Services\CardService;
    }

    /**/    
    public function register()
    {
        if (\Auth::check()) {
            return redirect(route('home'))->with('success', 'You are already logged in');
        }
        
    	return view('web.pages.members.register');
    }

    /**/
    public function profile()
    {
        $user = \Auth::user();

        $user->load('lmsMember.cards');
        $member = $user->lmsMember;
        $cards = [];

        if ($member) {
            $cards = $member->cards;
        }

        return view('web.pages.members.profile', compact('member', 'cards'));
    }

    /**
     * Controller for setting primary card
     */
    public function setPrimaryCard(Request $request)
    {
        $user = \Auth::user();
        $cardNo = $request->primary_card_no;

        $member = $user->lmsMember;

        if ($member) {
            $member->setPrimaryCard($cardNo);

            return redirect(route('member_profile'))->with('success', 'Successfully set card as primary');
        }

        return redirect(route('member_profile'))->with('error', 'Failed to set card as primary');
    }

    /**
     * Controller for setting primary card
     */
    public function updateContact(Request $request)
    {
        $user = \Auth::user();
        $email = $request->email;
        $mobile = $request->mobile;

        $member = $user->lmsMember;

        if ($member) {
            // Should send update profile request to LMS but pending API

            return redirect(route('member_profile'))->with('success', 'Successfully set card as primary');
        }

        return redirect(route('member_profile'))->with('error', 'Failed to set card as primary');
    }

    /**/    
    public function shopOnline()
    {
        $chunkedStores = \App\ShoponlineStore::active()->get()->chunk(6);

        return view('web.pages.shoponline.shop_online', ['chunkedStores' =>  $chunkedStores]);
    }

    /**/
    public function shopOnlineFilter($key) {
        $chunkedStores = \App\ShoponlineStore::active()->get()->where('category', $key)->chunk(6);

        return view('web.pages.shoponline.shop_online', ['chunkedStores' =>  $chunkedStores]);
    }

    /**/
    public function earnPoints()
    {
        $merchants = \App\Merchant::all();
        $offers = \App\MerchantOffer::active()->get();
        $offersArray = $offers->map(function($offer) { return $offer->earnPartnerPromoArray; });

        return view('web.pages.earn_points', ['merchants' => $merchants, 'offers' => $offersArray]);
    }

    /**/
    public function redeemPoints()
    {
        return view('web.pages.redeem_points');
    }

    /**/
    public function shopOnlineRedirect(MerchantFormRequest $request)
    {
        $cardId = $request->cardID;
        $shopId = $request->shopID;
        
        $shop = \App\ShoponlineStore::find($shopId);

        if ($shop) {
            $affiliateUrl = $shop->getAffiliateUrl($cardId);
            return redirect($affiliateUrl);
        }

        return redirect('shoponline_list')->with('error', 'Some error occured');
    }

    /**/
    public function membershipPage()
    {
        return view('web.pages.members.membership');
    }

    /**/
    public function surveyPanelistPage()
    {
        return view('web.pages.members.survey_panelist');
    }

    /**/
    public function pointsConvert(Request $request)
    {
        if($request->all()) {
            $user = \Auth::user();

            if ($user) {
                $pointsToConvert = $request->get('point_convert_amount');
                $totalPointsAvailable = $user->getTotalBpointsAttribute();

                if ($totalPointsAvailable >= $pointsToConvert) {
                    MailHelper::sendBPointsConvertRequest($request->all());

                    PointConvertForm::create([
                        'binf_number' => $request->get('binf_number'),
                        'binf_name' => $request->get('binf_name'),
                        'mobile_number' => $request->get('mobile_number'),
                        'email' => $request->get('email'),
                        'member_uid_number' => $request->get('member_uid_number'),
                        'point_convert_amount' => $pointsToConvert,
                    ]);

                    return redirect('/exchange')->with('success_title','Thanks!')->with('description_message','Your request has been processed and may take a few days to appear in your '.$request->get('partner_name').' account');
                } else {
                    return redirect('/exchange')->with('error_title','Error')->with('description_message','Your point is not enough for conversion');
                }
            }
        }
    }

    /**/
    public function cardRegister(Request $request) {
        $user = \Auth::user();

        if ($user->lmsMember->isRegistered) {
            return redirect(route('home'))->with('success', 'You have already registered');
        }

        if ($request->isMethod('post')) {
            $request->flash();
            // TODO: Complete registration form

            $validator = \Validator::make($request->all(), [
                'full_name' => 'required|max:255',
                'nationality' => 'required|max:255',
                'country' => 'required|max:255',
            ]);

            $error = $validator->errors()->first();
            if ($error && $error != '') {
                return redirect()->back()->with('error', $error);
            }

            $name = $request->full_name;
            $nationality = $request->nationality;
            $country = $request->country;
            $ic_passport = $request->ic_passport;

            try {
                $result = $this->cardService->registerSSO($user, $name, $nationality, $country,
                    $ic_passport, $ic_passport/*, $dob, $address1, $address2, 
                    $address3, $postcode, $city, $state, 
                    $paymentRegistered, $masterRegistered*/);
            } catch (LMSErrorException $e) {
                if ($e->is(LMSErrorException::MEMBER_ALREADY_REGISTERED)) {

                    // Should attempt to get member profile?
                    $this->cardService->syncMemberCards($user);
                    return redirect(route('home'))->with('error', 'You have already been registered');
                }

                // Returns generic error
                return $redirect->with('error', $e->getFriendlyMessage());
            }
        }

        return view('web.pages.members.register_card', compact('user'));
    }

    /**/
    public function verifyMobile(Request $request) {
        $user = \Auth::user();
        $member = $user->lmsMember;

        // If already registered member redirect
        if ($member->isRegistered) {
            return redirect(route('home'))->with('success', 'You have already registered');
        }

        if ($request->isMethod('post')) {

            $validator = \Validator::make($request->all(), [
                'pin_number' => 'min:6|integer',
            ]);

            // If got validation error return
            $error = $validator->errors()->first();
            if ($error && $error != '') {
                return redirect()->back()->with('error', $error);
            }

            try {
                // Resend OTP
                if ($request->resend_otp == '1') {
                    // Check if already resent in the last 60 seconds
                    $lastResent = session('otp_resend', false);
                    if ($lastResent) {
                        if ($lastResent->diffInSeconds() < 60) {
                            $difference = 60 - $lastResent->diffInSeconds();
                            return redirect()->back()->with('error', "Please wait $difference more seconds to resend");
                        }
                    }

                    // Ok to resend
                    $result = \MYLMS::ssoResendOtp($user->lms_sso_id, $member->lms_Email);

                    // Set session to check later if needed
                    session([ 'otp_resend' => now() ]);

                    // To show the last 4 digits of mobile phone
                    $mobileDigits = substr($member->lms_Mobile, -4);
                    return redirect()->back()->with('success', "Resent OTP to mobile number ending with $mobileDigits");
                    
                } else if ($request->pin_number != '') {
                    // Verify OTP
                    $result = \MYLMS::ssoCheckEmailOtp($member->lms_Email, $member->lms_Mobile, $request->pin_number);

                    // If no throw error assume successful
                    $user->status = User::statusActive;
                    $user->save();

                    return redirect(route('member_register_sso'))->with('success', 'Successfully verified OTP');
                }
            } catch (LMSErrorException $e) {
                if ($e->isMessage('Invalid OTP')) {
                    return redirect()->back()->with('error', 'Invalid OTP');
                }

                // Returns generic error
                return redirect()->back()->with('error', $e->getFriendlyMessage());
            }
        }

        return view('web.pages.members.verify_mobile', compact('user'));
    }

    /**/
    public function virtualCardRegister() {
        return view('web.pages.members.register_virtual_card');
    }

    /**/
    public function pointsHistroy() {
        $user = \Auth::user();

        if ($user) {
            $rewards = \MYLMS::ssoGetRewardHistory($user->email, $user->lms_sso_id, '6298430000004611')->data;
        }
        

        // dd($rewards);

        return view('web.pages.members.points_transaction_histroy');
    }

    /**/
    public function networkOfPartner(Request $request){
        $allBrandCategory = \App\BrandCategory::all();
        $submitForm = false;
        $contact_regards = '';
        $brand_category = '';

        if($request->all()){
            //MailHelper::sendNetworkPartnerDetails($request->all());

            if(!empty($request->get('brand_category'))){
                if(count($request->get('brand_category'))>0){
                    $all_brand_category = $request->get('brand_category');
                    $brand_category = implode(',', $all_brand_category); 
                }
            }
            
            if(!empty($request->get('contact_regards'))){
                if(count($request->get('contact_regards'))>0){
                    $all_contact_regards = $request->get('contact_regards');
                    $contact_regards = implode(',', $all_contact_regards); 
                }
            }

            NetworkPartner::create([
                'business_name' => $request->get('business_name'),
                'website' => $request->get('website'),
                'brand_name' => $request->get('brand_name'),
                'brand_website' => $request->get('brand_website'),
                'brand_category' => $brand_category,
                'contact_name' => $request->get('contact_name'),
                'office_contact' => $request->get('office_contact'),
                'mobile_contact' => $request->get('mobile_contact'),
                'contact_regards' => $contact_regards
            ]);

        $submitForm = true;

        }
        return view('web.pages.members.network_of_partner', ['allBrandCategory'=>$allBrandCategory, 'submitForm' => $submitForm]);
    }
}
