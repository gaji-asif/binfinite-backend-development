<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CardController extends Controller
{
    public function linkCard(Request $request) {
        //$result = MYLMS::GetCard($request->input('bcardNumber'), $request->input('pinNumber'));
        $bcardNumber = $request->input('bcardNumber');
        $pinNumber = $request->input('pinNumber');

        $cs = new \App\Services\CardService();
        $cs->linkUserToCard(\Auth::user(), $bcardNumber, $pinNumber);
        
        return jsend_success('Successfully linked card');
    }

    public function profile() {
    	return view('web.pages.members.profile');
    }
    
    public function shopOnline() {
        return view('web.pages.shop_online');
    }

    public function earnPoints() {
        return view('web.pages.earn_points');
    }

    public function redeemPoints() {
        return view('web.pages.redeem_points');
    }
}
