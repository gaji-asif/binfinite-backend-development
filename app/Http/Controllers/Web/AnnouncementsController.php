<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Announcements;

class AnnouncementsController extends Controller
{
    /**
     * Announcements detail page
     *
     */
    public function announcementsDetailPage(Request $request, $announcementsId, $announcementsSlug = null) {
        $announcements = Announcements::find($announcementsId);

        if (! $announcements) {
            return redirect(route('home'));
        }

        if ($announcements->slug != $announcementsSlug) {
            return redirect(route('announcements_detail', [$announcements->id, $announcements->slug]));
        }

    	return view('web.pages.announcements.announcements_detail', ['announcements' => $announcements]);
    }
}
