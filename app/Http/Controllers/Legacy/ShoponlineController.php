<?php

namespace App\Http\Controllers\Legacy;

class ShoponlineController extends BaseController
{
	public $allowedFiles = [
		'insurance.php'
	];

    public $folder = './shoponline-sg';

    public function index()
    {
		$dir = $this->getFullPath();
		chdir($dir);
		include_once('shop-online-sg.php');
    }

    public function check()
    {
		$dir = $this->getFullPath();
		chdir($dir);
		include_once('shop-online-sg-check.php');
    }

    public function redirect()
    {
		$dir = $this->getFullPath();
		chdir($dir);
		include_once('shop-online-sg-redirect.php');
    }

}
