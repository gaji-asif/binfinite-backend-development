<?php

namespace App\Http\Controllers\Legacy;

class InsuranceTravelController extends BaseController
{
	public $allowedFiles = [
		'insurance-travel.php'
	];

    public $folder = 'insurance-travel';

    public function index()
    {
		$dir = $this->getFullPath();
		chdir($dir);
		include_once('insurance-travel.php');

		// return view('web.legacy.insurance-travel');
    }
}
