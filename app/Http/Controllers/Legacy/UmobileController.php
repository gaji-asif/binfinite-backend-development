<?php

namespace App\Http\Controllers\Legacy;

class UmobileController extends BaseController
{
	public $allowedFiles = ['bottom.php',
		'bpoints-expiry.php',
		'config.php',
		'config-staging.php',
		'connect.php',
		'edit-home-detail.php',
		'edit-office-detail.php',
		'edit-personal-detail.php',
		'forgot-pin.php',
		'functions.php',
		'functions-jeff.php',
		'functions-staging.php',
		'home-details.php',
		'login.php',
		'menu-bar.php',
		'merchant-details.php',
		'merchants-dynamic.php',
		'office-details.php',
		'personal-details.php',
		'process-login.php',
		'process-login-redemption.php',
		'promotions-dynamic.php',
		'redemption.php',
		'redemption2.php',
		'redemption-details.php',
		'redemption-history.php',
		'redemption-live.php',
		'register.php',
		'register-successful.php',
		'sign-up-form-validation.php',
		'token-history.php',
		'top.php',
		'transaction-history.php',
		'php_helpers/date.php',
	];

    public $folder = 'umobile';
}
