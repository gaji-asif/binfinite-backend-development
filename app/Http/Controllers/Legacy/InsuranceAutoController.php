<?php

namespace App\Http\Controllers\Legacy;

class InsuranceAutoController extends BaseController
{
	public $allowedFiles = [
		'aig-car-insurance.php',
		'auto-purchase-conditions.php',
		'bottom.php',
		'claims.php',
		'exist-register.php',
		'how-to-renew.php',
		'index.php',
		'netallianz-config.php',
		'recaptcha.php',
		'register-success.php',
		'road-tax-renewal.php',
		'robot-verification.php',
		'side-bar.php',
		'third-party-bodily-injury-claim.php',
		'third-party-property-damage-claim.php',
		'top.php',
		'windscreen-claim.php',
		'renew-now.php',
		'admin/login.php',
		'admin/download.php',
	];

    public $folder = 'insurance-auto';

    public function index()
    {
		$dir = $this->getFullPath();
		chdir($dir);
		include_once('insurance-auto.php');
    }
}
