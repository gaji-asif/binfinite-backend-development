<?php

namespace App\Http\Controllers\Legacy;

class InsuranceController extends BaseController
{
	public $allowedFiles = [
		'insurance.php'
	];

    public $folder = './';

    public function index()
    {
		$dir = $this->getFullPath();
		chdir($dir);
		include_once('insurance.php');

		// return view('web.legacy.insurance-travel');
    }
}
