<?php

namespace App\Http\Controllers\Legacy;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as RoutingController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;

class BaseController extends RoutingController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $allowedFiles = [];

    public $basePath = '/Legacysite/';

    public $folder = '';

    public function getFullPath()
    {
    	return app_path() . $this->basePath . $this->folder . '/';
    }

    public function all(Request $request, $filename = 'index.php')
    {
		// Only allow to include predefined php files
		if (in_array($filename, $this->allowedFiles)) {
			$dir = $this->getFullPath();
			chdir($dir);
			include_once($filename);
		} else {
			abort(404);
		}
    }
}
