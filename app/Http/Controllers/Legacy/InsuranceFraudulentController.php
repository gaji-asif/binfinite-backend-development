<?php

namespace App\Http\Controllers\Legacy;

class InsuranceFraudulentController extends BaseController
{
	public $allowedFiles = [
		'submission/bottom.php',
		'submission/exist.php',
		'submission/form-validation.js',
		'submission/index.php',
		'submission/recaptcha.php',
		'submission/register-success.php',
		'submission/robot-verification.php',
		'submission/top.php',
	];

    public $folder = 'insurance-fraudulent-cover';

    public function index()
    {
		$dir = $this->getFullPath();
		chdir($dir);
		include_once('insurance-fraudulent-cover.php');
    }
}
