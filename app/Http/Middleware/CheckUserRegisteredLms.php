<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckUserRegisteredLms
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            $user = Auth::user();

            // If user doesnt have lms member, will need to bail and error out
            if (! $user->lmsMember) {
                Auth::logout();
                return redirect(route('home'))->with('error', 'Sorry, there is something wrong with your account registration. Please contact support.');
            }

            // Already registered
            if ($user->lmsMember->isRegistered) {
                return $next($request);
            }

            return redirect(route('member_register_sso'));
        }

        // If not logged in can attempt to proceed
        return $next($request);
    }
}
