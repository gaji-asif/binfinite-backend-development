<?php

namespace App\Traits;

use Storage;

trait StorageImageResolver
{
    /**
     * @internal
     */
    public function resolveImage($url)
    {
    	if (! $url) return null;

		if (str_is_url($url)) {
			return $url;
		}

		return Storage::url($url);
    }
}
