<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PointConvertForm extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'points_convert_form';

    protected $fillable = [
        'binf_number', 'binf_name', 'mobile_number', 'email', 'member_uid_number', 'point_convert_amount'
    ];
}
