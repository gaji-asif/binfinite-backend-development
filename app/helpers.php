<?php
use Carbon\Carbon;

function sanitize_input($input)
{
    if (is_array($input)) {
        foreach ($input as $key=>$value) {
            $output[$key] = sanitize_input($value);
        }
    } else {
        $output = clean_input($input);
        $output = strip_tags($output);
        $output = trim(filter_var($output, FILTER_SANITIZE_STRING));
    }
    return $output;
}

function clean_input($input)
{
    $search = array(
    '@<script[^>]*?>.*?</script>@si',   // Strip out javascript
    '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
    '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
    '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
    );

    $output = preg_replace($search, '', $input);
    return $output;
}

function convert_to_float($input)
{
    return filter_var($input, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
}

function format_money($value, $decimal = 2)
{
    return number_format((float)$value, $decimal, ".", ",");
}

function format_date($value, $format = '')
{
    if (empty($value) || $value == '0000-00-00') {
        return '';
    }
    if (empty($format)) {
        $format = config('propwall.date_format');
    }
    if (is_string($value)) {
        return Carbon::parse($value)->format($format);
    }
    if ($value instanceof Carbon) {
        return $value->format($format);
    }

    return 'Invalid date';
}

function version()
{
    return '0.4.30';
}

function array_not_empty($key, $input)
{
    if (isset($input[$key]) && !empty($input[$key])) {
        return true;
    } else {
        return false;
    }
}

function sha1_hash($input)
{
    return hash('sha1', $input);
}

function is_server_ssl() {
    $url = strtolower(config('app.url'));
    if(strpos($url, 'https:') !== false)
        return true;
    else
        return false;
}

function str_replace_https($value) 
{
    return str_replace('http:','https:', $value);
}

function str_is_url($s)
{
    return !(strpos($s, '://') === false);
}


function is_mobile() {
    $useragent = $_SERVER['HTTP_USER_AGENT'];
    if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)
            ||
            preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))
        ) {
        return true;
    } else {
        return false;
    }
}

function build_http_query( $query ){
    $query_array = array();
    foreach( $query as $key => $key_value ){
        $query_array[] = $key . '=' . urlencode( $key_value );
    }
    return implode( '&', $query_array );
}

function compress_html($buffer) {
    //$buffer = preg_replace('/<!--(.|\s)*?-->/', '', $buffer); // remove html comments
    $buffer = preg_replace('/\s+/', ' ', $buffer); 
    return $buffer;
}

function slug_name_url($string) {
    $replacements = array('!', '*', "'", "(", ")", ";", ":", "@", "&", "=", "+", "$", ",", "/", "?", "%", "#", "[", "]", " ","&#39;", "\"", "“", "‘", "”", "’", "^");
    return str_replace($replacements, '-', strtolower($string));
}

function ellipsize_text($text, $pos = 40) {
    if (strlen($text) > $pos) {
        $lastPos = ($pos - 3) - strlen($text);
        $text = substr($text, 0, strrpos($text, ' ', $lastPos)) . '...';
    }
    return $text;
}

function get_token()
{
    return hash_hmac('sha256', str_random(40), config('app.key'));
}


// Following is modified from https://raw.githubusercontent.com/shalvah/laravel-jsend/master/src/helpers.php

if (!function_exists("jsend_error")) {
    /**
     * @param string $message Error message
     * @param string $code Optional custom error code
     * @param string | array $data Optional data
     * @param int $status HTTP status code
     * @param array $extraHeaders
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    function jsend_error($message, $code = null, $data = null, $status = 500, $extraHeaders = [])
    {
        $response = [
            "status" => "error",
            "message" => $message
        ];
        !is_null($code) && $response['code'] = $code;
        !is_null($data) && $response['data'] = $data;

        return response()->json($response, $status, $extraHeaders);
    }
}

if (!function_exists("jsend_fail")) {
    /**
     * @param string $message
     * @param array $data
     * @param int $status HTTP status code
     * @param array $extraHeaders
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    function jsend_fail($message, $data = [], $status = 400, $extraHeaders = [])
    {
        $response = [
            "status" => "fail",
            "message" => $message,
            "data" => $data
        ];

        return response()->json($response, $status, $extraHeaders);
    }
}

if (!function_exists("jsend_success")) {
    /**
     * @param string $message
     * @param array | Illuminate\Database\Eloquent\Model $data
     * @param int $status HTTP status code
     * @param array $extraHeaders
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    function jsend_success($message, $data = [], $status = 200, $extraHeaders = [])
    {
        $response = [
            "status" => "success",
            "message" => $message,
            "data" => $data
        ];

        return response()->json($response, $status, $extraHeaders);
    }
}