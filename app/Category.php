<?php

namespace App;

use Rinvex\Categories\Models\Category as RinvexCategory;
use DB;

class Category extends RinvexCategory
{
	/**
	 * Gets all categories for a specific class
	 *
	 * @param $class Classname
	 * @return Kalnoy\NestedSet\Collection
	 */
    public static function forClass($class)
    {
    	$categoryIds = DB::table(config('rinvex.categories.tables.categorizables'))
    		->where('categorizable_type', $class)
    		->select('category_id')
    		->distinct('category_id')
    		->get()
    		->pluck('category_id');

    	return static::whereIn('id', $categoryIds)->get();
    }
}
