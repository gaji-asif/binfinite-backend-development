<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';

    public static $statusPendingActive = 'Pending Activation';
    public static $statusActive = 'Active';
    public static $statusInactive = 'Inactive';
    public static $statusBlocked = 'Blocked';
    
    public static $roleAdministrator = 'Administrator';
    public static $roleMember = 'Member';
    public static $roleMerchant = 'Merchant';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'display_name', 'email', 'password', 'provider', 'provider_id', 'role', 'status', 'created_at', 'updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function isEmailExists($email)
    {
        if (static::where('email', '=', $email)->count() > 0) {
           return true;
        } else {
            return false;
        }
    }

    /**
     * Member profile relationship
     *
     * @return HasOne
     */
    public function lmsMember()
    {
        return $this->hasOne('App\Member');
    }

    /**
     * By default should get LMS's name attribute
     *
     * @return string
     */
    public function getLmsNameAttribute()
    {
        $member = $this->lmsMember;
        if ($member) {
            return $member->lms_Name;
        }
        return $this->name;
    }

    /**
     * Total bpoints for all cards attribute
     *
     * @return int
     */
    public function getTotalBpointsAttribute()
    {
        $member = $this->lmsMember;
        if ($member) {
            $cards = $member->cards;

            if ($cards) {
                $total = $cards->sum(function($card) { return (int) $card->lms_BalancePoint; });
                return format_money($total, 0);
            }
        }
        return 0;
    }
}
