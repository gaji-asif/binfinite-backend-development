<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberCard extends Model
{
    protected $table = 'lms_member_cards';

    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * Relation to lms_members
     *
     */
    public function member()
    {
    	return $this->belongsTo('App\Member', 'lms_MemberID', 'lms_MemberID');
    }

    /**
     * Total bpoints for all cards attribute
     *
     */
    public function getFormattedBpointsAttribute()
    {
        return format_money($this->lms_BalancePoint, 0);
    }
}
