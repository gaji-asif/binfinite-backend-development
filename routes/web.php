<?php
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Web'], function () 
{
	// Check if a logged in user is already registered in SSO. Else redirect to force register first
	Route::group(['middleware' => ['ssoregistered']], function() {
		Route::get('/', 'HomeController@index')->name('home');
		Route::get('/?showlogin', 'HomeController@index')->name('login');

		Route::get('/terms', 'HomeController@termsPage')->name('terms');
		Route::get('/privacy', 'HomeController@privacyPage')->name('privacy');
		Route::get('/exchange', 'HomeController@pointsExchangePage')->name('points_exchange');

		Route::get('/announcements', 'HomeController@announcementsPage')->name('announcements');
		Route::get('/announcements/{announcementsId}/{announcementsSlug?}', 'AnnouncementsController@announcementsDetailPage')->name('announcements_detail');

		//Payment
		Route::get('/payment/payment_product_detail', 'PaymentController@paymentProductDetail')->name('payment_product_detail');
		Route::get('/payment/payment_payment_detail', 'PaymentController@paymentPaymentDetail')->name('payment_payment_detail');
		Route::get('/payment/payment_complete', 'PaymentController@paymentComplete')->name('payment_complete');

		// Partners
		Route::get('/partners/{partnerId}/{partnerSlug?}/profile', 'PartnerController@earnPartnerProfilePage')->name('partner_profile');
		Route::get('/partners/{partnerId}/{partnerSlug}/offer/{offerId}', 'PartnerController@earnPartnerOfferPage')->name('partner_offer');
		Route::get('/partners/{partnerId}/{partnerSlug?}', 'PartnerController@earnPartnerDetailPage')->name('partner_detail');
		
	    //Memberships
		Route::get('/member/register', 'MemberController@register');
		Route::get('/member/profile', 'MemberController@profile');
		Route::get('/member/survey_panelist', 'MemberController@surveyPanelistPage');
		
		Route::get('/shop', 'MemberController@shopOnline')->name('shoponline_list');
		Route::get('/shop/{key}', 'MemberController@shopOnlineFilter')->name('shoponline_filter');
		Route::get('/shop-redirect', 'MemberController@shopOnlineRedirect')->name('shoponline_redirect');

		Route::get('/earn_bpoints', 'MemberController@earnPoints')->name('earn_bpoints');
		Route::get('/redeem_bpoints', 'MemberController@redeemPoints')->name('redeem_bpoints');
		Route::get('/membership', 'MemberController@membershipPage')->name('membership');

		// All following routes require login and already registered
		Route::group(['middleware' => ['auth']], function() {
			Route::get('/member/profile', 'MemberController@profile')->name('member_profile');
			Route::post('/member/set_primary_card', 'MemberController@setPrimaryCard')->name('set_primary_card');
			Route::post('/member/virtual_card_register', 'MemberController@virtualCardRegister')->name('register_virtual_card');
			Route::post('/member/update_contact', 'MemberController@updateContact')->name('update_contact');
			Route::get('/member/points_history', 'MemberController@pointsHistroy');

			//Convert points API
			Route::post('/exchange', 'MemberController@pointsConvert');

			// Card related APIs
			Route::post('/card/link', 'CardController@linkCard');
			Route::match(['get', 'post'], '/member/network_of_partner', 'MemberController@networkOfPartner')->name('network_of_partner');
		});
	});

	
	
	// All following routes require only logged in user, but not registered SSO.
	Route::group(['middleware' => ['auth']], function() {
		Route::any('/member/verify_mobile', 'MemberController@verifyMobile')->name('member_verify_mobile');
		Route::any('/member/card_register', 'MemberController@cardRegister')->name('member_register_sso');
	});

	Route::match(['get', 'post'], '/support', 'HomeController@supportPage')->name('support');

	//Dummy test page
	if(config('app.env') <> 'production') {
		Route::get('/payment/dummy','PaymentController@dummy' );
	}

	Route::get('/payment/summary','PaymentController@order');
	Route::any('/payment/capture','PaymentController@capture' );

	//Convert points API
	Route::post('/exchange', 'MemberController@pointsConvert');

});

//Route::get('/home', 'HomeController@index')->name('home');

//Auth and Socialites login
Route::group(['namespace' => 'Auth'], function () 
{
	Route::get('/auth/{provider}', 'AuthController@redirectToProvider');
	Route::get('/auth/{provider}/callback', 'AuthController@handleProviderCallback');

	Route::get('/logout', 'AuthController@logout');
	Route::post('/member_login','AuthController@memberLogin');
	Route::post('/member_register', 'AuthController@memberRegister');
	// Route::post('/member_register', function(){ dd("TESYSETSETSE"); });

	Route::get('/account/activation/{user_id}/{activation_code}', 'AuthController@memberActivation');

	Route::post('/test_form', 'TestFormController@test');
});

// uMobile and old campaigns
Route::group(['namespace' => 'Legacy'], function () 
{
	Route::any('/umobile/{filename}', 'UmobileController@all')->where('filename', '.*');

	// Insurance pages
	Route::any('/insurance-auto.php', 'InsuranceAutoController@index')->name('insurance_auto_index');
	Route::any('/insurance-fraudulent-cover.php', 'InsuranceFraudulentController@index')->name('insurance_fraudulent_index');
	Route::any('/insurance-travel.php', 'InsuranceTravelController@index')->name('insurance_travel');
	Route::any('/insurance.php', 'InsuranceController@index')->name('insurance_index');

	// Shoponline SG
	Route::any('/shop-online-sg.php', 'ShoponlineController@index')->name('shoponline_sg_index');
	Route::any('/shop-online-sg-check.php', 'ShoponlineController@check')->name('shoponline_sg_check');
	Route::any('/shop-online-sg-redirect.php', 'ShoponlineController@redirect')->name('shoponline_sg_redirect');

	// Insurance microsites (with db too)
	Route::any('/insurance-auto/{filename}', 'InsuranceAutoController@all')->where('filename', '.*')->name('insurance_auto');
	Route::any('/insurance-fraudulent-cover/{filename}', 'InsuranceFraudulentController@all')->where('filename', '.*')->name('insurance_fraudulent');
});

// To remove the following after test
if (App::environment(['local', 'development', 'staging'])) {
	Route::get('/ilyas', 'Web\IlyasController@testilyas');
	Route::get('/testmylms', 'Web\IlyasController@testmylms');
}
