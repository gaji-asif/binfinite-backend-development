<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// Currently used by nova for populating outlets based on selected merchant
Route::get('/merchant/{merchant_id}/outlets', function (Request $request) {
	if ($request->merchant_id) {
		$outlets = \App\MerchantOutlet::where('merchant_id', $request->merchant_id)->get();

		if ($outlets) {
			return $outlets->map(function($outlet) {
				return ['value' => $outlet->id, 'display' => $outlet->name];
			});
		}
	}
	
	return [];
});