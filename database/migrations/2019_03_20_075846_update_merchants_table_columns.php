<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMerchantsTableColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('merchants', function (Blueprint $table) {
            $table->renameColumn('full_or_partial_redemption', 'full_or_partial_redemption_html');
            $table->renameColumn('description', 'description_html');
            $table->renameColumn('content', 'about_html');
            $table->renameColumn('points_earning', 'points_earning_html');
            $table->string('profile_banner_url', 250)->nullable();
            $table->string('profile_footer_banner_url', 250)->nullable();
            $table->text('terms_html')->nullable();
            //$table->string()
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('merchants', function (Blueprint $table) {
            $table->renameColumn('full_or_partial_redemption_html', 'full_or_partial_redemption');
            $table->renameColumn('description_html', 'description');
            $table->renameColumn('about_html', 'content');
            $table->renameColumn('points_earning_html', 'points_earning');
            $table->dropColumn('profile_banner_url');
            $table->dropColumn('profile_footer_banner_url');
            $table->dropColumn('terms_html');
        });
    }
}
