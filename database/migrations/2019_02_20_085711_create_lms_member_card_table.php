<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLmsMemberCardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lms_member_cards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('card_no');
            $table->string('card_pin');
            $table->string('status');

            // Data from LMS
            $table->string('lms_CardTypeName'); // 50 length
            $table->string('lms_CardImage'); // 50 length
            $table->string('lms_Status'); // 10 length
            $table->string('lms_PrincipalPoint'); // 10 length
            $table->string('lms_TokenPoint'); // 10 length
            $table->string('lms_MinToken'); // 10 length
            $table->string('lms_TotalPoint'); // 10 length
            $table->string('lms_CreateDate'); // yyyy-MM-ddThh:mm:ss GMT+8 // 19 length
            $table->string('lms_ActivationDate')->nullable(); // yyyy-MM-ddThh:mm:ss GMT+8 // 19 length

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lms_member_cards');
    }
}
