<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeEmailActivationCreatedOnToCreatedAt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('email_activation', function (Blueprint $table) {
            $table->dropColumn('created_on');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('email_activation', function (Blueprint $table) {
            $table->dropColumn('created_at');
            $table->timestamp('created_on')->default(\DB::raw('CURRENT_TIMESTAMP'));
        });
    }
}
