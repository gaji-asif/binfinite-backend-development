<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablesForAigFraudulentCampaign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aig_fraudulent_admins', function (Blueprint $table) {
            $table->increments('admin_id');
            $table->string('username');
            $table->text('password');
            $table->string('admin_fname');
            $table->string('admin_lname');
            $table->integer('active')->default(1);
            $table->text('session_id');
        });

        Schema::create('aig_fraudulent_members', function (Blueprint $table) {
            $table->increments('member_id');
            $table->string('name');
            $table->string('ic', 20);
            $table->string('email', 50);
            $table->string('mobile', 11);
            $table->string('bcard', 16);
            $table->string('voucher', 255);
            $table->date('date_created');
        });

        Schema::create('aig_fraudulent_vouchers', function (Blueprint $table) {
            $table->increments('voucher_id');
            $table->string('voucher_code', 255);
            $table->integer('voucher_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aig_fraudulent_admins');
        Schema::dropIfExists('aig_fraudulent_members');
        Schema::dropIfExists('aig_fraudulent_vouchers');
    }
}
