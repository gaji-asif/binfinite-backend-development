<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMerchantsTableAddPointsEarning extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('merchants', function (Blueprint $table) {
            $table->string('points_earning_listing')->nullable();
            $table->string('points_earning_profile')->nullable();
            $table->string('points_redeem_profile')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('merchants', function (Blueprint $table) {
            $table->dropColumn('points_earning_listing');
            $table->dropColumn('points_earning_profile');
            $table->dropColumn('points_redeem_profile');
        });
    }
}
