<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RevampLmsMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lms_members', function (Blueprint $table) {
            $table->dropColumn('card_no');
            $table->dropColumn('lms_Title');
            $table->dropColumn('lms_FullName');
            $table->dropColumn('lms_IC');
            $table->dropColumn('lms_BirthDate');
            $table->dropColumn('lms_Gender');
            $table->dropColumn('lms_Race');
            $table->dropColumn('lms_Nationality');
            $table->dropColumn('lms_MaritalStatus');
            $table->dropColumn('lms_OwnCar');
            $table->dropColumn('lms_OwnCreditCard');
            $table->dropColumn('lms_BalancePoint');
            $table->dropColumn('lms_HomeAddress1');
            $table->dropColumn('lms_HomeAddress2');
            $table->dropColumn('lms_HomeAddress3');
            $table->dropColumn('lms_HomeCity');
            $table->dropColumn('lms_HomeState');
            $table->dropColumn('lms_HomeCountry');
            $table->dropColumn('lms_HomeZip');
            $table->dropColumn('lms_HomePhone');
            $table->dropColumn('lms_HomeEmail');
            $table->dropColumn('lms_MobilePhone');
            $table->dropColumn('lms_OfficeAddress1');
            $table->dropColumn('lms_OfficeAddress2');
            $table->dropColumn('lms_OfficeAddress3');
            $table->dropColumn('lms_OfficeCity');
            $table->dropColumn('lms_OfficeState');
            $table->dropColumn('lms_OfficeCountry');
            $table->dropColumn('lms_OfficeZip');
            $table->dropColumn('lms_OfficeEmail');
            $table->dropColumn('lms_OfficePhone');
            $table->dropColumn('lms_OfficeExt');
            $table->dropColumn('lms_OfficeFax');
            $table->dropColumn('lms_MSISDN');
            $table->dropColumn('lms_CreateDate');
            $table->dropColumn('lms_ActivationDate');

            $table->string('lms_CreatedDateTime', 255)->nullable();
            $table->string('lms_Email', 255)->nullable();
            $table->string('lms_MemberID', 255)->nullable();
            $table->string('lms_Mobile', 255)->nullable();
            $table->string('lms_Name', 255)->nullable();
            $table->string('lms_RegisteredDateTime', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lms_members', function (Blueprint $table) {
            $table->dropColumn('lms_CreatedDateTime');
            $table->dropColumn('lms_Email');
            $table->dropColumn('lms_MemberID');
            $table->dropColumn('lms_Mobile');
            $table->dropColumn('lms_Name');
            $table->dropColumn('lms_RegisteredDateTime');

            $table->string('card_no');
            $table->string('lms_Title')->nullable(); // 50 length
            $table->string('lms_FullName'); // 60 length
            $table->string('lms_IC'); // 14 length
            $table->string('lms_BirthDate')->nullable(); // yyyy-MM-dd, GMT+8 // 10 length
            $table->string('lms_Gender')->nullable(); // nullable length
            $table->string('lms_Race')->nullable(); // 30 length
            $table->string('lms_Nationality')->nullable(); // 41 length
            $table->string('lms_MaritalStatus')->nullable(); // 50 length
            $table->string('lms_OwnCar')->nullable(); // 1 length
            $table->string('lms_OwnCreditCard')->nullable(); // 1 length
            $table->string('lms_BalancePoint'); // 10 length
            $table->string('lms_HomeAddress1')->nullable(); // 100 length
            $table->string('lms_HomeAddress2')->nullable(); // 100 length
            $table->string('lms_HomeAddress3')->nullable(); // 100 length
            $table->string('lms_HomeCity')->nullable(); // 50 length
            $table->string('lms_HomeState')->nullable(); // 50 length
            $table->string('lms_HomeCountry')->nullable(); // 41 length
            $table->string('lms_HomeZip')->nullable(); // 11 length
            $table->string('lms_HomePhone')->nullable(); // 11 length
            $table->string('lms_HomeEmail')->nullable(); // 50 length
            $table->string('lms_MobilePhone')->nullable(); // 11 length
            $table->string('lms_OfficeAddress1')->nullable(); // 100 length
            $table->string('lms_OfficeAddress2')->nullable(); // 100 length
            $table->string('lms_OfficeAddress3')->nullable(); // 100 length
            $table->string('lms_OfficeCity')->nullable(); // 50 length
            $table->string('lms_OfficeState')->nullable(); // 50 length
            $table->string('lms_OfficeCountry')->nullable(); // 41 length
            $table->string('lms_OfficeZip')->nullable(); // 11 length
            $table->string('lms_OfficeEmail')->nullable(); // 50 length
            $table->string('lms_OfficePhone')->nullable(); // 11 length
            $table->string('lms_OfficeExt')->nullable(); // 5 length
            $table->string('lms_OfficeFax')->nullable(); // 50 length
            $table->string('lms_MSISDN'); // 12 length
            $table->string('lms_CreateDate'); // yyyy-MM-ddThh:mm:ss // 19 length
            $table->string('lms_ActivationDate')->nullable(); // 19 length
        });
    }
}
