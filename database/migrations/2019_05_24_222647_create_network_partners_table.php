<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNetworkPartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('network_partners', function (Blueprint $table) {
            $table->increments('id');
            $table->string('business_name')->nullable();
            $table->string('website')->nullable();
            $table->string('brand_name')->nullable();
            $table->string('brand_website')->nullable();
            $table->string('brand_category')->nullable();
            $table->string('contact_name')->nullable();
            $table->string('office_contact')->nullable();
            $table->string('mobile_contact')->nullable();
            $table->string('contact_regards')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('network_partners');
    }
}
