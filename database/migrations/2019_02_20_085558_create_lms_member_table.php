<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLmsMemberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lms_members', function (Blueprint $table) {
            $table->increments('id');
            $table->string('card_no');
            $table->integer('user_id')->unsigned()->nullable();

            // Data from LMS
            $table->string('lms_Title')->nullable(); // 50 length
            $table->string('lms_FullName'); // 60 length
            $table->string('lms_IC'); // 14 length
            $table->string('lms_BirthDate')->nullable(); // yyyy-MM-dd, GMT+8 // 10 length
            $table->string('lms_Gender')->nullable(); // nullable length
            $table->string('lms_Race')->nullable(); // 30 length
            $table->string('lms_Nationality')->nullable(); // 41 length
            $table->string('lms_MaritalStatus')->nullable(); // 50 length
            $table->string('lms_OwnCar')->nullable(); // 1 length
            $table->string('lms_OwnCreditCard')->nullable(); // 1 length
            $table->string('lms_BalancePoint'); // 10 length
            $table->string('lms_HomeAddress1')->nullable(); // 100 length
            $table->string('lms_HomeAddress2')->nullable(); // 100 length
            $table->string('lms_HomeAddress3')->nullable(); // 100 length
            $table->string('lms_HomeCity')->nullable(); // 50 length
            $table->string('lms_HomeState')->nullable(); // 50 length
            $table->string('lms_HomeCountry')->nullable(); // 41 length
            $table->string('lms_HomeZip')->nullable(); // 11 length
            $table->string('lms_HomePhone')->nullable(); // 11 length
            $table->string('lms_HomeEmail')->nullable(); // 50 length
            $table->string('lms_MobilePhone')->nullable(); // 11 length
            $table->string('lms_OfficeAddress1')->nullable(); // 100 length
            $table->string('lms_OfficeAddress2')->nullable(); // 100 length
            $table->string('lms_OfficeAddress3')->nullable(); // 100 length
            $table->string('lms_OfficeCity')->nullable(); // 50 length
            $table->string('lms_OfficeState')->nullable(); // 50 length
            $table->string('lms_OfficeCountry')->nullable(); // 41 length
            $table->string('lms_OfficeZip')->nullable(); // 11 length
            $table->string('lms_OfficeEmail')->nullable(); // 50 length
            $table->string('lms_OfficePhone')->nullable(); // 11 length
            $table->string('lms_OfficeExt')->nullable(); // 5 length
            $table->string('lms_OfficeFax')->nullable(); // 50 length
            $table->string('lms_MSISDN'); // 12 length
            $table->string('lms_CreateDate'); // yyyy-MM-ddThh:mm:ss // 19 length
            $table->string('lms_ActivationDate')->nullable(); // 19 length

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lms_members');
    }
}
