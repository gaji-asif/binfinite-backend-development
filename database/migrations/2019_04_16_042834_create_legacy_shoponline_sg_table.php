<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLegacyShoponlineSgTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('legacy_shoponline_sg', function (Blueprint $table) {
            $table->increments('estore_id');
            $table->string('estore_name', 255);
            $table->string('estore_image', 255);
            $table->string('estore_link', 255);
            $table->string('estore_desc', 255);
            $table->string('estore_category', 255);
            $table->string('status', 1);
            $table->integer('display'); // 1 = Desktop, 2 = ios, 3 = android, 4 = all, 5 = all mobile
            $table->integer('feature'); // 0 = no, 1 = yes
            $table->integer('message')->nullable(); // 1 = for mobile platform only message
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('legacy_shoponline_sg');
    }
}
