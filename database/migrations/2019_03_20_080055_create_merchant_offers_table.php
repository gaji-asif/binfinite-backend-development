<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMerchantOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchant_offers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('thumbnail_image_url')->nullable();
            $table->string('offer_text')->nullable();
            $table->string('offer_subtext')->nullable();
            $table->string('offer_banner_url')->nullable();
            $table->string('conversion_text')->nullable();
            $table->text('terms_html')->nullable();
            $table->integer('merchant_id')->unsigned();
            $table->date('expire_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchant_offers');
    }
}
