<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id');
            $table->string('invoice_no');
            $table->string('full_name');
            $table->string('address')->nullable();
            $table->decimal('amount',11,2);
            $table->decimal('net_amount',11,2);
            $table->decimal('sst_amount',11,2)->nullable();
            $table->string('currency',20)->default('MYR');
            $table->enum('status', ['pending', 'failed', 'completed'])->default('pending');
            $table->string('payment_method')->default('cash');
            $table->string('category')->nullable();
            $table->string('refund_amount')->nullable();
            $table->decimal('refund_ref',11,2)->nullable();
            $table->date('refund_date',11,2)->nullable();
            $table->string('response_code')->nullable();
            $table->string('response_message')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->index(['user_id'], 'idxKey01');
            $table->index(['invoice_no',], 'idxKey02');
            $table->index(['invoice_no','user_id'], 'idxKey03');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
