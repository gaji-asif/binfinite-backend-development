<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RevampLmsMemberCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lms_member_cards', function (Blueprint $table) {
            $table->dropColumn('card_pin');
            $table->dropColumn('status');
            $table->dropColumn('lms_CardTypeName');
            $table->dropColumn('lms_CardImage');
            $table->dropColumn('lms_Status');
            $table->dropColumn('lms_PrincipalPoint');
            $table->dropColumn('lms_TokenPoint');
            $table->dropColumn('lms_MinToken');  
            $table->dropColumn('lms_TotalPoint');
            $table->dropColumn('lms_CreateDate');
            $table->dropColumn('lms_ActivationDate');

            $table->string('lms_BalancePoint', 255);
            $table->string('lms_TotalTokenPoint', 255);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lms_member_cards', function (Blueprint $table) {
            $table->dropColumn('lms_BalancePoint');
            $table->dropColumn('lms_TotalTokenPoint');

            // Data from LMS
            $table->string('lms_CardTypeName'); // 50 length
            $table->string('lms_CardImage'); // 50 length
            $table->string('lms_Status'); // 10 length
            $table->string('lms_PrincipalPoint'); // 10 length
            $table->string('lms_TokenPoint'); // 10 length
            $table->string('lms_MinToken'); // 10 length
            $table->string('lms_TotalPoint'); // 10 length
            $table->string('lms_CreateDate'); // yyyy-MM-ddThh:mm:ss GMT+8 // 19 length
            $table->string('lms_ActivationDate')->nullable(); // yyyy-MM-ddThh:mm:ss GMT+8 // 19 length
            $table->string('card_pin');
            $table->string('status');
        });
    }
}
