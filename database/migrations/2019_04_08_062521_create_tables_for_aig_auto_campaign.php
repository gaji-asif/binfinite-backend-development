<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablesForAigAutoCampaign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aig_auto_members', function (Blueprint $table) {

// member_id   int(11) Auto Increment   
// name    varchar(50)  
// bcard   bigint(20)   
// ic  bigint(20)   
// exp_date    varchar(50)  
// car_reg_no  varchar(50)  
// mobile  int(11)  
// email   varchar(50)  
// date_created    datetime




            $table->increments('member_id');
            $table->string('name');
            $table->string('bcard', 16);
            $table->string('ic', 20);
            $table->string('email', 50);
            $table->string('mobile', 11);
            $table->string('exp_date');
            $table->string('car_reg_no');
            $table->datetime('date_created');
        });

        Schema::create('aig_auto_admins', function (Blueprint $table) {
//             admin_id    int(11) Auto Increment   
// username    varchar(50)  
// password    text     
// admin_fname varchar(50)  
// admin_lname varchar(50)  
// active  int(11) 1=no 2=yes
// session_id  text
            $table->increments('admin_id');
            $table->string('username');
            $table->text('password');
            $table->string('admin_fname');
            $table->string('admin_lname');
            $table->integer('active');
            $table->text('session_id');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aig_auto_members');
        Schema::dropIfExists('aig_auto_admins');
    }
}
