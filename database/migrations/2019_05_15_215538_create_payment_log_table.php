<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_log', function (Blueprint $table) {
            $table->increments('id');
            $table->string('invoice_no');
            $table->string('masked_account_number')->nullable();
            $table->decimal('amount',11,2)->nullable();
            $table->string('currency',20)->nullabe();
            $table->string('transaction_state')->nullable();
            $table->string('status_code_1')->nullable();
            $table->string('status_code_2')->nullable();
            $table->string('status_code_3')->nullable();
            $table->text('status_description_1')->nullable();
            $table->text('status_description_2')->nullable();
            $table->text('status_description_3')->nullable();
            $table->datetime('completion_time_stamp')->nullable();
            $table->string('source')->default('wirecard');
            $table->softDeletes();
            $table->timestamps();
            $table->index(['invoice_no',], 'idxKey01');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_log');
    }
}
