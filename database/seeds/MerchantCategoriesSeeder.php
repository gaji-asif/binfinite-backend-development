<?php

use Illuminate\Database\Seeder;
use App\Category;

class MerchantCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cats = ['banking-finance' => 'Banking & Finance',
		'e-commerce' => 'E-Commerce',
		'education' => 'Education',
		'fb' => 'F&B',
		'health-beauty' => 'Health & Beauty',
		'leisure-recreation' => 'Leisure & Recreation',
		'motor-fuel' => 'Motor & Fuel',
		'others' => 'Others',
		'retail' => 'Retail',
		'utilities' => 'Utilities'];

		foreach ($cats as $slug => $cat) {
			$c = new Category();
			$c->name = $cat;
			$c->slug = $slug;
			$c->description = $cat;
			$c->save();
		}
    }
}
