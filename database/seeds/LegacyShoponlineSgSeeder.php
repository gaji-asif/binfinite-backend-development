<?php

use Illuminate\Database\Seeder;

class LegacyShoponlineSgSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// estore_id,estore_name,estore_image,estore_link,estore_desc,estore_category,status,display,feature,message
        $data = ['128,Bus Online Ticket,busonlineticket.png,407,$1 = 1 BPoint,travels,1,4,0',
				'129,Catch That Bus,catch_that-bus.png,549,$1 = 1 BPoint,travels,1,4,0',
				'130,DoubleWoot,double_woot.png,597,$1 = 1 BPoint,fashion,1,4,0',
				'131,The Tinsel Rack,the_tinsel_rack.jpg,925,$1 = 1 BPoint,fashion,1,4,0',
				'156,Shopee Web,shopee.png,100376,$1 = 1 BPoint (1%),estore,1,4,0',
				'133,Kinokuniya SG,kinokuniya_sg.jpg,1046,$1 = 1 BPoint,lifestyle,1,4,0',
				'134,Zalora SG,zalora_sg.png,1210,$1 = 1 BPoint,fashion,1,4,0',
				'135,Stylodeco SG,stylodeco_sg.jpg,2080,$1 = 1 BPoint,lifestyle,1,4,0',
				'136,Hermo SG,hermo_sg.png,100081,$1 = 1 BPoint,beauty,1,4,0',
				'137,Red Hotel,red_hotel.jpg,100248,,travels,,4,0',
				'138,Roaming Man SG,roamingman.jpg,,,travels,,4,0',
				'139,Sonno SG,sonno_sg.png,100305,$1 = 1 BPoint,lifestyle,1,4,0',
				'140,Malaysia Airlines,malaysia_airlines.jpg,1086,$1 = 1 BPoint,travels,1,4,0',
				'141,Redmart (SG),,,,eshop,,4,0',
				'142,Harvey Norman (SG),harvey_norman_sg.jpg,,,lifestyle,,4,0',
				'143,PanelPlace SG,panelplace_international.png,100309,Sign up and Earn 50 BBPoints,lifestyle,1,4,0',
				'144,Airpaz,airpaz.png,703,$1 = 1 BPoint,travels,1,4,0',
				'145,MrLens,mrlens.jpg,100059,$1 = 1 BPoint,lifestyle,1,4,0',
				'146,Jeoel Jewellery (MY),jeoel_jewellery_my.png,100021,$1 = 1 BPoint,fashion,1,4,0',
				'147,Kei Mag (MY),kei_mag_my.png,164,$1 = 1 BPoint,fashion,1,4,0',
				'148,Happy Bunch (SG),happy_bunch_sg.png,100080,$1 = 1 BPoint,lifestyle,1,4,0',
				'149,Hotels.com (APAC),hotels_com_apac.png,100170,$1 = 1 BPoint,travels,1,4,0',
				'150,Ohvola (SG),ohvola_sg.jpg,2022,$1 = 1 BPoint,fashion,1,4,0',
				'151,Myprotein APAC,myprotein-apac.png,,50 BPoints per transaction,lifestyle,,4,0',
				'152,Vidi (Touristly),vidi_touristly.png,375,$1 = 1 BPoint,travels,1,4,0',
				'153,Printcious (SG),printcious_sg.png,705,$1 = 1 BPoint,lifestyle,1,4,0',
				'154,Kaodim (SG),kaodim_sg.png,1896,$1 = 1 BPoint,estore,1,4,0',
				'155,KKDay.com - Asia,kkday_com_asia.png,100335,$1 = 1 BPoint,travels,1,4,0',
				'157,Zilingo,zilingo.png,1232,$1 = 1 BPoint (1%),estore,1,4,0',
				'158,Asos,asos.jpg,100109,$1 = 1 BPoint (1%),fashion,1,4,0',];

		foreach($data as $shop) {
			list($estore_id, 
				$estore_name, 
				$estore_image, 
				$estore_link, 
				$estore_desc, 
				$estore_category, 
				$status, 
				$display, 
				$feature) = explode(',', $shop);

			DB::table('legacy_shoponline_sg')->insert([
				'estore_name' => $estore_name,
				'estore_image' => $estore_image,
				'estore_link' => $estore_link,
				'estore_desc' => $estore_desc,
				'estore_category' => $estore_category,
				'status' => $status,
				'display' => $display,
				'feature' => $feature
	        ]);	
		}
    }
}
