<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	if(config('app.env') != 'production') {
    		DB::table('users')->insert([
	            'name' => 'Demo Account',
                'display_name' => 'Demo Account',
	            'email' => 'demo@binfinite.com',
	            'password' => bcrypt('Abcd123$'),
	            'status' => 'Active'
	        ]);
    	}
    }
}
