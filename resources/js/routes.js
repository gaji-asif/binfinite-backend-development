var baseRoute = '/'

var childRoutes = {
	home: '',
	link_bcard: 'card/link',
	member_update_contact: 'member/update_contact',
	member_profile: 'member/profile',
};

var routes = [];
for (var key in childRoutes) {
	routes[key] = baseRoute + childRoutes[key];
}

export default {
	...routes
}
