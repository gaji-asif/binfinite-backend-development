// /**
//  * First we will load all of this project's JavaScript dependencies which
//  * includes Vue and other libraries. It is a great starting point when
//  * building robust, powerful web applications using Vue and Laravel.
//  */

window.Vue = require('vue');

import { attachVueComponent, getUrlParameter } from './utils/utils';

// *
//  * The following block of code may be used to automatically register your
//  * Vue components. It will recursively scan this directory for the Vue
//  * components and automatically register them with their "basename".
//  *
//  * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>

// // const files = require.context('./', true, /\.vue$/i)
// // files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))


// /**
//  * Next, we will create a fresh Vue application instance and attach it to
//  * the page. Then, you may begin adding components to this application
//  * or customize the JavaScript scaffolding to fit your unique needs.
//  */

// const app = new Vue({
//     el: '#app_body'
// });


// Setup vue
const EventBus = new Vue();
const AsyncPost = require('./plugins/vue.asyncpost.js').default;

Vue.use(AsyncPost);

var routes = require('./routes').default;
Object.defineProperties(Vue.prototype, {
	$bus: {
		get: function () {
			return EventBus
		}
	},
	$redirect: {
		get: function () {
			return (link) => window.top.location.href = link;
		}
	},
	$routes: {
		get: function (r) {
			return routes;
		},
		set: function (r) {
			return routes[r];
		}
	}
});

// Attach all components to #id selector
var AddCardDialogComponent = Vue.component('add-card-dialog-component', require('./components/AddCardDialogComponent.vue').default);
var UpdateContactDetailsComponent = Vue.component('update-contact-details-component', require('./components/UpdateContactDetails.vue').default);

attachVueComponent(AddCardDialogComponent, '#vue_add_card_dialog');
attachVueComponent(UpdateContactDetailsComponent, '#vue_update_contact_details');

require('./plugins/jquery.faded_slick.js');
require('./plugins/jquery.faded_slick_alt.js');
require('./plugins/jquery.redeemables_slider.js');

// Check login modal
if (getUrlParameter('showlogin')) {
	$('#loginModal').modal('show');
}