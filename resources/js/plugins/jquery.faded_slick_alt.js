// Requires jQuery and slick library to be loaded first!

(function ($) {

    $.fn.fadedSlick4 = function () {
        // Function to set opacity class
        var renderOpacity = (slick, nextSlide) => {
            var slidesShown = slick.options.slidesToShow;
            if (slick.activeBreakpoint && slick.breakpointSettings[slick.activeBreakpoint].slidesToShow) {
                slidesShown = slick.breakpointSettings[slick.activeBreakpoint].slidesToShow;
            }

            // Current slide is always at the center
            var slideCount = slick.slideCount;
            var slidesQuery = [];

            // Reset all slides first
            this.find('.slick-slide').removeClass('no-opacity');

            if (slidesShown == 1) {
                this.find('.slick-slide[data-slick-index=' + nextSlide + ']').addClass('no-opacity');
            } else {
                // More than 1 slide to show, base it on the center
                for (var i = -2; i <= -3 + slidesShown; i++) {
                    slidesQuery.push('.slick-slide[data-slick-index=' + (nextSlide + i) + ']');
                }
                // Add no-opacity to active ones
                this.find(slidesQuery.join(',')).addClass('no-opacity');
            }
        }

        var totalSlides = this.children('.item').length;
        var initialSlide = 0;

        if (totalSlides >= 4) {
            initialSlide = 1;
        }

        if (totalSlides >= 5) {
            initialSlide = 2;
        }

        this.on('init', (event, slick) => {
            renderOpacity(slick, initialSlide);
        }).on('reInit', (event, slick) => {
            renderOpacity(slick, initialSlide);
        }).on('destroy', (event, slick) => {
            this.removeClass('faded-slick-4');
        });

        this.addClass('faded-slick-4');
        this.slick({
            infinite: true,
            speed: 1000,
            slidesToShow: 4,
            initialSlide: initialSlide,
            centerMode: true,
            // variableWidth: true,
            slidesToScroll: 1,
            autoplay: true,
            centerPadding: '408px',
            responsive: [{
                    breakpoint: 1800,
                    settings: {
                        centerPadding: '408px',
                    }
                },
                {
                    breakpoint: 1360,
                    settings: {
                        centerPadding: '150px',
                    }
                }, {
                    breakpoint: 1270,
                    settings: {
                        centerPadding: '90px',
                    }
                },
                {
                    breakpoint: 1050,
                    settings: {
                        centerPadding: '50px',
                    }
                },
                {
                    breakpoint: 990,
                    settings: {
                        centerPadding: '90px',
                        slidesToShow: 2,
                    }
                },
                {
                    breakpoint: 792,
                    settings: {
                        centerPadding: '50px',
                        slidesToShow: 2,
                    }
                },
                {
                    breakpoint: 663,
                    settings: {
                        centerPadding: '80px',
                        slidesToShow: 1,
                    }
                },
                {
                    breakpoint: 505,
                    settings: {
                        centerPadding: '40px',
                        slidesToShow: 1,
                    }
                }
            ]
        }).on('beforeChange', (event, slick, currentSlide, nextSlide) => {
            renderOpacity(slick, nextSlide);
        })
    }
}(jQuery));
