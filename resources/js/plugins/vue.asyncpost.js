export default {
	install: (Vue, options) => {
		// Setup csrf token
		$.ajaxSetup({
		    headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    }
		})
		Vue.prototype.$asyncPost = async (url, data) => {
			try {
				return await $.post(url, $.param(data)).promise();
			} catch (e) {
				Swal.fire({
			      type: 'error',
			      title: 'Oops...',
			      text: e.responseJSON.message
			    });

				if (e.responseJSON) {
					return e.responseJSON;
				}
				
				return e;
			}
		}
	}
}