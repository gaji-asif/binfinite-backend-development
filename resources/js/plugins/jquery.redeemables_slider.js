// Requires jQuery and slick library to be loaded first!

(function($) {

	$.fn.redeemablesSlider = function(prevBtn, nextBtn, pageCounter) {

		var $prevBtn = $(prevBtn);
		var $nextBtn = $(nextBtn);
		var $pageCounter = $(pageCounter);

		var totalSlides = this.children('.item').length;
		var slidesPerPage = 3;

		var getCountText = (currentSlide) => {
			return Math.ceil(currentSlide / slidesPerPage) + ' of ' + totalSlides / slidesPerPage;
		}

		var refreshSlidesShown = (slick) => {
			if (slick.activeBreakpoint && slick.breakpointSettings[slick.activeBreakpoint].slidesToShow) {
				slidesPerPage = slick.breakpointSettings[slick.activeBreakpoint].slidesToShow;
			}
			$pageCounter.text(getCountText(slick.currentSlide + 1));
		}

		this.on('init', (event, slick) => {
			refreshSlidesShown(slick);
		}).on('reInit', (event, slick) => {
			refreshSlidesShown(slick);
		}).on('breakpoint', (event, slick, breakpoint) => {
			refreshSlidesShown(slick);
		}).on('destroy', (event, slick) => {
			$prevBtn.off('click');
			$nextBtn.off('click');
		});

		var instance = this.slick({
			infinite: false,
			speed: 600,
			slidesToShow: slidesPerPage,
			slidesToScroll: slidesPerPage,
			responsive: [
				{
					breakpoint: 1172,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2,
					}
				},
				{
					breakpoint: 620,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
					}
				},
			]
		}).on('beforeChange', (event, slick, currentSlide, nextSlide) => {
			$pageCounter.text(getCountText(nextSlide + 1));
		});

		$prevBtn.on('click', () => {
			instance.slick('slickPrev');
		});

		$nextBtn.on('click', () => {
			instance.slick('slickNext');
		});

	}
}(jQuery));
