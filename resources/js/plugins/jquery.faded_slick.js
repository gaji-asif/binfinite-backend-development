// Requires jQuery and slick library to be loaded first!

(function($) {
    $.fn.fadedSlick = function() {
        
        // Support multiple elements
        if (this.length && this.length > 1 && this.get) {
            $.each(this.get(), (v, i) => {
                console.log(i);
                $(i).fadedSlick();
            });

            return;
        }

        // Function to set opacity class
        var renderOpacity = (slick, nextSlide) => {
            var slidesShown = slick.options.slidesToShow;
            if (
                slick.activeBreakpoint &&
                slick.breakpointSettings[slick.activeBreakpoint].slidesToShow
            ) {
                slidesShown =
                    slick.breakpointSettings[slick.activeBreakpoint]
                        .slidesToShow;
            }

            // Current slide is always at the center
            var slideCount = slick.slideCount;
            var slidesQuery = [];

            // Reset all slides first
            this.find(".slick-slide").removeClass("no-opacity");

            if (slidesShown == 1) {
                this.find(
                    ".slick-slide[data-slick-index=" + nextSlide + "]"
                ).addClass("no-opacity");
            } else {
                // More than 1 slide to show, base it on the center
                for (var i = -1; i <= -2 + slidesShown; i++) {
                    slidesQuery.push(
                        ".slick-slide[data-slick-index=" + (nextSlide + i) + "]"
                    );
                }
                // Add no-opacity to active ones
                this.find(slidesQuery.join(",")).addClass("no-opacity");
            }
        };

        var totalSlides = this.children(".item").length;
        var initialSlide = 0;

        if (totalSlides >= 4) {
            initialSlide = 1;
        }

        if (totalSlides >= 5) {
            initialSlide = 2;
        }

        this.on("init", (event, slick) => {
            renderOpacity(slick, initialSlide);
        })
            .on("reInit", (event, slick) => {
                renderOpacity(slick, initialSlide);
            })
            .on("destroy", (event, slick) => {
                this.removeClass("faded-slick");
            });

        this.addClass("faded-slick");
        this.slick({
            infinite: true,
            speed: 1000,
            slidesToShow: 3,
            initialSlide: initialSlide,
            centerMode: true,
            variableWidth: true,
            slidesToScroll: 1,
            autoplay: true,
            centerPadding: "375.150px",
            responsive: [
                {
                    breakpoint: 1800,
                    settings: {
                        centerPadding: "375.150px"
                    }
                },
                {
                    breakpoint: 1360,
                    settings: {
                        centerPadding: "150px"
                    }
                },
                {
                    breakpoint: 1270,
                    settings: {
                        centerPadding: "90px"
                    }
                },
                {
                    breakpoint: 1050,
                    settings: {
                        centerPadding: "50px"
                    }
                },
                {
                    breakpoint: 990,
                    settings: {
                        centerPadding: "90px",
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 792,
                    settings: {
                        centerPadding: "50px",
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 663,
                    settings: {
                        centerPadding: "80px",
                        slidesToShow: 1
                    }
                },
                {
                    breakpoint: 505,
                    settings: {
                        centerPadding: "40px",
                        slidesToShow: 1
                    }
                }
            ]
        }).on("beforeChange", (event, slick, currentSlide, nextSlide) => {
            renderOpacity(slick, nextSlide);
        });

        // Old code for not "centerMode"
        // // Only if theres 5 shown?
        // if (slidesShown == 5) {
        //   // Real slides, excluding 2 dummies
        //   var slideCount = slick.slideCount;

        //   // Offset for nearing the edges
        //   var offset = 0;

        //   $('.slick-slide').removeClass('no-opacity');

        //   var slidesQuery = [];

        //   if (nextSlide + 3 > slideCount) {
        //       offset = nextSlide + 3 - slideCount;
        //   }
        //   console.log('offset', offset);
        //   for (var i = 0 - offset; i <= 2 - offset; i++) {
        //       slidesQuery.push('.slick-slide[data-slick-index=' + (nextSlide+i) + ']');
        //   }

        //   // Add no-opacity to classes
        //   $(slidesQuery.join(',')).addClass('no-opacity');

        // }
    };
})(jQuery);
