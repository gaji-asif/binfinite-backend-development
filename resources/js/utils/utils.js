// Gets GET url parameter
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};

// Attach vue component to #id for better browser support
var attachVueComponent = (component, el) => {
    // If element doesnt exist silently bail
    if ($(el).length < 1) { return }

    var components = {}
    components[component.options.name] = component;
    
    return new Vue({
        el,
        components,
        render: function(createElement) {
            // Pass attributes from root node as prop
            var attributes = this.$el.attributes;
            var props = {};
            for (var i = 0; i < attributes.length; i++) {
                props[attributes[i].name] = attributes[i].value;
            }

            // Create element
            return createElement(component, {
                props
            });
        }
    });
}

export { getUrlParameter, attachVueComponent }