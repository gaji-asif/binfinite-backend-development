<div class="item">
    @if(isset($item['redeemPoint']) && isset($item['isEnoughPoint']))
    <div class="ribbon clearfix">
        <div class="ribbon-amount">{!! $item['redeemPoint'] !!} BPOINTS</div>
        @if ($item['isEnoughPoint'])
        <div class="ribbon-qualify"><i class="fas fa-check"></i>&ensp;ENOUGH
            POINTS</div>
        @endif
    </div>
    @endif
    <div class="overlay"></div>
    @isset($item['ribbon'])
    <div class="ribbon">
        <div class="ribbon-text">{!! $item['ribbon'] !!}</div>
    </div>
    @endisset
    <h3 style="{{ BH::backgroundImage($item['image']) }}"></h3>
    <div class="text">
        @isset($item['company'])
        <span class="company">{!! $item['company'] !!}</span>
        @endisset
        @if(isset($item['isRedeemItem']))
        <div class="point-and-redeem">
            @if (isset($item['url']))
                <a href="{{ $item['url'] }}"><h2 class="title">{!! $item['title'] !!}</h2></a>
            @else
                <h2 class="title">{!! $item['title'] !!}</h2>
            @endif
            <a href="" class="btn-pink redeem">
                @if (isset($item['actionTitle']))
                {!! $item['actionTitle'] !!}
                @else
                REDEEM
                @endif
            </a>
        </div>
        <div class="clear"></div>
        @else
        @if (isset($item['url']))
            <a href="{{ $item['url'] }}"><h2 class="title">{!! $item['title'] !!}</h2></a>
        @else
            <h2 class="title">{!! $item['title'] !!}</h2>
        @endif
        <div class="point-and-redeem">
            <span class="point">
                @isset($item['point'])
                {!! $item['point'] !!}
                @endisset
                <br />
                Valid until {!! $item['validDate'] !!}
            </span>
            @if (Route::current()->uri() == 'earn_bpoints')
            <a href="{{ $item['url'] }}" class="btn-pink redeem">
                @if (isset($item['actionTitle']))
                {!! $item['actionTitle'] !!}
                @else
                REDEEM
                @endif
            </a>
            @else
            <a href="{{ route('payment_product_detail') }}" class="btn-pink redeem">
                @if (isset($item['actionTitle']))
                {!! $item['actionTitle'] !!}
                @else
                REDEEM
                @endif
            </a>
            @endif
        </div>
        @endif
    </div>
</div>
