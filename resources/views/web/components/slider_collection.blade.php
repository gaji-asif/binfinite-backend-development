<div class="{{ $key }}">
    <span class="centered-title">{!! $collection['name'] !!}</span>
    <div class="slider faded-slick responsive">
        @if (Route::current()->uri() == 'shop')
        @foreach($collection['items'] as $key => $item)
        @include('web.components.special_deal_shop_item', $item)
        @endforeach
        @foreach($collection['items'] as $key => $item)
        @include('web.components.special_deal_shop_item', $item)
        @endforeach
        @else
        @foreach($collection['items'] as $key => $item)
        @include('web.components.slider_item', $item)
        @endforeach
        @foreach($collection['items'] as $key => $item)
        @include('web.components.slider_item', $item)
        @endforeach
        @endif
    </div>
</div>
