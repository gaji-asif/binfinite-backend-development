<section class="collection-grid-4">
    <div class="header row align-items-end justify-content-between pt-5">
        <span>{!! $collection['name'] !!}</span>
        @if (isset($isEnableViewAll) && $isEnableViewAll)
        <a class="btn btn-outline-blue" href="#">VIEW ALL</a>
        @endif
    </div>

    <div class="parent">
        @foreach($collection['items'] as $key => $item)
        @include('web.components.partner_item', $item)
        @endforeach
        @foreach($collection['items'] as $key => $item)
        @include('web.components.partner_item', $item)
        @endforeach
        @foreach($collection['items'] as $key => $item)
        @include('web.components.partner_item', $item)
        @endforeach
        @foreach($collection['items'] as $key => $item)
        @include('web.components.partner_item', $item)
        @endforeach
    </div>
</section>
