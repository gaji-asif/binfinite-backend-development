<div class="item">
    <a href="">
        @isset($item['ribbon'])
        <div class="ribbon">{!! $item['ribbon'] !!}</div>
        @endisset
        <div class="overlay"></div>
        <h3 style="{{ BH::backgroundImage($item['image']) }}"></h3>
        <div class="text">
            <h2 class="title">{!! $item['title'] !!}</h2>
            <div class="description">{!! $item['description'] !!}</div>
            <div class="points">
                <span class="small-text">BPTS</span>
                <span class="point-amount">{!! $item['points'] !!}</span>
            </div>
        </div>
    </a>
</div>
