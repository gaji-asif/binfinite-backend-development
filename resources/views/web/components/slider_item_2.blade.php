<div class="item">
    <div class="overlay"></div>
    <h3 style="{{ BH::backgroundImage($item['image']) }}"></h3>
    <div class="text my-2">
        <h2 class="title-flex">{!! $item['title'] !!}</h2>
        <div class="point-and-redeem">
            <span class="point">
                @isset($item['bio_html'])
                {!! $item['bio_html'] !!}
                @endisset
                <br />
            </span>
        </div>
    </div>
</div>
