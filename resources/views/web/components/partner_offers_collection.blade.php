@if (isset($collection['isSliderCollection']) && $collection['isSliderCollection'])
<div class="container">
    <div class="header row align-items-end justify-content-between mb-0">
        <span>{!! $collection['name'] !!}</span>
        @if (isset($isEnableViewAll) && $isEnableViewAll)
        <a class="btn btn-outline-blue" href="#">VIEW ALL</a>
        @endif
    </div>
</div>
<div class="container-fluid pb-5">
    <div class="{{ $key }}">
        <div class="slider faded-slick responsive">
            @foreach($collection['items'] as $key => $item)
            @include('web.components.slider_item', $item)
            @endforeach
            @foreach($collection['items'] as $key => $item)
            @include('web.components.slider_item', $item)
            @endforeach
        </div>
    </div>
</div>
@else
<div class="container">
    @if (isset($isShowViewAll))
    <span class="centered-title">{!! str_replace("{company}", $collection['company'], $collection['name']) !!}</span>
    @else
    <div class="header row align-items-end justify-content-between">
        @if (isset($totalPoints))
        <span>{!! str_replace("{points}", $totalPoints, $collection['name']) !!}</span>
        @else
        <span>{!! str_replace("{points}", "20,123", $collection['name']) !!}</span>
        @endif
        @if (isset($isEnableViewAll) && $isEnableViewAll)
        <a class="btn btn-outline-blue" href="#">VIEW ALL</a>
        @endif
        @endif
    </div>
    <section class="collection-grid-3">
        <div class="parent">
            @switch($key)
            @case('outlets')
            @foreach($collection['items'] as $key => $item)
            @include('web.components.slider_item_2', $item)
            @endforeach
            @break

            @default
            @foreach($collection['items'] as $key => $item)
            @include('web.components.slider_item', $item)
            @endforeach

            @endswitch
        </div>
    </section>

    @isset($collection['isMoreButtonUrl'])
    <div class="button-container text-center">
        <a class="btn btn-blue" href="{!! $collection['isMoreButtonUrl'] !!}">MORE ABOUT {!!
            $collection['company']
            !!}</a>
    </div>
    @endisset
</div>
@endif
