<div class="earn-brand">
    <a href="/partners/{!! $item['partner'] !!}">
        <div class="ribbon clearfix">
            <div class="ribbon-terms">{!! $item['point'] !!}</div>
        </div>
        <div class="overlay"></div>
        <h3 style="{{ BH::backgroundImage($item['image']) }}"></h3>
        @if (isset($item['actionTitle']) && isset($item['actionUrl']))
        <div class="button-container text-center my-0 pb-3 white">
            <a class="btn btn-outline-blue btn-rounded" href="{!! $item['actionUrl'] !!}" data-toggle="modal"
                data-target="#convertBPointsModal"
                onclick="setupConversionForm('{{ $item['partner'] }}', '{{ $item['brandName'] }}', '{{ $item['fieldIdTitle'] }}', '{{ $item['fieldIdName'] }}');return false;">{!!
                $item['actionTitle']
                !!}</a>
        </div>
        @endif
    </a>
</div>
