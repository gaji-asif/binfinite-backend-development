<div class="item">
    <div class="ribbon">{!! $item['ribbon'] !!}</div>
    <h3 style="{{ BH::backgroundImage($item['image']) }}"></h3>
    <div class="text">
        <h2 class="title">{!! $item['title'] !!}</h2>
        <a href="" class="btn-blue collect">{!! $item['actionTitle'] !!}</a>
        <div class="clear"></div>
    </div>
</div>
