<a href="{{ $eachAnnouncementsListing->slugUrl }}" class="item">
    <h3 style="{{ BH::backgroundImage($eachAnnouncementsListing->thumbnail_url) }}"></h3>
    <div class="text">
        <h6 class="date">{{ \Carbon\Carbon::parse($eachAnnouncementsListing->created_at)->format('d F Y')}}</h6>
        <h2 class="title">{!! $eachAnnouncementsListing->announcements_title !!}</h2>
        <div class="clear"></div>
    </div>
</a>
