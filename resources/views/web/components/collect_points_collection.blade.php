<div class="collect-points">
    <span class="centered-title">{!! $collection['name'] !!}</span>
    <div class="parent">
        @foreach($collection['items'] as $key => $item)
        @include('web.components.collect_points_item', $item)
        @endforeach
    </div>
</div>
