<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('custom_meta')

    <title>@yield('head_title','B-Infinite | 2018')</title>
    <!-- Bootstrap core CSS -->
    <link href="//stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//use.fontawesome.com/releases/v5.6.0/css/all.css"
        integrity="sha384-aOkxzJ5uQz7WBObEZcHvV5JvRW3TUc2rNPA7pe3AwnsUohiw1Vj2Rgx2KSOkF5+h" crossorigin="anonymous">

    <link rel="stylesheet" href="/assets/js/nouislider/nouislider.css">

    <link href="/assets/css/main.css" rel="stylesheet">
    <link href="/css/app.css" rel="stylesheet">
    <link href="/assets/css/steve.css" rel="stylesheet">
    <link href="/assets/css/asif.css" rel="stylesheet">

    <link rel="stylesheet" href="/assets/slick/slick.css">
    <link rel="stylesheet" href="/assets/slick/slick-theme.css">

    @yield('custom_css')
</head>

<body>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-81045901-2', 'auto');
        ga('send', 'pageview');

    </script>
    <!-- Google Tag Manager -->
    <script>
        (function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-NJXXSGL');

    </script>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NJXXSGL" height="0" width="0"
            style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <!-- End Google Tag Manager -->
    <script src="/vendor/jquery/jquery.min.js"></script>
    <script src="/assets/slick/slick.min.js"></script>
    <script src="//stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.bundle.min.js"></script>
    <script src="/assets/js/nouislider/nouislider.js"></script>
    <script src="/assets/js/bootstrap-number-input.js"></script>

    @switch(Route::current()->getName())
    @case("payment_payment_detail")
    @case("payment_complete")
    @include('web.include.payment_menu')
    @break
    @default
    @include('web.include.menu')
    @endswitch

    @include('web.include.secondary_navbar')

    @yield('content')

    @include('web.include.footer')

    @include('web.include.login_dialog')



    @include('web.include.nobcard_message')

    @include('web.include.dialog.alert_action_dialog')

    @include('web.include.dialog.alert_message_dialog')

    @include('web.include.dialog.link_card_main_dialog')

    @include('web.include.dialog.link_card_verify_dialog')

    <!-- Bootstrap core JavaScript -->

    @include('web.include.flash_alert')
    <script src="/js/app.js"></script>
    @yield('custom_js')
</body>

</html>
