<section class="secondary-navbar">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <ul>
                    <li><a class="btn-pink btn-shop-online" href="/shop">Shop Online!</a></li>
                    <li><a href="{{ route('earn_bpoints') }}">Earn BPoints</a></li>
                    <li><a href="{{ route('redeem_bpoints') }}">Redeem BPoints</a></li>
                    <li><a href="{{ route('support') }}">Support</a></li>
                    <li><a href="{{ route('announcements') }}">Announcements</a></li>
                    @if (!Auth::check())
                    <li><a href="#">Convert to Bpoints</a></li>
                    @endif
                </ul>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</section>
