@if (Auth::check() && session('no_bcard') && session('no_bcard') == 'true')
<?php $user = Auth::user(); ?>
<div class="modal fade welcome-modal" id="noBcardMessage" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content container">
            <div class="modal-container">
                <a href="javascript:void(0);" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <div class="clear"></div>

                <div class="dialog-body text-center welcome-content">
                    <h1>Welcome {{ $user->lmsName }}!</h1>
                    <p class="small-text">Are you an existing cardholder?</p>

                    <div class="btn-container text-center">
                        <button type="button" class="btn btn-yes btn-pink" data-toggle="modal" data-target="#linkCardModal"
                            data-dismiss="modal">Yes</button>

                        <a class="btn btn-no btn-pink" href="/member/card_register">No, Sign Me Up</a>
                    </div>/
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('#noBcardMessage').modal('show');

</script>
@endif
