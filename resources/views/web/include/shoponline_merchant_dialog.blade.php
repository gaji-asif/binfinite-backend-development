<!-- Modal -->
<div class="modal fade merchant-dialog-modal" id="shoponlineMerchantDialog" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content container">
            <div class="row mx-0">
                <div class="modal-container col-md">
                    <a href="javascript:void(0);" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></a>
                    <div class="clear"></div>

                    <div class="dialog-body">
                        <h1>Please confirm which account you would like to shop with!</h1>

                        <div class="info-container">
                            <div class="form-sec">
                                    <div class="form-group">
                                        <label>USE THIS CARD</label>
                                        <div class="input-group mb-3">
                                            <select class="form-control" id="transferFromCard" name="transferFromCard">
                                                <option value="600000000000" selected>BCard 1234 5678 9012 3456&nbsp;&nbsp;&nbsp;Available
                                                        Points 20,000</option>
                                                <option value="600000000001">BCard 1234 5678 9012 3456&nbsp;&nbsp;&nbsp;Available
                                                        Points 20,000</option>
                                                <option value="600000000003">BCard 1234 5678 9012 3456&nbsp;&nbsp;&nbsp;Available
                                                        Points 20,000</option>
                                            </select>
                                        </div>
                                    </div>

                                    <a href="javascript:void(0);" data-toggle="modal" data-target="#shoponlineMerchantDialogSteps" class="btn btn-pink category-all">CONTINUE TO SHOP</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
