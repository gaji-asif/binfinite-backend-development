<div class="top-picks">
    <span class="top-picks-title">Top Picks</span>
    <div class="slider faded-slick responsive sample">
        @foreach([1,2,3] as $i)
        <div class="item">
            <div class="overlay"></div>
            <h3 style="{{ BH::backgroundImage('8e4b3378e00274c3ddd2fac0129609dd.png') }}"></h3>
            <div class="text">
                <span class="company">Caltex</span>
                <h2 class="title">Example of a deal or promotion</h2>
                <div class="point-and-redeem">
                    <span class="point">
                        RM1 = 1BPoint <br />
                        Valid until 31st December 2018
                    </span>
                    <a href="" class="btn-pink redeem">REDEEM</a>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="overlay"></div>
            <h3 style="{{ BH::backgroundImage('marc-babin-334972-unsplash.png') }}"></h3>
            <div class="text">
                <span class="company">Berjaya GROUP</span>
                <h2 class="title">Complimentary stay when you have a long offer line</h2>
                <div class="point-and-redeem">
                    <span class="point">
                        RM1 = 1BPoint <br />
                        Valid until 31st December 2018
                    </span>
                    <a href="" class="btn-pink redeem">REDEEM</a>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="overlay"></div>
            <div class="ribbon">
                <div class="ribbon-text">5 days left!</div>
            </div>
            <h3 style="{{ BH::backgroundImage('205bbd329dd1fbc81d918d670996c429.png') }}"></h3>
            <div class="text">
                <span class="company">ZALORA</span>
                <h2 class="title">15% OFF</h2>
                <div class="point-and-redeem">
                    <span class="point">
                        RM1 = 1BPoint <br />
                        Valid until 31st December 2018
                    </span>
                    <a href="" class="btn-pink redeem">REDEEM</a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
