<div class="redeem-suggestions">
    <div class="row">
        <div class="suggest-header col-md-3">
            <div class="redeem-suggestions-header">
                @if (Auth::check())
                <span class="redeem-suggestions-title">With your<br>2,000 points<br>you can get these now!</span>
                @else
                <span class="redeem-suggestions-title">Products You May Like To Redeem!</span>
                @endif
                <br>
                <span class="redeem-suggestions-carousel-page"></span>
            </div>

            <!-- Controls -->
            <div class="controls">
                <a class="left fa fa-arrow-left btn btn-slider" href="#carousel-generic" data-slide="prev"></a><a class="right fa fa-arrow-right btn btn-slider"
                    href="#carousel-generic" data-slide="next"></a>
            </div>
        </div>

        <div class="col-md-9">
            <div id="carousel-generic" class="carousel slide hidden-xs" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner redeem">
                    <div class="carousel-item active">
                        <div class="slider">
                            @foreach([1,2,3] as $i)
                            <div class="item">
                                <a class="col-item" href="#">
                                    <h3 class="photo" style="{{ BH::backgroundImage('Product Image.png') }}"></h3>
                                    <div class="info text">
                                        <div class="redeem-text">
                                            <div class="redeem-item">White Brighten SPF</div>
                                            <div class="redeem-company">Bliss Spa</div>
                                            <div class="redeem-point">200 BPoints</div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </a>
                            </div>
                            <div class="item">
                                <a class="col-item" href="#">
                                    <h3 class="photo" style="{{ BH::backgroundImage('Product Image-2.png') }}"></h3>
                                    <div class="info text">
                                        <div class="redeem-text">
                                            <div class="redeem-item">1 Slurpee</div>
                                            <div class="redeem-company">7-Eleven</div>
                                            <div class="redeem-point">2,000 BPoints</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="item">
                                <a class="col-item" href="#">
                                    <h3 class="photo" style="{{ BH::backgroundImage('Product Image-1.png') }}"></h3>
                                    <div class="info text">
                                        <div class="redeem-text">
                                            <div class="redeem-item">2 Tickets</div>
                                            <div class="redeem-company">Berjaya Square Theme Park</div>
                                            <div class="redeem-point">20,000 BPoints</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
