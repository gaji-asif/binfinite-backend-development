<div class="preferred-category">
    <span class="centered-title">Your Preferred Categories</span>

    <div class="row">
        <a href="/shop/beauty" class="col-lg-6 col-md-12">
            <div class="ribbon">HEALTH AND BEAUTY</div>
            <h2 style="{{ BH::backgroundImage('shoponline/beauty.jpg') }}"></h2>
        </a>

        <a href="/shop/fashion" class="col-lg-6 col-md-12">
            <div class="ribbon">FASHION</div>
            <h2 style="{{ BH::backgroundImage('shoponline/fashion.jpg') }}"></h2>
        </a>

        <a href="/shop/estore" class="col-lg-6 col-md-12">
            <div class="ribbon">ESHOP</div>
            <h2 style="{{ BH::backgroundImage('shoponline/eShop.jpg') }}"></h2>
        </a>

        <a href="/shop/lifestyle" class="col-lg-6 col-md-12">
            <div class="ribbon">LIFESTYLE</div>
            <h2 style="{{ BH::backgroundImage('shoponline/lifestyle.jpg') }}"></h2>
        </a>

        <a href="/shop/grocery" class="col-lg-6 col-md-12">
            <div class="ribbon">GROCERY</div>
            <h2 style="{{ BH::backgroundImage('shoponline/grocery.jpg') }}"></h2>
        </a>

        <a href="/shop/travels" class="col-lg-6 col-md-12">
            <div class="ribbon">TRAVEL</div>
            <h2 style="{{ BH::backgroundImage('shoponline/travel.jpg') }}"></h2>
        </a>

        <a class="btn btn-pink category-all" href="/shop">ALL</a>
    </div>

    {{-- @foreach($arrayChunks2 as $items)
    <div class="row">
        @foreach($items as $item)
        <div class="col-lg-6 col-md-12">
            <div class="ribbon">{{ $item }}</div>
            <h2 style="{{ BH::backgroundImage('a.png') }}"></h2>
        </div>
        @endforeach
    </div>
    @endforeach

    @foreach($arrayChunks3 as $items)
    <div class="row">
        @foreach($items as $item)
        <div class="col-lg-4 col-md-12">
            <div class="ribbon">
                <div class="ribbon-text">{{ $item }}</div>
            </div>
            <h3 style="{{ BH::backgroundImage('a.png') }}"></h3>
        </div>
        @endforeach
    </div>
    @endforeach --}}
{{-- 
    <div class="button-container text-center">
        <a class="btn btn-outline-blue" href="/member/card_register">VIEW ALL CATEGORIES</a>
    </div> --}}
</div>
