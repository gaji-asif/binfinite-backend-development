<nav class="primary-navbar navbar navbar-expand-lg bg-pink py-0">
    <div class="container">
        <div class="mx-0">
            <a class="navbar-brand" href="/">
                <img src="/assets/images/hero_logo.png" />
            </a>
        </div>

        <div class="collapse navbar-collapse" id="search">
            <form class="navbar-form" role="search">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <button class="search-dropdown btn btn-secondary btn-sm dropdown-toggle" type="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            All
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another 3action</a>
                            <a class="dropdown-item" href="#">Something else here</a>
                            <div role="separator" class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Separated link</a>
                        </div>
                    </div>

                    <input type="text" class="search-text form-control" placeholder="" name="srch-term" id="srch-term" />
                    <div class="input-group-btn">
                        <button class="btn-search btn btn-default" type="submit">
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>

        <div class="collapse navbar-collapse" id="links">
            <ul class="right-menu country-selector navbar-nav mr-auto">
                <li class="nav-item dropdown active">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        Malaysia
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Singapore</a>
                        <a class="dropdown-item" href="#">Phillipines</a>
                        <a class="dropdown-item" href="#">Vietnam</a>
                        <a class="dropdown-item" href="#">Indonesia</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Membership</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Partner Enquiry</a>
                </li>
            </ul>
        </div>
        <div class="collapse navbar-collapse" id="account">
            <ul class="navbar-nav ml-auto">
                @if (Auth::check())
                <?php $user = Auth::user(); ?>
                <li class="login-profile nav-item">
                    <div class="dropdown show">
                        <a class="nav-link dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            <img class="avatar" src="{{asset('assets/images/Avatar default@2x.png')}}" />
                            <!-- <i class="far fa-user-circle fa-2x" aria-hidden="true"></i> -->
                            <div class="login-profile-text">
                                <span class="greet-name">Hello, {{$user->name ?? 'user'}}</span><br />
                                <span class="greet-points"><b>{{$user->totalBpoints}}</b>
                                    BPoints</span>
                            </div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                            <a class="dropdown-item" href="/member/profile">My Profile</a>
                            <a class="dropdown-item" href="/member/points_history">My Points History</a>
                            <a class="dropdown-item" href="/umobile/login.php">Redeem Talk Time</a>
                            <a class="dropdown-item" href="/insurance.php">Insurance</a>
                            <a class="dropdown-item" href="/member/survey_panelist">Survey Panelist</a>
                            <a class="dropdown-item" href="/logout">Log Out</a>
                        </div>
                    </div>
                </li>
                @else
                <li class="login-link nav-item">
                    <a class="nav-link" href="javascript:void(0);" data-toggle="modal" data-target="#loginModal"><i
                            class="fas fa-user"></i>&nbsp;Sign In</a>
                </li>
                @endif
            </ul>
        </div>
    </div>
</nav>
