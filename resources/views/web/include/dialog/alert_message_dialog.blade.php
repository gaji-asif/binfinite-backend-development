@if (Session::has('success_title') || Session::has('error_title'))
<!-- Modal -->
<div id="alertMessageModal" class="modal fade alert-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content container">
            <div class="row mx-0">
                <div class="modal-container col-md" style="margin-bottom: 60px;">
                    <a href="javascript:void(0);" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></a>
                    <div class="clear"></div>

                    <div class="dialog-body text-center">
                        @if (Session::has('success_title'))
                        <h1>{{ Session::get('success_title') }}</h1>
                        @else
                        <h1>{{ Session::get('error_title') }}</h1>
                        @endif
                        <p class="small-text" style="padding-left: 17%; padding-right: 17%;">
                            {{ Session::get('description_message') }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('#alertMessageModal').modal('show');

</script>
@endif
