<!-- Modal -->
<div class="modal fade change-mobile-email-modal" id="changeMobileEmailModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content container">
            <div class="row mx-0">
                <div class="modal-container col-md">
                    <a href="javascript:void(0);" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></a>
                    <div class="clear"></div>

                    <div id="vue_update_contact_details" data-mobile="{{ $mobile }}" data-email="{{ $email }}"></div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
