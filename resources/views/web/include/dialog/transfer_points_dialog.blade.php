<!-- Modal -->
<div class="modal fade transfer-points-modal" id="transferPointsModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content container">
            <div class="row mx-0">
                <div class="modal-container col-md">
                    <a href="javascript:void(0);" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></a>
                    <div class="clear"></div>

                    <div class="dialog-body">
                        <h1>Transfer Points</h1>

                        <div class="info-container">
                            <div class="form-sec">
                                <form name="qryform" id="qryform" method="POST" action="/points_transfer">
                                    @csrf
                                    <div class="form-group">
                                        <label>TRANSFER FROM</label>
                                        <div class="input-group mb-3">
                                            <select class="form-control" id="transferFromCard" name="state">
                                                <option selected>BCard 1234 5678 9012 3456<span class="balance-text">Available
                                                        Points 20,000</span></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>TRANSFER TO</label>
                                        <div class="input-group mb-3">
                                            <select class="form-control" id="transferToCard" name="state">
                                                <option selected>BCard 1234 5678 9012 3456<span class="balance-text">Available
                                                        Points 20,000</span></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>POINTS TRANSFER AMOUNT</label>
                                        <input type="text" class="form-control col-md-4" id="transferAmount"
                                            placeholder="" name="postcode" value="10,000" />
                                    </div>
                                    <button type="submit" class="btn btn-blue">
                                        TRANSFER
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
