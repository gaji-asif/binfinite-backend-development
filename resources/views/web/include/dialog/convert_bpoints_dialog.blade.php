<!-- Modal -->
<?php $user = Auth::user(); ?>
<div class="modal fade convert-bpoints-modal" id="convertBPointsModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content container">
            <div class="row mx-0">
                <div class="modal-container col-md">
                    <a href="javascript:void(0);" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></a>
                    <div class="clear"></div>

                    <div class="dialog-body">
                        <h1>Convert BPoints to <brandName></brandName> Points</h1>

                        <div class="info-container">
                            <div class="form-sec">
                                <form name="convert-form" id="convert-form" method="POST" action="{{url('/exchange')}}">
                                    @csrf
                                    <input type="hidden" id="partner" name="partner">
                                    <input type="hidden" id="partner_name" name="partner_name">
                                    <div class="form-group">
                                        <label>USE THIS CARD</label>
                                        <div class="input-group mb-3">
                                            <select class="form-control" id="binf_number" name="binf_number">
                                                <option selected>BCard 1234 5678 9012 3456<span
                                                        class="balance-text">Available
                                                        Points 20,000</span></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>NAME ON BCARD</label>
                                        <input type="text" class="form-control" id="binf_name" name="binf_name"
                                            value="{{$user->name}}" />
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label>CONTACT NUMBER</label>
                                            <input type="number" class="form-control" id="mobile_number"
                                                name="mobile_number" value="" />
                                        </div>
                                        <div class="form-group col-md-8">
                                            <label>EMAIL</label>
                                            <input type="email" class="form-control" id="email" placeholder=""
                                                name="email" value="{{$user->email}}" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label id="memberUID">memberUID</label>
                                        <input type="text" class="form-control" id="member_uid_number"
                                            placeholder="Insert here..." name="member_uid_number" />
                                    </div>
                                    <div class="form-group">
                                        <label>POINTS TO CONVERT (must be in blocks of 100)</label>
                                        <input type="number" class="form-control col-md-4" id="point_convert_amount"
                                            placeholder="Insert points..." name="point_convert_amount" value="10,000" />
                                    </div>
                                    <button type="submit" class="btn btn-blue">
                                        CONTINUE TO CONVERT
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
