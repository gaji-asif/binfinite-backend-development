<!-- Modal -->
<div id="linkCardVerifyModal" class="modal fade alert-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content container">
            <div class="row mx-0">
                <div class="modal-container col-md">
                    <a href="javascript:void(0);" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></a>
                    <div class="clear"></div>

                    <div class="dialog-body text-center">
                        <h1>Your PIN has been sent to your mobile number ending with 4075</h1>

                        <div class="info-container">
                            <button type="submit" class="btn btn-pink">
                                PROCEED
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
