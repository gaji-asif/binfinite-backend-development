<!-- Modal -->
<div class="modal fade set-primary-card-modal" id="setPrimaryCardModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content container">
            <div class="row mx-0">
                <div class="modal-container col-md">
                    <a href="javascript:void(0);" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></a>
                    <div class="clear"></div>

                    <div class="dialog-body">
                        <h1>Set Another Card As Primary</h1>

                        <div class="info-container">
                            <div class="form-sec">
                                <form name="qryform" id="qryform" method="post" action="{{ route('set_primary_card') }}">
                                    @csrf
                                    <div class="form-group">
                                        <label>SELECT CARD</label>
                                        <div class="input-group mb-3">
                                            <select class="form-control" name="primary_card_no" id="primary_card_no">
                                                @foreach($cards as $card)
                                                <option {{ $card->is_primary ? 'selected' : '' }}>{{ $card->card_no }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-blue">
                                        Submit
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
