<script src="//cdn.jsdelivr.net/npm/sweetalert2@8"></script>

@if(session('error'))
<script>
   Swal.fire({
      type: 'error',
      title: 'Oops...',
      text: '{{session('error')}}'
    })
</script>
@endif

@if(session('success'))
<script>
   Swal.fire({
      type: 'success',
      title: 'Success',
      text: '{{session('success')}}'
    })
</script>
@endif