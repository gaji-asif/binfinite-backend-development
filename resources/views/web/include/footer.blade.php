<footer>
    <div class="container">
        <div class="row">
            <div class="brand-logo col-md-3">
                <img src="/assets/images/footer_logo.png" />
            </div>
            <div class="col-md-3">
                <div class="site-title">Company</div>
                <ul class="site-map">
                    <li>
                        <a href="#">Company Overview</a>
                    </li>
                    <li>
                        <a href="#">Brand Story</a>
                    </li>
                    <li>
                        <a href="{{ route('support') }}">FAQ</a>
                    </li>
                    <li>
                        <a href="#">Work with us</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3">
                <div class="site-title">Highlights</div>
                <ul class="site-map">
                    <li>
                        <a href="#">Latest announcements</a>
                    </li>
                    <li>
                        <a href="#">Free BPoints - Be A Survey Panelist</a>
                    </li>
                    <li>
                        <a href="#">B Infinite Benefits and Features</a>
                    </li>
                    <li>
                        <a href="#">Cash Conversion</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3">
                <a class="btn btn-pink partner-login" href="#">Partner Login</a>
                <div class="partner-text">
                    Want to be a merchant partner? <br />
                    Apply here!
                </div>
            </div>
        </div>

        <hr />

        <div class="row">
            <div class="col-md-3">
                <p class="mobile-app-banner"><img src="{{ asset('assets/images/ios-256x256.png') }}" /></p>
                <p class="mobile-app-banner"><img src="{{ asset('assets/images/google-256x256.png') }}" /></p>
            </div>
            <div class="col-md-3">
                <div class="site-title">Contact Us</div>
            </div>
            <div class="col-md-3">
                <div class="site-title">B Infinite network</div>
                <ul class="site-map">
                    <li>
                        <a href="#">Singapore</a>
                    </li>
                    <li>
                        <a href="#">Phillipines</a>
                    </li>
                    <li>
                        <a href="#">Vietnam</a>
                    </li>
                    <li>
                        <a href="#">Indonesia</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3">
                <div class="site-title">Visit Us</div>
                <ul class="social-media">
                    <li><a href="#"><i class="fab fa-lg fa-facebook-f"></i></a></li>
                    <li><a href="#"><i class="fab fa-lg fa-instagram"></i></a></li>
                    <li><a href="#"><i class="fab fa-lg fa-pinterest"></i></a></li>
                    <li><a href="#"><i class="fab fa-lg fa-twitter"></i></a></li>
                </ul>
                <div class="clear"></div>
            </div>
        </div>

        <div class="row copyright">
            <div class="col-md-9">
                <ul>
                    <li>&copy; 2018 BLoyalty Sdn Bhd. All right reserved</li>
                    <li><a href="/terms">Terms and Conditions</a></li>
                    <li><a href="/privacy">Privacy Policy</a></li>
                </ul>
            </div>
            <div style="text-align: right;" class="col-md-3">Web2Square</div>
        </div>
    </div>
</footer>
