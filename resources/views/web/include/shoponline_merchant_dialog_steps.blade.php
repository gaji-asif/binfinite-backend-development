<!-- Modal -->
<div class="modal fade merchant-dialog-steps-modal" id="shoponlineMerchantDialogSteps" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content container">
            <div class="row mx-0">
                <div class="modal-container col-md">
                    <a href="javascript:void(0);" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></a>
                    <div class="clear"></div>

                    <div class="dialog-body">
                        <h1>Few important things to take note before shopping </h1>
                        <h4>In order for you to EARN BPoints</h4>

                        <div class="info-container row">
                            <div class="col-lg-4">
                                <img src="/images/merchant/step1.png">
                                Stay within the same website link to complete your purchase
                            </div>
                            <div class="col-lg-4">
                                <img src="/images/merchant/step2.png">
                                Ensure that you have unblocked your browser cookies before clicking into any merchant link
                            </div>
                            <div class="col-lg-4">
                                <img src="/images/merchant/step3.png">
                                Should there be any error, return back to the Shop Online! main page
                            </div>
                            <form name="qryform" id="qryform" method="GET" action="{{ route('shoponline_redirect') }}">
                                
                                <input type="hidden" class="hidden shopID" name="shopID"></input>
                                <input type="hidden" class="hidden cardID" name="cardID"></input>
                                <button type="submit" class="btn btn-pink btn-continue">Click here to continue</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
