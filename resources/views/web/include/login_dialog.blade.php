<!-- Modal -->
<div class="modal fade login-modal" id="loginModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content container">
            <div class="row">
                <div class="modal-container login-left col-md">
                    <a href="javascript:void(0);" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></a>
                    <div class="clear"></div>
                    <div class="login-social-media">
                        <h1 class="welcome">Welcome back!</h1>
                        <p class="small-text">Your rewards are waiting for you</p>
                        <hr />
                        {{-- <p>
                            <a href="{{ url('/auth/facebook') }}" class="btn btn-facebook btn-lg"><i class="fab fa-facebook-f"></i><span>Sign
                                    in with facebook</span></a>
                            <div class="clear"></div>
                        </p>
                        <p>
                            <a href="{{ url('/auth/google') }}" class="btn btn-google btn-lg"><i class="fab fa-google"></i><span>Sign
                                    in with google</span></a>
                            <div class="clear"></div>
                        </p> --}}
                    </div>
                    <form class="login-form" method="POST" action="{{url('/member_login')}}">
                        @csrf
                        <div style="margin-top:30px" class="form-group">
                            <input type="email" class="form-control" name="email" id="loginEmail" placeholder="Your email"
                                required autofocus>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="password" id="loginPassword" placeholder="Your password"
                                required>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-pink btn-lg login-btn">Sign in</button>
                        </div>
                        <div class="form-group">
                            <span class="signup-text">Not a member? </span><a class="signup-btn" href="/member/register">Sign
                                Up</a>
                        </div>
                    </form>
                </div>
                <div class="login-right col-md-auto">
                    <h3 style="{{ BH::backgroundImage('Welcome-Image.jpg', '/images/WebSliders') }}"></h3>
                    <!-- <img src="https://via.placeholder.com/510x610?text=Gucci Image" /> -->
                </div>
            </div>
        </div>
    </div>
</div>
