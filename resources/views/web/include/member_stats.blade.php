<section class="hero-bottom-navbar">
    <div class="container">
        @if (Auth::check())
        <div class="card-text">
            <div class="menber-stats row">
                <div class="col-md-2">
                    <span>
                        Quick Monthly Snapshot
                    </span>
                </div>
                <div class="col-md-2 text-center">
                    <span class="light-text">
                        You have
                    </span>
                    <br>
                    <span class="bold-text">
                        20,000
                    </span>
                    <br>
                    <span class="small-text">
                        BPoints
                    </span>
                </div>
                <div class="col-md-2 text-center">
                    <span class="light-text">
                        Redeemable for
                    </span>
                    <br>
                    <span class="bold-text">
                        $200
                    </span>
                    <br>
                    <span class="small-text">
                        B Dollars
                    </span>
                </div>
                <div class="col-md-2 text-center">
                    <span class="light-text">
                        You used your BCard
                    </span>
                    <br>
                    <span class="bold-text">
                        40
                    </span>
                    <br>
                    <span class="small-text">
                        times
                    </span>
                </div>
                <div class="col-md-2 text-center">
                    <span class="light-text">
                        You collected points at
                    </span>
                    <br>
                    <span class="bold-text">
                        5
                    </span>
                    <br>
                    <span class="small-text">
                        B Infinite partners
                    </span>
                </div>
                <div class="col-md-2 text-center">
                    <span class="light-text">
                        You redeemed
                    </span>
                    <br>
                    <span class="bold-text">
                        5,000
                    </span>
                    <br>
                    <span class="small-text">
                        points
                    </span>
                </div>
            </div>
        </div>
        @else
        <div class="b-card">
            <div class="card-img" style="{{ BH::backgroundImage('card.PNG') }}"></div>
            <div class="card-text justify-content-between d-flex align-items-center">
                <div class="pr-1"><span>Collect BPoints and use as cash to make payment!</span></div>
                <div class="flex-no-shrink"><a href="#" class="btn btn-pink btn-round text-right">BE A MEMBER NOW</a></div>
            </div>
        </div>
        @endif
    </div>
</section>
