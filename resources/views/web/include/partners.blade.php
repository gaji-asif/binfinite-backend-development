<div class="partners">
    <span class="title">Partners</span>

    <div class="row">
        <div class="col-md-5">
            <div class="carousel slide media-carousel" id="media" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="row">
                            <div class="col">
                                <div class="partner-left-image" style="{{ BH::backgroundImage('alexa-suter-575932-unsplash.png') }}"></div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="row">
                            <div class="col">
                                <div class="partner-left-image" style="{{ BH::backgroundImage('alexa-suter-575932-unsplash.png') }}"></div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="row">
                            <div class="col">
                                <div class="partner-left-image" style="{{ BH::backgroundImage('alexa-suter-575932-unsplash.png') }}"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <a data-slide="prev" href="#media" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
                <a data-slide="next" href="#media" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>

                <ol class="carousel-indicators">
                    <li data-target="#media" data-slide-to="0" class="active"></li>
                    <li data-target="#media" data-slide-to="1"></li>
                    <li data-target="#media" data-slide-to="2"></li>
                </ol>

            </div>
            <div class="partners-carousel-text">
                <span class="partners-header">Earn 3X BPoints with Jocom Mobile App</span>
            </div>
        </div>
        <div class="col-md-7 pl-5">
            <div class="find-out-more-item">
                <div class="partner-right-image" style="{{ BH::backgroundImage('Group 8533.png') }}">
                    <div class="find-out-more-item-text col-md-8">
                        <span class="partner-right-image-text">Spend at Starbucks and Redeem x VPoints!</span>
                        <br>
                        <a href="" class="btn btn-pink more">FIND OUT MORE</a>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            <div style="margin-top: 40px;" class="find-out-more-item">
                <div class="partner-right-image" style="{{ BH::backgroundImage('Group 8533.png') }}">
                    <div class="find-out-more-item-text col-md-8">
                        <span class="partner-right-image-text">Redeem
                            BPoints on all RayBan glasses</span>
                        <br>
                        <a href="" class="btn btn-pink more">FIND OUT MORE</a>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
