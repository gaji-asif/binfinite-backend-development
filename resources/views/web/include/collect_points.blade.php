<div class="collect-points">
    <span class="centered-title">Get the most out of B Infinite</span>
    <div class="parent">
        <div class="item">
            <div class="ribbon">Fasttrack your BPoints</div>
            <h3 style="{{ BH::backgroundImage('8e4b3378e00274c3ddd2fac0129609dd.png') }}"></h3>
            <div class="text">
                <h2 class="title">Collect TRIPLE POINTS when you fill fuel at Caltex!</h2>
                <a href="" class="btn-blue collect">LEARN MORE</a>
                <div class="clear"></div>
            </div>
        </div>
        <div class="item">
            <div class="ribbon">Collect Bonus Points</div>
            <h3 style="{{ BH::backgroundImage('marc-babin-334972-unsplash.png') }}"></h3>
            <div class="text">
                <h2 class="title">Look out for BONUS POINTS deals all around Tesco, in emails, deals and coupons</h2>
                <a href="" class="btn-blue collect">VIEW ALL DEALS</a>
                <div class="clear"></div>
            </div>
        </div>
        <div class="item">
            <div class="ribbon">Be Rewarded</div>
            <h3 style="{{ BH::backgroundImage('205bbd329dd1fbc81d918d670996c429.png') }}"></h3>
            <div class="text">
                <h2 class="title">Reward your healthy habits, collect and redeem points with our health partners</h2>
                <a href="" class="btn-blue collect">VIEW HEALTH PARTNERS</a>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</div>
