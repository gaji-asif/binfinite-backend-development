@if (Auth::check())
@if (Route::current()->uri() == 'shop')
<section class="hero">
    <div class="hero-image shop" style="{{ BH::backgroundImage('ShopOnline-slider-01.jpg', '/images/WebSliders') }}">
    </div>
    <div class="container">
        @if (Route::current()->uri() == 'shop')
        <div class="col-md-6 px-0">
            <h1 style="color: white;">Spend any amount when you shop online with us and earn 100 BONUS BPoints!</h1>
        </div>
        @else
        <div class="col-md-4 px-0">
            <h1>Get the rewards you want sooner</h1>
            <p>Combine your points with money to redeem</p>
        </div>
        @endif
    </div>
</section>
@elseif (Route::current()->uri() == 'redeem_bpoints')
<section class="hero">
    <div class="hero-image shop" style="{{ BH::backgroundImage('redeem-slider-01.jpg', '/images/WebSliders') }}"></div>
    <div class="container">
        @if (Route::current()->uri() == 'shop')
        <div class="col-md-6 px-0">
            <h1>Spend any amount when you shop online with us and earn 100 BONUS BPoints!</h1>
        </div>
        @else
        <div class="col-md-4 px-0">
            <h1>Get the rewards you want sooner</h1>
            <p>Combine your points with money to redeem</p>
        </div>
        @endif
    </div>
</section>
@elseif (Request::is('partners/*/profile'))
<section class="hero-partner-profile">
    <div class="container5">
        <div id="partner-profile-carousel" class="carousel slide multi-item-carousel" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#partner-profile-carousel" data-slide-to="0" class="active"></li>
                <li data-target="#partner-profile-carousel" data-slide-to="1"></li>
                <li data-target="#partner-profile-carousel" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <img class="item__third" src="{{ asset('images/837dc1c799eebd9356a9760289982dd9.png') }}"
                        alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="item__third" src="{{ asset('images/837dc1c799eebd9356a9760289982dd9.png') }}"
                        alt="Second slide">
                </div>
                <div class="carousel-item">
                    <img class="item__third" src="{{ asset('images/837dc1c799eebd9356a9760289982dd9.png') }}"
                        alt="Third slide">
                </div>
            </div>
            <a class="carousel-control-prev" href="#partner-profile-carousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#partner-profile-carousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</section>
@else
<section class="hero-member">
    <div class="container5">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <!-- <img class="d-block w-100" src="..." alt="First slide"> -->
                    <div class="hero-image"
                        style="{{ BH::backgroundImage('homepage-slider-01.jpg', '/images/WebSliders') }}"></div>
                </div>
                <div class="carousel-item">
                    <!-- <img class="d-block w-100" src="..." alt="Second slide"> -->
                    <div class="hero-image"
                        style="{{ BH::backgroundImage('homepage-slider-02.jpg', '/images/WebSliders') }}"></div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</section>
@endif
@else
<section class="hero">
    <div class="hero-image" style="{{ BH::backgroundImage('1-slide-default.PNG') }}"></div>
    <div class="container">
        <div class="col-md-6 px-0">
            <h1>Become a B Infinite Member today and enjoy infinite rewards!</h1>
            <a href="/member/register" class="btn btn-pink">SIGN UP NOW</a>
        </div>
        <div class="col-md-6"></div>
    </div>
</section>
@endif
