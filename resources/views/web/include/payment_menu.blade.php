<nav class="primary-navbar navbar navbar-expand-lg bg-pink py-0">
    <div class="container">
        <div class="mx-0">
            <a class="navbar-brand" href="/">
                <img src="/assets/images/hero_logo.png" />
            </a>
        </div>

        <div class="collapse navbar-collapse navbar-payment" id="links">
            <ul class="right-menu navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link active" href="#"><img src="/images/8.0 Payment/icon_shipment.png">CART & SHIPPING INFO</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><img src="/images/8.0 Payment/icon_method.png">PAYMENT METHOD</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><img src="/images/8.0 Payment/icon_collection.png">COLLECTION</a>
                </li>
            </ul>
        </div>
        <div class="collapse navbar-collapse" id="account">
            <ul class="navbar-nav ml-auto">
                @if (Auth::check())
                <?php $user = Auth::user(); ?>
                <li class="login-profile nav-item">
                    <div class="dropdown show">
                        <a class="nav-link dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            <img class="avatar" src="{{asset('assets/images/Avatar default@2x.png')}}" />
                            <!-- <i class="far fa-user-circle fa-2x" aria-hidden="true"></i> -->
                            <div class="login-profile-text">
                                <span class="greet-name">Hello, {{$user->name}}</span><br />
                                <span class="greet-points"><b>{{$user->totalBpoints}}</b>
                                    BPoints</span>
                            </div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                            <a class="dropdown-item" href="/member/profile">My Profile</a>
                            <a class="dropdown-item" href="/member/points_history">My Points History</a>
                            <a class="dropdown-item" href="#">Redeem Talk Time</a>
                            <a class="dropdown-item" href="#">Insurance</a>
                            <a class="dropdown-item" href="#">Survey Panelist</a>
                            <a class="dropdown-item" href="/logout">Log Out</a>
                        </div>
                    </div>
                </li>
                @else
                <li class="login-link nav-item">
                    <a class="nav-link" href="javascript:void(0);" data-toggle="modal" data-target="#loginModal"><i
                            class="fas fa-user"></i>&nbsp;Sign In</a>
                </li>
                @endif
            </ul>
        </div>
    </div>
</nav>
