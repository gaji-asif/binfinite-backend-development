@extends('web.master')

@section('content')

<?php 
?>

<section class="announcements">
    <section class="content gray">
        <div class="container py-5">
            <div class="row mx-0">
                <span class="col-lg-8">Latest Announcements</span></span>
            </div>
            <section class="content">
                <div class="container pt-2">
                    <div class="announcements_listing">
                        <div class="parent">
                            @foreach($announcementsListing as $key => $eachAnnouncementsListing)
                                @include('web.components.announcements_item', $eachAnnouncementsListing)
                            @endforeach
                        </div>
                    </div>
                </div>
            </section>
    </section>
</section>

@endsection
