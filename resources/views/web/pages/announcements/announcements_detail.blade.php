@extends('web.master')

@section('content')


<section class="announcements_detail">
    <section class="content gray">
        <div class="container py-5">
            <div class="row mx-0">
                <span class="col-lg-12">{{ $announcements->announcements_title }}</span></span>
            </div>
            <section class="content">
                <div class="container pt-2">
                    {!! $announcements->announcements_html !!}
                </div>
            </section>
    </section>
</section>


@endsection
