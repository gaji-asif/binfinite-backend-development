@extends('web.master')

@section('content')

<?php 
$collections = [
    'earn-points' => [
            'name' => 'With your {points} points you can get these now!',
            'items' => [
                [
                    'redeemPoint' => '10,000',
                    'isEnoughPoint' => true,
                    'image' => '8e4b3378e00274c3ddd2fac0129609dd.png',
                    'company' => 'Caltex',
                    'title' => 'RM50 fuel with 10,000 Points!',
                    'validDate' => '31st December 2018',
                    'url' => '#',
                    'actionTitle' => 'REDEEM',
                ],
                [
                    'redeemPoint' => '19,000',
                    'isEnoughPoint' => true,
                    'image' => 'marc-babin-334972-unsplash.png',
                    'company' => 'Berjaya GROUP',
                    'title' => 'Complimentary Stay when you have a long offer line',
                    'point' => 'RM1 = 1BPoint',
                    'validDate' => '31st December 2018',
                    'url' => '#',
                    'actionTitle' => 'REDEEM',
                ],
                [
                    'redeemPoint' => '5,000',
                    'isEnoughPoint' => true,
                    'image' => '205bbd329dd1fbc81d918d670996c429.png',
                    'company' => 'ZALORA',
                    'title' => 'RM50 Gift Card',
                    'validDate' => '31st December 2018',
                    'url' => '#',
                    'actionTitle' => 'REDEEM',
                ]
            ]
    ],
    'special-deals' => [
            'name' => 'Special Deals',
            'isSliderCollection' => true,
            'items' => [
                [
                    'isRedeemItem' => true,
                    'redeemPoint' => '5,000',
                    'isEnoughPoint' => true,
                    'image' => '7.png',
                    'company' => 'BOOKS',
                    'title' => 'Ready Player One - Ernest Cline',
                    'url' => '#',
                    'actionTitle' => 'REDEEM',
                ],
                [
                    'isRedeemItem' => true,
                    'redeemPoint' => '10,000',
                    'isEnoughPoint' => true,
                    'image' => '7.png',
                    'company' => 'ZALORA',
                    'title' => 'Ray Ban Shield Sunglasses',
                    'url' => '#',
                    'actionTitle' => 'REDEEM',
                ],
                [
                    'isRedeemItem' => true,
                    'redeemPoint' => '30,000',
                    'isEnoughPoint' => false,
                    'image' => '7.png',
                    'company' => 'CHEWIE ONLINE',
                    'title' => 'Darth Vader 1TB Thumb Drive',
                    'url' => '#',
                    'actionTitle' => 'REDEEM',
                ]
            ]
    ],
    'food-beverage' => [
            'name' => 'Food and Beverage',
            'isSliderCollection' => true,
            'items' => [
                [
                    'isRedeemItem' => true,
                    'redeemPoint' => '5,000',
                    'isEnoughPoint' => true,
                    'image' => '7.png',
                    'company' => 'BOOKS',
                    'title' => 'Ready Player One - Ernest Cline',
                    'url' => '#',
                    'actionTitle' => 'REDEEM',
                ],
                [
                    'isRedeemItem' => true,
                    'redeemPoint' => '10,000',
                    'isEnoughPoint' => true,
                    'image' => '7.png',
                    'company' => 'ZALORA',
                    'title' => 'Ray Ban Shield Sunglasses',
                    'url' => '#',
                    'actionTitle' => 'REDEEM',
                ],
                [
                    'isRedeemItem' => true,
                    'redeemPoint' => '30,000',
                    'isEnoughPoint' => false,
                    'image' => '7.png',
                    'company' => 'CHEWIE ONLINE',
                    'title' => 'Darth Vader 1TB Thumb Drive',
                    'url' => '#',
                    'actionTitle' => 'REDEEM',
                ]
            ]
    ]
]
?>

@include('web.include.hero_slider')

<section class="redeem-points">
    <section class="content2 menu">
        <div class="container">
            @include('web.include.category_icon_menu')
        </div>
    </section>

    <section class="content gray pb-5">
        <div class="container py-5">
            @isset($collections['earn-points'])

            @if (Auth::check())
            <?php $user = Auth::user(); ?>
            @include('web.components.partner_offers_collection', ['key' => 'earn-points', 'collection' =>
            $collections['earn-points'], 'totalPoints' => $user->totalBpoints])
            @else
            @include('web.components.partner_offers_collection', ['key' => 'earn-points', 'collection' =>
            $collections['earn-points']])
            @endif

            @endisset
        </div>

        <div class="redeem">
            @isset($collections['special-deals'])
            @include('web.components.partner_offers_collection', ['key' => 'special-deals', 'collection' =>
            $collections['special-deals']])
            @endisset

            @isset($collections['food-beverage'])
            @include('web.components.partner_offers_collection', ['key' => 'food-beverage', 'collection' =>
            $collections['food-beverage']])
            @endisset
        </div>
    </section>
</section>

@endsection @section('custom_js')
<script>
    $(".single-slick").slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        centerMode: true,
        variableWidth: true
    });

    $(".responsive").fadedSlick();

    $(document).ready(function () {
        $("#media").carousel({
            pause: true,
            interval: false
        });
    });

</script>
@endsection
