@extends('web.master')

@section('content')

<?php 
// Following is temporary
/*
$faqs = [
    'general' => [
        'title' => 'General',
        'items' => [
            [
                'question' => 'What is B Infinite?',
                'answer' => 'A lifestyle and consumer-focused reward program designed for members to earn and pay with points at participating partners.',
            ],
            [
                'question' => 'What is BCard?',
                'answer' => 'BCard is a physical or virtual Membership BCard issued by BLoyalty to members who joined our B Infinite reward program.',
            ],
            [
                'question' => 'How do I join B Infinite Membership?',
                'answer' => '<p>Just download the B Infinite Mobile App to obtain a Virtual BCard via</p>
<ul>
<li>Google Play store for Android users or</li>
<li>Apple App store for IOS users</li>
</ul>
<h4>Or simply walk into partner outlets (eg: Caltex, Kenny Rogers Roasters, Tealive, TBM) and apply for a physical co brand BCard.</h4>
<p>For more info; logon to this page <a href="https://www.binfinite.com.my/get-started.php">https://www.binfinite.com.my/get-started.php</a></p>'
            ]
        ]
    ],
    'bpoints' => [
        'title' => 'BPoints',
        'items' => [
            [
                'question' => 'Where can I earn BPoints?',
                'answer' => '<p>You may earn BPoints at over 130 B Infinite participating brands with more than 4,000 outlets nationwide.
                                    Check our listed partners at <a href="https://www.binfinite.com.my/merchants.php">MERCHANTS</a>
                                    and our latest promotion at <a href="https://www.binfinite.com.my/promotions.php">PROMOTIONS</a>
                                    or B Infinite Mobile App.</p>',
            ],
        ]
    ],
    'pin' => [
        'title' => 'PIN',
        'items' => [
            [
                'question' => 'What is purpose of the PIN for my BCard?',
                'answer' => '<p>The BCard PIN is a Personal Identification Number which is required when performing redemption using
                                    BPoints and to also access to your card account profile via our web login at <a href="https://www.binfinite.com.my/login.php">LOGIN</a></p>',
            ],
        ]
    ],

]*/
?>

<section class="support">
    <section class="content gray">
        <div class="container py-5">
            <div class="row mx-0">
                <span class="col-lg-8">Need more help? Check out our Frequently Asked Questions.</span></span>
            </div>
            <div class="row mx-0">
                <div class="col-lg-8">
                    <p>Just in case you had more questions, we’ve got more answers. Other possible description here if
                        needed.</p>
                </div>
            </div>

            <ul class="section-menu nav" role="tablist">
                @foreach($categories as $key => $category)
                <li class="nav-item">
                    <a class="btn btn-outline-blue nav-link {{ $loop->first ? ' show active' : ''}}" id="nav-{{ $key }}-tab"
                        data-toggle="tab" href="#tab-pane-{{ $key }}" role="tab" aria-controls="nav-{{ $key }}"
                        aria-selected="{{ $loop->first ? 'true' : 'false' }}" href="#">{{ $category }}</a>
                </li>
                @endforeach
                {{-- <li>
                    <a class="btn btn-outline-blue" href="#">Card</a>
                </li>
                <li>
                    <a class="btn btn-outline-blue" href="#">Redemption and Cash</a>
                </li>
                <li>
                    <a class="btn btn-outline-blue" href="#">Points</a>
                </li>
                <li>
                    <a class="btn btn-outline-blue" href="#">Security</a>
                </li>
                <li>
                    <a class="btn btn-outline-blue" href="#">Account</a>
                </li>
                <li>
                    <a class="btn btn-outline-blue" href="#">Shipping</a>
                </li> --}}
            </ul>


            <div class="tab-content">
                @foreach($sortedFaqs as $key => $faqs)
                <div class="tab-pane fade{{ $loop->first ? ' show active' : '' }}" id="tab-pane-{{ $faqs[0]->categorySlug }}" role="tabpanel"
                    aria-labelledby="nav-{{ $faqs[0]->categorySlug }}-tab">
                    <div id="accordion" class="support-cards" role="tabpanel" aria-multiselectable="true">

                        @foreach($faqs as $item)
                        <?php $uniqueIdx = $item->categorySlug . '-' . $loop->index ?>
                        <div class="card">
                            <h5 class="card-header" role="tab" id="heading-{{ $uniqueIdx }}">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $uniqueIdx }}"
                                    aria-expanded="false" aria-controls="collapse{{ $uniqueIdx }}" class="collapsed d-block">{{
                                    $item['question'] }}
                                </a>
                            </h5>

                            <div id="collapse{{ $uniqueIdx }}" class="collapse" role="tabpanel" aria-labelledby="heading-{{ $uniqueIdx }}">
                                <div class="card-body">
                                    {!! $item['answer'] !!}
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                @endforeach
            </div>
    </section>

    <section class="content">
        <div class="container py-5">
            <div class="row mx-0">
                <span class="col-lg-8">Contact Us</span></span>
            </div>
            <div class="row mx-0">
                <div class="col-lg-8">
                    <p>Can’t find what you are looking for? Leave us a message and we’ll be in touch</p>
                </div>
            </div>

            <div id="accordion" class="support-cards" role="tablist" aria-multiselectable="true">
                <div class="card">
                    <h5 class="card-header" role="tab" id="headingForm">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseForm" aria-expanded="false"
                            aria-controls="collapseForm" class="collapsed d-block">Or fill in this form below, and
                            we’ll
                            be in touch!
                        </a>
                    </h5>

                    <div id="collapseForm" class="collapse" role="tabpanel" aria-labelledby="headingForm">
                        <div class="card-body">
                            <div class="form-sec">
                                <form name="qryform" id="qryform" method="post">
                                @csrf
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>FULL NAME</label>
                                                <input type="text" class="form-control" id="full_name" name="full_name" required/>
                                            </div>
                                            <div class="form-group">
                                                <label>EMAIL</label>
                                                <input type="email" class="form-control" id="email" placeholder="" name="email" required/>
                                            </div>
                                            <div class="form-group">
                                                <label>MOBILE NUMBER</label>
                                                <input type="text" class="form-control" id="mobile_number" name="mobile_number" />
                                            </div>

                                            <div class="form-group">
                                                <label>REASON</label>
                                                <input type="text" class="form-control" id="reason" name="reason" />
                                            </div>

                                            <div class="form-group">
                                                <label>BINFINITE MEMBERSHIP NUMBER</label>
                                                <input type="text" class="form-control" id="binf_number" name="binf_number" />
                                            </div>
                                        </div>
                                        <div class="col-lg-6  text-right">
                                            <div class="form-group text-left">
                                                <label>DETAILS</label>
                                                <textarea class="form-control" id="details" name="details" rows="10"></textarea>
                                            </div>

                                            {!! NoCaptcha::display() !!}

                                            <button type="submit" class="btn btn-blue">
                                                Submit
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>

@endsection

@section('custom_js')
{!! NoCaptcha::renderJs() !!}
<script>
    $(document).ready(function () {
        $('#qryform').on('submit', function (){
            var rc = grecaptcha.getResponse();
            if(rc.length == 0) {
                Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Please solve the reCAPTCHA'
                });

                return false;
            }
            return true;
        });
    });

</script>
@if($sentContact)
<script>
   Swal.fire({
      type: 'success',
      title: 'Success',
      text: 'You message has been successfully sent.'
    })
</script>
@endif
@endsection