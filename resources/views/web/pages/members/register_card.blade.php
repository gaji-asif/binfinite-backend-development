@extends('web.master')

@section('content')

<section class="card-register">
    <form name="qryform" id="qryform" method="post" action=""
        onsubmit="return(validate());" novalidate="novalidate">
    @csrf
    <section class="content profile gray pt-4 pb-0">
        <div class="container">
            <span class="centered-title heading">First, we need some information…</span>
            <div class="row">
                <div class="col-md section">
                    <div class="info-container">
                        <div class="form-sec">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="required-label">FULL NAME</label>
                                            <input type="text" class="form-control col" id="full_name" placeholder="Fill in your name"
                                                name="full_name" value="{{ old('full_name') }}" required/>
                                        </div>
                                        <div class="form-group">
                                            <label>EMAIL ADDRESS</label>
                                            <input type="email" class="form-control col" id="email" placeholder=""
                                                name="email" value="{{ $user->email }}" disabled/>
                                        </div>
                                        <div class="form-group">
                                            <label>MOBILE NUMBER</label>
                                            <input type="number" class="form-control col" id="mobile_number"
                                                placeholder="Fill in mobile number" name="mobile_number" value="{{ $user->lmsMember->lms_Mobile }}" disabled />
                                        </div>
                                        <div class="form-group">
                                            <label class="required-label">NATIONALITY</label>
                                            <input type="text" class="form-control col" id="nationality" placeholder=""
                                                name="nationality" value="{{ old('nationality') }}" required/>
                                        </div>
                                        <div class="form-group">
                                            <label class="required-label">COUNTRY</label>
                                            <input type="text" class="form-control col" id="country" placeholder=""
                                                name="country" value="{{ old('country') }}" required/>
                                        </div>
                                        <div class="form-group">
                                            <label>IC / PASSPORT</label>
                                            <input type="text" class="form-control col" id="ic_passport" placeholder="Fill in your IC or passport"
                                                name="ic_passport" value="{{ old('ic_passport') }}" />
                                        </div>
                                        <div class="form-group">
                                            <label>DATE OF BIRTH</label>
                                            <input type="text" class="form-control col" id="dob" placeholder="Fill in your date of birth"
                                                name="dob" value="{{ old('dob') }}" />
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <label>UNIT NUMBER</label>
                                                    <input type="text" class="form-control" id="unit_number"
                                                        placeholder="" name="unit_number" value="13-A-09" />
                                                </div>
                                                <div class="col-lg">
                                                    <label>BUILDING NAME</label>
                                                    <input type="text" class="form-control" id="building_name"
                                                        placeholder="" name="building_name" value="Seni Mont Kiara" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>STREET NAME</label>
                                            <input type="email" class="form-control col" id="street_name" placeholder=""
                                                name="street_name" value="Jalan 19/70A" />
                                        </div>
                                        <div class="form-group">
                                            <label>SUBURB/CITY</label>
                                            <input type="text" class="form-control col" id="city" placeholder=""
                                                name="city" value="Desa Sri Hartamas" />
                                        </div>
                                        <div class="form-group">
                                            <label>STATE</label>
                                            <div class="input-group mb-3">
                                                <select class="form-control col" id="state" name="state">
                                                    <option selected>Kuala Lumpur</option>
                                                    <option value="1">One</option>
                                                    <option value="2">Two</option>
                                                    <option value="3">Three</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>POSTCODE</label>
                                            <input type="text" class="form-control col" id="postcode" placeholder=""
                                                name="postcode" value="50480" />
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="content2 profile py-5">
        <div class="container">
            <span class="centered-title">You will receive the following upon registration!</span>
                <div class="row pt-4">
                    <div class="col section mx-auto px-0 my-0 text-center">
                        <div class="d-flex justify-content-center">
                            <div
                                style="{{ BH::backgroundImage('BInfinite-Virtual-BCard.png', '/images/BICoBrandCards') }}"><img
                                    src="/images/BICoBrandCards/BInfinite-Virtual-BCard.png"
                                    style="visibility: hidden;" /></div>
                        </div>
                        <section class="row card-selector cf">
                            {{-- <div class="member-card-col col-sm-6 col-md-4 col-lg-3">
                                <input type="radio" name="card-select" id="inifnite" value="B Infinite B Card"
                                    checked />
                                <label class="inifnite-label four" for="inifnite"
                                    style="{{ BH::backgroundImage('BInfinite-Generic-BCard.png', '/images/BICoBrandCards') }}"><img
                                        src="/images/BICoBrandCards/BInfinite-Generic-BCard.png"
                                        style="visibility: hidden;" /></label>
                            </div>
                            <div class="member-card-col col-sm-6 col-md-4 col-lg-3">
                                <input type="radio" name="card-select" id="inifniteVirtual"
                                    value="B Infinite Virtual B Card" checked />
                                <label class="inifnite-label four" for="inifniteVirtual"
                                    style="{{ BH::backgroundImage('BInfinite-Virtual-BCard.png', '/images/BICoBrandCards') }}"><img
                                        src="/images/BICoBrandCards/BInfinite-Virtual-BCard.png"
                                        style="visibility: hidden;" /></label>
                            </div>
                            <div class="member-card-col col-sm-6 col-md-4 col-lg-3">
                                <input type="radio" name="card-select" id="borders" value="Borders Rewards Card" />
                                <label class="borders-label four" for="borders"
                                    style="{{ BH::backgroundImage('Borders-BCard.png', '/images/BICoBrandCards') }}"><img
                                        src="/images/BICoBrandCards/Borders-BCard.png"
                                        style="visibility: hidden;" /></label>
                            </div>
                            <div class="member-card-col col-sm-6 col-md-4 col-lg-3">
                                <input type="radio" name="card-select" id="caltex" value="Caltex
                                Journey Card" />
                                <label class="caltex-label four" for="caltex"
                                    style="{{ BH::backgroundImage('Caltex-Journer-BCard.png', '/images/BICoBrandCards') }}"><img
                                        src="/images/BICoBrandCards/Caltex-Journer-BCard.png"
                                        style="visibility: hidden;" /></label>
                            </div>
                            <div class="member-card-col col-sm-6 col-md-4 col-lg-3">
                                <input type="radio" name="card-select" id="krr" value="KRR Card" />
                                <label class="krr-label four" for="krr"
                                    style="{{ BH::backgroundImage('Kenny-Rogers-Roasters-BCard.png', '/images/BICoBrandCards') }}"><img
                                        src="/images/BICoBrandCards/Kenny-Rogers-Roasters-BCard.png"
                                        style="visibility: hidden;" /></label>
                            </div>
                            <div class="member-card-col col-sm-6 col-md-4 col-lg-3">
                                <input type="radio" name="card-select" id="pick_a_trip" value="Pick A Trip Card" />
                                <label class="pick_a_trip-label four" for="pick_a_trip"
                                    style="{{ BH::backgroundImage('Pick-A-Trip-BCard.png', '/images/BICoBrandCards') }}"><img
                                        src="/images/BICoBrandCards/Pick-A-Trip-BCard.png"
                                        style="visibility: hidden;" /></label>
                            </div>
                            <div class="member-card-col col-sm-6 col-md-4 col-lg-3">
                                <input type="radio" name="card-select" id="tbm" value="TBM
                                Member Card" />
                                <label class="tbm-label four" for="tbm"
                                    style="{{ BH::backgroundImage('TBM-BCard.png', '/images/BICoBrandCards') }}"><img
                                        src="/images/BICoBrandCards/TBM-BCard.png"
                                        style="visibility: hidden;" /></label>
                            </div>
                            <div class="member-card-col col-sm-6 col-md-4 col-lg-3">
                                <input type="radio" name="card-select" id="tealive" value="Tealive Member Card" />
                                <label class="tealive-label four" for="tealive"
                                    style="{{ BH::backgroundImage('Unitea-BCard.png', '/images/BICoBrandCards') }}">
                                    <img src="/images/BICoBrandCards/Unitea-BCard.png"
                                        style="visibility: hidden;" /></label>
                            </div> --}}
                        </section>
                    </div>
                </div>

                <div class="row pt-4">
                    <div class="col-md section mx-auto text-center">
                        <h1 id="card-selected-name">B Infinite Virtual B Card</h1>
                        <p>
                            No Annual / Membership Fee<br />Lifetime
                            Membership<br />Earn & Pay with BPoints at
                            participating B Infinite Partners<br />Enjoy special
                            privileges & discounts all year round
                        </p>
                        <div class="info-container">
                            <button type="submit" class="btn btn-pink">
                                SIGN ME UP FOR THIS ONE!
                            </button>
                        </div>
                    </div>
                </div>
        </div>
    </section>
    </form>
</section>

@endsection @section('custom_js')
<script>
    $('input[name="card-select"]').click(function () {
        var radioValue = $('input[name="card-select"]:checked').val();

        if (this.checked) {
            $("#card-selected-name").text(radioValue);
        }
    });

</script>
@endsection
