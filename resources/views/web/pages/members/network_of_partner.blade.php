@extends('web.master')

@section('content')

<section class="card-register">
    <section class="content profile gray pt-4 pb-0">
        <div class="container">
            <span class="centered-title title_network_partners">Be part of the B Infinite network of partners <br> 
            to grow your business</span>
            <form class="needs-validation pb-4" name="qryform" id="qryform" novalidate>
                <div class="row">
                    <div class="col-md section">
                        <h1>Business Details</h1>
                        <div class="info-container">
                            <div class="form-sec">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>REGISTERED BUSINESS NAME</label>
                                            <input type="text" class="form-control col" id="first_name"
                                            placeholder="LALA SDN BSD" name="business_name" required>
                                            <div class="invalid-feedback">
                                                Please choose a business name.
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Website Address</label>
                                            <input type="text" class="form-control col" id="website"
                                            placeholder="Website Name" name="website" required>
                                            <div class="invalid-feedback">
                                                Please enter a website.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr />
                <div class="row pt-4">
                    <div class="col-md section">
                        <h1>Business Details</h1>
                        <div class="info-container">
                            <div class="form-sec">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>BRAND NAME</label>
                                            <input type="text" class="form-control col" id="brand_name"
                                            placeholder="123Brand" name="brand_name" required>
                                            <div class="invalid-feedback">
                                                Please choose a brand name.
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>BRAND WEBSITE ADDRESS (If different with business website)</label>
                                            <input type="text" class="form-control col" id="brand_website"
                                            placeholder="www.123.brand.com.we" name="brand_website">
                                            <div class="invalid-feedback">
                                                Please enter a brand website.
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <h6>Brand Category</h6>
                                            @if(isset($allBrandCategory))
                                            @foreach($allBrandCategory as $value)
                                            <div class="custom-control custom-checkbox mr-sm-2">
                                                <input type="checkbox" class="custom-control-input" id="brand_cat_{{$value->id}}" value="{{$value->id}}" name="brand_category[]">
                                                <label class="custom-control-label" for="brand_cat_{{$value->id}}">{{$value->category_name}}</label>
                                            </div>
                                            @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr />
                <div class="row pt-4">
                    <div class="col-md section">
                        <h1>Contact Details</h1>
                        <div class="info-container">
                            <div class="form-sec">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>NAME</label>
                                            <input type="text" class="form-control col" id="contact_name"
                                            placeholder="BOB" name="contact_name" required>
                                            <div class="invalid-feedback">
                                                Please choose a name.
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>OFFICE CONTACT</label>
                                            <input type="text" class="form-control col" id="office_contact"
                                            placeholder="123456" name="office_contact" required>
                                            <div class="invalid-feedback">
                                                Please enter a contact number.
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>MOBILE CONTACT</label>
                                            <input type="text" class="form-control col" id="mobile_contact"
                                            placeholder="123456" name="mobile_contact" required>
                                            <div class="invalid-feedback">
                                                Please enter a mobile number.
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <h6>I would like to be contacted regaeding(tick where applicable)</h6>
                                            <div class="custom-control custom-checkbox mr-sm-2">
                                                <input type="checkbox" value="customer_reward" class="custom-control-input" id="customer_reward" name="contact_regards[]">
                                                <label class="custom-control-label" for="customer_reward">Customer Reward</label>
                                            </div>
                                            <div class="custom-control custom-checkbox mr-sm-2">
                                                <input type="checkbox" value="staff_reward" class="custom-control-input" id="staff_reward" name="contact_regards[]">
                                                <label class="custom-control-label" for="staff_reward">Staff/Incentive Reward</label>
                                            </div>
                                            <div class="custom-control custom-checkbox mr-sm-2">
                                                <input type="checkbox" value="b2b_reward" class="custom-control-input" id="b2b_reward" name="contact_regards[]">
                                                <label class="custom-control-label" for="b2b_reward">B2b Reward</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr />
                <div class="info-container">
                    <h6>By providing Bloyalty my Business and Brand Details, I hereby declare that the above are complete and legtimate, I agree that Bloyalty may collect and use this information for the purpose of gatering and analyzing information.</h6>
                </div>
                <button type="submit" class="btn btn-blue">
                    Submit
                </button>
            </form>
        </div>
    </section>
</section>

@endsection

@section('custom_js')
<script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function () {
        'use strict';
        window.addEventListener('load', function () {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();

</script>
@if($submitForm)
<script>
   Swal.fire({
      type: 'success',
      title: 'Success',
      text: 'You message has been successfully sent.'
  })
</script>
@endif
@endsection
