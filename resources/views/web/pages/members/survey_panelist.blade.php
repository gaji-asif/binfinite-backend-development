@extends('web.master')

@section('content')


<section class="survey_panelist">
    <section class="content gray">
        <div class="container py-5">
            <div class="row mx-0">
                <iframe frameborder="0" height="100%" width="100%" src="https://docs.google.com/forms/d/e/1FAIpQLSdo1W-LRjlHqwMJcfkpDG4VW0JzNyPV9GP-smipS3PPGLWG3A/viewform?embedded=true"></iframe>
            </div>
        </div>  
    </section>
</section>


@endsection @section('custom_js')
<script>

</script>
@endsection