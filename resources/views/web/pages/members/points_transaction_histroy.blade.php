@extends('web.master')

@section('content')

<section class="points-history">
    <section class="content profile gray">
        <div class="container pt-5">
            <span class="left-aligned-title">Transaction History</span>

            <div class="points-transaction section">
                <div class="info-container">
                    <form name="qryform" id="qryform" method="post" action="mail.php" onsubmit="return(validate());"
                        novalidate="novalidate">
                        <div class="form-group">
                            <div class="row mt-3">
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" id="search_box" placeholder="Search your points history"
                                        name="search_box" />
                                </div>
                            </div>
                            <div class="row py-3 trh">
                                <div class="inline-label-input col-lg">
                                    <h5>Show</h5>
                                    <select class="form-control col" id="state" name="state">
                                        <option selected>All</option>
                                        <option value="1">Earned</option>
                                        <option value="2">Redeemed</option>
                                        <option value="3">Token</option>
                                    </select>
                                </div>
                                <div class="inline-label-input col-lg">
                                    <h5>Card</h5>
                                    <select class="form-control col" id="state" name="state">
                                        <option selected>All</option>
                                        <option value="1">BCard …4053</option>
                                        <option value="2">TeaLive …5678</option>
                                        <option value="3">Caltex …7654</option>
                                    </select>
                                </div>
                                <div class="inline-label-input col-lg">
                                    <h5>Filter from</h5>
                                    <input type="text" class="form-control" id="filter_from" placeholder="DD/MM/YY"
                                        name="filter_from" />
                                </div>
                                <div class="inline-label-input col-lg">
                                    <h5>To</h5>
                                    <input type="text" class="form-control" id="filter_to" placeholder="DD/MM/YY" name="filter_to" />
                                </div>
                                <div class="col-lg-auto btn-container text-center pl-0">
                                    <a class="btn btn-outline-blue" href="/member/card_register">Reset</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section class="content2 points-transaction">
        <div class="container pb-5">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Transaction Date</th>
                        <th>Partner</th>
                        <th>Amount</th>
                        <th>BPoints</th>
                        <th>Token</th>
                    </tr>
                </thead>
                <tbody>
                    @for ($i = 0; $i < 15; $i++) <tr>
                        <td>09/12/18</td>
                        <td>
                            <div><b>Lazada</b><br>Kuala Lumpur</div>
                        </td>
                        <td>50.00</td>
                        <td>200</td>
                        <td>0</td>
                        </tr>
                        @endfor
                </tbody>
            </table>
        </div>
    </section>
</section>

@endsection
