@extends('web.master')

@section('content')

<section class="card-register">
    <section class="content profile gray pt-4 pb-0">
        <div class="container">
            <span class="centered-title">Virtual Card Registration</span>
            <form class="needs-validation" name="qryform" id="qryform" novalidate>
                <div class="row">
                    <div class="col-md section">
                        <h1>Personal Details</h1>
                        <div class="info-container">
                            <div class="form-sec">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>FIRST NAME</label>
                                            <input type="text" class="form-control col" id="first_name"
                                                placeholder="First Name" name="first_name" required>
                                            <div class="invalid-feedback">
                                                Please choose a username.
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>SALUTATION</label>
                                            <div class="input-group mb-3">
                                                <select class="form-control col" id="salutation" name="salutation"
                                                    required>
                                                    <option selected>Mr</option>
                                                    <option value="1">Mrs</option>
                                                    <option value="2">Miss</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>NRIC / PASSPORT</label>
                                            <input type="text" class="form-control col" id="id_passport"
                                                placeholder="921111101234" name="id_passport" required>
                                        </div>
                                        <div class="form-group">
                                            <label>GENDER</label>
                                            <div class="input-group mb-3">
                                                <select class="form-control col" id="gender" name="gender" required>
                                                    <option selected>Male</option>
                                                    <option value="1">Female</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>NATIONALITY</label>
                                            <div class="input-group mb-3">
                                                <select class="form-control col" id="nationality" name="nationality"
                                                    required>
                                                    <option selected>Malaysia</option>
                                                    <option value="1">One</option>
                                                    <option value="2">Two</option>
                                                    <option value="3">Three</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>EMAIL</label>
                                            <input type="email" class="form-control col" id="email"
                                                placeholder="example@hotmail.com" name="email" required>
                                        </div>
                                        <div class="form-group">
                                            <label>OWN A CAR</label>
                                            <div class="input-group mb-3">
                                                <select class="form-control col" id="own_car" name="own_car" required>
                                                    <option selected>Yes</option>
                                                    <option value="1">No</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>LAST NAME</label>
                                            <input type="text" class="form-control col" id="last_name"
                                                placeholder="Last Name" name="last_name" required>
                                        </div>
                                        <div class="form-group">
                                            <label>MARITAL STATUS</label>
                                            <div class="input-group mb-3">
                                                <select class="form-control col" id="marital_status"
                                                    name="marital_status" required>
                                                    <option selected>Single</option>
                                                    <option value="1">Married</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>DATE OF BIRTH</label>
                                            <input type="text" class="form-control col" id="date_of_birth"
                                                placeholder="" name="date_of_birth" required>
                                        </div>
                                        <div class="form-group">
                                            <label>RACE</label>
                                            <div class="input-group mb-3">
                                                <select class="form-control col" id="race" name="race" required>
                                                    <option selected>Malay</option>
                                                    <option value="1">Chinese</option>
                                                    <option value="2">Indian</option>
                                                    <option value="3">Others</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <label>MOBILE NUMBER</label>
                                                    <div>
                                                        <select class="form-control col" id="mobile_number_country"
                                                            name="mobile_number_country" required>
                                                            <option selected>+60</option>
                                                            <option value="1">+65</option>
                                                            <option value="2">+62</option>
                                                            <option value="3">+61</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg">
                                                    <label> </label>
                                                    <input type="text" class="form-control" id="mobile_number"
                                                        placeholder="128888888" name="mobile_number" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>CONFIRM EMAIL</label>
                                            <input type="email" class="form-control col" id="confirm_email"
                                                placeholder="example@hotmail.com" name="confirm_email" required>
                                        </div>
                                        <div class="form-group">
                                            <label>OWN A CREDIT CARD</label>
                                            <div class="input-group mb-3">
                                                <select class="form-control col" id="own_credit_card"
                                                    name="own_credit_card" required>
                                                    <option selected>Yes</option>
                                                    <option value="1">No</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr />
                <div class="row pt-4">
                    <div class="col-md section">
                        <h1>Home Details</h1>
                        <div class="info-container">
                            <div class="form-sec">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>ADDRESS LINE 1</label>
                                            <input type="text" class="form-control col" id="address_1" name="address_1"
                                                required>
                                        </div>
                                        <div class="form-group">
                                            <label>ADDRESS LINE 2</label>
                                            <input type="text" class="form-control col" id="address_2"
                                                name="address_2" />
                                        </div>
                                        <div class="form-group">
                                            <label>ADDRESS LINE 3</label>
                                            <input type="text" class="form-control col" id="address_3"
                                                name="address_3" />
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <label>HOME PHONE</label>
                                                    <div>
                                                        <select class="form-control col" id="home_number_country"
                                                            name="home_number_country">
                                                            <option selected>+60</option>
                                                            <option value="1">+65</option>
                                                            <option value="2">+62</option>
                                                            <option value="3">+61</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg">
                                                    <label> </label>
                                                    <input type="text" class="form-control" id="home_number"
                                                        placeholder="128888888" name="home_number" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>COUNTRY</label>
                                            <div class="input-group mb-3">
                                                <select class="form-control col" id="country" name="country" required>
                                                    <option selected>Malaysia</option>
                                                    <option value="1">One</option>
                                                    <option value="2">Two</option>
                                                    <option value="3">Three</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>STATE</label>
                                            <div class="input-group mb-3">
                                                <select class="form-control col" id="state" name="state" required>
                                                    <option selected>Kuala Lumpur</option>
                                                    <option value="1">One</option>
                                                    <option value="2">Two</option>
                                                    <option value="3">Three</option>
                                                </select>
                                                <div class="invalid-feedback">
                                                    Please provide a valid state.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>CITY</label>
                                            <input type="text" class="form-control col" id="city" placeholder=""
                                                name="city" />
                                        </div>
                                        <div class="form-group">
                                            <label>POSTCODE</label>
                                            <input type="text" class="form-control col" id="postcode"
                                                placeholder="50480" name="postcode" required>
                                            <div class="invalid-feedback">
                                                Please provide a valid zip.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr />
                <div class="row pt-4">
                    <div class="col-md section">
                        <h1>Office Details</h1>
                        <div class="info-container">
                            <div class="form-sec">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>ADDRESS LINE 1</label>
                                            <input type="text" class="form-control col" id="office_address_1"
                                                name="office_address_1" />
                                        </div>
                                        <div class="form-group">
                                            <label>ADDRESS LINE 2</label>
                                            <input type="text" class="form-control col" id="office_address_2"
                                                name="office_address_2" />
                                        </div>
                                        <div class="form-group">
                                            <label>ADDRESS LINE 3</label>
                                            <input type="text" class="form-control col" id="office_address_3"
                                                name="office_address_3" />
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <label>OFFICE PHONE</label>
                                                    <div>
                                                        <select class="form-control col" id="office_number_country"
                                                            name="office_number_country">
                                                            <option selected>+60</option>
                                                            <option value="1">+65</option>
                                                            <option value="2">+62</option>
                                                            <option value="3">+61</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg">
                                                    <label> </label>
                                                    <input type="text" class="form-control" id="office_number"
                                                        placeholder="128888888" name="office_number" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <label>OFFICE FAX</label>
                                                    <div>
                                                        <select class="form-control col" id="office_fax_number_country"
                                                            name="office_fax_number_country">
                                                            <option selected>+60</option>
                                                            <option value="1">+65</option>
                                                            <option value="2">+62</option>
                                                            <option value="3">+61</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg">
                                                    <label> </label>
                                                    <input type="text" class="form-control" id="office_fax_number"
                                                        placeholder="128888888" name="office_fax_number" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>COUNTRY</label>
                                            <div class="input-group mb-3">
                                                <select class="form-control col" id="office_country"
                                                    name="office_country">
                                                    <option selected>Malaysia</option>
                                                    <option value="1">One</option>
                                                    <option value="2">Two</option>
                                                    <option value="3">Three</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>STATE</label>
                                            <div class="input-group mb-3">
                                                <select class="form-control col" id="office_state" name="office_state">
                                                    <option selected>Kuala Lumpur</option>
                                                    <option value="1">One</option>
                                                    <option value="2">Two</option>
                                                    <option value="3">Three</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>CITY</label>
                                            <input type="text" class="form-control col" id="office_city" placeholder=""
                                                name="office_city" />
                                        </div>
                                        <div class="form-group">
                                            <label>POSTCODE</label>
                                            <input type="text" class="form-control col" id="office_postcode"
                                                placeholder="50480" name="office_postcode" />
                                        </div>
                                        <div class="form-group">
                                            <label>WORK EMAIL</label>
                                            <input type="email" class="form-control col" id="office_email"
                                                placeholder="example@hotmail.com" name="office_email" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="info-container">
                    <div class="custom-control custom-checkbox mr-sm-2">
                        <input type="checkbox" class="custom-control-input" id="customControlAutosizing">
                        <label class="custom-control-label" for="customControlAutosizing">I agree to the BCard Terms &
                            Conditions</label>
                        <div class="invalid-feedback">
                            You must agree before submitting.
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-blue">
                    Submit
                </button>
            </form>
        </div>
    </section>
</section>

@endsection

@section('custom_js')
<script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function () {
        'use strict';
        window.addEventListener('load', function () {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();

</script>
@endsection
