@extends('web.master')

@section('content')

<section class="content register gray py-100">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1 class="welcome">Sign Up with us now!</h1>

                @if(session('register_success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{session('register_success')}}
                </div>
                @endif

                @if(session('register_error'))
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{session('register_error')}}
                </div>
                @endif

                <div id="validation-message" class="alert alert-danger alert-dismissible" role="alert" style="display: none">
                    <h4 class="alert-heading">Error</h4>
                    <p></p>
                </div>

                <form method="POST" action="/member_register">
                    @csrf
                    {{-- <div class="form-group">
                        <input type="text" class="form-control" name="name" id="name" maxlength="255" placeholder="Your Full Name">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="display_name" id="display_name" placeholder="Your Display Name (Maximum 20 characters)"
                            maxlength="20">
                    </div> --}}
                    <div class="form-group">
                        <input type="mobile" class="form-control" name="mobile" id="mobile" maxlength="12" placeholder="Your mobile number" value="{{ old('mobile') }}">
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" name="email" id="email" maxlength="255" placeholder="Your email" value="{{ old('email') }}">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="password" id="password" maxlength="50"
                            placeholder="Your password">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="password_confirmation" id="password_confirmation"
                            maxlength="50" placeholder="Your confirm password">
                    </div>
                    <div class="form-group">
                        <button id="btnSignUp" class="btn-signup btn btn-pink">Sign Up</button>
                    </div>
                </form>
            </div>
            <div class="col-md-6 login-right">
                <h3 style="{{ BH::backgroundImage('Welcome-Image.jpg', '/images/WebSliders') }}"></h3>
                <!-- <img src="https://via.placeholder.com/510x610?text=Gucci Image" /> -->
            </div>
        </div>
    </div>
</section>

@endsection

@section('custom_js')
<script src="/assets/js/custom_validator.js"></script>
<script type="text/javascript">
    $("#btnSignUp").click(function () {
        // reset_error("#name");
        // reset_error("#display_name");
        reset_error("#email");
        reset_error("#mobile");
        reset_error("#password");
        reset_error("#password_confirmation");

        // var name = $('#name').val();
        // var display_name = $('#display_name').val();
        var email = $('#email').val();
        var mobile = $('#mobile').val();
        var password = $('#password').val();
        var password_confirmation = $('#password_confirmation').val();

        // if (name == '') {
        //     alert_error('Name field is required.', '#name');
        //     return false;
        // }

        if (mobile == '') {
            alert_error('Mobile number field is required.', '#mobile');
            return false;
        }

        if (email == '') {
            alert_error('Email field is required.', '#email');
            return false;
        } else {
            var re =
                /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (re.test(email) == false) {
                alert_error('Please enter a valid email.', '#email');
                return false;
            }
        }

        if (password == '') {
            alert_error('Password field is required.', '#password');
            return false;
        } else {
            if (password.length < 8) {
                alert_error('Minimum password length is 8.', '#password');
                return false;
            }
        }

        if (password_confirmation == '') {
            alert_error('Confirm password field is required.', '#password_confirmation');
            return false;
        }

        if (password_confirmation != password) {
            alert_error(
                'Confirm password is not same with password. <br />Please make sure the confirm password is same with password',
                '#password_confirmation');
            return false;
        }
        return true;
    });

</script>
@endsection
