@extends('web.master')

@section('custom_css')
    <link href="/assets/pinlogin/jquery.pinlogin.css" rel="stylesheet">
    <style>
        .pinlogin .pinlogin-field {
            font-size: 60px;
            width: 100px;
            height: 100px;

            color: transparent;
            text-shadow: 0 0 0 #000;
        }
    </style>
@endsection

@section('content')

<section class="card-register">
    <form name="verify_form" id="verify_form" method="post" action=""
        onsubmit="" novalidate="novalidate">
    @csrf
    <input type="hidden" id="pin_number" name="pin_number" value="">
    <input type="hidden" id="resend_otp" name="resend_otp" value="0">
    </form>
    <section class="content profile gray pt-4 pb-0">
        <div class="container">
            <span class="centered-title heading">OTP Verification</span>
            <div class="row">
                <div class="col-md section">
                    <div class="">
                        <div class="form-sec">
                                <div class="row">
                                    <div class="col-lg-12 mx-auto">
                                        @if(session('register_success'))
                                        <div class="alert alert-success alert-dismissible" role="alert">
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                            {{session('register_success')}}
                                        </div>
                                        @endif

                                        @if(session('register_error'))
                                        <div class="alert alert-danger alert-dismissible" role="alert">
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                            {{session('register_error')}}
                                        </div>
                                        @endif
                                        <h3 class="text-center mb-3">Please enter the pin you received:</h3>
                                        <div id="pinelement" class="text-center mx-auto mb-5"></div>
                                        {{-- <div class="form-group">
                                            <label class="required-label">OTP Number</label>
                                            <input type="text" class="form-control col" id="otp_number" placeholder=""
                                                name="otp_number" value="{{ old('otp_number') }}" maxlength="6" required/>
                                        </div> --}}
                                       {{--  <div class="text-center">
                                            <button type="submit" class="btn btn-pink">
                                                Submit
                                            </button>
                                        </div>
 --}}
                                        <p class="text-center"><a href="#" id="resend_otp_button">Did not receive OTP? Click here to resend</a></p>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>

@endsection

@section('custom_js')
<script src="/assets/pinlogin/jquery.pinlogin.min.js"></script>
<script>
    $(function() {
        var pinLogin = $('#pinelement').pinlogin({
            fields: 6,
            hideinput: false,
            reset: false,
            complete: function(pin) {
                console.log('You entered: ' + pin);
                pinLogin.disable();
                $('#pin_number').val(pin).attr('disabled', false);
                $('#resend_otp').val(0).attr('disabled', true);
                $('#verify_form').submit();
            }
        });

        $('#resend_otp_button').on('click', function(e) {
            e.preventDefault();
            $('#pin_number').val('').attr('disabled', true);
            $('#resend_otp').val(1).attr('disabled', false);
            $('#verify_form').submit();
        })
    })
</script>
@endsection
