@extends('web.master') @section('content')

<section class="membership">
    <section class="content gray pt-4 pb-0">
        <div class="container pt-2">
            <span class="centered-title">How to start living with infinite rewards</span>
            <div class="parent">
                <div class="item">
                    <img src="/images/be-a-member.png">
                    <div class="text">
                        <h2 class="title">Be a Member</h2>
                        <h5 class="description">
                            Join us by signing up for a virtual or B Card, it’s
                            simple!
                        </h5>
                    </div>
                </div>
                <div class="item">
                    <img src="/images/earn-points.png">
                    <div class="text">
                        <h2 class="title">Earn Points</h2>
                        <h5 class="description">
                            Just be sure to show your card when you shop, or use
                            your virtual card when you shop online
                        </h5>
                    </div>
                </div>
                <div class="item">
                    <img src="/images/get-rewarded.png">
                    <div class="text">
                        <h2 class="title">Get Rewarded</h2>
                        <h5 class="description">
                            Redeem for products or services or cash, and get
                            better discounts and bonus points with our partners!
                        </h5>
                    </div>
                </div>
            </div>
            <div class="button-container text-center">
                <button type="submit" class="btn btn-pink">
                    SIGN UP NOW!
                </button>
            </div>
        </div>
    </section>

    <section class="content profile white-content pt-4 pb-0">
        <div class="container">
            <span class="centered-title">Download our mobile app</span>
            <div class="row">
                <div class="col-7">
                    <div class="text">
                        <h1 class="title">The Power of Infinity Rewards</h1>
                        <p>
                            Check your points, latest promotions and the latest
                            news using our app
                        </p>
                    </div>
                    <div class="appstore-links">
                        <p class="mobile-app-banner"><img src="{{ asset('assets/images/ios-256x256.png') }}" /></p>
                        <p class="mobile-app-banner"><img src="{{ asset('assets/images/google-256x256.png') }}" /></p>
                    </div>
                    <iframe src="https://www.youtube-nocookie.com/embed/EaRZQLqSCes?controls=0" frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
                </div>
                <div class="col">
                    <h3 style="{{ BH::backgroundImage('appstore-mobile-hero.png') }}"></h3>
                </div>
            </div>
        </div>
    </section>
    <section class="content2 profile gray pt-5">
        <div class="container mb-5">
            <span class="centered-title">Types of Cards</span>
            <form class="form cf">
                <div class="row pt-4">
                    <div class="col section mx-auto px-0 my-0 text-center">
                        <section class="row card-selector cf">
                            <div class="member-card-col col-sm-6 col-md-4 col-lg-3">
                                <input type="radio" name="card-select" id="inifnite" value="B Infinite B Card"
                                    checked />
                                <label class="inifnite-label four" for="inifnite"
                                    style="{{ BH::backgroundImage('BInfinite-Generic-BCard.png', '/images/BICoBrandCards') }}"><img
                                        src="/images/BICoBrandCards/BInfinite-Generic-BCard.png"
                                        style="visibility: hidden;" /></label>
                            </div>
                            <div class="member-card-col col-sm-6 col-md-4 col-lg-3">
                                <input type="radio" name="card-select" id="inifniteVirtual"
                                    value="B Infinite Virtual B Card" checked />
                                <label class="inifnite-label four" for="inifniteVirtual"
                                    style="{{ BH::backgroundImage('BInfinite-Virtual-BCard.png', '/images/BICoBrandCards') }}"><img
                                        src="/images/BICoBrandCards/BInfinite-Virtual-BCard.png"
                                        style="visibility: hidden;" /></label>
                            </div>
                            <div class="member-card-col col-sm-6 col-md-4 col-lg-3">
                                <input type="radio" name="card-select" id="borders" value="Borders Rewards Card" />
                                <label class="borders-label four" for="borders"
                                    style="{{ BH::backgroundImage('Borders-BCard.png', '/images/BICoBrandCards') }}"><img
                                        src="/images/BICoBrandCards/Borders-BCard.png"
                                        style="visibility: hidden;" /></label>
                            </div>
                            <div class="member-card-col col-sm-6 col-md-4 col-lg-3">
                                <input type="radio" name="card-select" id="caltex" value="Caltex
                                Journey Card" />
                                <label class="caltex-label four" for="caltex"
                                    style="{{ BH::backgroundImage('Caltex-Journer-BCard.png', '/images/BICoBrandCards') }}"><img
                                        src="/images/BICoBrandCards/Caltex-Journer-BCard.png"
                                        style="visibility: hidden;" /></label>
                            </div>
                            <div class="member-card-col col-sm-6 col-md-4 col-lg-3">
                                <input type="radio" name="card-select" id="krr" value="KRR Card" />
                                <label class="krr-label four" for="krr"
                                    style="{{ BH::backgroundImage('Kenny-Rogers-Roasters-BCard.png', '/images/BICoBrandCards') }}"><img
                                        src="/images/BICoBrandCards/Kenny-Rogers-Roasters-BCard.png"
                                        style="visibility: hidden;" /></label>
                            </div>
                            <div class="member-card-col col-sm-6 col-md-4 col-lg-3">
                                <input type="radio" name="card-select" id="pick_a_trip" value="Pick A Trip Card" />
                                <label class="pick_a_trip-label four" for="pick_a_trip"
                                    style="{{ BH::backgroundImage('Pick-A-Trip-BCard.png', '/images/BICoBrandCards') }}"><img
                                        src="/images/BICoBrandCards/Pick-A-Trip-BCard.png"
                                        style="visibility: hidden;" /></label>
                            </div>
                            <div class="member-card-col col-sm-6 col-md-4 col-lg-3">
                                <input type="radio" name="card-select" id="tbm" value="TBM
                                Member Card" />
                                <label class="tbm-label four" for="tbm"
                                    style="{{ BH::backgroundImage('TBM-BCard.png', '/images/BICoBrandCards') }}"><img
                                        src="/images/BICoBrandCards/TBM-BCard.png"
                                        style="visibility: hidden;" /></label>
                            </div>
                            <div class="member-card-col col-sm-6 col-md-4 col-lg-3">
                                <input type="radio" name="card-select" id="tealive" value="Tealive Member Card" />
                                <label class="tealive-label four" for="tealive"
                                    style="{{ BH::backgroundImage('Unitea-BCard.png', '/images/BICoBrandCards') }}">
                                    <img src="/images/BICoBrandCards/Unitea-BCard.png"
                                        style="visibility: hidden;" /></label>
                            </div>
                        </section>
                    </div>
                </div>
            </form>
        </div>
        <div class="content white">
            <div class="container">
                <div class="row py-100">
                    <div class="col-md section mx-auto text-center">
                        <h1 id="card-selected-name">B Infinite B Card</h1>
                        <p>
                            No Annual / Membership Fee<br />Lifetime
                            Membership<br />Earn & Pay with BPoints at
                            participating B Infinite Partners<br />Enjoy special
                            privileges & discounts all year round
                        </p>
                        <div class="info-container">
                            <button type="submit" class="btn btn-blue">
                                SIGN ME UP FOR THIS ONE!
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>

@endsection @section('custom_js')
<script>
    $('input[name="card-select"]').click(function () {
        var radioValue = $('input[name="card-select"]:checked').val();

        if (this.checked) {
            $("#card-selected-name").text(radioValue);
        }
    });

</script>
@endsection
