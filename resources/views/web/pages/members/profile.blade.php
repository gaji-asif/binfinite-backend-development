@extends('web.master')

@section('content')

<section class="content profile gray">
    <div class="container py-5">
        <span>Account Details</span>
        <div class="row mt-3">
            <div class="col-md section">
                <h1>Account</h1>
                {{-- <p>
                    Below is your Card member number. You can change your
                    password using the button below
                </p>
                <div class="info-container">
                    <div class="row">
                        <div class="col-md-3">
                            <h5>Member/Card number</h5>
                        </div>
                        <div class="col-md-auto">
                            <p>1234 5678 9112 3456</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <h5>Password/PIN</h5>
                            <!-- <p>1234 5678 9112 3456</p> -->
                        </div>
                    </div>
                </div> --}}
                <div class="row account-photo-container mx-0">
                    <div class="account-photo">
                        <img class="img-circle" src="http://www.reactiongifs.com/r/overbite.gif"
                            style="width: 100px;height:100px;" />
                        <!--<img class="img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/kolage/128.jpg" style="width: 100px;height:100px;">-->
                    </div>
                    <div class="info-container">
                        <h5>Change profile picture</h5>
                        <button type="submit" class="btn btn-blue">
                            Upload
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <div class="row pt-4">
            <div class="col-md section cardholders">
                <h1>Cardholders</h1>

                <div class="info-container">
                    <div class="row pb-2">
                        @foreach($cards as $card)
                        <div class="col-md-4">
                            <div class="card">
                                <div class="unlinked"></div>
                                <div class="card-body">
                                    <p class="card-text">{{ $card->is_primary ? 'Primary - ' : '' }}BCard</p>
                                    <h5 class="card-title">{{ $card->card_no }}</h5>
                                    <div class="row d-flex align-items-end">
                                        <div class="btn-text-list col-md-auto">
                                            <a class="btn-text-pink btn-sm" href="#">Reset PIN</a>
                                            <a id="unlink-{{ $card->id }}" class="btn-text-pink btn-sm"
                                                href="javascript:;">Unlink
                                                Card</a>
                                        </div>
                                        <div class="col-md text-right">
                                            <span class="card-points"><b>{{ $card->formattedBpoints }}</b> BPts</span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="btn-text-list">
                    <a class="btn-text-pink" href="javascript:void(0);" data-toggle="modal"
                        data-target="#setPrimaryCardModal">Set
                        Another Card As Primary Card</a>
                    <a class="btn-text-pink" href="javascript:void(0);" data-toggle="modal"
                        data-target="#linkCardModal">Link
                        Another Card</a>
                    {{-- <a class="btn-text-pink" href="javascript:void(0);" data-toggle="modal"
                        data-target="#transferPointsModal">Transfer
                        Points</a> --}}
                </div>
            </div>
        </div>
        <hr />
        <div class="row pt-4">
            <div class="col-md section">
                <h1>Contact Details</h1>
                <p>
                    Have your contact details changed? Update them below and
                    we'll do everything else!
                </p>
                <div class="info-container">
                    <div class="row">
                        <div class="col-md-3">
                            <h5>Mobile Phone</h5>
                        </div>
                        <div class="col-md-auto">
                            <p>{{ $member->lms_Mobile }}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <h5>Email</h5>
                        </div>
                        <div class="col-md-auto">
                            <p>{{ $member->lms_Email }}</p>
                        </div>
                    </div>
                </div>
                <div class="btn-text-list">
                    <a class="btn-text-pink" href="javascript:void(0);" data-toggle="modal"
                        data-target="#changeMobileEmailModal">Change mobile or email</a>
                            </div>
                        </div>
                </div>
                <hr />
                <div class=" row pt-4">
                        <div class="col-md section">
                            <h1>Newsletter Settings</h1>
                            <div class="info-container">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio1" name="customRadio"
                                        class="custom-control-input" {{ $member->newsletter_setting ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="customRadio1">Yes, I want to receive
                                        offers, promotions, discounts from BInfinite.</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio2" name="customRadio"
                                        class="custom-control-input"{{ ! $member->newsletter_setting ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="customRadio2">No, I don't want to
                                        receive
                                        offers, promotions, discounts from BInfinite.</label>
                                </div>
                            </div>
                        </div>
                </div>
                <hr />
                <div class=" row pt-4">
                    <div class="col-md section">
                        <h1>Residential Address</h1>
                        <div class="info-container">
                            <div class="form-sec">
                                <form name="qryform" id="qryform" method="post" action="mail.php"
                                    onsubmit="return(validate());" novalidate="novalidate">
                                    <div class="form-group">
                                        <label>UNIT NUMBER</label>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control col-md-8"
                                                            id="unit_number" placeholder="" name="unit_number"
                                                            value="13-A-09" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>BUILDING NAME</label>
                                        <input type="text" class="form-control col-md-5" id="building_name"
                                            placeholder="" name="building_name" value="Seni Mont Kiara" />
                                    </div>
                                    <div class="form-group">
                                        <label>STREET NAME</label>
                                        <input type="email" class="form-control col-md-5" id="street_name"
                                            placeholder="" name="street_name" value="Jalan 19/70A" />
                                    </div>

                                    <div class="form-group">
                                        <label>SUBURB/CITY</label>
                                        <input type="text" class="form-control col-md-5" id="city" placeholder=""
                                            name="city" value="Desa Sri Hartamas" />
                                    </div>
                                    <div class="form-group">
                                        <label>STATE</label>
                                        <div class="input-group mb-3">
                                            <select class="form-control col-md-5" id="state" name="state">
                                                <option selected>Kuala Lumpur</option>
                                                <option value="1">One</option>
                                                <option value="2">Two</option>
                                                <option value="3">Three</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>POSTCODE</label>
                                        <input type="text" class="form-control col-md-5" id="postcode" placeholder=""
                                            name="postcode" value="50480" />
                                    </div>
                                    <button type="submit" class="btn btn-blue">
                                        Submit
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</section>

@include('web.include.dialog.set_primary_card_dialog', ['cards' => $cards])

@include('web.include.dialog.transfer_points_dialog')

@include('web.include.dialog.update_mobile_email_dialog', ['mobile' => $member->lms_Mobile, 'email' => $member->lms_Email])

@endsection

@section('custom_js')
<script>
    $(document).ready(function () {
        $("[id^=unlink]").click(function () {
            $(this).closest(".card").children(".unlinked").addClass("active");
        });
    });

</script>
@endsection
