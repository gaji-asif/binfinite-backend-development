<div class="popular-brands">
    <span class="centered-title">Popular Brands</span>

    @foreach($chunkedStores as $items)
    <div class="row mx-0 px-0">
        @foreach($items as $item)
        <div class="col-lg-2">
            @if( Auth::check() && Auth::user()->defaultMember )
            <a href="{{ $item->affiliateUrl }}"><h3 style="{{ BH::backgroundImage($item->logoUrl) }}"></h3></a>
            @else
            <a class="eachMerchantDialog" href="javascript:void(0);" data-toggle="modal"
            data-target="#shoponlineMerchantDialog" data-shop-id="{{ $item->id }}"><h3 style="{{ BH::backgroundImage($item->logoUrl) }}"></h3></a>
            @endif

        </div>
        @endforeach
    </div>
    @endforeach
</div>
