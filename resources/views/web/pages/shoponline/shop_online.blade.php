@extends('web.master')

@section('content')

<?php 
$collections = [
    'special-deals' => [
            'name' => 'Special Deals',
            'items' => [
                [
                    'ribbon' => 'Best Seller',
                    'image' => 'special-1.png',
                    'title' => 'NARS Hot Tryst Cheek Palette',
                    'description' => 'A limited edition cheek palette featuring six covetable shades.',
                    'points' => '25,000',
                ],
                [
                    'ribbon' => 'New',
                    'image' => 'special-2.png',
                    'title' => 'ERNO LASZLO Detoxifying Set',
                    'description' => 'Double the detox every time you cleanse with this pair of Erno Laszlo’s…',
                    'points' => '25,000',
                ],
                [
                    'image' => 'special-3.png',
                    'title' => 'SMEG Retro Blender',
                    'description' => 'A joyfully designed kitchen appliance collection based on the curved…',
                    'points' => '25,000',
                ],
                [
                    'image' => 'special-4.png',
                    'title' => 'BOSE Soundsport Free',
                    'description' => 'These headphones have an unmatched combination of comfort and stability.',
                    'points' => '25,000',
                ]
            ]
    ]
]
?>

<div class="container-fluid">
    @include('web.include.hero_slider')
</div>

<section class="shop-online">
    <section class="content2 menu">
        <div class="container">
            @include('web.include.category_icon_menu')
        </div>
    </section>

    <section class="content gray">
        <div class="container pt-5">
            @include('web.include.ads')
        </div>

        <div class="container-fluid pb-5">
            {{-- @isset($collections['special-deals'])
            @include('web.components.slider_collection', ['key' => 'special-deals', 'collection' =>
            $collections['special-deals']])
            @endisset --}}
        </div>
    </section>

    <div class="container pb-5">
        @include('web.pages.shoponline.popular_brands', ['chunkedStores' => $chunkedStores])
    </div>
</section>

<section class="content py-100">
    <div class="container">
        @include('web.include.ads')

        @include('web.include.preferred_category')
    </div>
</section>
</section>

@include('web.include.shoponline_merchant_dialog')
@include('web.include.shoponline_merchant_dialog_steps')

@endsection

@section('custom_js')
<script>
    $('.single-slick').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        centerMode: true,
        variableWidth: true
    });

    $('.responsive').fadedSlick4();

    $(document).ready(function () {
        $('#media').carousel({
            pause: true,
            interval: false,
        });

        $('.eachMerchantDialog').on('click', function() {
            // Set shop ID & initial card ID
            $('.shopID').val($(this).data('shop-id'));
            $('.cardID').val($('#transferFromCard').val());
        });

        $('#transferFromCard').on('change', function() {
            $('.cardID').val(this.value);
        });
    });

</script>
@endsection
