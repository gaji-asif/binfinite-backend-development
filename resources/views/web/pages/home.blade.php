@extends('web.master')

@section('content')

<?php 
$collections = [
    'collect-points' => [
            'name' => 'Get the most out of B Infinite',
            'items' => [
                [
                    'ribbon' => 'Fasttrack your BPoints',
                    'image' => '8e4b3378e00274c3ddd2fac0129609dd.png',
                    'title' => 'Collect TRIPLE POINTS when you fill fuel at Caltex!',
                    'actionTitle' => 'LEARN MORE',
                ],
                [
                    'ribbon' => 'Collect Bonus Points',
                    'image' => 'marc-babin-334972-unsplash.png',
                    'title' => 'Look out for BONUS POINTS deals all around Tesco, in emails, deals and coupons',
                    'actionTitle' => 'VIEW ALL DEALS',
                ],
                [
                    'ribbon' => 'Be Rewarded',
                    'image' => '205bbd329dd1fbc81d918d670996c429.png',
                    'title' => 'Reward your healthy habits, collect and redeem points with our health partners',
                    'actionTitle' => 'VIEW HEALTH PARTNERS',
                ]
            ]
    ],
    'latest-deals' => [
            'name' => 'Latest Deals',
            'items' => [
                [
                    'image' => '8e4b3378e00274c3ddd2fac0129609dd.png',
                    'company' => 'Caltex',
                    'title' => 'Example of a deal or promotion',
                    'point' => 'RM1 = 1BPoint',
                    'validDate' => '31st December 2018',
                ],
                [
                    'image' => 'marc-babin-334972-unsplash.png',
                    'company' => 'Berjaya GROUP',
                    'title' => 'Complimentary stay when you have a long offer line',
                    'point' => 'RM1 = 1BPoint',
                    'validDate' => '31st December 2018',
                ],
                [
                    'image' => '205bbd329dd1fbc81d918d670996c429.png',
                    'company' => 'ZALORA',
                    'title' => '15% OFF',
                    'point' => 'RM1 = 1BPoint',
                    'validDate' => '31st December 2018',
                ]
            ]
    ],
    'top-picks' => [
            'name' => 'Top Picks',
            'items' => [
                [
                    'image' => '8e4b3378e00274c3ddd2fac0129609dd.png',
                    'company' => 'Caltex',
                    'title' => 'Example of a deal or promotion',
                    'point' => 'RM1 = 1BPoint',
                    'validDate' => '31st December 2018',
                ],
                [
                    'image' => 'marc-babin-334972-unsplash.png',
                    'company' => 'Berjaya GROUP',
                    'title' => 'Complimentary stay when you have a long offer line',
                    'point' => 'RM1 = 1BPoint',
                    'validDate' => '31st December 2018',
                ],
                [
                    'ribbon' => '5 days left!',
                    'image' => '205bbd329dd1fbc81d918d670996c429.png',
                    'company' => 'ZALORA',
                    'title' => '15% OFF',
                    'point' => 'RM1 = 1BPoint',
                    'validDate' => '31st December 2018',
                ]
            ]
    ]
]
?>

<section class="home">
    <div class="container-fluid">
        @include('web.include.hero_slider')

        @include('web.include.member_stats')
    </div>

    <section class="content gray">
        <div class="container pt-2">
            @include('web.include.ads')

            @if (Auth::check())
            @include('web.components.collect_points_collection', ['key' => 'collect-points', 'collection' =>
            $collections['collect-points']])
            @endif
        </div>
        <div class="container-fluid pb-5">
            @if (!Auth::check() && isset($collections['latest-deals']))
            @include('web.components.slider_collection', ['key' => 'latest-deals', 'collection' =>
            $collections['latest-deals']])
            @endif

            @isset($collections['top-picks'])
            @include('web.components.slider_collection', ['key' => 'top-picks', 'collection' =>
            $collections['top-picks']])
            @endisset
        </div>
    </section>

    <section class="content py-100">
        <div class="container">
            @include('web.include.ads')

            @include('web.include.partners')
        </div>
    </section>

    <section class="content gray py-100">
        <div class="container">
            @include('web.include.redeemables')
        </div>
    </section>
</section>

@endsection

@section('custom_js')
<script>
    $(document).ready(function () {
        // $('#media').carousel({
        //     pause: true,
        //     interval: false,
        // });

        // $('.single-slick').slick({
        //     dots: true,
        //     infinite: true,
        //     speed: 300,
        //     slidesToShow: 1,
        //     centerMode: true,
        //     variableWidth: true
        // });

        $('.faded-slick').each(function () {
            $(this).fadedSlick();
        });

        $('#carousel-generic .slider').redeemablesSlider(
            $('.redeem-suggestions a[data-slide="prev"]'),
            $('.redeem-suggestions a[data-slide="next"]'),
            $('.redeem-suggestions .redeem-suggestions-carousel-page'),
        );
    });

</script>
@endsection
