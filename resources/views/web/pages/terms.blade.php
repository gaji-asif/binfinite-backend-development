@extends('web.master')

@section('content')

<section class="content profile terms">
    <div class="container">
        <span>Terms & Conditions</span>
        <h5>Definition</h5>
        <div class="row">
            <div class="col">
                <p>In these Terms and Conditions, unless the context otherwise requires, the following words and
                    expressions shall have the following meanings: -</p>
                <p><span class="font-bold">"B Infinite"</span> or <span class="font-bold">"B Infinites"</span> means B
                    Infinite
                    Loyalty Program card(s) issued by BLoyalty to Member(s);</p>
                <p><span class="font-bold">"BCard"</span> means the physical or virtual
                    Membership card issued by BLoyalty to the Member</p>
                <p><span class="font-bold">"BLoyalty"</span> means BLoyalty Sdn Bhd (Company No.
                    154570-W);</p>
                <p><span class="font-bold">"Co-branded BCard"</span> means physical or virtual Membership card issued
                    jointly by BLoyalty
                    and its Merchant partner to the Merchant's members</p>
                <p><span class="font-bold">"Member"</span> or <span class="font-bold">"Members"</span> means
                    individual(s) who
                    has been accepted by BLoyalty as participating member(s) of the Program in accordance to the Terms
                    and Conditions set forth herein;</p>
                <p><span class="font-bold">"Membership"</span> means membership to the Program in accordance to the
                    Terms and Conditions herein;</p>
                <p><span class="font-bold">"Merchant"</span> or <span class="font-bold">"Merchants"</span> means the
                    participating merchant(s) of the
                    Program;</p>
                <p><span class="font-bold">"Mobile App"</span> means the B Infinite application made available for
                    downloading into the
                    Member's Android or iOS mobile phone</p>
                <p><span class="font-bold">"BPoints"</span> means the BPoints awarded to Members for purchases
                    and redemption of Rewards at participating Merchants' outlets;</p>
                <p><span class="font-bold">"PIN"</span> means a 6-digit Personal
                    Identification Number that is only known to the Member</p>
                <p><span class="font-bold">"Program"</span> means the B Infinite Loyalty
                    Program operated by BLoyalty;</p>
                <p><span class="font-bold">"Rewards"</span> means the products, services, rewards, gifts or other
                    benefits made available by BLoyalty under a rewards program established by BLoyalty which may be
                    redeemed by Members;</p>
                <p><span class="font-bold">"Website"</span> means the website owned and operated by BLoyalty in
                    relation to the
                    Program and located at www.binfinite.com.my or at such other domain name registered by BLoyalty.</p>
            </div>
        </div>

        <h5>Membership</h5>
        <div class="row">
            <div class="col">
                <p>These Terms and Conditions (including the Policy on Privacy and Data Protection) govern the award
                    and
                    use of BPoints by Members, and set out the terms of the agreement between BLoyalty and each Member
                    with
                    regards to the Program. A person intending to participate in the Program can sign up and register
                    for
                    the Program with BLoyalty by applying for a BCard and can then earn BPoints on various purchases at
                    participating Merchants' outlets. By applying to register with the Program or using the BCard, a
                    Member
                    is deemed to have accepted these Terms and Conditions. The Member is agreeable to provide the
                    mandatory
                    information required for the purpose of registering as the Member for the BCard. BLoyalty may, in
                    its
                    sole discretion decide and without the need to assign any reason, refuse an application made by any
                    person to be a Member.</p>
                <p>BLoyalty reserves the right to amend these Terms and Conditions at any time and
                    from time to time as it deems fit at its absolute discretion and with or without prior notification
                    to
                    Members. Members may obtain the updated version of these Terms and Conditions from B Infinite's
                    website
                    www.binfinite.com.my or the Mobile App. Notwithstanding the foregoing, continued use of the Website
                    or
                    Mobile App will be deemed to constitute your acceptance of the new Terms and Conditions. Failure to
                    observe the Terms and Conditions stated herein by a Member may result in termination of Membership.</p>
                <p>BLoyalty reserves the right to, at any time, vary or terminate the Program or any privileges under
                    the
                    Program or withdraw the B Infinites from use with or without prior notification to Members and
                    without
                    being liable in any way to Members. BLoyalty may, at its sole discretion, remove any or all Members
                    from the Program at any time.</p>
                <p>The Program is operated by:<br>BLoyalty Sdn Bhd,<br>16th Floor,<br>Office Block
                    Plaza Berjaya,<br>12 Jalan Imbi, 55100 Kuala Lumpur.<br>Email: enquiry@binfinite.com.my<br>Call
                    centre:
                    03-2141
                    8080 (Monday - Friday, 9.00am to 6.00pm)</p>
                <p>Unless stated otherwise in writing by B Infinite and/or
                    Co-brand Merchants, Membership is open to individuals who are 18 years of age and above. Residents
                    and
                    non-residents of Malaysia may apply for membership.</p>
                <p>BCard is a privilege card and is non-transferable.
                    It is not a credit card, debit card, prepaid nor a charge card.</p>
                <p>BCard is given out to Members without
                    charge. For Co-branded BCard, membership fee is determined by Co-brand Merchants.</p>
                <p>A Member may use B
                    Infinite only in participating Merchants' outlets or at such places or on such items as BLoyalty
                    may
                    specify from time to time.</p>
                <p>No annual fee will be charged for the Membership. For Co-branded BCard,
                    annual fee is determined by Co-brand Merchants.</p>
                <p>BCards must be presented to the cashier of the
                    participating Merchants' outlets BEFORE each purchase to record the amount spent. BCards presented
                    after the transaction will not be accepted for points accumulation.</p>
                <p>There shall be no accumulation of
                    BPoints for backdated receipts, special savings, offers, bulk purchases and sales, unless otherwise
                    stated.</p>
                <p>POINT ACCUMULATION WILL BEGIN FROM NIL. Members are advised to keep all receipts for at least 6
                    months of each qualifying period in the event of discrepancies in the accumulated BPoints.</p>
                <p>BPoints
                    expire 36 months from the issuance date on a first in first out basis. BPoints can be used as cash
                    to
                    buy things from merchants but cannot be converted to hard cash nor transferred to any third party.</p>
            </div>
        </div>

        <h5>BPoints Awarding and Redemption</h5>
        <div class="row">
            <div class="col">
                <p>BPoints will be awarded at a rate agreed between BLoyalty and Merchant when a Member purchases goods
                    or
                    services at participating Merchant's outlets. Items that are not eligible for BPoints will be
                    determined by respective Merchants. BLoyalty may alter the method and rate at which BPoints are
                    awarded
                    at its discretion from time to time. Each Point is equivalent to the value of RM0.01 (1 Sen) or
                    such
                    other value as BLoyalty may revise from time to time. For any Cross-Border transactions, BPoints
                    will
                    be awarded and converted back to the local denomination at a rate determined by BLoyalty.</p>
                <p>BPoints will
                    also be awarded for the value of items redeemed when a Member redeems for goods or services at
                    selected
                    Merchant's outlets (please refer to the Website or Mobile App for further information). A Member
                    may
                    redeem partially or all his BPoints subject to the Member complying with the procedures for
                    redemption.
                    BPoints may be redeemed at the participating Merchant's outlets, through the Website, Mobile App or
                    by
                    any other methods as BLoyalty may determine from time to time. Member is required to input a valid
                    6-digit PIN for each redemption upon request from merchants. BLoyalty reserves the right to reject
                    any
                    Redemption with invalid PIN. Once redemption has been accepted by BLoyalty, it cannot be cancelled,
                    exchanged or returned. BPoints cannot be exchanged for cash and can only be used for redemption of
                    Rewards. Rewards may be redeemed by a Member using BPoints or a combination of BPoints plus
                    cash/card/vouchers. No BPoints will be issued if redemption is made by a Member online through the
                    Website. On confirmation of redemption, BPoints used for redemption will be deducted from the
                    Member's
                    account and if applicable, additional BPoints equivalent to the value of Rewards will be added to
                    the
                    Member's account.</p>
                <p>All goods and services are subject to availability and further subject to all
                    applicable legal rules and the terms and conditions (including booking requirements, cancellation
                    restrictions, return conditions, warranties and limitations of liability) as imposed by the
                    supplier of
                    the Reward and BLoyalty. BLoyalty makes no representation or warranty of any kind (whether express
                    or
                    implied) with regards to the condition, fitness for purposes, merchantable quality or otherwise of
                    any
                    Rewards redeemed. When BPoints are used to redeem for a Reward that is to be supplied by a third
                    party,
                    BLoyalty may (but is not obliged to) act as an intermediary between the Member and the third party.
                    BLoyalty will use its best endeavours to ensure such Rewards are supplied by the third party.
                    BLoyalty
                    shall not however be responsible for any failure or delay by a third party to supply such Reward,
                    or
                    loss or damage to such Reward during delivery.</p>
                <p>BPoints cannot be used for redemption until they are
                    credited into the Member's account. BPoints will be recorded in the Member's account only after the
                    Merchant has notified BLoyalty of the details of the relevant transaction which BPoints are issued.
                    The
                    BPoints will be recorded in the Member's account depending on the frequency of notification from
                    the
                    respective participating Merchants. For any Cross-Border redemption, BPoints will be converted to
                    the
                    local denomination at a rate determined by BLoyalty and deducted accordingly.</p>
            </div>
        </div>

        <h5>General Provisions</h5>
        <div class="row">
            <div class="col">
                <p>BLoyalty may, at its discretion, replace or substitute any advertised Reward with a similar Reward.</p>
                <p>A
                    Member may check his BPoints online at the Website or Mobile App or via short messaging service
                    (SMS).
                    Charges will apply for checking of BPoints via SMS.</p>
                <p>In the event of a failure or breakdown of any
                    equipment or system used in connection with the Program, BLoyalty may refuse request for redemption
                    or
                    to award BPoints on any transaction. BLoyalty and the Merchants shall not be responsible or liable
                    in
                    any manner in the event BPoints are not awarded or redemption cannot be made or a Member is unable
                    to
                    check his BPoints, due to any failure in the equipment or system used in connection with the
                    Program.</p>
                <p>The use of the Website and Mobile App is at the Members' own risk. Members are responsible for the
                    security of their user login ID, password and PIN. BLoyalty accepts no liability for the disclosure
                    of
                    the user login ID or password or PIN by the Member to a third party, whether intentionally or not.
                    BLoyalty reserves the right to block a Member from accessing his account online if BLoyalty has
                    reasonable grounds to suspect that fraud or misconduct has been committed by the Member or a third
                    party.</p>
                <p>While BLoyalty uses reasonable efforts to include up to date information in the Website or
                    Mobile App and in all its publications, BLoyalty makes no warranties or representations as to their
                    accuracy, reliability, completeness or otherwise. The contents, materials, products or other
                    services
                    available in BLoyalty's publications or accessible through the Website or Mobile App are on "as is"
                    and
                    "as available" basis. BLoyalty disclaims all warranties (express or implied) including but not
                    limited
                    to, merchantability, fitness for purpose and non-infringement, in relation to the contents,
                    materials,
                    products or other services published in any of its publications or available on the Website and
                    Mobile
                    App. BLoyalty does not warrant that the Website and Mobile App will be error-free, free of viruses,
                    bugs or other harmful components or access to the Website or Mobile App will be uninterrupted.
                    Members
                    are responsible to implement security measures in their computers or Mobile phone before accessing
                    the
                    Website or Mobile App. BLoyalty shall not be liable in any way for any direct, indirect, punitive,
                    incidental, consequential or other damages howsoever arising out of (i) the use of, or access to,
                    the
                    Website or Mobile App; or (ii) delay or inability to use or access the Website or Mobile App; or
                    (iii)
                    for any content, information, material, products or services published in, posted on, advertised in
                    or
                    obtained through BLoyalty's publications or the Website or Mobile App.</p>
                <p>A Member may apply for a new
                    BCard at selected participating merchants' outlets or create a virtual BCard via Mobile App if it
                    is
                    lost or stolen, subject to payment of any charges or such other terms, if applicable, imposed by
                    BLoyalty for replacement of card. The BPoints balance at the time the BCard was lost or stolen may
                    (at
                    the sole discretion of BLoyalty) be transferred to the new BCard upon request by member.</p>
                <p>Notification
                    of any matter in relation to the Program shall be deemed given to Members if it is made via any one
                    of
                    the methods below:<br>• by posting on the Website; or<br>• by sending an email to Members who have
                    provided
                    email address to BLoyalty; or<br>• by publication in a newspaper; or<br>• sending by ordinary post
                    to the
                    last known address of Members appearing in BLoyalty's records.</p>
                <p>If BLoyalty sells or transfers the
                    Program to another party, BLoyalty may transfer all of BLoyalty's rights and obligations under
                    these
                    Terms and Conditions without any consent from any Member. BLoyalty may further disclose and
                    transfer
                    all and any information and data which BLoyalty holds or which resides in the system of BLoyalty in
                    relation to the Members and all transactions made by Members, including purchase and redemption
                    transactions ("Information") to the new transferee, or disclose any such Information to a
                    prospective
                    new buyer. The Member hereby unconditionally and irrevocably agrees to such transfer and disclosure
                    of
                    the Information to the new transferee or the prospective new buyer.</p>
                <p>BLoyalty will only be liable to a
                    Member (and not any other third party) who suffers loss in connection with the Program arising from
                    BPoints being wrongly deducted or non-credit of BPoints entitled by a Member and in such a case,
                    BLoyalty's sole liability will be limited to crediting to the relevant Member's account such
                    BPoints
                    which have been wrongly deducted or should have been credited but were not. BLoyalty shall not be
                    responsible where: (i) there is no breach of a legal duty of care owed to such Member by BLoyalty
                    or by
                    any of BLoyalty's employees, staffs, authorized personnel or agents; or (ii) such loss or damage is
                    not
                    a reasonably foreseeable result of any such breach at the time BLoyalty enters into this agreement
                    with
                    such Member; or (iii) any increase in loss or damage resulting from breach by such Member of the
                    Program.</p>
                <p>BLoyalty and the Merchants are not responsible or liable to the Members for indirect,
                    consequential or economic losses, loss of profits, loss of opportunity or punitive damages of any
                    kind.</p>
                <p>BLoyalty may further establish rules, procedures and policies in relation to any matter regarding
                    the
                    Program, all of which shall form part of the Terms and Conditions. These Terms and Conditions as
                    set
                    out herein shall prevail in the event of any conflict or inconsistency with any other documents,
                    statements, rules, procedures, policies or communications, issued by BLoyalty, including FAQ,
                    advertising or promotional materials.</p>
                <p>Member confirms that he has read, understood and agreed to be
                    bound by the Privacy Notice of BLoyalty (which is available at www.binfinite.com.my), as may relate
                    to
                    the processing of his personal information. For the avoidance of doubt, Member agrees that the said
                    Privacy Notice shall be deemed to be incorporated by reference into this Terms & Conditions.</p>
                <p>These
                    Terms and Conditions are governed by the laws of Malaysia and Members shall submit to the exclusive
                    jurisdiction of the courts of Malaysia.</p>
                <p>If you have any enquiries please contact the Call Centre at
                    03-21418080 or email us at enquiry@binfinite.com.my. For any changes to the contact information, we
                    will update at our website www.binfinite.com.my from time to time.</p>
                <p>Some materials on the Website and
                    other BLoyalty promotional materials are the intellectual property of participating Merchants, or
                    other
                    third parties. Members have no right to use such intellectual property.</p>
                <p>Each exclusion or limitation of
                    liability in these Terms and Conditions shall also apply for the benefit of each of the
                    participating
                    Merchants and their employees or agents.</p>
            </div>
        </div>
    </div>
</section>

@endsection
