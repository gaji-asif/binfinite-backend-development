@extends('web.master')

@section('content')

<?php 
$collections = [
    'points-exchange' => [
            'name' => 'Convert to BPoints',
            'items' => [
                [
                    'partner' => 'mbb',
                    'image' => 'mbb-logo.png',
                    'point' => '3,000 POINTS = 1,000 BPOINTS',
                    'actionTitle' => 'convert',
                    'actionUrl' => 'javascript:void(0);',
                    'brandName' => 'Maybank Loyalty',
                    'fieldIdTitle' => 'MAYBANK LOYALTY ID',
                    'fieldIdName' => 'maybankLoyaltyNumber',
                ],
                [
                    'partner' => 'cimb',
                    'image' => 'cimb-logo.png',
                    'point' => '3,000 POINTS = 1,000 BPOINTS',
                    'actionTitle' => 'convert',
                    'actionUrl' => 'javascript:void(0);',
                    'brandName' => 'CIMB',
                    'fieldIdTitle' => 'CIMB POINTS ID',
                    'fieldIdName' => 'cimbPointsId',
                ],
                [
                    'partner' => 'aab',
                    'image' => 'aab-logo.png',
                    'point' => '3,000 POINTS = 1,000 BPOINTS',
                    'actionTitle' => 'convert',
                    'actionUrl' => 'javascript:void(0);',
                    'brandName' => 'AirAsia BIG',
                    'fieldIdTitle' => 'AIRASIA BIG MEMBER ID',
                    'fieldIdName' => 'bigMemberId',
                ],
                [
                    'partner' => 'kris_sg',
                    'image' => 'kris_sg-logo.png',
                    'point' => '3,000 POINTS = 1,000 BPOINTS',
                    'actionTitle' => 'convert',
                    'actionUrl' => 'javascript:void(0);',
                    'brandName' => 'KrisFlyer',
                    'fieldIdTitle' => 'KRISFLYER NUMBER',
                    'fieldIdName' => 'krisflyerNumber',
                ]
            ]
    ],
    'collect-points' => [
            'name' => 'Get the most out of B Infinite',
            'items' => [
                [
                    'ribbon' => 'Fasttrack your BPoints',
                    'image' => '8e4b3378e00274c3ddd2fac0129609dd.png',
                    'title' => 'Collect TRIPLE POINTS when you fill fuel at Caltex!',
                    'actionTitle' => 'LEARN MORE',
                ],
                [
                    'ribbon' => 'Collect Bonus Points',
                    'image' => 'marc-babin-334972-unsplash.png',
                    'title' => 'Look out for BONUS POINTS deals all around Tesco, in emails, deals and coupons',
                    'actionTitle' => 'VIEW ALL DEALS',
                ],
                [
                    'ribbon' => 'Be Rewarded',
                    'image' => 'as.png',
                    'title' => 'Reward your healthy habits, collect and redeem points with our health partners',
                    'actionTitle' => 'VIEW HEALTH PARTNERS',
                ]
            ]
    ]
];
?>

<section class="earn-points">
    <section class="content gray exchange">
        <div class="container">
            @isset($collections['points-exchange'])
            @include('web.components.earn_brand_collection', ['key' => 'points-exchange', 'collection' =>
            $collections['points-exchange'], 'isEnableViewAll' => false])
            @endisset

            <div class="button-container text-center">
                <a class="btn btn-outline-blue" href="/#">LOAD MORE</a>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container pt-2">
            @include('web.components.collect_points_collection', ['key' => 'collect-points', 'collection' =>
            $collections['collect-points']])
        </div>
        <div class="container py-5">
            @include('web.include.ads')
        </div>
    </section>
</section>

@include('web.include.dialog.convert_bpoints_dialog')

@endsection

@section('custom_js')
<script>
    $(document).ready(function () {
        $("[id^=unlink]").click(function () {
            $(this).closest(".card").children(".unlinked").addClass("active");
        });
    });

    function setupConversionForm(partner, brandName, fieldIdTitle, fieldIdName) {
        $("#partner").val(partner);
        $("#partner_name").val(brandName);

        $('brandName').text(brandName);
        $('#memberUID').text(fieldIdTitle);
        $('input#memberUIDNumber').attr('name', fieldIdName);
    }

</script>
@endsection
