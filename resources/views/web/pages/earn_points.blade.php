@extends('web.master')

@section('content')

<?php 
$collections = [
//    'earn-points' => [
//            'name' => 'Earn BPoints with these exciting brands',
//            'items' => [
//                [
//                    'partner' => 'caltex',
//                    'image' => '8e4b3378e00274c3ddd2fac0129609dd.png',
//                    'company' => 'Caltex',
//                    'title' => '3X Points When You Fill Fuel With Us!',
//                    'validDate' => '31st December 2018',
//                    'actionTitle' => 'Earn',
//                ],
//                [
//                    'partner' => 'berjaya',
//                    'image' => 'marc-babin-334972-unsplash.png',
//                    'company' => 'Berjaya GROUP',
//                    'title' => 'Complimentary Stay when you have a long offer line',
//                    'point' => 'RM1 = 1BPoint',
//                    'validDate' => '31st December 2018',
//                    'actionTitle' => 'Earn',
//                ],
//                [
//                    'partner' => 'zalora',
//                    'image' => '205bbd329dd1fbc81d918d670996c429.png',
//                    'company' => 'ZALORA',
//                    'title' => 'Spend at Zalora, get 2X Points for every RM spent!',
//                    'validDate' => '31st December 2018',
//                    'actionTitle' => 'Earn',
//                ]
//            ]
//    ],
//    'earn-brand' => [
//            'name' => 'Earn BPoints with our Partners',
//            'items' => [
//                [
//                    'partner' => 'rakuten',
//                    'image' => 'rkt.png',
//                    'point' => 'RM 1 = 1 BPOINT',
//                ],
//                [
//                    'partner' => '711',
//                    'image' => '711-logo.png',
//                    'point' => 'RM 1 = 1 BPOINT',
//                ],
//                [
//                    'partner' => '11street',
//                    'image' => '11street-logo.png',
//                    'point' => 'RM 1 = 1 BPOINT',
//                ],
//                [
//                    'partner' => 'aeon',
//                    'image' => 'e128d0669a09965286dc8ed12d47e885.png',
//                    'point' => 'RM 1 = 1 BPOINT',
//                ]
//            ]
//    ],
    'collect-points' => [
            'name' => 'Get the most out of B Infinite',
            'items' => [
                [
                    'ribbon' => 'Fasttrack your BPoints',
                    'image' => '8e4b3378e00274c3ddd2fac0129609dd.png',
                    'title' => 'Collect TRIPLE POINTS when you fill fuel at Caltex!',
                    'actionTitle' => 'LEARN MORE',
                ],
                [
                    'ribbon' => 'Collect Bonus Points',
                    'image' => 'marc-babin-334972-unsplash.png',
                    'title' => 'Look out for BONUS POINTS deals all around Tesco, in emails, deals and coupons',
                    'actionTitle' => 'VIEW ALL DEALS',
                ],
                [
                    'ribbon' => 'Be Rewarded',
                    'image' => 'as.png',
                    'title' => 'Reward your healthy habits, collect and redeem points with our health partners',
                    'actionTitle' => 'VIEW HEALTH PARTNERS',
                ]
            ]
    ]
];

//dd($merchants);
?>

<section class="earn-points">
    <section class="content gray-2">
        <div class="container">
            <div class="header row mx-0 mb-2">
                <span>Earn <span class="font-bold">BPoints</span></span>
            </div>

            <div class="row mx-0">
                <div class="col-lg-8">
                    <p>Our extensive network of Partners includes some of the largest household names, so that you can
                        collect
                        points on things you do everyday. There are so many other ways for you to collect and
                        accelerate
                        your
                        points, all you need is your B Infinite virtual card.</p>
                </div>
            </div>
        </div>
    </section>

    <section class="content2 menu">
        <div class="container">
            @include('web.include.category_icon_menu')
        </div>
    </section>

    <section class="content gray earn pb-5">
        <div class="container pt-5">
            @isset($offers)
            @include('web.components.partner_offers_collection', 
                ['key' => 'earn-points', 'collection' => ['name' => 'Earn BPoints with these exciting brands', 'items' => $offers]])
            @endisset
        </div>

        <div class="container">
            <div class="header row align-items-end justify-content-between pt-5">
                <span>Earn BPoints with our Partners</span>
                <a class="btn btn-outline-blue" href="/partners/trec/offer">VIEW ALL</a>
            </div>

            <div class="parent">
                @foreach ($merchants as $merchant)
                <div class="earn-brand">
                    <a href="{{ $merchant->conditionalUrl }}">
                        <div class="ribbon clearfix">
                            <div class="ribbon-terms">{{ $merchant->points_earning_listing }}</div>
                        </div>
                        <div class="overlay"></div>
                        <h3 style="{{ BH::backgroundImage($merchant->logo_image) }}"></h3>
                    </a>
                </div>
                {{-- <div class="earn-brand">
                    <a href="{{ route('partner_detail', '711') }}">
                <div class="ribbon clearfix">
                    <div class="ribbon-terms">RM 1 = 1 BPOINT</div>
                </div>
                <div class="overlay"></div>
                <h3 style="{{ BH::backgroundImage('711-logo.png') }}"></h3>
                </a>

            </div>
            <div class="earn-brand">
                <a href="{{ route('partner_detail', '11street') }}">
                    <div class="ribbon clearfix">
                        <div class="ribbon-terms">RM 1 = 1 BPOINT</div>
                    </div>
                    <div class="overlay"></div>
                    <h3 style="{{ BH::backgroundImage('11street-logo.png') }}"></h3>
                </a>
            </div>
            <div class="earn-brand">
                <a href="#">
                    <div class="ribbon clearfix">
                        <div class="ribbon-terms">RM 1 = 1 BPOINT</div>
                    </div>
                    <div class="overlay"></div>
                    <h3 style="{{ BH::backgroundImage('e128d0669a09965286dc8ed12d47e885.png') }}"></h3>
                </a>
            </div> --}}
            @endforeach
        </div>
        </div>
    </section>

    <section class="content">
        <div class="container pt-2">
            @include('web.components.collect_points_collection', ['key' => 'collect-points', 'collection' =>
            $collections['collect-points']])
        </div>
        <div class="container py-5">
            @include('web.include.ads')
        </div>
    </section>
</section>

@endsection

@section('custom_js')
<script>
    $('.single-slick').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        centerMode: true,
        variableWidth: true
    });

    $('.responsive').fadedSlick();

    $(document).ready(function () {
        $('#media').carousel({
            pause: true,
            interval: false,
        });
    });

</script>
@endsection
