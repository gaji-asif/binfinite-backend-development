@extends('web.master')

@section('content')

<section class="content profile privacy">
    <div class="container">
        <div class="row align-items-end justify-content-between">
            <span>Privacy Policy</span>
            <a class="btn-text-pink" href="javascript:void(0);">Bahasa Malaysia</a>
        </div>

        <div class="row pt-4">
            <div class="col">
                <p>Your privacy is important to us. This Privacy Policy explains our policy on collecting, using and
                    disclosing your Personal Information. The processing of your Personal Information is also subject
                    to applicable laws. The Personal Information which you provide to us now or from time to time or
                    have provided to us previously will be used and processed, and continued to be used and processed
                    by us, in accordance with this Privacy Policy (as amended, varied or revised from time to time). We
                    will take reasonable care and precaution to prevent the loss or misuse of your Personal
                    Information. We may amend, vary or revise this Privacy Policy from time to time. If we revise the
                    Privacy Policy, we will post the changes on our website or send a copy of the revised Privacy
                    Policy to you by post or email, in accordance to your preferred manner of receiving notices from
                    us.</p>
                <p>In this Privacy Policy, <span class="font-bold">"B Infinite Loyalty Program"</span> means the
                    customer loyalty program
                    operated by us.</p>
                <p><span class="font-bold">"Merchants"</span> means the participating
                    merchants of B Infinite Loyalty Program;</p>
                <p><span class="font-bold">"Personal Information"</span> means any information in our possession or
                    control that relates directly or
                    indirectly to you (or any other individual) to the extent that you (or such other individual) are
                    identified or identifiable from that information or from other information in our possession.</p>
                <p><span class="font-bold">"We",
                        "us"</span> or<span class="font-bold"> "our"</span> means BLoyalty Sdn Bhd (Company No.
                    154570-W).</p>
                <p>1. Personal Information</p>
                <p>1.1 What we
                    collect from you</p>
                <p>Personal Information we collect about you, include but is not limited, to the
                    following:-</p>
                <p>- personal details such as name, identity card number, age, gender, nationality, date
                    of birth, address, telephone number, email address and other details you provide when you apply to
                    be a member for B Infinite Loyalty Program<br>- information in connection with products and
                    services
                    you have purchased from Merchants or have registered your interest with us for purchase,
                    information, updates, inquiries, etc<br>- other information that are, have been or may be collected
                    by
                    us or Merchants, or which you provide to us or Merchants from time to time, in connection with any
                    service, transaction, contest, survey, promotion, questionnaire or communication with us relating
                    to B Infinite Loyalty Program or otherwise<br>- personal interests or preferences such as language,
                    product or content interests, communicating and marketing preferences<br>- username, password and
                    other identifiers when you register at our website</p>
                <p>Certain information requested by us are
                    mandatory which you must provide and agree to the processing of such mandatory information by us,
                    failing which, we will not be able to approve or continue your membership with B Infinite Loyalty
                    Program or provide you information, updates, products or services you requested or relating to the
                    B Infinite Loyalty Program.</p>
                <p>1.2 How your Personal Information is collected</p>
                <p>- when you apply to be a
                    member of B Infinite Loyalty Program or register your interest with us to obtain information and
                    updates on any products and services<br>- when you visit our website or social networking sites<br>-
                    when
                    you enter contests or respond to surveys, promotions or questionnaires from our Merchants<br>- when
                    you make any transaction at any of Merchants' outlet pursuant to any transaction or inquiry or
                    communication made with or to us</p>
                <p>1.3 Purpose of collecting and processing your Personal Information</p>
                <p>- to communicate with you<br>- to process your application, inquiry or request<br>- to provide the
                    products and services relating to the B Infinite Loyalty Program or such other goods and services
                    you requested<br>- for administration of B Infinite Loyalty Program<br>- to respond to your queries<br>-
                    to
                    conduct surveys, research and statistical analysis<br>- to help us monitor and improve our services<br>-
                    to comply with applicable laws, proceedings or inquiries from regulatory authorities, and
                    enforcement agencies<br>- any other purposes permitted by applicable laws<br>- to market and
                    promote
                    other products and services that are or may be offered by us, our partners, Merchants or sponsors
                    from time to time for direct marketing purposes</p>
                <p>to provide you with information and updates on B
                    Infinite Loyalty Program or products and services requested</p>
                <p>1.4 Sensitive Personal Data</p>
                <p>- We do not
                    collect Sensitive Personal Data from you unless required by applicable laws. If you do not want
                    your Sensitive Personal Data collected by us, please do not submit it.<br>- "Sensitive Personal
                    Data"
                    means any personal data consisting of information as to physical or mental health or condition,
                    political opinions, religious beliefs or other beliefs of a similar nature, the commission or
                    alleged commission of an offence or any personal data prescribed under law as sensitive personal
                    data.<br>- If you do choose to submit your Sensitive Personal Data to us, such data shall then be
                    deemed to be and will be treated by us as Personal Information which will be subject to the terms
                    and scope of this Privacy Policy including as to processing, usage and disclosure thereof.</p>
                <p>2
                    Disclosure</p>
                <p>To facilitate the purposes above and the provision of products and services to you, we
                    may process, use and disclose your Personal Information to the following classes of third parties:
                    -</p>
                <p>- regulatory authorities and enforcement agencies<br>- any court of law or any relevant party in
                    connection with any claim or legal proceedings<br>- our contractors, service providers,
                    consultants,
                    auditors and advisors on a need to know basis<br>- our Merchants, partners and sponsors<br>- any
                    actual
                    or potential assignee, transferee or acquirer of our company or business</p>
                <p>3 Transfer of Personal
                    Information outside Malaysia</p>
                <p>Your Personal Information may be transferred to a country outside
                    Malaysia. You hereby consent to us transferring your Personal Information outside of Malaysia,
                    including to the class of third parties set out above.</p>
                <p>4 Access or Update your Personal Information</p>
                <p>4.1 You are responsible for providing accurate and complete information about yourself and any
                    other person whose personal information you provide us (for e.g., your spouse, children, relatives
                    or third parties), and as such, as and when such information becomes incorrect or outdated you
                    should correct or update such information by contacting us or submitting fresh information.</p>
                <p>4.2 You
                    may, by writing to us at the address set out in Section 6 below:</p>
                <p>request for a copy of the Personal
                    Information we hold about you request to rectify or update your Personal Information Please provide
                    proof of your identity, address and sufficient information to enable us to identify you when
                    submitting your request to us.</p>
                <p>4.3 On receipt of your written request, we may:</p>
                <p>comply or refuse
                    your request to access or rectify your Personal Information. If we refuse, we will notify you the
                    reasons for such refusal charge a fee for processing your request for access or rectification.</p>
                <p>5
                    Limit the process of your Personal Information</p>
                <p>By giving or making available your Personal
                    Information or using our website, you expressly agree and consent to the use and process of your
                    Personal Information in accordance to this Privacy Policy, including but not limited to, consenting
                    to receiving communications, offers, promotions and marketing materials from us from time to time.
                    If you:</p>
                <p>- wish to opt out from receiving offers, promotions and marketing materials and information
                    on other products and services from us our partners, Merchants, or sponsors<br>- or would like us
                    to
                    cease processing your Personal Information for a specified purpose or in a specified manner</p>
                <p>please
                    notify us in writing to the address set out in Section 6 below. In the absence of such written
                    notification from you, we will deem it that you consent to the processing, usage and dissemination
                    of your Personal Information in the manner, to the extent and within the scope of this Privacy
                    Policy. If you do not consent to the processing of your Personal Information as above, we may not
                    be able to approve or continue your membership with B Infinite Loyalty Program or provide you
                    information, updates, products or services you requested or relating to the B Infinite Loyalty
                    Program.</p>
                <p>6 How to Contact Us</p>
                <p>Write in to us at:<br>BLoyalty Sdn Bhd<br>16th Floor,<br>Office Block Plaza
                    Berjaya,<br>12 Jalan Imbi, 55100 Kuala Lumpur<br>Email: enquiry@bcard.com.my<br>Contact us at:
                    03-2141 8080</p>

                <p class="pt-4"><span class="font-bold">NOTE: In the event of discrepancies or inconsistencies between
                        the English
                        language version and
                        Bahasa Malaysia version of this Privacy Policy, the English language version shall prevail.</span></p>
            </div>
        </div>
    </div>
</section>

@endsection
