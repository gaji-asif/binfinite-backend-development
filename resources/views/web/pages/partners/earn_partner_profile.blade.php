@extends('web.master')

@section('content')

<?php
//$partners = [
//    'name' => 'Offers from Caltex',
//    'company' => 'Caltex',
//    'items' => [
//        'caltex' => [
//            'name' => 'Caltex',
//            'logoImage' => asset('images/608673846f1f8c90eca0b0af9afc88b1.png'),
//            // 'bannerImage1' => asset('images/837dc1c799eebd9356a9760289982dd9.png'),
//            // 'bannerImage2' => asset('images/rkt2.png'),
//            // 'bannerImage3' => asset('images/rkt2.png'),
//            'ribbon' => 'EARN 3 BPOINTS FOR EVERY RM 1',
//            'image' => '837dc1c799eebd9356a9760289982dd9.png',
//            'title' => '3X Points When You Fill Fuel With Us!',
//            'point' => 'RM1 = 1BPoint',
//            'validDate' => '31st December 2018',
//            'actionTitle' => 'REDEEM',
//            
//                ],
//        'berjaya' => [
//            'name' => 'Berjaya Group',
//            'logoImage' => asset('images/711-logo.png'),
//            'bannerImage1' => asset('images/rkt1.png'),
//            'bannerImage2' => asset('images/711-banner.jpg'),
//            'bannerImage3' => asset('images/rkt2.png'),
//            'ribbon' => 'REDEEM 30 POINTS!',
//            'image' => 'tt.png',
//            'title' => 'Example of online redemption headline here',
//            'point' => 'RM1 = 1BPoint',
//            'validDate' => '31st December 2018',
//            'actionTitle' => 'REDEEM',
//            'bio' => '<p>7-Eleven Malaysia Holdings Berhad operates 7-Eleven stores throughout Malaysia.</p>
//    <p>The 7-Eleven franchise is the single largest convenience store chain in Malaysia with more than 2,000 stores nationwide serving over 900,000 customers daily.</p>
//    <p>This means that B Infinite\'s Members have the opportunity to earn points within this extensive store network.</p>
//    <p>Please note that point collection and point redemption are excluded for purchases of tobacco, tobacco-related accessories, instore-services and reload services. Other than these items, simply present your BCard when making purchases to earn your reward points. You may redeem your favourite item with your BPoints here.</p>'
//        ],
//        'zalora' => [
//            'name' => 'ZALORA',
//            'logoImage' => asset('images/205bbd329dd1fbc81d918d670996c429.png'),
//            'bannerImage1' => asset('images/rkt1.png'),
//            'bannerImage2' => asset('images/11street-banner.png'),
//            'bannerImage3' => asset('images/rkt2.png'),
//            'ribbon' => 'PURCHASE VOUCHERS AND GET 50 BPOINTS!',
//            'image' => '837dc1c799eebd9356a9760289982dd9.png',
//            'title' => 'Example of offers that lead to Shop Online',
//            'point' => 'RM1 = 1BPoint',
//            'validDate' => '31st December 2018',
//            'actionTitle' => 'SHOP NOW',
//            'bio' => '<p>11street.my is Malaysia&rsquo;s one stop open marketplace online store.</p>
//    <p>Adapted from Korea&rsquo;s best e-commerce website 11st.co.kr, Celcom and SK Planet has teamed up to bring Malaysians one of top platforms in the business.</p>
//    <p>Malaysians can browse a range of products and services from all over the world. Attractive deals and prices await!</p>
//    <p>Collect BPoints when purchasing at 11street\'s Shocking Deals section.</p>
//    <p>Providing Malaysian buyers and sellers with the safest escrow service, you can buy with peace of mind at 11street.my. There is also a Trust &amp; Safety policy which helps to prevent illegitimate transactions, products and members.</p>'
//        ],
//    ],
//];
//
//$partnerList = $partners['items'];
//$partner = $partnerList[$partnerSlug];
?>

<section class="earn-partner">
    <div class="container-fluid">
        @include('web.include.hero_slider')
    </div>
    <div class="hero">
        <div class="ribbon clearfix">
            <div class="ribbon-amount">{{ $partner->points_earning_profile }}&nbsp;</div>
            <div class="ribbon-qualify">{{ $partner->points_redeem_profile }}&nbsp;</div>
        </div>
    </div>

    <section class="content gray py-5">
        <div class="container">
            @isset($collection)

            @include('web.components.partner_offers_collection', ['key' => 'earn-points', 'isShowViewAll' => false, 'collection' => $collection])

            @endisset
        </div>
        </div>
    </section>

    <section class="content">
        <div class="container partner-about">
            <div class="partner_logo">
                <img style="max-width: 500px; max-height: 400px" src="{{ $partner->logo_image }}" />
            </div>

            <span class="partner-title">About {{ $partner->name }}</span>
            <div class="partner-bio">
                {!! $partner->about_html !!}
            </div>
        </div>
    </section>

    <section class="content gray">
        <div class="container">
            @if($partner->terms_html)
            <div id="accordion" class="earn-partner-cards" role="tablist" aria-multiselectable="true">
                <div class="card">
                    <h5 class="card-header" role="tab" id="headingOne">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false"
                            aria-controls="collapseOne" class="collapsed d-block">Terms and Conditions
                        </a>
                    </h5>

                    <div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="card-body">
                            {!! $partner->terms_html !!}
                        </div>
                    </div>
                </div>
            </div>
            <div style="margin-top: 20px"></div>
            @endif
        </div>
    </section>
</section>

@endsection

@section('custom_js')
<script>
    $('.multi-item-carousel .carousel-item').each(function () {
        var next = $(this).next();
        if (!next.length) next = $(this).siblings(':first');
        next.children(':first-child').clone().appendTo($(this));
    });
    $('.multi-item-carousel .carousel-item').each(function () {
        var prev = $(this).prev();
        if (!prev.length) prev = $(this).siblings(':last');
        prev.children(':nth-last-child(2)').clone().prependTo($(this));
    });

</script>
@endsection
