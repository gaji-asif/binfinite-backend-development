@extends('web.master')

@section('content')

<?php 
//$partner = [
//    'partner_hero_image' => 'https://via.placeholder.com/1168x740?text=TREC_hero_image',
//    'company' => 'TREC',
//    'points_earning_profile' => 'Earn 1 BPoint for every RM 1 spent',
//    'logo_image' => 'https://via.placeholder.com/522x125?text=TREC_logo',
//    'description_html' => '<h1>Spend RM50 and above at TREC’s participating outlets and earn 2,000 BPoints*</h1>
//    <p>+ 3 lucky winners stand a chance to win a pair of Traveloka flight voucher with RM1,000</p>
//    <br>
//    <p>Valid Until: 31 December 2018</p>
//',
//    'about_html' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla in diam augue. Nullam bibendum turpis
//    egestas, congue elit eget, sagittis risus. Nam at dui a neque facilisis consequat. Nunc eu orci magna.
//    Praesent congue elementum tristique. Phasellus rhoncus convallis varius. Aenean eu risus in tortor
//    mattis lobortis eu aliquam quam. Etiam imperdiet varius faucibus. Donec quis consectetur lorem.
//    Suspendisse non lacus vitae ligula fringilla congue sed sodales magna. Cras risus nisi, bibendum quis
//    accumsan varius, suscipit quis urna. In bibendum velit ut risus elementum varius. Aliquam erat
//    volutpat. Duis sit amet condimentum leo. Integer sed eleifend dui. Quisque quis sapien ipsum. 3.
//    Vestibulum ut rutrum arcu, ac pretium libero. Nullam turpis nisl, porttitor ut tempor in, dignissim ut
//    elit. Donec id finibus ipsum. Aenean leo ligula, posuere in fermentum in, ultricies quis urna. Cras ut
//    tincidunt erat. Duis vitae urna dictum, porta turpis nec, interdum lacus. 4. Proin sapien odio,
//    vehicula ac porttitor ut, accumsan in nisi. Praesent nulla felis, accumsan dapibus leo in, egestas
//    aliquam ex. Curabitur ante risus, tincidunt at ligula at, congue consectetur ligula. In sit amet
//    lobortis tortor. Donec eget porta ligula. 5. Praesent vel augue tellus. Integer mollis nulla sit amet
//    magna convallis pellentesque. Suspendisse ac imperdiet justo. Nam id iaculis tortor, sed ullamcorper
//    velit.
//
//    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla in diam augue. Nullam bibendum turpis
//    egestas, congue elit eget, sagittis risus. Nam at dui a neque facilisis consequat. Nunc eu orci magna.
//    Praesent congue elementum tristique. Phasellus rhoncus convallis varius. Aenean eu risus in tortor
//    mattis lobortis eu aliquam quam. Etiam imperdiet varius faucibus. Donec quis consectetur lorem.
//    Suspendisse non lacus vitae ligula fringilla congue sed sodales magna. Cras risus nisi, bibendum quis
//    accumsan varius, suscipit quis urna. In bibendum velit ut risus elementum varius. Aliquam erat
//    volutpat. Duis sit amet condimentum leo. Integer sed eleifend dui. Quisque quis sapien ipsum. 3.
//    Vestibulum ut rutrum arcu, ac pretium libero. Nullam turpis nisl, porttitor ut tempor in, dignissim ut
//    elit. Donec id finibus ipsum. Aenean leo ligula, posuere in fermentum in, ultricies quis urna. Cras ut
//    tincidunt erat. Duis vitae urna dictum, porta turpis nec, interdum lacus. 4. Proin sapien odio,
//    vehicula ac porttitor ut, accumsan in nisi. Praesent nulla felis, accumsan dapibus leo in, egestas
//    aliquam ex. Curabitur ante risus, tincidunt at ligula at, congue consectetur ligula. In sit amet
//    lobortis tortor. Donec eget porta ligula. 5. Praesent vel augue tellus. Integer mollis nulla sit amet
//    magna convallis pellentesque. Suspendisse ac imperdiet justo. Nam id iaculis tortor, sed ullamcorper
//    velit.'
//,
//    'terms_html' => 'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad
//    squid. 3 wolf
//    moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt
//    laborum eiusmod.
//    Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla
//    assumenda
//    shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt
//    sapiente ea
//    proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table,
//    raw denim
//    aesthetic synth nesciunt you probably haven\'t heard of them accusamus labore sustainable
//    VHS.
//',
//];

//$outlets = [
//    'name' => 'Participating Outlets',
//    'company' => 'TREC',
//    'isMoreButtonUrl' => '/partners/zalora/offer',
//    'items' => [
//        '1' => [
//            'image' => 'gg.png',
//            'title' => 'Gasoline KL',
//            'bio_html' => 'T: +6012 372 2374<br>E: gasolineKL@gmail.com'
//        ],
//        '2' => [
//            'image' => 'gg.png',
//            'title' => 'The Iron Fairies KL',
//            'bio_html' => 'T: +6012 372 2374<br>E: gasolineKL@gmail.com'
//        ],
//        '3' => [
//            'image' => 'gg.png',
//            'title' => 'DZH Seafood Saunaboat',
//            'bio_html' => 'T: +6012 372 2374<br>E: gasolineKL@gmail.com'
//        ],
//        '4' => [
//            'image' => 'gg.png',
//            'title' => 'The Mojito Man',
//            'bio_html' => 'T: +6012 372 2374<br>E: gasolineKL@gmail.com'
//        ],
//        '5' => [
//            'image' => 'gg.png',
//            'title' => 'The Owls Cafe',
//            'bio_html' => 'T: +6012 372 2374<br>E: gasolineKL@gmail.com'
//        ],
//        '6' => [
//            'image' => 'gg.png',
//            'title' => 'Donkey and Crow Irish Pub',
//            'bio_html' => 'T: +6012 372 2374<br>E: gasolineKL@gmail.com'
//        ],
//        '7' => [
//            'image' => 'gg.png',
//            'title' => 'Meja Kitchen and Bar',
//            'bio_html' => 'T: +6012 372 2374<br>E: gasolineKL@gmail.com'
//        ],
//        '8' => [
//            'image' => 'gg.png',
//            'title' => 'RD90 Dining and Bar',
//            'bio_html' => 'T: +6012 372 2374<br>E: gasolineKL@gmail.com'
//        ],
//        '9' => [
//            'image' => 'gg.png',
//            'title' => 'Le Noir KL',
//            'bio_html' => 'T: +6012 372 2374<br>E: gasolineKL@gmail.com'
//        ],
//    ],
//];
?>

<section class="earn-partner">
    <div class="hero">
        <div class="overlay"></div>
        <img class="fullwidth-image" src="{{ $partner->profile_banner_image }}">
        <div class="container">
            <div class=""></div>
        </div>

        <div class="ribbon clearfix">
            <div class="ribbon-amount-only">{{ $partner->points_earning_profile }}&nbsp;</div>
        </div>
    </div>

    <section class="white-content">
        <div class="container">
            <div class="partner-intro p-0">
                <img style="max-width: 500px; max-height: 400px" src="{{ $partner->logo_image }}" />
                <div class="partner-description">
                    {!! $offer->offer_subtext !!}
                </div>
            </div>
        </div>
    </section>

    @if(isset($collection) && sizeOf($collection['items']) > 0)
    <section class="content gray py-5">
        <div class="container">
            @include('web.components.partner_offers_collection', ['key' => 'outlets', 'isShowViewAll' => false, 'collection'
            =>
            $collection])

        </div>
    </section>
    @endif

    <section class="content pt-4">
        <div class="container partner-about">
            <span class="partner-title">About {{ $partner->name }}</span>
            <div class="partner-bio">
                {!! $partner->about_html !!}
            </div>
        </div>
    </section>

    <section class="content gray">
        <div class="container">
            @if($offer->terms_html)
            <div id="accordion" class="earn-partner-cards" role="tablist" aria-multiselectable="true">
                <div class="card">
                    <h5 class="card-header" role="tab" id="headingOne">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false"
                            aria-controls="collapseOne" class="collapsed d-block">Terms and Conditions
                        </a>
                    </h5>

                    <div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="card-body">
                            {!! $offer->terms_html !!}
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </section>
</section>

@endsection
