@extends('web.master')

@section('content')

<section class="earn-partner">
    <div class="hero">
        <div class="overlay"></div>
        {{-- <h3 style="{{ BH::backgroundImage('rkt1.png') }}"></h3> --}}
        <img class="fullwidth-image" src="{{ $partner->profile_banner_image }}">
        <div class="container">
            <div class=""></div>
        </div>

        <div class="ribbon clearfix">
            <div class="ribbon-amount">{{ $partner->points_earning_profile }}&nbsp;</div>
            <div class="ribbon-qualify">{{ $partner->points_redeem_profile }}&nbsp;</div>
        </div>
    </div>

    <section class="white-content">
        <div class="container">
            <div class="partner-intro">
                <img style="max-width: 500px; max-height: 400px" src="{{ $partner->logo_image }}" />
                <div class="partner-description">
                    {!! $partner->description_html !!}
                </div>
            </div>
        </div>
    </section>

    <div class="hero">
        <div class="overlay"></div>
        <img class="fullwidth-image" src="{{ $partner->profile_footer_banner_image }}">
        {{-- <h3 style="{{ BH::backgroundImage('rkt2.png') }}"></h3> --}}
    </div>

    <section class="content">
        <div class="container partner-about">
            @if($partner->about_html)
            <span class="partner-title">About {{ $partner->name }}</span>
            <div class="partner-bio">
                {!! $partner->about_html !!}
            </div>
            @endif

            @if($partner->terms_html)
            <div id="accordion" class="earn-partner-cards" role="tablist" aria-multiselectable="true">
                <div class="card">
                    <h5 class="card-header" role="tab" id="headingOne">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false"
                            aria-controls="collapseOne" class="collapsed d-block">Terms and Conditions
                        </a>
                    </h5>

                    <div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="card-body">
                            {!! $partner->terms_html !!}
                        </div>
                    </div>
                </div>
            </div>
            <div style="margin-top: 20px"></div>
            @endif
        </div>
    </section>
</section>

@endsection
