<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Demo shop</title>
    <script src="https://test.wirecard.com.sg/engine/hpp/paymentPageLoader.js" type="text/javascript"></script>

    <style>
      body {
        font-family: "Courier New", Courier, monospace;
      }
      .container{
        margin:0 auto;
        padding-top:30px;
        width:500px;
      }
    </style>
  </head>
  <body>
    <div class="container">
        <h1>Order Summary</h1>
        <p>Order ID: {{$order->invoice_no}}</p>
        <p>Full Name: {{$order->full_name}}</p>
        <p>Email: {{$user->email}}</p>
        <p>Price: RM {{format_money($order->net_amount)}}</p>
        <p>SST: RM {{format_money($order->sst_amount)}}</p>
        <p>Total: RM {{format_money($order->amount)}}</p>
        <p>
            <input id="elastic_pay_btn" type="button" onclick="pay()" value="Pay Now"/>
        </p>
    </div>
    
    <script type="text/javascript">
      function pay() {
        var requestedData = {
            merchant_account_id: "{{ $data['merchant_account_id'] }}",
            request_id: "{{ $data['request_id'] }}",
            request_time_stamp: "{{ $data['request_time_stamp'] }}",
            payment_method: "creditcard",
            transaction_type: "{{ $data['transaction_type'] }}",
            requested_amount: "{{ $data['requested_amount'] }}",
            requested_amount_currency: "{{ $data['requested_amount_currency'] }}",
            locale: "en",
            request_signature: "{{ $data['signature'] }}",
            redirect_url: "{{$data['redirect_url']}}",
            //ip_address: " $data['ip_address'] ",
            //first_name: "John",
            //last_name: "Doe",
        }

        ElasticPaymentPage.embeddedPay(requestedData);
      }
    </script>
  </body>
</html>