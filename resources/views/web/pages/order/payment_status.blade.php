<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Payment Status</title>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  </head>
  <body>
    <div class="container">
        <table class="table">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Invoice No</th>
              <th scope="col">Credit Card</th>
              <th scope="col">Completion Date Time</th>
              <th scope="col">Amount</th>
              <th scope="col">Transaction Status</th>
            </tr>
          </thead>
          <tbody>
            @foreach($results as $result)
            <tr>
              <th scope="row">{{$result->id}}</th>
              <td>{{$result->invoice_no}}</td>
              <td>{{$result->masked_account_number}}</td>
              <td>{{$result->completion_time_stamp}}</td>
              <td>RM {{format_money($result->amount)}}</td>
              <td>{{$result->transaction_state}}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
    </div>

  </body>
</html>