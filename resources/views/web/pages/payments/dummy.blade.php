<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Demo shop</title>
    <script src="https://test.wirecard.com.sg/engine/hpp/paymentPageLoader.js" type="text/javascript"></script>

    <style>
      body {
        font-family: "Courier New", Courier, monospace;
      }
    </style>
  </head>
  <body>

    <b>Parameters:</b> <br/>
    merchant_account_id: {{ $data['merchant_account_id'] }} <br/>
    request_id: {{ $data['request_id'] }} <br/>
    request_time_stamp: {{ $data['request_time_stamp'] }} <br/>
    payment_method: creditcard <br/>
    transaction_type: {{ $data['transaction_type'] }} <br/>
    requested_amount: {{ $data['requested_amount'] }} <br/>
    requested_amount_currency: {{ $data['requested_amount_currency'] }} <br/>
    locale: en <br/>
    redirect_url: {{ $data['redirect_url'] }} <br/>
    request_signature: {{ $signature }} <br/>
    <!-- ip_address:  $data['ip_address']  <br/ -->
    first_name: John <br/>
    last_name: Doe <br/><br/>

    <b>Pre SHA-256 string</b><br/>
    {{ $signatureSourceString }}<br/><br/>
    <b>SHA-256 signature</b><br/>
    {{ $signature }}<br/>


    <input id="elastic_pay_btn" type="button" onclick="pay()" value="Pay Now"/>
    <script type="text/javascript">
      function pay() {
        var requestedData = {
          merchant_account_id: "{{ $data['merchant_account_id'] }}",
          request_id: "{{ $data['request_id'] }}",
          request_time_stamp: "{{ $data['request_time_stamp'] }}",
          payment_method: "creditcard",
          transaction_type: "{{ $data['transaction_type'] }}",
          requested_amount: "{{ $data['requested_amount'] }}",
          requested_amount_currency: "{{ $data['requested_amount_currency'] }}",
          locale: "en",
          request_signature: "{{ $signature }}",
          redirect_url: "{{$data['redirect_url']}}",
          //ip_address: " $data['ip_address'] ",
          first_name: "John",
          last_name: "Doe",
        }

        ElasticPaymentPage.embeddedPay(requestedData);
      }
    </script>
  </body>
</html>