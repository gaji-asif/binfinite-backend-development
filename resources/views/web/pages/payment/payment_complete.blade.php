@extends('web.master')

@section('content')

<section class="payment_complete gray">
    <div class="container pt-5">
        <div class="gift_element">
            <img src="/images/8.0 Payment/payment_complete_gift.png">
        </div>
        <div class="col-sm-12 pt-5">
            <h2>Congratulations!</h2>
            <h5>Please collect your items at</h5>
            <p>BLoyalty Sdn Bhd<br>16th Floor, Menara Cosway Plaza Berjaya,<br>12 Jalan Imbi, 55100 Kuala Lumpur.</p>
            <p>Give us a call<br>03 2141 8080</p>
            <p>You can call us during our working hours<br>Monday - Friday from 9am to 6pm<br>excluding public holidays)</p>
        </div>
        <div class="col-sm-12 pt-3 pb-5">
            <a class="btn btn-pink partner-login" href="#">OK! Back to main website</a>
        </div>
    </div>

</section>

@endsection @section('custom_js')
<script>

</script>
@endsection
