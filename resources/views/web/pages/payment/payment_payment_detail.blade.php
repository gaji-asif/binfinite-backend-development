@extends('web.master')

@section('content')

<?php
$default_value = [
    'bpoint' => "15,000",
    'bprice' => "1,421.00"
];
?>
<section class="payment_detail gray">
        <div class="container shipping_info">
            <div class="row">
                <div class="col-sm-6 pt-5 pb-5">
                    <h1>Shipping Information</h1>
                    <p class="pt-4">Ryan Gosling<br>13-A-09, Seni Mont Kiara<br>Jalan 19/70A, Desa Sri Hartamas<br>50480 Kuala Lumpur</p>
                    <p class="pt-4">012 407 7044<br>ryangosling@gmail.com</p>
                    <a class="pt-4 pink_title" href="#"><h5>Edit Information</h5></a>

                    <h1 class="pt-5">Shipping Method</h1>
                    <div class="mt-5 p-4 col-xl-8 custom-control custom-radio radio_holder_t1">
                        <input type="radio" class="custom-control-input" id="traditionalDelivery" name="groupOfDelivery" value="traditionalDelivery">
                        <label class="custom-control-label ml-4" for="traditionalDelivery">
                            <p>Traditional Delivery<br><br>
                            Estimated 30-40 day shipping; its totally free with no hidden fees</p>
                        </label>
                    </div>
                    <div class="mt-3 p-4 col-xl-8 custom-control custom-radio radio_holder_t1">
                      <input type="radio" class="custom-control-input" id="fastDelivery" name="groupOfDelivery" checked value="fastDelivery">
                        <label class="custom-control-label ml-4" for="fastDelivery">
                            <p>Fast Delivery<br><br>
                            Estimated 5-7 day shipping; it costs RM40.00 with no hidden fees.</p>
                        </label>
                    </div>
                </div>
                <div class="col-sm-6 white p-4 pt-5 pb-5">
                    <h5 class="blue_title">Shopping Cart</h5>
                    <div class="row mx-0 mt-3 line"></div>
                    <div class="row mt-5">
                        <div class="col-lg-3 col-sm-8">
                            <img src="/images/8.0 Payment/product_item.jpg">
                        </div>
                        <div class="col-lg-5">
                            <p>Ray Ban Shield Sunglasses<br>Longer Description up to three lines<br>Black</p>
                            <p class="blue_p">RM173.00 or 10,000 BPoints</p>
                        </div>
                        <div class="number_holder p-4 col-lg-4">
                            <input class="number form-control" type="text" value="1" min="1" max="10" style="text-align: center;">
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-lg-3 col-sm-8">
                            <img src="/images/8.0 Payment/product_item.jpg">
                        </div>
                        <div class="col-lg-5">
                            <p>Ray Ban Shield Sunglasses<br>Longer Description up to three lines<br>Black</p>
                            <p class="blue_p">RM173.00 or 10,000 BPoints</p>
                        </div>
                        <div class="number_holder p-4 col-lg-4">
                            <input class="number form-control" type="text" value="1" min="1" max="10" style="text-align: center;">
                        </div>
                    </div>
                    <div class="row mx-0 mt-3 line"></div>
                    <div class="row mt-4">
                        <div class="col-lg-12">
                            <p>Subtotal (2 items) <span class="blue_h5">RM346.00 or 20,000 BPoints</span></p>
                            <p>Shipping <span class="blue_h5">Free</span></p>
                            <p>Taxes (6%) <span class="blue_h5">RM14.16</span></p>
                        </div>
                    </div>
                    <div class="row mx-0 mt-3 line2"></div>
                    <div class="row mt-4">
                        <div class="col-lg-12">
                            <p>Total <span class="blue_h5 blue_h5_bold">RM132.16 or 21,000 Bpoints</span></p>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-lg-12">
                            <p class="normal_p">*You will get to choose how to pay or redeem in the next step</p>
                        </div>
                    </div>
                    <div class="row mt-4 mb-5">
                        <div class="col-lg-12">
                            <div class="btn btn-pink d-inline-flex btn_next">CONTINUE</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container payment_method">
            <div class="row">
                <div class="col-lg-6 pt-5 pb-5">
                    <h1>Select your payment method(s)</h1>
                    <p class="pt-2">You can choose to redeem and one additional payment method.</p>
                    <div class="row mt-4 col-lg-12 method_holder white">
                        <div class="p-4 pt-5  col-lg-12 custom-control custom-radio radio_holder_t2">
                            <input type="radio" class="custom-control-input" id="payWithBPoint" name="groupOfPayWith" checked value="payWithBPoint">
                            <label class="custom-control-label ml-4 pr-4" for="payWithBPoint">
                                <img class="card" src="/images/8.0 Payment/b_card.png">
                                <h5>Redeem with your BPoints</h5>
                                <p class="normal_p gray_p">Use your default card points to redeem your purchase!</p>
                            </label>
                        </div>
                        <div class="p-2 pt-4 pb-4 col-lg-11 col-sm-11 col-11  payWith payWithBPoint">
                            <div class="range_holder range2">
                                <div class="point_holder">
                                    <label>Points</label>
                                    <input class="point" type="text" disabled>
                                </div>
                                <div class="main_slider"></div>
                                <div class="pay_holder">
                                    <label>Pay</label>
                                    <input class="pay" type="text" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="p-4 pt-5 col-lg-12 custom-control custom-radio radio_holder_t2">
                            <input type="radio" class="custom-control-input" id="payWithOnlineBanking" name="groupOfPayWith" value="payWithOnlineBanking">
                            <label class="custom-control-label ml-4 pr-4" for="payWithOnlineBanking">
                                <img class="card" src="/images/8.0 Payment/b_card.png">
                                <h5>Online Banking</h5>
                                <p class="normal_p gray_p">Use your local bank account to complete your purchase</p>
                            </label>
                        </div>
                        <div class="p-4 pt-5 col-lg-12 custom-control custom-radio radio_holder_t2">
                            <input type="radio" class="custom-control-input" id="payWithCreditCard" name="groupOfPayWith" value="payWithCreditCard">
                            <label class="custom-control-label ml-4 pr-4" for="payWithCreditCard">
                                <img class="card" src="/images/8.0 Payment/visa_card.png">
                                <img class="card" src="/images/8.0 Payment/master_card.png">
                                <img class="card" src="/images/8.0 Payment/amex_card.png">
                                <h5>Credit Card</h5>
                                <p class="normal_p gray_p">B Infinite accepts all major credit and debit cards</p>
                            </label>
                        </div>
                        <div class="p-2 pt-4 pb-4 col-lg-11 col-sm-11 col-11 payWith payWithCreditCard">
                            <div class="row">
                                <div class="col-lg-12">
                                    <label>CARD NUMBER</label>
                                    <input type="text" class="col-lg-12 credit-card-number" name="number" inputmode="numeric" autocomplete="cc-number" autocompletetype="cc-number" x-autocompletetype="cc-number" placeholder="0000 0000 0000 0000">
                                </div>
                                <div class="col-lg-6 pt-3">
                                    <label>CVC<span class="glyphicon-question-sign">&#63;</span></label>
                                    <input type="text" class="col-lg-12 security-code" inputmode="numeric" type="text" name="security-code">
                                </div>
                                <div class="col-lg-6 pt-3">
                                    <label>EXPIRY DATE</label>
                                    <input type="text" class="col-lg-12 expiration-month-and-year" type="text" name="expiration-month-and-year" placeholder="MM / YY">
                                </div>
                                <div class="col-lg-12 pt-3">
                                    <label>CARD HOLDER</label>
                                    <span class="checkmark">
                                        <div class="checkmark_circle"></div>
                                        <div class="checkmark_stem"></div>
                                        <div class="checkmark_kick"></div>
                                    </span>
                                    <input type="text" class="col-lg-12 credit-card-name greenHighlight" type="text" name="name" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 white p-4 pt-5 pb-5 side">
                    <h5 class="blue_title">Summary</h5>
                    <div class="row mt-4">
                        <div class="col-lg-12">
                            <p>Subtotal in RM <span class="blue_h5">RM346.00</span></p>
                            <p>Subtotal in BPoints <span class="blue_h5">10,000</span></p>
                            <p>Shipping <span class="blue_h5">Free</span></p>
                            <p>Taxes (6%) <span class="blue_h5">RM14.16</span></p>
                        </div>
                    </div>
                    <div class="row mx-0 mt-3 line2"></div>
                    <div class="row mt-4">
                        <div class="col-lg-12">
                            <p>Total <span class="blue_h5 blue_h5_bold">RM132.16 +<br>21,000 Bpoints</span></p>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-lg-12">
                            <p class="tiny_p">GIFT CARDS AND PROMOTIONAL CODES</p>
                            <input class="codes" type="text">
                            <div class="btn_blue mt-3">APPLY</div>
                        </div>
                    </div>
                    <div class="row mt-4 mb-5">
                        <div class="col-lg-12">
                            <div class="btn btn-pink d-inline-flex btn_submit">Dummy GET VALUE</div>
                            <a href="{{ route('payment_complete') }}" class="btn btn-pink d-inline-flex">CONFIRM AND PAY</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>

@endsection @section('custom_js')
<link rel="stylesheet" href="/assets/js/creditly/creditly.css">
<script src="/assets/js/creditly/creditly.js"></script>
<script>
$(function() {
    var creditly = Creditly.initialize(
        '.expiration-month-and-year',
        '.credit-card-number',
        '.security-code');
    });
    
    $(".btn_submit").click(function() {
        console.log($(".expiration-month-and-year").val());
        console.log($(".credit-card-number").val());
        console.log($(".security-code").val());
        console.log($(".credit-card-name").val());
    });


    var b_value = <?php echo json_encode($default_value); ?>;
    var bpoint = parseInt(b_value["bpoint"].replace(",", ""));
    var bprice = parseInt(b_value["bprice"].replace(",", ""));
    $('.number').bootstrapNumber({
        upClass: 'upNum',
        downClass: 'downNum'
    });

    var slider = $('.range2 .main_slider')[0];
    noUiSlider.create(slider, {
        start: 50,
        connect: [true, false],
        step: 1,
        range: {
            'min': 0,
            'max': 100
        }
    });
    function updatePointPay(values, handle, unencoded, tap, positions) {
        var pointPayPercantage = values[0];
        var resultPoint = bpoint/100*(100-pointPayPercantage);
        var resultPay = bprice/100*pointPayPercantage;

        $(".point").val(resultPoint.toLocaleString()+" Bpts");
        $(".pay").val("$"+resultPay.toLocaleString());
    }
    slider.noUiSlider.on('update', updatePointPay);

    $(".shipping_info .btn_next").click(function() {
        $(".shipping_info").hide();
        $(".payment_method").show();
        $(".payWithBPoint").show();
    });

    $('input[type=radio][name=groupOfPayWith]').change(function() {
        switch(this.value) {
            case "payWithBPoint":
                $(".payWithBPoint").show();
                $(".payWithCreditCard").hide();
            break;
            case "payWithOnlineBanking":
                $(".payWithBPoint").hide();
                $(".payWithCreditCard").hide();
            break;
            case "payWithCreditCard":
                $(".payWithBPoint").hide();
                $(".payWithCreditCard").show();
            break;
        }
    });
</script>
@endsection
