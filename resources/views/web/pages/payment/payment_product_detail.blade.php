@extends('web.master')

@section('content')

<?php 
$default_value = [
    'bpoint' => "15,000",
    'bprice' => "1,421.00"
];
$collections = [
    'earn-points' => [
            'name' => 'With your {points} points you can get these now!',
            'items' => [
                [
                    'redeemPoint' => '10,000',
                    'isEnoughPoint' => true,
                    'image' => '8e4b3378e00274c3ddd2fac0129609dd.png',
                    'company' => 'Caltex',
                    'title' => 'RM50 fuel with 10,000 Points!',
                    'validDate' => '31st December 2018',
                    'url' => '#',
                    'actionTitle' => 'REDEEM',
                ],
                [
                    'redeemPoint' => '19,000',
                    'isEnoughPoint' => true,
                    'image' => 'marc-babin-334972-unsplash.png',
                    'company' => 'Berjaya GROUP',
                    'title' => 'Complimentary Stay when you have a long offer line',
                    'point' => 'RM1 = 1BPoint',
                    'validDate' => '31st December 2018',
                    'url' => '#',
                    'actionTitle' => 'REDEEM',
                ],
                [
                    'redeemPoint' => '5,000',
                    'isEnoughPoint' => true,
                    'image' => '205bbd329dd1fbc81d918d670996c429.png',
                    'company' => 'ZALORA',
                    'title' => 'RM50 Gift Card',
                    'validDate' => '31st December 2018',
                    'url' => '#',
                    'actionTitle' => 'REDEEM',
                ]
            ]
    ],
    'special-deals' => [
            'name' => 'Special Deals',
            'isSliderCollection' => true,
            'items' => [
                [
                    'isRedeemItem' => true,
                    'redeemPoint' => '5,000',
                    'isEnoughPoint' => true,
                    'image' => '7.png',
                    'company' => 'BOOKS',
                    'title' => 'Ready Player One - Ernest Cline',
                    'url' => '#',
                    'actionTitle' => 'REDEEM',
                ],
                [
                    'isRedeemItem' => true,
                    'redeemPoint' => '10,000',
                    'isEnoughPoint' => true,
                    'image' => '7.png',
                    'company' => 'ZALORA',
                    'title' => 'Ray Ban Shield Sunglasses',
                    'url' => '#',
                    'actionTitle' => 'REDEEM',
                ],
                [
                    'isRedeemItem' => true,
                    'redeemPoint' => '30,000',
                    'isEnoughPoint' => false,
                    'image' => '7.png',
                    'company' => 'CHEWIE ONLINE',
                    'title' => 'Darth Vader 1TB Thumb Drive',
                    'url' => '#',
                    'actionTitle' => 'REDEEM',
                ]
            ]
    ],
    'food-beverage' => [
            'name' => 'Food and Beverage',
            'isSliderCollection' => true,
            'items' => [
                [
                    'isRedeemItem' => true,
                    'redeemPoint' => '5,000',
                    'isEnoughPoint' => true,
                    'image' => '7.png',
                    'company' => 'BOOKS',
                    'title' => 'Ready Player One - Ernest Cline',
                    'url' => '#',
                    'actionTitle' => 'REDEEM',
                ],
                [
                    'isRedeemItem' => true,
                    'redeemPoint' => '10,000',
                    'isEnoughPoint' => true,
                    'image' => '7.png',
                    'company' => 'ZALORA',
                    'title' => 'Ray Ban Shield Sunglasses',
                    'url' => '#',
                    'actionTitle' => 'REDEEM',
                ],
                [
                    'isRedeemItem' => true,
                    'redeemPoint' => '30,000',
                    'isEnoughPoint' => false,
                    'image' => '7.png',
                    'company' => 'CHEWIE ONLINE',
                    'title' => 'Darth Vader 1TB Thumb Drive',
                    'url' => '#',
                    'actionTitle' => 'REDEEM',
                ]
            ]
    ]
]
?>

@include('web.include.hero_slider')

<section class="payment_product_detail">
    <section class="content2 menu">
        <div class="container">
            @include('web.include.category_icon_menu')
        </div>
    </section>

    <section class="content gray pb-5">
        <div class="container pt-5">
            <div class="pb-3">Rewards > Fashion > Ray Ban Shield Sunglasses</div>
            <div class="p-4 white">
                <div class="row mx-0">
                    <div class="header row align-items-end justify-content-between col-lg-9">
                        <span>Ray Ban Shield Sunglasses Longer Description up to two lines</span>
                        <span class="h2_pink">{{ $default_value['bpoint'] }} BPoints or RM{{ $default_value['bprice'] }}</span>
                    </div>
                    <div class="col-lg-4 mobile_product_image">
                        <img src="/images/8.0 Payment/product_item.jpg">
                    </div>
                    <div class="payment_form col-lg-8">
                        <form class="pb-3">
                            <div class="col-lg-2">
                                <h5>Colour</h5>
                                <select class="minimal">
                                    <option selected>Black</option>
                                    <option value="Red">Red</option>
                                    <option value="Blue">Blue</option>
                                    <option value="Yellow">Yellow</option>
                                </select>
                            </div>
                            <div class="col-lg-10 pt-4">
                                <h5 class="col-lg-7">Select your points and choose Visa or Mastercard to pay.</h5>
                                <div class="range_holder range1">
                                    <div class="point_holder">
                                        <label>Points</label>
                                        <input class="point" type="text" disabled>
                                    </div>
                                    <div class="main_slider"></div>
                                    <div class="pay_holder">
                                        <label>Pay</label>
                                        <input class="pay" type="text" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8 pt-4">    
                                <div class="number_holder d-inline-flex col-lg-4">
                                    <input class="number form-control" type="text" value="1" min="1" max="10" style="text-align: center;">
                                </div>
                                <a href="{{ route('payment_payment_detail') }}" class="btn btn-pink d-inline-flex">REDEEM AND / OR PAY</a>
                            </div>
                        </form>
                        <div class="link">
                            <a class="pt-1" href="#"><img src="/images/8.0 Payment/icon_shipment.jpg">When will I get my reward?</a>
                            <a class="pt-1" href="#"><img src="/images/8.0 Payment/icon_notice.jpg">Terms and Conditions</a>
                        </div>
                        <h5 class="pt-4">Details</h5>
                        <p>Offer valid 12 November to 16 December 2018, strictly until stocks last.</p>
                        <p></p>
                        <p>Gold-tone arms<br>Non-polarized lenses<br><span style="color:#C3C3C3">Made in Italy</span></p>
                    </div>
                    <div class="col-lg-4 desktop_product_image">
                        <img src="/images/8.0 Payment/product_item.jpg">
                    </div>
                </div>
                <div class="row mx-0 col-lg-12">
                    <h5 class="accordion">Show More <div class="downT">&#9660;</div></h5>
                    <div class="panel">
                        <p>Lorem ipsum... Lorem ipsum... Lorem ipsum...</p>
                        <p>Lorem ipsum... Lorem ipsum... Lorem ipsum...</p>
                        <p>Lorem ipsum... Lorem ipsum... Lorem ipsum...</p>
                        <p>Lorem ipsum... Lorem ipsum... Lorem ipsum...</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="redeem">
            @isset($collections['special-deals'])
            @include('web.components.partner_offers_collection', ['key' => 'special-deals', 'collection' =>
            $collections['special-deals']])
            @endisset
        </div>
    </section>
</section>

@endsection @section('custom_js')
<script>
    var b_value = <?php echo json_encode($default_value); ?>;
    var bpoint = parseInt(b_value["bpoint"].replace(",", ""));
    var bprice = parseInt(b_value["bprice"].replace(",", ""));
    $('.number').bootstrapNumber({
        upClass: 'upNum',
        downClass: 'downNum'
    });

    var slider = $('.range1 .main_slider')[0];
    noUiSlider.create(slider, {
        start: 50,
        connect: [true, false],
        step: 1,
        range: {
            'min': 0,
            'max': 100
        }
    });

    function updatePointPay(values, handle, unencoded, tap, positions) {
        var pointPayPercantage = values[0];
        var resultPoint = bpoint/100*(100-pointPayPercantage);
        var resultPay = bprice/100*pointPayPercantage;

        $(".point").val(resultPoint.toLocaleString()+" Bpts");
        $(".pay").val("$"+resultPay.toLocaleString());
    }
    slider.noUiSlider.on('update', updatePointPay);

    var acc = document.getElementsByClassName("accordion");
    var i;
    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight){
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
            } 
        });
    }

    $(".single-slick").slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        centerMode: true,
        variableWidth: true
    });

    $(".responsive").fadedSlick();

    $(document).ready(function () {
        $("#media").carousel({
            pause: true,
            interval: false
        });
    });

</script>
@endsection
