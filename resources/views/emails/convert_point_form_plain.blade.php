Hi,

Convert to BPoints request
--------------------------

CardholderMobile: {{ $mobile_number }}
Email: {{ $email }}
member_uid_number: {{ $member_uid_number }}
point_convert_amount: {{ $point_convert_amount }}
partner_name: {{ $partner_name }}

End of mail.

© {{ date('Y') }} {{ config('app.name', 'Laravel') }}.
