<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>Verify Your Email Address</h2>
        <p>Hi {{ $name }}!</p>
        <div>
            Welcome to BInfinite! Just one more step to complete your registration.<br/>
            Please follow the link below to verify your email address:<br/>
            <a href="{{ $activation_link }}">{{ $activation_link }}</a>

        </div>
        <p>Glad to have you onboard!</p>

		<p>Cheers,<br/><strong>BInfinite Team</strong></p>

    </body>
</html>
